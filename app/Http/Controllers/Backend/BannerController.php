<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\BackendController;
use App\Repositories\BannersRepository;
use App\Http\Requests\BannerCreateRequest;
use Carbon\Carbon;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\BannerUpdateRequest;

class BannerController extends BackendController
{
    protected $bannerRepository;
    public function __construct(BannersRepository $bannerRepository)
    {
        $this->bannerRepository = $bannerRepository;
    }
    public function index()
    {
        $listBanner = $this->bannerRepository->getAllListBanner();
        return view('backend.banner.index',compact('listBanner'));
    }

    public function create()
    {
        $flag = request()->has('flag') ? config('const.flag')['from_confirm'] : config('const.flag')['from_new'];
        if(!request()->has('flag') or (request()->session()->has('errors') and request()->session()->get('errors')->first('banner_url') != null)){
            Session::forget('banner_url_temp');
        }
        return view('backend.banner.create',compact('flag'));
    }

    public function createConfirm(BannerCreateRequest $request)
    {
        $data = $request->only('link_url','public_start_date','public_end_date','status');
        Session::put('data',$data);
        return view('backend.banner.confirm_create');
    }
    public function store(){
        $urlTempImage = Session::get('banner_url_temp');
        $banner_url = moveImage($urlTempImage,'banner');
        $data = Session::get('data');
        $data['banner_url'] = $banner_url;
        try{
            $this->bannerRepository->create($data);
            Session::forget('banner_url_temp');
            Session::forget('data');
            return redirect()->route('banner.index')->with('success',config('messages.DB_INSERT'));
        }catch (\Exception $e){
            return redirect()->route('banner.index')->with('error',config('messages.SYSTEM_ERROR'));
        }


    }

    public function edit($id)
    {
        $valueArray = $this->bannerRepository->getValueAttribute($id);
        $valueArray['public_start_date'] = Carbon::parse($valueArray['public_start_date'])->format('Y/m/d');
        $valueArray['public_end_date'] = Carbon::parse($valueArray['public_start_date'])->format('Y/m/d');
        Session::put('data',$valueArray);
        $flag = request()->get('flag') ? config('const.flag')['from_confirm'] : config('const.flag')['from_new'];
        if($flag == config('const.flag')['from_new']){
            Session::put('banner_url_temp_edit',$valueArray['banner_url']);
        }
        if (request()->session()->has('errors') and request()->session()->get('errors')->first('banner_url') != null) {
            Session::forget('banner_url_temp_edit');
        }
        return view('backend.banner.edit',compact(''));
    }

    public function editConfirm(BannerUpdateRequest $request)
    {
        $dataUpdate = $request->only('link_url','public_start_date','public_end_date','status');
        Session::put('dataUpdate',$dataUpdate);
        return view('backend.banner.confirm_edit');
    }
    public function update(){
          $IDBanner = Session::get('data')['id'];
          $dataUpdate =  Session::get('dataUpdate');
          //process image
        $oldUrlBanner = Session::get('data')['banner_url'];
        $newUrlBanner = Session::get('banner_url_temp_edit');
        if($oldUrlBanner !== $newUrlBanner){
            deleteImage($oldUrlBanner,'banner');
            $banner_url = moveImage($newUrlBanner,'banner');
            $dataUpdate['banner_url'] = $banner_url;
        }
        try{
            $this->bannerRepository->update($IDBanner,$dataUpdate);
            Session::forget('banner_url_temp_edit');
            Session::forget('data');
            Session::forget('dataUpdate');
            return redirect()->route('banner.index')->with('success',config('messages.DB_UPDATE'));
        }catch (\Exception $e){
            return redirect()->route('banner.index')->with('errors',config('messages.SYSTEM_ERROR'));
        }
    }
    public function destroy($id){
        try {
            $this->bannerRepository->softDelete($id);
            return redirect()->route('banner.index')->with('success',config('messages.DB_DELETE'));
        }catch (\Exception $e){
            return redirect()->route('banner.index')->with('error',config('messages.SYSTEM_ERROR'));
        }
    }
}

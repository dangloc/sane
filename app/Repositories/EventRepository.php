<?php

namespace App\Repositories;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * class EventRepository.
 *
 * @package namespace App\Repositories;
 */
class EventRepository extends BaseRepository
{
    protected $option_shop_repository;

    function __construct()
    {
        $this->_model = \App\Models\Event::class;
    }

    /**
     * @return mixed
     * get list event top
     */
    public function getEvent()
    {
        $data = $this->getModel()->select('events.code', 'events.name')
            ->leftJoin('event_views', 'events.code', '=', 'event_views.event_code')
            ->where(DB::raw('STR_TO_DATE ( `events`.start_date,  "%Y%m%d")'), '<=', date('Ymd'))
            ->where(DB::raw('STR_TO_DATE ( `events`.end_date,  "%Y%m%d")'), '>=', date('Ymd'))
            ->where('events.del_flag', '=', 0)
            ->orderBy('event_views.sort', 'ASC')
            ->withoutGlobalScopes()
            ->get()->toArray();

        return $data;
    }

    public function paging()
    {
        $sort = request()->get('sort', 'code');
        $direction = request()->get('direction', 'desc');
        $params = request()->all();

        return $this->getModel()
            ->when(isset($params['code']) && $params['code'] != '', function ($q) use ($params) {
                return $q->where('code', 'like', '%' . $params['code'] . '%');
            })
            ->when(isset($params['name']) && $params['name'] != '', function ($q) use ($params) {
                return $q->where('name', 'like', '%' . $params['name'] . '%');
            })
            ->when(isset($params['end_date']) && $params['end_date'] != '', function ($q) use ($params) {
                return $q->where('start_date', '<=', date('Ymd', strtotime($params['end_date'])));
            })
            ->when(isset($params['start_date']) && $params['start_date'] != '', function ($q) use ($params) {
                return $q->where('end_date', '>=', date('Ymd', strtotime($params['start_date'])));
            })
            ->orderBy($sort, $direction)
            ->paginate(config('const.paging.backend'));
    }

    // for admin item-search
    //SELECT events.code, events.name
    //FROM events
    //LEFT JOIN event_categories ON events.code = event_categories.event_code
    //LEFT JOIN event_views ON events.code = event_views.event_code
    //WHERE event_categories.category_code = {選択したカテゴリーコード}
    //AND event_categories.del_flag = 0
    //ORDER BY event_views.sort ASC
    public function getEventByCategory($category_code)
    {
        $query = DB::table('events')
            ->select('events.code', 'events.name')
            ->leftJoin('event_categories', 'events.code', '=', 'event_categories.event_code')
            ->leftJoin('event_views', 'events.code', '=', 'event_views.event_code')
            ->where('event_categories.category_code', '=', $category_code)
            ->where('event_categories.del_flag', '=', 0)
            ->where('events.del_flag', '=', 0)
            ->where('event_views.del_flag', '=', 0)
            ->orderBy('event_views.sort', 'asc');
        return $query->get();
    }


    public function getEventName($eventCode){
        return $this->getModel()
            ->select('name')
            ->where('code', $eventCode)
            ->first()->name;
    }
}


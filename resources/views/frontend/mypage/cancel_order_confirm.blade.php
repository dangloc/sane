@extends('frontend.layouts.main')
@section('content')
    <div id="mypage_cancel_order_confirm" class="password-wrap">
        <div class="container">
            <div class="mypage_cancel_order_confirm">
                <h1 class="title_page">マイページ</h1>
                <div class="button_group">
                    <button class="button_back" onclick="location.href='{{route('mypage.index')}}'">マイページTOPへ戻る</button>
                    <button class="button_submit" onclick="location.href='{{route('mypage.change_password_entry')}}'">
                        メールアドレス・パスワードの変更
                    </button>
                    <button class="button_logout" onclick="location.href='{{route('frontend.logout')}}'"><i
                                class="fa fa-sign-out-alt"></i>ログアウト
                    </button>
                </div>
                @if(!empty($bookingData))

                    <div class="description_page">ご注文のキャンセル（確認）</div>
                    <div class="infor_order"><span>{{$bookingData['date']['year']}}年{{$bookingData['date']['month']}}月{{$bookingData['date']['day']}}日にご注文いただいた商品</span>                    </div>
                    <div class="button_back_header">
                        <button onclick="location.href='{{ url()->previous() }}'"><i class="fas fa-chevron-left"></i>前の画面に戻る</button>
                    </div>
                    <div class="notification">以下のご注文内容（伝票番号: {{$bookingData['slip_number']}}）をキャンセルいたします。</div>
                    <?php $sumPrice = $totalQuantity = 0;?>
                    @foreach($bookingData['item'] as $item)
                        <?php $sumPrice += !empty($item['total_price']) ? $item['total_price'] : 0;?>
                        <?php $totalQuantity += !empty($item['item_count']) ? $item['item_count'] : 0;?>
                        <div class="cart">
                            <div class="image_product">
                                <img src="{{ ($item['item_image_folder_name'] ==null || $item['item_image_folder_name']=='_') ? asset('/frontend/images/item/noimage.jpg')
                                                    : config('const.url_item_image').$item['item_image_folder_name']}}"
                                     alt="">
                            </div>
                            <div class="info_product">
                                <div class="product_name">{{$item['name']}}
                                    @if($item['required_confirm_flag']!=config('const.not_with'))
                                        <label class="required-confirm"
                                               style="<?= ($item['required_flag'] == '0') ? "display:inline-block" : "display:none"?>">

                                            @if($item['required_confirm_flag']== config('const.with_wasabi'))
                                                わさび入り @elseif($item['required_confirm_flag']==config('const.with_kamaboko'))
                                                かまぼこ赤 @elseif($item['required_confirm_flag']==config('const.with_classic'))
                                                定番
                                            @endif
                                        </label>
                                        <label class="required-confirm"
                                               style="<?= ($item['required_flag'] == '1') ? "display:inline-block" : "display:none"?>">
                                            @if($item['required_confirm_flag']==config('const.with_wasabi'))
                                                わさび抜き@elseif($item['required_confirm_flag']==config('const.with_kamaboko'))
                                                かまぼこ白 @elseif($item['required_confirm_flag']==config('const.with_classic'))
                                                法事
                                            @endif
                                        </label>
                                    @endif
                                </div>
                                <div class="detail_product">
                                    <div class="price">
                                        <label>価格</label>
                                        <span>{{number_format($item['price'])}}<span>円+税</span></span>
                                    </div>
                                    <div class="count">
                                        <label>数量</label>
                                        <span>{{number_format($item['item_count'])}}</span>
                                    </div>
                                    <div class="total">
                                        <label>小計</label>
                                        <span>{{number_format($item['total_price'])}}<span>円+税</span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <hr>
                    <div class="bill">
                            <span class="count_product">
                                <label>商品数</label>
                                <span>{{number_format($totalQuantity)}}品</span>
                            </span>
                        <span class="total_price">
                                <label>合計金額</label>
                                <span>{{number_format($sumPrice)}}<span>円+税</span></span>
                            </span>
                    </div>
                    <div class="date_time_title">商品の受取り日と受取り店舗</div>
                    <div class="date_time_order">
                        <table>
                            <tr class="row-1">
                                <td class="label_td">受取り日</td>
                                @if(!empty($bookingData['date']))
                                    <td class="time_order"><span>{{formatDate($bookingData['receipt_date'])}}
                                            日({{dayJapan($bookingData['receipt_date'])}})</span> <span>{{$bookingData['receipt_time']}}時</span></td>
                                @endif
                            </tr>
                            <tr class="row-2">
                                <td class="label_td">受取り店舗</td>
                                <td class="address"><span class="text">{{$bookingData['shop_name']}}</span><span
                                            class="tel">TEL : {{$bookingData['customer_tel']}}</span></td>
                            </tr>
                            <tr class="row-3">
                                <td class="label_td"></td>
                                <td class="city">{{$bookingData['city_name'].$bookingData['address']}}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="customer">
                        <div class="info_customer_title">お客さま情報</div>
                        <div class="info_customer">
                            <div>
                                <label>お名前</label>
                                <span>{{$bookingData['customer_name']}}</span>
                            </div>
                            <div>
                                <label>電話番号</label>
                                <span>{{$bookingData['customer_tel']}}</span>
                            </div>
                            <div>
                                <label>サンエーカード番号</label>
                                <span>{{formatCardNo($bookingData['customer_card_no'])}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="button_group_footer">
                        <button class="button_footer_back" onclick="location.href='{{route('mypage.index')}}'"><i
                                    class="fas fa-chevron-left"></i>前の画面に戻る
                        </button>
                        <button class="button_footer_submit"
                                onclick="Frontend.showPopupCancel('{{$bookingData['slip_number']}}')">注文をキャンセルする
                            <img class="lazy-load" src="{{asset('frontend/images/ajax-loader.gif')}}">
                        </button>
                    </div>
                @endif
                <div class="title_warning">
                    <p>上のキャンセルボタンを押下すると「戻る」事ができません。<br>
                        内容をご確認の上、次へお進みください。</p>
                </div>
                <div class="cancel_policy">
                    <div class="policy_title">キャンセルについて</div>
                    <div class="policy_content">
                        <p class="bold">▪️キャンセルについて</p>
                        <p>受注生産につき、ご予約後のキャンセル・変更は、お受取りの３日前までにお願いします。</p>
                    </div>
                </div>
            </div>
            <div class="cancel-popup-confirm"></div>
        </div>
    </div>
@endsection

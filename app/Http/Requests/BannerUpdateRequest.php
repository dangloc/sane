<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;

class BannerUpdateRequest extends FormRequest
{
    protected $redirect;

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule = ['link_url' => 'max:128',
            'public_start_date' => 'required',
            'public_end_date' => "required",
        ];
        if(!Session::has('banner_url_temp_edit')){
            $rule['banner_url'] = 'required|max:2000|mimes:jpeg,bmp,png,jpg|dimensions:width=700,height=345';
        } else {
            $rule['banner_url'] = 'max:2000|mimes:jpeg,bmp,png,jpg|dimensions:width=700,height=345';
        }
        $this->redirect = route('banner.edit',[Session::get('data')['id'],'flag'=>1]);
        return $rule;
    }
    public function attributes()
    {
        return
            [
                'banner_url' => 'バナー画像',
                'link_url' => 'リンク先URL',
            ];
    }
    public function messages()
    {
        return [
            'banner_url.max' => 'バナー画像 2MB以下のファイルを指定してください。',
            'banner_url.dimensions' => 'アップロードされた画像のサイズは不正です。'
        ];
    }

    protected function validationData()
    {
        $all = parent::validationData();
        if (request()->hasFile('banner_url') ){
            $file = Input::file('banner_url');
            $imageFileName = time(). $file->getClientOriginalName();
            $file->storeAs('public/temp/', $imageFileName);
            $urlFile = 'storage/temp/' . $imageFileName;
            Session::put('banner_url_temp_edit',$urlFile);
        }
        if(request()->link_url == null) {
            request()->merge([
                'link_url' => '',
            ]);
        }
        request()->flash();
        return $all;
    }
}

@extends('frontend.layouts.main')
@section('content')
    <div id="my_page_change_password" class="password-wrap">
        <div class="container">
            <div class="my_page_change_password">
                <h1 class="title">マイページ</h1>
                <div class="card_button">
                    <button class="button_back" onclick="location.href='{{route('mypage.index')}}'">マイページTOPへ戻る</button>
                    <button class="button_login" onclick="location.href='{{route('frontend.logout')}}'"><i
                                class="fas fa-sign-in-alt"></i>ログアウト
                    </button>
                </div>
                <form action="{{route('mypage.change_password_entry_action')}}" id="form-mypage-changepass"
                      method="POST" autocomplete="off">
                    @csrf
                    <div class="form_title">メールアドレス・パスワードの変更</div>
                    <div class="group_input">
                        <label>新しいメールアドレス</label>
                        <input type="text" name="email" value="{{old('email')}}" placeholder="半角でご入力ください">
                        <div class="pc-ml-315">
                            @include('frontend.elements.validate_error', ['message' => !empty($errors->first('email')) ?$errors->first('email') :""])
                        </div>
                    </div>
                    <div class="group_input">
                        <label>新しいメールアドレス(再入力)</label>
                        <input type="text" name="confirm_email" value="{{old('confirm_email')}}"
                               placeholder="確認のためもう一度ご入力ください">
                        @if(empty($errors->first('email')))
                            <div class="pc-ml-315">
                                @include('frontend.elements.validate_error', ['message' => !empty($errors->first('confirm_email')) ?$errors->first('confirm_email') :""])
                            </div>
                        @endif
                    </div>
                    <div class="group_input">
                        <label>新しいパスワード</label>
                        <input type="password" name="new_password" autocomplete="new-password" value="{{old('new_password')}}"
                               placeholder="8〜20文字の半角英数字">
                        <div class="pc-ml-315">
                            @include('frontend.elements.validate_error', ['message' => !empty($errors->first('new_password')) ?$errors->first('new_password') :""])
                        </div>
                    </div>
                    <div class="group_input">
                        <label>新しいパスワード（再入力)</label>
                        <input type="password" name="confirm_password" value="{{old('confirm_password')}}"
                               placeholder="確認のためもう一度ご入力ください">
                        @if(empty($errors->first('new_password')))
                            <div class="pc-ml-315">
                                @include('frontend.elements.validate_error', ['message' => !empty($errors->first('confirm_password')) ?$errors->first('confirm_password') :""])
                            </div>
                        @endif
                    </div>
                    <div class="title_change">安全性の高いパスワード</div>
                    <div class="content">
                        <p>安全性の高いパスワードにするには、数字、文字、記号の組み合わせを使用し、</p>
                        <p>推測しにくく、実際にある単語に類似せず、他のアカウントで使用していないものにすることをオススメ致します。</p>
                        <br>
                        <p>パスワードを変更した場合、スマートフォンを含むすべての端末でも、新しいパスワードを使用してログインしてください。</p>
                    </div>
                    <div class="button_submit">
                        @if(Session::has('error_not_blank'))
                            <div class="error_no_blank">{{Session::get('error_not_blank')}}</div>
                            <br>
                        @endif
                        <button>変更する</button>
                        @if(Session::has('success'))
                            <div class="message_success">メールアドレス・パスワードを変更しました。<a href="{{route('frontend.login')}}">ここをクリックしてもう一度ログインしてください。</a></div>
                        @endif
                    </div>
                </form>
            </div>
        </div>

    </div>
@endsection

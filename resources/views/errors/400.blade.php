@if(strlen(strstr(url()->current(), 'management')) > 0)
    @include('errors.backend.400')
@else
    @include('errors.frontend.400')
@endif
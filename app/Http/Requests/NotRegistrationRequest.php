<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Factory;

class NotRegistrationRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function __construct(Factory $validationFactory)
    {
        $validationFactory->extend('check_card',
            function () {
                $card_on = implode(request()->card);
                if (strlen($card_on) == 12) {
                    $check = checkCard($card_on);
                    return $check;
                }
            }
        );
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule = [
            'email' => 'required|check_email',
            'confirm_email' => 'required|required_with:email|same:email',
            'name' => 'required|katakana',
            'tel' => 'required|max:11|regex:/^[0-9]*$/i',
            'card.*' => 'required|numeric|digits:4',
            'card' => 'check_card'
        ];
        return $rule;
    }

    public function attributes()
    {
        return [
            'email' => 'メールアドレス',
            'confirm_email' => 'メールアドレス(再入力)',
            'name' => 'お名前',
            'tel' => '電話番号',
            'card.*' => 'サンエーカード番号',
            'card'=>'カード番号'
        ];
    }
    public function messages()
    {
        return [
            'tel.regex'=>':attributeには半角数字を指定してください。'
        ];
    }
}

<?php $dateBooking = !empty($order['receptionist_date']) ? getDateTime($order['receptionist_date']) : getDateTime(date('Ymd')) ?>
<?php $dateReceive = !empty($order['receipt_date']) ? getDateTime($order['receipt_date']) : getDateTime(date('Ymd')) ?>
<p>このたびは、「サンエーネット予約サービス」をご利用いただき</p>
<p>誠にありがとうございます。</p>
<p>ご予約内容をご確認の上、ご指定の店舗にてお受取りください。</p><br>

<p>━━━━━━━━━━━━━━━━━━</p>
<p>ご予約内容</p>
<p>━━━━━━━━━━━━━━━━━━</p>
<img src="{{asset($order['images'])}}" alt="">
<p>伝票番号：{{$order['slip_number']}}</p>
<p>受付日：{{$dateBooking['year']}}年{{$dateBooking['month']}}月{{$dateBooking['day']}}日</p>
<p>受取り日時：{{$dateReceive['year']}}年{{$dateReceive['month']}}月{{$dateReceive['day']}}日 {{$order['time']}}時</p>
<p>受取り店舗：{{$order['cityName']}}{{$order['shopName']}}</p>
<p>お名前：{{$order['customer_name']}}様</p>
<p>電話番号：{{$order['customer_tel']}}</p>
<p>サンエーカード番号：{{formatCardNo($order['customer_card_no'])}}</p><br>

<p>━━━━━━━━━━━━━━━━━━</p>
<p>ご予約商品</p>
<p>━━━━━━━━━━━━━━━━━━</p>
@if(!empty($order['items']))
    @foreach($order['items'] as $item)
        <p>商品名：{{$item['name']}}</p>
        <p>価格：{{number_format($item['price'])}}円（税抜）</p>
        <p>数量：{{$item['item_count']}}</p>
    @endforeach
@endif
<p>------------------</p>
<p>合計（税抜）：{{number_format($order['total_price'])}}円</p><br>


<p>▽お受取りについて</p>
<p>【お受取り方法】</p>
<p>・上記受取り店舗までお越しいただき、本メール添付のバーコードをご提示いただくか、</p>
<p>　「お名前」、「お電話番号」を店舗スタッフにお知らせください。</p><br>

<p>【お支払い方法】</p>
<p>商品お受取りの際に、レジにてお支払いをお願い致します。</p><br>

<p>▽キャンセルについて</p>
<p>受注生産につき、ご予約後のキャンセル・変更は、お受取りの３日前までにお願いします。</p><br>

<p>━━━━━━━━━━━━━━━━━━</p>
<p>ご予約内容の変更・取消について</p>
<p>━━━━━━━━━━━━━━━━━━</p>
<p>【ご予約内容の変更について】</p>
<p>・会員登録されたお客様</p>
<p>ご登録いただいたメールアドレス・パスワードにてマイページにログインいただき、</p>
<p>お客様情報の変更、ご予約のキャンセルが可能です。</p>
<p>商品の変更については受取り店舗へお問い合わせいただくか、</p>
<p>一度ご予約のキャンセルを行なった後、改めてご予約をお願い致します。</p><br>

@extends('frontend.layouts.main')
@section('css')
    <link rel="stylesheet" href="{{asset('frontend/css/static.css')}}">
@endsection
@section('content')
    <div class="browser">
        <div class="container">
            <h1 class="title_page">推奨環境</h1>
            <div class="browser_content">
                <div class="browser_description">
                    <p>サンエーネット予約サービスを快適にご利用いただくために、以下の環境でのご利用を推奨いたします。</p>
                    <p>また、正しくご利用いただくため、スタイルシートを有効にし、JavaScript、Cookieが使用できる状態でご利用ください。</p>
                </div>
            </div>
            <table class="table_browser">
                <tr>
                    <td class="browser_label">【PC】推奨ブラウザ</td>
                    <td class="browser_content">
                        <div class="browser_detail">
                            <p class="browser_name">[Windows]</p>
                            <p class="browser_name1">Internet Explorer 11、Microsoft Edge (最新版)、Mozilla Firefox (最新版)、Google Chrome （最新版）</p>
                        </div>
                        <div class="browser_detail">
                            <p class="browser_name">[Mac(Macintosh)]</p>
                            <p class="browser_name1">Safari （最新版）、Google Chrome 最新版</p>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="browser_label">
                        <p>【タブレット　/ スマートフォン】</p>
                        <p>&nbsp;&nbsp;推奨ブラウザ</p>
                    </td>
                    <td class="browser_content">
                        <div class="browser_detail">
                            <p class="browser_name">[Android]</p>
                            <p class="browser_name1">Android（最新バージョンの標準ブラウザ）</p>
                        </div>
                        <div class="browser_detail">
                            <p class="browser_name">[iPhone]</p>
                            <p class="browser_name1">iOS（最新バージョンの標準ブラウザ）</p>
                        </div>
                    </td>
                </tr>
            </table>
            <div class="browser_note">
                <p>※上記対応環境に含まれる場合でも、OSのバージョンやお客様のご使用環境によっては、ご利用頂けない場合がございます。</p>
                <p>※フィーチャーフォンには対応しておりません。</p>
                <p>※Windows、Internet Explorerは、米国Microsoft Corporationの米国およびその他の国における登録商標です。</p>
                <p>※Firefoxは、米国Mozilla Foundationの米国およびその他の国における登録商標です。</p>
                <p>※Mac OS、Safariは、Apple Computer,Inc.の米国およびその他の国における登録商標です。</p>
                <p>※その他、記載の会社名、商品名はそれぞれの会社の商標もしくは登録商標です。</p>
            </div>
            <div class="browser_back"><a href="{{route('site.index')}}">TOPページへ戻る</a></div>
        </div>
    </div>
@endsection
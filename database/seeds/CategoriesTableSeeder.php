<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->truncate();
        DB::table('categories')->insert([
            [
                'code'=>'123',
                'name'=>'Category1',
                'division_code'=>'1',
                'del_flag'=>0,
                'ins_id'=>1,
                'ins_datetime'=>Carbon::now()
            ],
            [
                'code'=>'124',
                'name'=>'Category2',
                'division_code'=>'2',
                'del_flag'=>0,
                'ins_id'=>1,
                'ins_datetime'=>Carbon::now()
            ],
            [
                'code'=>'125',
                'name'=>'Category1',
                'division_code'=>'1',
                'del_flag'=>0,
                'ins_id'=>1,
                'ins_datetime'=>Carbon::now()
            ]
        ]);
    }
}

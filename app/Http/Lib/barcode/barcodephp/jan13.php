<?php

require_once("CheckDigit.php");

class jan13
{
    function draw($code, $minWidthDot, $height)
    {
        $FontName = app_path('Http/Lib/barcode/font/mplus-1p-black.ttf') ;
        $KuroBarCousei = 0;
        $BarThick = 1;
        $TextWrite = true;
        $FontSize = 10;
        if (preg_match("/^[0-9]+$/", $code) == false) {
            return false;
        }

        $codeAll = null;
        if (strlen($code) == 13) {
            if (substr($code, 12, 1) != modulus10W3(substr($code, 0, 12))) {
                return false;
            }
            $codeAll = $code;
        } else if (strlen($code) == 12) {
            $codeAll = $code . modulus10W3(substr($code, 0, 12));
        } else {
            return false;
        }

        $pre = (int)substr($codeAll, 0, 1);
        $codeAllL = substr($codeAll, 1, 6);
        $codeAllR = substr($codeAll, 7, 6);

        $x0 = $minWidthDot;
        $x1 = $minWidthDot * 2.5;
        if ($minWidthDot % 2 != 0) {
            $x1 = $minWidthDot * 3;
        }

        $dot = array($x0, $x1);

        $xPos = 0;
        $xPos += $dot[0] * (3 + 42 + 5 + 42 + 3);

        $gazouHeight = $height;
        if ($TextWrite == true) {
            $gazouHeight = $height + $FontSize + 3;
        }
        $img = imagecreate($xPos, $gazouHeight);
        $white = ImageColorAllocate($img, 0xFF, 0xFF, 0xFF);
        $black = ImageColorAllocate($img, 0x00, 0x00, 0x00);

        imagesetthickness($img, $BarThick);

        $oe = array(0x00, 0x0b, 0x0d, 0x0e, 0x13, 0x19, 0x1c, 0x15, 0x16, 0x1a);
        $left_1 = array(0x0d, 0x19, 0x13, 0x3d, 0x23, 0x31, 0x2f, 0x3b, 0x37, 0x0b);
        $left_2 = array(0x27, 0x33, 0x1b, 0x21, 0x1d, 0x39, 0x05, 0x11, 0x09, 0x17);
        $left = array($left_1, $left_2);
        $right = array(0x72, 0x66, 0x6c, 0x42, 0x5c, 0x4e, 0x50, 0x44, 0x48, 0x74);


        $xPos = 0;
        for ($j = 0; $j < 3; $j++) {
            if ($j % 2 == 0) { //黒バー
                imagefilledrectangle($img, $xPos, 0, $xPos + $dot[0] - 1 + $KuroBarCousei, $height, $black);
            }
            $xPos += $dot[0];
        }
        $chkOE = 0x20;
        for ($i = 0; $i < strlen($codeAllL); $i++) {
            $flgOE = 0;
            if (($oe[$pre] & ($chkOE >> $i)) != 0) {
                $flgOE = 1;
            }
            $c = (int)substr($codeAllL, $i, 1);
            $chk = 0x40;
            $l = 0;
            for ($j = 0; $j < 7; $j++) {
                if (($left[$flgOE][$c] & ($chk >> $j)) != 0) {
                    $l++;
                } elseif ($l != 0) {
                    imagefilledrectangle($img, $xPos, 0, $xPos + ($dot[0] * $l) - 1 + $KuroBarCousei, $height, $black);
                    $xPos += $dot[0] * $l + $dot[0];
                    $l = 0;
                } else {
                    $xPos += $dot[0];
                }
            }
            if ($l != 0) {
                imagefilledrectangle($img, $xPos, 0, $xPos + ($dot[0] * $l) - 1 + $KuroBarCousei, $height, $black);
                $xPos += $dot[0] * $l;
            }
        }

        //センターコード
        for ($j = 0; $j < 5; $j++) {
            $x0or1 = 0;
            if ($j % 2 != 0) { //黒バー
                imagefilledrectangle($img, $xPos, 0, $xPos + $dot[$x0or1] - 1 + $KuroBarCousei, $height, $black);
            }
            $xPos += $dot[$x0or1];
        }

        //Right 6キャラクタ
        for ($i = 0; $i < strlen($codeAllR); $i++) {
            $c = (int)substr($codeAllR, $i, 1);
            $chk = 0x40;
            $l = 0;
            for ($j = 0; $j < 7; $j++) {
                if (($right[$c] & ($chk >> $j)) != 0) {
                    $l++;
                } elseif ($l != 0) {
                    imagefilledrectangle($img, $xPos, 0, $xPos + ($dot[0] * $l) - 1 + $KuroBarCousei, $height, $black);
                    $xPos += $dot[0] * $l + $dot[0];
                    $l = 0;
                } else {
                    $xPos += $dot[0];
                }
            }
            if ($l != 0) {
                imagefilledrectangle($img, $xPos, 0, $xPos + ($dot[0] * $l) - 1 + $KuroBarCousei, $height, $black);
                $xPos += $dot[0] * $l;
            }
        }

        //ストップコード
        for ($j = 0; $j < 3; $j++) {
            if ($j % 2 == 0) { //黒バー
                imagefilledrectangle($img, $xPos, 0, $xPos + $dot[0] - 1 + $KuroBarCousei, $height, $black);
            }
            $xPos += $dot[0];
        }

        if ($TextWrite) {
            $interval = ($xPos - pointTo($FontSize - cmTo(0.01))) / (strlen($codeAll) - 1) - 1;
            for ($i = 0; $i < strlen($codeAll); $i++) {
                ImageTTFText($img, $FontSize, 0, $i * $interval + cmTo(0.05), $gazouHeight + cmTo(0.01)
                    , $black, $FontName, substr($codeAll, $i, 1));
            }
        }
        return $img;
    }
}
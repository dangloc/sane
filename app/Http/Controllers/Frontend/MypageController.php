<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests\ChangeCustomerInfoRequest;
use App\Http\Requests\ChangePassEntryRequest;
use App\Mail\ChangCustomerInfoMail;
use App\Models\CustomerOrder;
use App\Models\Reserve;
use App\Repositories\CitiesRepository;
use App\Repositories\CustomerOrderRepository;
use App\Repositories\CustomerRepository;
use App\Http\Controllers\Controller;
use App\Repositories\ItemRepository;
use App\Repositories\ReserveRepository;
use App\Repositories\SendMailRepository;
use App\Repositories\ShopsRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class MypageController extends Controller
{
    public $customerRepository;
    public $customerOrder;
    public $reserves;
    public $itemRepository;
    public $sendMailRepository;
    public $shopRepository;
    public $citiesRepository;

    public function __construct(CustomerRepository $customerRepository, ItemRepository $itemRepository, SendMailRepository $sendMailRepository, ShopsRepository $shopRepository, CitiesRepository $citiesRepository, CustomerOrderRepository $customerOrder, ReserveRepository $reserveRepository)
    {
        $this->customerRepository = $customerRepository;
        $this->itemRepository = $itemRepository;
        $this->sendMailRepository = $sendMailRepository;
        $this->shopRepository = $shopRepository;
        $this->citiesRepository = $citiesRepository;
        $this->customerOrder = $customerOrder;
        $this->reserves = $reserveRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * My page view
     */
    public function index()
    {
        $customer_id = Auth::guard(config('const.guard.frontend'))->id();
        $data_booking = $this->customerOrder->getCustomerOrder($customer_id, config('const.status_order.booking'));
        $data_customer_cancel_order = $this->customerOrder->getCustomerOrder($customer_id, config('const.status_order.customer_cancel'));
        $data_customer_change_order = $this->customerOrder->getCustomerOrder($customer_id, config('const.status_order.shop_change_confirm'));
        $data_shop_cancel_order = $this->customerOrder->getCustomerOrder($customer_id, config('const.status_order.shop_cancel'));
        return view('frontend.mypage.index', compact('data_booking', 'data_customer_cancel_order', 'data_customer_change_order', 'data_shop_cancel_order'));
    }


    /**
     * @param int $slipNumber
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * cancel confirm
     */
    public function cancelOrderConfirm($slipNumber = 0)
    {
        $check = $this->customerOrder->checkSlipCustomer($slipNumber);
        if ($check) {
            $bookingData = $this->reserves->getDataOrderBySlip($slipNumber);
            if (!empty($bookingData)){
                $bookingData['item'] = $this->itemRepository->getItemDataOrder($bookingData['slip_number']);
                if (!empty($bookingData['item'])){
                    foreach ($bookingData['item'] as $key => $item){
                        $bookingData['item'][$key]['total_price'] = !empty($item['price']) ? $item['price'] * $item['item_count'] : 0;
                        $bookingData['date'] = !empty($bookingData['receptionist_date']) ? getDateTime($bookingData['receptionist_date']) : array('year' => '', 'month' => '', 'day' => '');
                    }
                    return view('frontend.mypage.cancel_order_confirm', compact('bookingData'));
                }
            }
        }
        abort(404);
    }

    public function showPopupCancel(Request $request)
    {
        $slipNumber = $request->input('slipNumber', 0);
        if (!empty($slipNumber)) {
            $view = view("frontend.elements.popup_cancel_complete",
                compact('slipNumber'))->render();
            return response()->json(['success' => true, 'data' => $view]);
        }
        return response()->json(['success' => false, 'data' => '']);
    }

    public function updateCancelOrder(Request $request)
    {
        $slipNumber = $request->input('slipNumber', 0);
        $check = $this->customerOrder->checkSlipCustomer($slipNumber);
        if ($check) {
            $dataOrder = array(
                'status' => CustomerOrder::STATUS_CANCE,
                'web_cancel_timestamp' => now(),
                'upd_datetime' => now(),
                'upd_id' => 999999
            );
            $dataReserve = array(
                'update_flag' => Reserve::FLAG_CANCEL,
                'reserve_del_flag' => Reserve::FLAG_DEL_CANCEL,
                'update_timestamp' => Carbon::now()->format('Ymd'),
                'upd_datetime' => now(),
                'upd_id' => 999999,
            );
            $updateOrder = $this->customerOrder->updateInfoCustomerOrder(array('slip_number' => $slipNumber), $dataOrder);
            $updateReserve = $this->reserves->updateInfoReserve(array('slip_number' => $slipNumber), $dataReserve);
            if ($updateOrder && $updateReserve) {
                $bookingData = $this->reserves->getDataOrderBySlip($slipNumber);
                $shopInfo = $this->shopRepository->getShopInfoComplete($bookingData['shop_code']);
                $bookingData['shopTel'] = !empty($shopInfo['tel']) ? $shopInfo['tel'] : '';
                $bookingData['shopName'] = !empty($shopInfo['name']) ? $shopInfo['name'] : '';
                $bookingData['address'] = !empty($shopInfo['address']) ? $shopInfo['address'] : '';
                $bookingData['cityName'] = !empty($shopInfo['city_id']) ? $this->citiesRepository->findByAttribute('id', $shopInfo['city_id'], 'name') : "";
                $sendMail = $this->sendMailRepository->sendMail($request, $bookingData, 'confirm');
                if ($sendMail) {
                    return response()->json(['success' => true, 'data' => '']);
                }
            }
        }
        return response()->json(['success' => false, 'data' => '']);
    }

    public function cancelOrderComplete(Request $request, $slipNumber)
    {
        $check = $this->customerOrder->checkSlipCustomer($slipNumber);
        if ($check) {
            $bookingData = $this->reserves->getDataOrderBySlip($slipNumber);
            if (!empty($bookingData)) {
                $bookingData['item'] = $this->itemRepository->getItemDataOrder($bookingData['slip_number']);
                if (!empty($bookingData['item'])) {
                    foreach ($bookingData['item'] as $key => $item) {
                        $bookingData['item'][$key]['total_price'] = !empty($item['price']) ? $item['price'] * $item['item_count'] : 0;
                        $bookingData['date'] = !empty($bookingData['receipt_date']) ? getDateTime($bookingData['receipt_date']) : array('year' => '', 'month' => '', 'day' => '');
                    }
                }
            }
            return view('frontend.mypage.cancel_order_complete', compact('bookingData'));
        }
        abort(404);
    }


    public function changeCustomerInfo($slip_number)
    {
        $data_order_booking = $this->customerOrder->getCustomerOrderBySlipnumber($slip_number);
        if ($data_order_booking == null) {
            abort(404);
        }
        return view('frontend.mypage.change_customer_info', compact('data_order_booking','slip_number'));
    }

    /**
     * @param ChangeCustomerInfoRequest $request
     * @param $slip_number
     * @return \Illuminate\Http\RedirectResponse
     *
     */
    public function postChangeInfo(ChangeCustomerInfoRequest $request,$slip_number)
    {
        $id=Auth::guard(\config('const.guard.frontend'))->id();
        $user=$this->customerRepository->find($id);
        if(empty($user)){
            abort(404);
        }
        $customer_name = $request->name;
        $customer_tel = $request->tel;
        $cutomer_card_no = implode(request()->card);
        $data_update = array(
            'update_flag' => \config('const.update_on'),
            'upd_id' => Auth::guard(\config('const.guard.frontend'))->id(),
            'upd_datetime' => Carbon::now()
        );
        if (!empty($customer_name)) {
            $data_update += array(
                'customer_name' => $customer_name
            );
        }
        if (!empty($customer_tel)) {
            $data_update += array(
                'customer_tel' => $customer_tel
            );
        }
        if (!empty($cutomer_card_no)) {
            $data_update += array(
                'customer_card_no' => $cutomer_card_no,
            );
        }
        if (empty($customer_name) && empty($customer_tel) && empty($cutomer_card_no)) {
            return redirect()->route('mypage.index');
        }
        $this->reserves->updateInfoReserve(['slip_number' => $slip_number], $data_update);
        $data_update['send_status'] = \config('const.send_status_on');
        unset($data_update['update_flag']);
        $this->customerOrder->updateInfoCustomerOrder(['slip_number' => $slip_number], $data_update);
        $order=$this->customerOrder->find($slip_number);
        Mail::to($user->email)->send(new ChangCustomerInfoMail($order));
        return redirect()->route('mypage.index');
    }
}

<?php

namespace App\Repositories;

/**
 * class AjaxRepository.
 *
 * @package namespace App\Repositories;
 */
class AjaxRepository extends BaseRepository
{
    public $itemBonusPoint;
    public $citiesRepository;
    public $shopsRepository;
    public $areasRepository;
    public $itemRepository;
    public $itemLimitOrderRepository;
    public $shopPlanOrderRepository;

    function __construct(ItemBonusPointRepository $itemBonusPoint,
                         CitiesRepository $citiesRepository,
                         ShopsRepository $shopsRepository,
                         AreasRepository $areasRepository,
                         ItemRepository $itemRepository,
                         ItemLimitedOrderPossibleCountRepository $itemLimitOrderRepository,
                         ShopPlanOrderPossibleCountRepository $shopPlanOrderRepository
    )
    {
        $this->itemBonusPoint = $itemBonusPoint;
        $this->citiesRepository = $citiesRepository;
        $this->shopsRepository = $shopsRepository;
        $this->areasRepository = $areasRepository;
        $this->itemRepository = $itemRepository;
        $this->itemLimitOrderRepository = $itemLimitOrderRepository;
        $this->shopPlanOrderRepository = $shopPlanOrderRepository;
    }

    /**
     * @param $datas
     * @param $dateReceive
     * @return array
     * Xử lý dữ liệu gender list product top
     */
    public function controlDataGetProductTop($datas, $dateReceive)
    {
        if (!empty($datas) && is_array($datas)) {
            foreach ($datas as $key => $data) {
                $checkBonus = $this->itemBonusPoint->checkPointBonus($data['code'], $dateReceive);
                $datas[$key]['bonus'] = !empty($checkBonus) ? 'ボーナスポイント対象商品' : '';
                $stock = $this->checkStockCondition($data['shop_code'], $data['code'], $dateReceive);
                if ($stock['success'] == true) {
                    if (strlen($data['name']) >70){
                        $datas[$key]['name'] = subStringDot($data['name'], 70);
                    }
//                    $datas[$key]['stock'] = (int)$stock;
                } else {
                    unset($datas[$key]);
                }
            }
            return $datas;
        }
        return array();
    }


    /**
     * @param $areaId
     * @param $cityId
     * @param $textShop
     * @return array
     * Xử lý dữ liệu gender shop map top
     */
    public function controlDataRenderShopMap($areaId, $cityId, $textShop)
    {

        $data = array(
            'area_id' => '',
            'cities' => array(),
            'shops' => array(),
        );
        //DATA RETURN
        if ($areaId) {
            $data['area_id'] = $areaId;
            $data['cities'] = $this->citiesRepository->getCityByAreas($areaId);
            $data['shops'] = $this->shopsRepository->getShopByAreas($areaId);
        }
        if ($cityId) {
            $data['shops'] = $this->shopsRepository->getShopByCities($cityId);
        }
        if ($textShop) {
            $data['shops'] = $this->shopsRepository->searchKeyUpShop($textShop, $areaId, $cityId);
        }
        return $data;
    }

    /**
     * kiem tra dơn hang con du trong stock hay ko -- CART LIST
     */
    public function checkCartStockExist($shopCode, $eventCode, $dateReceiveRaw, $code, $quantity, $itemName)
    {
        $checkExits = $this->itemRepository->getListItem($shopCode, $eventCode, $dateReceiveRaw, 0, $code);
        $return = array('success' => false, 'message' => '', 'stock' => 0);
        if (count($checkExits) > 0) {
            $stock = $this->checkStockCondition($shopCode, $code, $dateReceiveRaw);
            if ( $stock['success'] == true) {
                $return['stock'] = (int)$stock['stock'];
                if ($stock['stock'] >= (int)$quantity) {
                    $return['success'] = true;
                    return $return;
                }else{
                    $return['message'] = config('const.stock.stock_extra_start').$return['stock'].' '.config('const.stock.stock_extra_end');
                }
            }else{
                $return['message'] = $stock['message'];
            }
        } else {
            $return['message'] = $itemName .' '. config('const.stock.not_exist_product');
        }
        return $return;
    }

    /**
     * @param $shopCode
     * @param $code
     * @param $dateReceive
     * @param int $number
     * @return array
     * Check điều kiện item được phép đặt hàng
     */
    public function checkStockCondition($shopCode, $code, $dateReceive, $number = 10)
    {
        $return = array('success' => false, 'message' => '', 'stock' => 0);
        $countLimit = $this->itemLimitOrderRepository->checkLimitCount($code);
        $countShopPlan = $this->shopPlanOrderRepository->checkCountPlan($shopCode, $code, $dateReceive);

        if (!$countLimit && !$countShopPlan) {
            $return['message'] = config('const.stock.stock_required');
        } else if (!$countLimit && $countShopPlan) {
            if ($countShopPlan >= $number) {
                $return['success'] = true;
                $return['stock'] = $countShopPlan;
            } else {
                $return['stock'] = $countShopPlan;
                $return['message'] = config('const.stock.stock_required');
            }
        } else if ($countLimit && !$countShopPlan) {
            if ($countLimit >= $number) {
                $return['success'] = true;
                $return['stock'] = $countLimit;
            } else {
                $return['stock'] = $countLimit;
                $return['message'] = config('const.stock.stock_required');
            }
        } else {
            if ($countLimit < $number && $countShopPlan >= $number) {
                $return['message'] = config('const.stock.stock_required');
                $return['stock'] = $countShopPlan;
            } else if ($countLimit >= $number && $countShopPlan < $number) {
                $return['message'] = config('const.stock.stock_required');
                $return['stock'] = $countLimit;
            } else if ($countLimit < $number && $countShopPlan < $number) {
                $return['message'] = config('const.stock.stock_required');
            } else {
                $return['success'] = true;
                $return['stock'] = ($countLimit <= $countShopPlan) ? $countLimit : $countShopPlan;
            }
        }
        return $return;
    }
}
<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\BackendController;
use App\Repositories\CustomerOrderRepository;
use App\Repositories\CustomerRepository;
use Illuminate\Http\Request;

class UserController extends BackendController
{
    protected $customerRepository;
    protected $customerOrderRepository;

    public function __construct(CustomerRepository $customerRepository, CustomerOrderRepository $customerOrderRepository)
    {
        $this->customerRepository = $customerRepository;
        $this->customerOrderRepository = $customerOrderRepository;
    }

    public function index(Request $request)
    {
        $customers = $this->customerRepository->searchCondition($request)->paginate(config('const.paging.backend'));
        return view('backend.user.index', compact('customers'))
            ->with([
                'card_no' => $request->get('card_no'),
                'tel' => $request->get('tel'),
                'name' => $request->get('name'),
                'email' => $request->get('email'),
            ]);
    }

    public function show(Request $request, $id)
    {
        $customer = $this->customerRepository->find($id);
        if ($customer == null) {
            return redirect()->route('user.index')->with('error', config('messages.NO_DATA'));
        }
        $has_password = $customer->password != null ? true : false;
        $orders = $this->customerOrderRepository->getAllCustomerOrder($id, $request)->paginate(config('const.paging.backend'));
        return view('backend.user.show', compact('customer'))
            ->with([
                'has_password' => $has_password,
                'orders' => $orders,
                'status_array' => config('const.status_customer_order')
            ]);
    }
}

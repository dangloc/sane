@extends('frontend.layouts.main')
@section('css')
    <link rel="stylesheet" href="{{asset('frontend/css/static.css')}}">
@endsection
@section('content')
    <div class="rule">
        <div class="container">
            <h1 class="page_title">会員規約</h1>
            <div class="rule_content">
                <div class="rule_description">サンエーネット予約サービス会員規約</div>
                <p class="content_top">サンエーネット予約サービス会員規約（以下「本規約」といいます）は、サンエーネット予約サービスをご利用いただくにあたり、 会員様と弊社との間に適用
                    される取り決めとなります。会員登録は、必ず本規約をご確認してください。</p>
                <div class="content_group">
                    <div class="content_group_title">第１条（本規約）</div>
                    <div class="content_group_rule">
                        <p>１. 本規約は、株式会社サンエー (以下「弊社」といいます) がインターネットにより提供するサービス (以下「本サービス」といいます) を、次条で定義</p>
                        <p>するサンエーネット予約サービス会員（以下「会員様」といいます）が利用することに伴うすべての事項にわたり適用するものとします。</p>
                        <p>２. 弊社は、会員様の事前の了承を得ることなく本規約等を変更できるものとし、会員様はこれを承諾するものとします。また、本規約の変更後は、変更後
                        </p>
                        <p>の本規約を適用するものとし、弊社が適切と考える手段によって随時会員様に告知するものとします。</p>
                        <p>３. 弊社は、会員様が本サービスを利用した場合、当該会員様が本規約等に同意したものとみなします。</p>
                    </div>
                </div>
                <div class="content_group">
                    <div class="content_group_title">第２条（会員定義）</div>
                    <div class="content_group_rule">
                        <p>本規約において「会員様」とは、本規約の内容を承認し、所定の方法により入会申し込みを行い、弊社に承認された方をいいます。</p>
                    </div>
                </div>
                <div class="content_group">
                    <div class="content_group_title">第３条（本サービスの享受）</div>
                    <div class="content_group_rule">
                        <p>１．本サービスを利用し、商品を購入することができるお客様は、必要情報を正しく入力された会員様ご本人様に限ります。</p>
                        <p>2．会員様は、弊社が別途定める「サンエーネット予約サービスご利用ガイド」に従って本サービスを享受するものとします。</p>
                        <p>3．会員様が未成年者であるときは、本サービスご利用の都度、本サービス利用について事前に親権者等の代表者の同意を得るものとします。</p>
                        <p>4．酒類の販売につきましては、法令により未成年の会員様へは販売できませんのでご了承下さい。</p>
                    </div>
                </div>
                <div class="content_group">
                    <div class="content_group_title">第４条(ユーザーＩＤおよびパスワードの管理)</div>
                    <div class="content_group_rule">
                        <p>１．会員様は、ユーザーＩＤおよび自ら設定したパスワードの管理責任を負うものとします。</p>
                        <p>２．会員様は、ユーザーＩＤおよびパスワードを第三者に譲渡、貸与、開示してはならないものとします。</p>
                        <p>３．会員様は、ユーザーＩＤおよびパスワードの不適切な管理、使用上の過誤、第三者による使用などに起因する損害につき自ら責任を負うものとします。</p>
                        <p>４．会員様は、ユーザーＩＤおよびパスワードを第三者によって不正に使用されていることが判明した場合には、 直ちに弊社に連絡するとともに、弊社からの
                            指示がある場合には、これに従うものとします。</p>
                    </div>
                </div>
                <div class="content_group">
                    <div class="content_group_title">第５条（個人情報保護方針）</div>
                    <div class="content_group_rule">
                        <p>弊社が、別途掲げる「個人情報保護方針」に基づくものとします。 ご利用前に会員様はサンエーネット予約サービス「個人情報保護方針」をご一読いただき、
                            内容を承認したものとします。</p>
                    </div>
                </div>
                <div class="content_group">
                    <div class="content_group_title">第６条(個人情報の登録)</div>
                    <div class="content_group_rule">
                        <p>１. 会員様は、入会申し込み時、その他弊社からの依頼により情報を申告する際、いかなる虚偽の申告も行わないものとします。</p>
                        <p>２．会員様は、弊社に登録した情報に変更が生じた場合、速やかに変更登録するものとします。</p>
                        <p>弊社は、変更登録がなされなかったことにより生じた損害について、一切責任を負いません。</p>
                        <p>３．会員様がサービスを利用の際、変更登録を行った場合でも、変更登録前に既に手続きがなされた注文は、変更登録前の情報に基づいて行われます。こ
                            のような注文について変更の必要がある場合には、速やかに弊社に連絡するものとします。</p>
                        <p>４．会員様が登録・申告された情報は、特定の個人を特定できる個人情報として、弊社が別掲「個人情報保護方針」に基づき管理するものとします。</p>
                        <p>５. 弊社は、原則として会員様の承諾なしに個人情報の修正・変更をしないものとします。</p>
                    </div>
                </div>
                <div class="content_group">
                    <div class="content_group_title">第７条(サンエーネット予約サービス会員情報の利用)利用目的</div>
                    <div class="content_group_rule">
                        <p>１．弊社ではご登録いただいた個人情報の利用は、以下の利用目的の範囲内で行います。</p>
                        <p>　(１)　サンエーネット予約サービスご利用会員登録（ご入会申込み）、訂正、利用停止等</p>
                        <p>　(２)　各種のご優待、セール・バーゲンのお知らせ</p>
                        <p>　(３)　会員様からご請求のあった資料・情報等のご提供</p>
                        <p>　(４)　ご登録頂いたメールアドレスへの受注・確定に関するご連絡</p>
                        <p>　(５)　電話、メール等によるご注文内容の確認、商品等の配達</p>
                        <p>　(６)　イベントやキャンペーン等の募集活動、応募者へのご連絡・説明、資料・情報・サービスの提供等</p>
                        <p>　(７)　サンエーネット予約サービスのサービス向上のための企画立案の参考データ</p>
                        <p>　(８)　会員様等からのお問合せに関する対応</p>
                        <p>　(９)　未成年者の会員様情報に関する親権者等への照会</p>
                        <p>　(１０)　その他、サンエーネット予約サービスを通じたサービスの提供および上記の継続確認</p>
                        <p class="rule_space">２．第三者への開示</p>
                        <p>弊社は、会員様の同意がないかぎりご提供いただいた個人情報を第三者に開示することはございません。但し、第６条第１項記載の利用目的の達成に必要</p>
                        <p>な範囲内において、あらかじめ当社との間で秘密保持契約を締結している業務委託先に個人情報を提供する場合があります。また、次の各号に該当する場</p>
                        <p>合は法令に基づきお客様の個人情報を第三者に提供したり、利用目的の範囲を超えて利用することがあります。</p>
                        <p>　(１)法令に基づく場合</p>
                        <p>　(２)人の生命、身体又は財産の保護のために必要がある場合であって、お客様の同意を得ることが困難である場合</p>
                        <p>　(３)公衆衛生の向上又は児童の健全な育成の推進のために特に必要がある場合であって、お客様の同意を得ることが困難である場合</p>
                        <p>　(４)国の機関若しくは地方公共団体又はその委託を受けた者が法令の定める事務を遂行することに対して 協力する必要がある場合であって、お客様の同</p>
                        <p>意を得ることにより当該事務の遂行に支障を及ぼす おそれがある場合</p>
                        <p class="rule_space">３．同意</p>
                        <p>会員様は、本第７条第１項および第２項に同意したものとします。</p>
                    </div>
                </div>
                <div class="content_group">
                    <div class="content_group_title">第８条（会員様の禁止行為）</div>
                    <div class="content_group_rule">
                        <p>１. 会員様は、弊社がインターネット、印刷物、チラシ等で提供する本サービスおよび商品等に関する情報について、営利を目的として利用してはならないも
                            のとします。</p>
                        <p>２. 弊社より会員様へ送付した電子メールの内容に関する著作権についても全て弊社に帰属しており、無断で転用・転載することはできません。</p>
                        <p>３. 当サイトへのリンク等も原則として禁止します。ただし、弊社が事前に承諾した場合はこの限りではありません。</p>
                        <p>３．会員様がサービスを利用の際、変更登録を行った場合でも、変更登録前に既に手続きがなされた注文は、変更登録前の情報に基づいて行われます。こ
                            のような注文について変更の必要がある場合には、速やかに弊社に連絡するものとします。</p>
                        <p class="rule_space">４.会員様の行為が下記に該当すると弊社が判断した場合、弊社は会員様に事前通知または催告すること なく退会させることが出来るものとします。</p>
                        <p>　(１) 公序良俗に違反する行為</p>
                        <p>　(２) 犯罪的行為に結びつく行為。</p>
                        <p>　(３) 諸費用及び商品代金の支払い、並びにその他本サービスの利用に伴って発生する債務の履行を遅滞、又は怠る行為。</p>
                        <p>　(４) 他の会員様または第三者の著作権を侵害する行為</p>
                        <p>　(５) 他の会員様または第三者の財産、プライバシー等を侵害する行為</p>
                        <p>　(６) 知的所有権、著作権、その他知的財産権について法律に反する行為</p>
                        <p>　(７) 本サービスの運営を妨げる行為、その他本サービスに支障をきたす恐れのある行為。</p>
                        <p>　(８) 他の会員様、第三者、またはサンエーネット予約サービスに対して、迷惑、不利益、もしくは損害を与える行為、又はその恐れのある行為。</p>
                        <p>　(９) 他の会員様、第三者、またはサンエーネット予約サービスおよびサンエーネット予約サービス業務を誹謗中傷する行為</p>
                        <p>　(１０) 選挙の事前運動、選挙運動またはこれらに類似する行為および公職選挙法に抵触する行為</p>
                        <p>　(１１)  弊社を通じて入手した情報を、弊社の承諾なく、複製、販売、出版その他私的利用の範囲を超えて 使用する行為</p>
                        <p>　(１２) その他、弊社が会員様として不適切であると判断した場合</p>
                    </div>
                </div>
                <div class="content_group">
                    <div class="content_group_title">第９条（情報提供）</div>
                    <div class="content_group_rule">
                        <p>１. 弊社より会員様に対し、ダイレクトメール（ハガキ等）、FAX、インターネット、電子メール等の手段を用いて商品情報、特売情報等情報提供を行なうこ
                            とについて、会員様はあらかじめこれを承諾し、何ら異議を申し 立てないものとします。</p>
                        <p>２. サンエーネット予約サービスが提供する商品価格、特売情報等と、サンエー各店から提供するそれら情報とは一致しないことがあります。会員様はあらか
                            じめこれを承諾した上でサービスを利用するものとし、何ら異議を申し立てないものとします。</p>
                    </div>
                </div>
                <div class="content_group">
                    <div class="content_group_title">第１０条（著作権等の帰属）</div>
                    <div class="content_group_rule">
                        <p>サンエーネット予約サービスのホームページに掲載される画像、デザイン等に関する著作権または商標権、その他の知的財産権は、すべて弊社またはその他
                            の著作権者等、業務委託先含む正当な権利者に帰属する ものであり、会員様はこれらの権利を侵害する行為を行わないものとします。</p>
                    </div>
                </div>
                <div class="content_group">
                    <div class="content_group_title">第１１条(サービスの停止、変更等)</div>
                    <div class="content_group_rule">
                        <p>１．弊社は、次のいずれかの場合、会員様への事前の通知、承認なく、本サービスを一時停止することが できます。</p>
                        <p>　(１)　本サービスの提供に必要な保守、点検が必要な場合およびシステムトラブルが発生した場合</p>
                        <p>　(２)　火災、停電、通信回線の障害等が発生し、本サービスの提供ができないとき</p>
                        <p>　(３)　天変地異、戦争、テロ等不可抗力により本サービスの提供ができないとき</p>
                        <p>　(４)　その他、止むを得ず本サービスの停止が必要と弊社が判断した場合</p>
                        <p>２. 弊社は、本サービスの内容の全部または一部について、会員様の承諾を受けることなく、変更、追加 または、削除を行うことができるものとします。</p>
                        <p>３．本条第１項または第２項により本サービスが停止、変更等されたことにより会員様が被った損害について、 弊社は一切の責任を負わないものとします。</p>
                        <p>４. 弊社は、何らかの外的要因(システムの故障など)により、サンエーネット予約サービスのデータが故障した場合、その責任は負わないものとします。</p>
                    </div>
                </div>
                <div class="content_group">
                    <div class="content_group_title">第１２条(免責事項)</div>
                    <div class="content_group_rule">
                        <p>１. 弊社は、本サービスの内容および会員様が本サービスを通じて得る情報等についてその完全性、正確性確実性等いかなる保証も行わないものとします。</p>
                        <p>２. 弊社は、本規約および追加規定、個別規定に明示的に定める場合を除き、弊社の責任に帰すべからざる事由から発生した損害、特別の事情から生じた
                            障害、 逸失利害、および第三者からの損害賠償請求に基づく会員様の損害についてはその責任は負わないものとします。</p>
                        <p>３. 弊社のウェブページ・サーバ・ドメインなどから送られるメール・コンテンツに、コンピューター・ ウィルスなどの有害なものが含まれてないことを保障い
                            たしません。</p>
                        <p>４．会員様が、本規約等に違反したことによって生じた損害については、弊社は一切責任を負わないものとします。</p>
                        <p>５. 商品のお届け時にお客様が不在などで受け渡しが出来ない場合は、確認されたお届け場所（指定場所）に商品を留め置くものとし、持ち帰り・再配達は
                            行いません。商品の所有権は、留め置き実施時に会員様へ移転しますので、その後の事故については、弊社は責任を負わないものとします。</p>
                    </div>
                </div>
                <div class="content_group">
                    <div class="content_group_title">第１３条（会員資格の停止、および資格の抹消）</div>
                    <div class="content_group_rule">
                        <p>会員情報に虚偽または不正が明らかになった場合、本規約に違反した等の理由により、弊社が不適当と判断した会員様については、弊社は会員様への事前</p>
                        <p>の通知を行なうことなく、本サービスの利用停止または 会員資格の抹消を行うことができるものとします。</p>
                        <p>また、弊社は会員資格の停止および資格の抹消を受けた会員様に対して再度の会員登録を拒否できるものとします。</p>
                    </div>
                </div>
                <div class="content_group">
                    <div class="content_group_title">第１４条(退会)</div>
                    <div class="content_group_rule">
                        <p>１.会員様は、弊社の定める方法に基づき通知することにより随時退会可能とします。</p>
                        <p>２.退会後であっても、入会中にサービスの提供を受けられた場合、その受けられたサービスについては 引き続き本規約が適用されるものとします。</p>
                    </div>
                </div>
                <div class="content_group">
                    <div class="content_group_title">第１５条（本サービスの終了）</div>
                    <div class="content_group_rule">
                        <p>１. 弊社は、営業上その他の理由により、本サービスの全部又は一部の提供を終了する場合、３０日前の 予告期間をもって以下の方法により会員様に知らし
                            めるものとします。</p>
                        <p>　(１)　ログイン画面又は本サービスに付随するサイト画面のいずれかにおいて告知します。</p>
                        <p>　(２)　電子メールまたはその他の方法により通知いたします。</p>
                        <p>２.会員様が、前項の定めによる告知又は通知を確認しなかったことにより不利益を被ったとしても、弊社は 会員様に対して一切責任を負わないものとします。</p>
                    </div>
                </div>
                <div class="content_group">
                    <div class="content_group_title">第１６条（規約の改訂）</div>
                    <div class="content_group_rule">
                        <p>本規約が弊社により改訂されることがあることを会員様はあらかじめ承諾するものとします。 尚、改訂後の会員規約は弊社の定める方法により会員様に通知を
                            発した時点から効力が生じます。</p>
                    </div>
                </div>
            </div>
            <div class="rule_back"><a href="{{'site.index'}}">TOPページへ戻る</a></div>
        </div>
    </div>
@endsection
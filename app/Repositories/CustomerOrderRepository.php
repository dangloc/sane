<?php

namespace App\Repositories;

use App\Models\CustomerOrder;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
/**
 * class CustomerOrderRepository.
 *
 * @package namespace App\Repositories;
 */
class CustomerOrderRepository extends BaseRepository
{
    public function __construct()
    {
        $this->_model = CustomerOrder::class;
    }

    public function getCustomerOrder($customer_id, $status)
    {
        $data = CustomerOrder::where('customer_id', $customer_id)->where('status', $status)->where('del_flag', 0)->with('shop')->with('reserves')->get();
        return $data;
    }

    public function insertCustomerOrder($reserveData = array(), $customerId)
    {
        if (!empty($reserveData)) {
            $slipNumberCheckArr = array();
            foreach ($reserveData as $key => $reserve) { // Đưa tất cả các slip number đã được insert vào 1 mảng
                if (!in_array($reserve['slip_number'], $slipNumberCheckArr)) {
                    array_push($slipNumberCheckArr, $reserve['slip_number']);
                }
            }
            $customer_insert = array();
            if (!empty($slipNumberCheckArr)) { // chạy vòng lặp check unique slip number và cong price
                foreach ($slipNumberCheckArr as $slipNumber) {
                    $customer_order = array(
                        'slip_number' => $slipNumber,
                    );
                    foreach ($reserveData as $key => $reserve) {
                        if ($reserve['slip_number'] == $slipNumber) {
                            $customer_order['customer_id'] = $customerId;
                            $customer_order['shop_code'] = $reserve['shop_code'];
                            $customer_order['receptionist_date'] = $reserve['receptionist_date'];
                            $customer_order['receipt_date'] = $reserve['receipt_date'];
                            $customer_order['receipt_time'] = $reserve['receipt_time'];
                            $customer_order['customer_name'] = $reserve['customer_name'];
                            $customer_order['customer_tel'] = $reserve['customer_tel'];
                            $customer_order['customer_card_no'] = $reserve['customer_card_no'];
                            $customer_order['status'] = 0;
                            $customer_order['send_status'] = 0;
                            $customer_order['web_order_timestamp'] = Carbon::now();
                            $customer_order['web_cancel_timestamp'] = null;
                            $customer_order['del_flag'] = 0;
                            $customer_order['ins_datetime'] = Carbon::now();
                            $customer_order['ins_id'] = 999999;
                            if (!empty($customer_order['total_price']) ){
                                $customer_order['total_price'] += (int) $reserve['item_count'] * $reserve['price'];
                            }else{
                                $customer_order['total_price'] = (int) $reserve['item_count'] * $reserve['price'];
                            }
                        }
                    }
                    $customer_insert[] = $customer_order;
                }
            }
            if ($this->getModel()->insert($customer_insert)) {  //Eloquent insert multi row
                return $customer_insert;
            }else{
                return false;
            }
        }
        return true;
    }

    public function getCustomerOrderBySlipnumber($slip_number){
        $data=$this->getModel()->where('slip_number',$slip_number)->where('status',\config('const.status_order.booking'))->where('del_flag',\config('const.delete_off'))->first();
        return $data;
    }
    public function updateInfoCustomerOrder($params=array(),$data){
        $query=$this->getModel();
        foreach ($params as $key=>$value){
            $query=$query->where($key,$value);
        }
        if ($query->update($data)){
            return true;
        }
        return false;
    }

    /**
     * @param $slipNumber
     * @return bool
     * Check Slip Number
     */
    public function checkSlipCustomer($slipNumber)
    {
        if (!empty(Auth::guard(config('const.guard.frontend'))->id()) && !empty($slipNumber)) {
            $customer_id = Auth::guard(config('const.guard.frontend'))->id();
            $check = $this->getModel()->select('customer_id')->where('slip_number', '=', $slipNumber)->first();
            if (!empty($check->customer_id)) {
                if ($customer_id == $check->customer_id) {
                    return true;
                }
            }
        }
        return false;
    }

    //  for admin search
    public function searchCondition($request)
    {
        $sort = $request->get('sort', 'slip_number');
        $direction = $request->get('direction', 'desc');
//        $query = DB::table('customer_orders')
        $query = $this->getModel()
            ->where('customer_name', 'like', '%'. $request->get('customer_name') . '%')
            ->where('customer_tel', 'like', '%' . $request->get('customer_tel') . '%')
            ->where('customer_card_no', 'like', '%' . $request->get('customer_card_no') . '%')
            ->when(null !== $request->get('slip_number'), function ($query) {
                return $query->where('slip_number', 'like', '%' . request()->get('slip_number') . '%');
            })
            ->when(null !== $request->get('status'), function ($query) {
                return $query->where('status', '=', request()->get('status'));
            })->when(null !== $request->get('receptionist_date_start'), function ($query) {
                return $query->where('receptionist_date', '>=', str_replace("/","",request()->get('receptionist_date_start')));
            })->when(null !== $request->get('receptionist_date_end'), function ($query) {
                return $query->where('receptionist_date', '<=', str_replace("/","",request()->get('receptionist_date_end')));
            })->when(null !== $request->get('receipt_date_start'), function ($query) {
                return $query->where('receipt_date', '>=', str_replace("/","",request()->get('receipt_date_start')));
            })->when(null !== $request->get('receipt_date_end'), function ($query) {
                return $query->where('receipt_date', '<=', str_replace("/","",request()->get('receipt_date_end')));
            });
        if (null !== $request->get('shop_code')){
            $query->where('shop_code', '=', request()->get('shop_code'));
        } elseif (null !== $request->get('area_id') or null !== $request->get('city_id')){
            $query->leftJoin('shops', 'customer_orders.shop_code', '=', 'shops.code');
            if (null !== $request->get('area_id')) {
                $query->where('area_id', '=', $request->get('area_id'));
            }
            if (null !== $request->get('city_id')) {
                $query->where('city_id', '=', $request->get('city_id'));
            }
        }
        return $query->orderBy($sort, $direction);
    }

    //override find , cuz customer_order 's key is slip_number
    public function find($slip_number)
    {
        return $this->getModel()->where('slip_number', '=', $slip_number)->first();
    }

    public function getAllCustomerOrder($customer_id, $request)
    {
        $sort = $request->get('sort', 'slip_number');
        $direction = $request->get('direction', 'desc');
        $query = $this->getModel()
            ->where('customer_id', '=', $customer_id);
        return $query->orderBy($sort, $direction);
    }
}
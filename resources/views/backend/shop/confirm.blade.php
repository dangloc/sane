@extends('backend.layouts.main')
@section('content')
    <div id="Contentwrap">
        <div id="PageContent" class="no_mar_bottom">
            <h2><i class="fas fa-shapes"></i> 店舗{{ array_key_exists('id', Session::get('session_data')) ? '編集' : '登録'}}
            </h2>

            <div class="order">
                <table class="sp_block flowtable">
                    <tr>
                        <th scope="row" style="width: 150px;">店舗コード</th>
                        <td>
                            {{$shop['code']}}
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" style="width: 150px;">店舗名漢字</th>
                        <td>
                            {{$shop['name']}}
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" style="width: 150px;">店舗名カナ</th>
                        <td>
                            {{$shop['name_kana']}}
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" style="width: 150px;">エリア</th>
                        <td>
                            {{$area_name}}
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" style="width: 150px;">郵便番号</th>
                        <td>
                            {{$shop['zip_code']}}
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" style="width: 150px;">市町村</th>
                        <td>
                            {{$city_name}}
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" style="width: 150px;">住所</th>
                        <td>
                            {{$shop['address']}}
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" style="width: 150px;">経度緯度</th>
                        <td>
                            {{$shop['latlng']}}
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" style="width: 150px;">電話番号</th>
                        <td>
                            {{$shop['tel']}}
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" style="width: 150px;">営業時間</th>
                        <td>
                            {{ formatTime($shop['open_time'])}}〜{{formatTime($shop['close_time'])}}
                        </td>
                    </tr>
                </table>

            </div>
            <!--ContentFoot -->
        </div>
        <!--PageContent -->
        <form method="post"
              action="{{ array_key_exists('id', Session::get('session_data')) ? route('shop.update', $shop['id']) : route('shop.store')}}"
              id="form_shop_{{array_key_exists('id', Session::get('session_data')) ? 'confirm_edit':'confirm_create'}}">
            {{ csrf_field() }}
            @if(array_key_exists('id', Session::get('session_data')))
                @method('PUT')
            @endif
            <div class="contentfoot">
                <p class="btn Searchclear">
                    <button type="button" value=""
                            onclick="window.location='{{ array_key_exists('id', Session::get('session_data')) ? route("shop.edit", $shop['id']):route("shop.create") }}'">
                        <i class="fas fa-undo"></i> 戻る
                    </button>
                </p>
                <p class="btn register">
                    <button type="button"
                            onclick="System.showDialog('shop', '{{ array_key_exists('id', Session::get('session_data')) ? 'confirm_edit' : 'confirm_create'}}')">
                        <i class="fas fa-check"></i> {{ array_key_exists('id', Session::get('session_data')) ? '編集' : '登録'}}する
                    </button>
                </p>
            </div>
            <!--PageContent -->
        </form>
    </div>
    <!--Contentwrap -->
@endsection
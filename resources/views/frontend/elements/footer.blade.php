<footer id="footer">

    <div class="top-footer">
        <div class="container">
            <div class="footer-menu" id="footer-menu">
                <nav class="navbar navbar-expand-lg">
                    <div class="collapse navbar-collapse">
                        <div class="navbar-nav">
                            <a class="nav-link" href="{{route('site.index')}}">トップページ</a>
                            <a class="nav-link" href="https://www.san-a.co.jp/company/about/" target="_blank">会社概要</a>
                            <a class="nav-link" href="{{route('guild')}}">ご利用ガイド</a>
                            <a class="nav-link" href="{{route('tokutei')}}">特定商取引に関する法律に基づく表示</a>
                            <a class="nav-link" href="{{route('browser')}}">推奨環境</a>
                            <a class="nav-link" href="{{route('sitemap')}}">サイトマップ</a>
                            <a class="nav-link" href="{{route('privacy')}}">プライバシーポリシー</a>
                            <a class="nav-link" href="{{route('rule')}}">会員規約</a>
                            <a class="nav-link" href="{{route('information')}}">お知らせ一覧</a>
                            {{--<a class="nav-link" href="#">お問い合わせ</a>--}}
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>

    <div class="middle-footer">
        <div class="container">
            <p><a id="goTop" href="#main-header"><i class="fas fa-step-forward"></i>ページの先頭へ</a></p>
        </div>
    </div>

    <div class="bottom-footer">
        <div class="container">
            <p><span>©</span> 2019 SAN-A CO.,LTD.</p>
        </div>
    </div>

</footer>
<script type="text/javascript" src="{{asset('frontend/js/main.js')}}"></script>
<script type="text/javascript" src="{{asset('frontend/js/autoload/Frontend.js') }}"></script>
<script type="text/javascript" src="{{asset('frontend/js/autoload/FrontendReady.js') }}"></script>
<script type="text/javascript" src="{{asset('js/laroute.js') }}"></script>
<script type="text/javascript" src="{{asset('frontend/js/datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('frontend/js/boostrap-datepicker.ja.js')}}"></script>
<script type="text/javascript" src="{{asset('frontend/js/toastr.js')}}"></script>

@yield('js')
<?php

namespace App\Models;
use App\Scopes\ActiveScope;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    public $timestamps = false;
    protected $_alias = 'events';
    protected $table = 'events';
    protected $fillable = [
        'code',
        'name',
        'start_date',
        'end_date',
        'del_flag',
        'ins_datetime',
        'ins_id',
        'upd_datetime',
        'upd_id'
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ActiveScope());
    }
}

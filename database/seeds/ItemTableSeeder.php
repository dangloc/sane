<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ItemTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('items')->insert([
            [
                'shop_code' => '002',
                'code' => '001',
                'item_image_name' => 'ダミーテキスト.jpg',
                'item_image_folder_name' => \Illuminate\Support\Str::random(10),
                'content1' => 'ダミーテキストダミーテ',
                'content2' => 'ダミーテキストダミーテキスト<br>ダミーテキストダミーテキストダミーテキストダミーテキ',
                'content3' => 'ダミーテキストダミーテキス',
                'required_confirm_flag' => '1',
                'limited_flag' => '1',
                'limited_count' => '11',
                'additive_code' => '232',
                'lead_time' => '12',
                'reserve_content1' => 'ダミーテキストダミーテキスト<br>ダミーテキスト',
                'reserve_content2' => '',
                'reserve_content3' => '',
                'del_flag' => '0',
                'ins_datetime' => date('Y-m-d', strtotime(' -1 day')),
                'ins_id' => '1',
                'upd_datetime' => date('Y-m-d', strtotime(' -1 day')),
                'upd_id' => '1',
            ]
        ]);
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;

class BannerCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected $redirect;

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule = ['link_url' => 'max:128',
            'public_start_date' => 'required',
            'public_end_date' => "required",
            ];
        if(!Session::has('banner_url_temp')){
            $rule['banner_url'] = 'required|max:2000|mimes:jpeg,bmp,png,jpg|dimensions:width=700,height=345';
        } else {
            $rule['banner_url'] = 'max:2000|mimes:jpeg,bmp,png,jpg|dimensions:width=700,height=345';
        }
        $this->redirect = route('banner.create',['flag'=>config('const.flag')['from_confirm']]);
        return $rule;
    }
    public function attributes()
    {
        return
        [
            'banner_url' => 'バナー画像',
            'link_url' => 'リンク先URL',
            'public_start_date' => "期間開始",
            'public_end_date' => "期間終了",
        ];
    }
    public function messages()
    {
        return [
            'banner_url.max' => 'バナー画像 2MB以下のファイルを指定してください。',
            'banner_url.dimensions' => 'アップロードされた画像のサイズは不正です。'
        ];
    }

    protected function validationData()
    {
        $all = parent::validationData();
        if (request()->hasFile('banner_url') ){
            $file = Input::file('banner_url');
            $imageFileName = time(). $file->getClientOriginalName();
            $file->storeAs('public/temp/', $imageFileName);
            $urlFile = 'storage/temp/' . $imageFileName;
            Session::put('banner_url_temp',$urlFile);
        }
        request()->flash();
        return $all;
    }
}

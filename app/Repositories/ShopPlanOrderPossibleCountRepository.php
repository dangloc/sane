<?php

namespace App\Repositories;

use App\Models\Customer;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

/**
 * class ShopPlanOrderPossibleCountRepository.
 *
 * @package namespace App\Repositories;
 */
class ShopPlanOrderPossibleCountRepository extends BaseRepository
{
    function __construct()
    {
        $this->_model = \App\Models\ShopPlanOrderPossibleCount::class;
    }
    /**
     * @param $shop_code
     * @param $item_code
     * @return mixed
     * Check so luong ke hoach
     */
    public function checkCountPlan($shop_code, $item_code, $dateReceive)
    {
        $data = $this->getModel()->select(DB::raw('(order_possible_count * 0.9) as order_possible_count'))
            ->where('shop_code', '=', $shop_code)
            ->where('item_code', '=', $item_code)
            ->where(DB::raw('STR_TO_DATE ( receipt_date,  "%Y%m%d")'), '=', Carbon::parse($dateReceive)->format('Y-m-d'))
            ->first();
        if (!empty($data)) {
            return !empty($data->order_possible_count) ? (int)$data->order_possible_count : false;
        }
        return false;
    }
}

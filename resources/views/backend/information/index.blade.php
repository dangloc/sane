@extends('backend.layouts.main')
@section('content')
    <div id="Contentwrap">
        @include('common.message_notification')
        <div id="PageContent">
            <form action="{{route('information.index')}}" method="get">
                <h2><i class="fas fa-search"></i> お知らせ検索</h2>
                <div class="order">
                    <table class="sp_block">
                        <tr>
                            <th scope="row" class="width-150">タイトル</th>
                            <td colspan="4">
                                <div class="inline_block rel full_input"><input type="text" placeholder="タイトル"
                                                                                tabindex="5" name="title"
                                                                                value="{{isset($title) ? $title : ''}}">
                                </div>
                            </td>
                        </tr>
                        <tr class="valign-m th">
                            <th scope="row" class="width-150">公開期間</th>
                            <td colspan="4" class="content-center">
                                <div class="inline_block rel">
                                    <input type="text" id="start_date" placeholder="例：2019/01/02" tabindex="5"
                                           name="public_start_date" autocomplete="off"
                                           value="{{isset($public_start_date) ? $public_start_date : ''}}">
                                </div>
                                <br class="sp">&nbsp;〜&nbsp;<br class="sp">
                                <div class="inline_block rel">
                                    <input type="text" id="end_date" placeholder="例：2019/01/02" tabindex="6"
                                           name="public_end_date" autocomplete="off"
                                           value="{{isset($public_end_date) ? $public_end_date : ''}}">
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="contentfoot">
                    <p class="btn Searchclear">
                        <button type="button" onclick="System.redirectLoading('{{route('information.index')}}')"
                                value="">
                            検索条件をクリア
                        </button>
                    </p>
                    <p class="btn Search">
                        <button type="submit" value="" tabindex="13" onclick="System.showLoading()"><img
                                src="{{ asset('backend/images/common/icon3.png') }}"
                                alt="検索"> 検索
                        </button>
                    </p>
                </div>
                <!--ContentFoot -->
            </form>
        </div>
        <!--PageContent -->


        <div id="PageContent" class="pt60">
            <h2><i class="fas fa-shapes"></i> お知らせ一覧<span class="btn register btnMedium sp-mt-neg-86" style="margin-top:-40px;">
                    <button
                        type="button" onclick="window.location='{{ route("information.create") }}'" tabindex="14"><i
                            class="fas fa-plus"></i> お知らせ登録</button>
                    </span></h2>

            <div class="PageContentNav">
                @include('backend/elements/paging', ['results' => $informations])
                {{ $informations->appends(request()->input())->links() }}
            </div>
            <table class="OrderForm responsive tdcenter Bordertd TextL table-hover">
                <tr>
                    <th scope="col" width="100px">@include("backend/elements.sortable", ["field" => "id", "title" => "ID"])</th>
                    <th scope="col">@include("backend/elements.sortable", ["field" => "title", "title" => "タイトル"])</th>
                    <th scope="col" width="250px">@include("backend/elements.sortable", ["field" => "public_start_date", "title" => "公開期間"])</th>
                    <th scope="col" width="120px">編集</th>
                    <th scope="col" width="120px">削除</th>
                </tr>
                @if(!$informations->isEmpty())
                    @foreach($informations as $information)
                        <tr>
                            <td>{{$information->id}}</td>
                            <td style="text-align:left;">{{$information->title}}</td>
                            <td style="text-align:center;">{{ \Carbon\Carbon::parse($information->public_start_date)->format('Y/m/d')}}
                                〜
                                {{ \Carbon\Carbon::parse($information->public_end_date)->format('Y/m/d')}}
                            </td>

                            <td><p class="btnmini Edit"><a
                                        href="{{route('information.edit', ['id' => $information->id])}}" class="TextC">編集</a>
                                </p></td>
                            <td>
                                <form id="{{'form_information_confirm_delete'.$information->id}}" method="post"
                                      action="{{ route('information.destroy', $information->id) }}">
                                    {{ csrf_field() }}
                                    @method('DELETE')
                                    <p class="btnmini">
                                        <button type="button"
                                                onclick="Backend.showDialog('information', 'confirm_delete', {{$information->id}})">
                                            削除
                                        </button>
                                    </p>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @else
                    @include('backend/elements/no_data',['col' => 5])
                @endif
            </table>
            <div class="PageContentNav">
                {{ $informations->appends(request()->input())->links() }}
            </div>
        </div>
        <!--PageContent -->

    </div>
    <!--Contentwrap -->
@endsection
@section('js')
    <script>
        $(document).ready(function () {
            System.setRangeDate($('#start_date'), $('#end_date'));
        });
    </script>
@endsection
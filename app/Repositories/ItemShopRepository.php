<?php

namespace App\Repositories;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * class ItemShopRepository.
 *
 * @package namespace App\Repositories;
 */
class ItemShopRepository extends BaseRepository
{

    function __construct()
    {
        $this->_model = \App\Models\ShopPlanOrderPossibleCount::class;
    }

    public function paging()
    {
        $sort = request()->get('sort', 'item_code');
        $direction = request()->get('direction', 'desc');
        $params = request()->all();

        return $this->getModel()
            ->when(isset($params['item_code']) && $params['item_code'] != '', function ($q) use ($params) {
                return $q->where('item_code', 'like', '%' . $params['item_code'] . '%');
            })
            ->when(isset($params['shop_code']) && $params['shop_code'] != '', function ($q) use ($params) {
                return $q->where('shop_code', 'like', '%' . $params['shop_code'] . '%');
            })
            ->when(isset($params['receipt_start_date']) && $params['receipt_start_date'] != '', function ($q) use ($params) {
                return $q->where('receipt_date', '>=' , convertDateToChar($params['receipt_start_date'],true));
            })
            ->when(isset($params['receipt_end_date']) && $params['receipt_end_date'] != '', function ($q) use ($params) {
                return $q->where('receipt_date', '<=' , convertDateToChar($params['receipt_end_date'],true));
            })
            ->orderBy($sort, $direction)
            ->paginate(config('const.paging.backend'));
    }
}

@extends('backend.layouts.main')
@section('css')
    <link rel="stylesheet" href="/frontend/css/error.css">
@endsection
@section('content')
    <div id="Contentwrap">
        <div class="color-error message-notification">
            システムエラーが発生しました。
        </div>
    </div>
@endsection
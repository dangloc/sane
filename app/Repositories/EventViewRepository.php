<?php

namespace App\Repositories;


use Illuminate\Http\Request;

/**
 * class EventViewRepository.
 *
 * @package namespace App\Repositories;
 */
class EventViewRepository extends BaseRepository
{
    function __construct()
    {
        $this->_model = \App\Models\EventView::class;
    }

    public function paging()
    {
        $sort = request()->get('sort', 'event_code');
        $direction = request()->get('direction', 'asc');
        $params = request()->all();

        return $this->getModel()
            ->when(isset($params['event_code']) && $params['event_code'] != '', function($q) use ($params) {
                return $q->where('event_code', 'like', '%' . $params['event_code'] . '%');
            })
            ->orderBy($sort, $direction)
            ->paginate(config('const.paging.backend'));
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Http\Common\logWriter;

class TsvExportController extends Controller
{
    function pushLog()
    {
        return new logWriter('test_logs/export-tsv-' . Carbon::now()->format('Y-m-d') . '.log');
    }

    function checkFile($model, $files, $type)
    {
        $path = config('tsv_export.' . $model . '.folder');
        $inputs = preg_grep('/^(' . $path . '\/' . $type . ')$/', $files);
        return !(sizeof($inputs) == 0);
    }


    function createFile($model, $type)
    {
        $path = config('tsv_export.' . $model . '.folder');
        Storage::disk('sftp')->put($path . '/' . $type, '');
    }


    function removeFile($model, $type)
    {
        $path = config('tsv_export.' . $model . '.folder');
        Storage::disk('sftp')->delete($path . '/' . $type);
    }

    function getRandomNumber($length = 5) {
        $characters = '0123456789';
        $string = '';

        for ($i = 0; $i < $length; $i++) {
            $string .= $characters[mt_rand(0, strlen($characters) - 1)];
        }

        return $string;
    }

    // flow basic , c
    public function export_tsv()
    {
        $sftp = Storage::disk('sftp');
        $model = 'Reserve';
        $path = config('tsv_export.' . $model . '.folder');
        $this->pushLog()->info("START EXPORT IN " . $path);

        //try to connect 3 time
        for ($i = 0; $i < 3; $i++) {
            try {
                $sftp->getAdapter()->getConnection();
            } catch (\Exception $exception) {
                $this->pushLog()->error($exception->getMessage());
                $sftp->getAdapter()->disconnect();
            }
        }
        $list_file = $sftp->allFiles($path);

        // check file ERR
        if ($this->checkFile($model, $list_file, 'ERR')) {
            // disconnect + push log ERROR
            $sftp->getAdapter()->disconnect();
            $this->pushLog()->error("WARNING ! Find ERR in " . $path);

        } elseif ($this->checkFile($model, $list_file, 'RUN')) {
            // disconnect + push log COMPLETE
            $sftp->getAdapter()->disconnect();
            $this->pushLog()->info("COMPLETE ! Find RUN in " . $path);
        } else {
            // start run process
            $this->createFile($model, 'RUN');
            // search in customer_order
            $query = DB::table('customer_orders')
                ->select('slip_number')
                ->where('send_status' , '=', 0)
                ->whereIn('status', [0, 1])
                ->where('del_flag', '=', 0);
            if (!sizeof($query->pluck('slip_number')->toArray())) {
                // disconnect + push log COMPLETE
                $sftp->getAdapter()->disconnect();
                $this->pushLog()->info("COMPLETE ! No acceptable slip_number in customer_orders");
            } else {
                // co record, loop tung record
                $slip_number_array = $query->pluck('slip_number')->toArray();

                $data_group_by = DB::table('reserves')
                    ->select('slip_number', 'shop_code', 'receptionist_date', 'receipt_date')
//                    ->groupBy('slip_number', 'shop_code', 'receptionist_date', 'receipt_date')
                    ->groupBy('slip_number', 'shop_code', 'receptionist_date', 'receipt_date', 'receipt_time', 'event_code')
                    ->get();

                foreach ($data_group_by as $data) {
                    if (in_array($data->slip_number, $slip_number_array)) {
                        $query = DB::table('reserves')
                            ->select('*')
                            ->where('slip_number', '=', $data->slip_number)
                            ->where('shop_code', '=', $data->shop_code)
                            ->where('receptionist_date', '=', $data->receptionist_date)
                            ->where('receipt_date', '=', $data->receipt_date);

                        if (!sizeof($query->get()->toArray())) {
                            // disconnect, push log COMPLETE, no record in reserves
                            $sftp->getAdapter()->disconnect();
                        } else {
                            // start create file TSV
                            $content = '';
                            foreach ($query->get()->toArray() as $record) {
                                $list_fields = config('tsv_export.Reserve.fields');
                                foreach ($list_fields as $field) {
                                    $content .= $record->$field . "\t";
                                }
                                $content .= "\n";
                                $content = str_replace("\t\n", "\n", $content);
                            }
                            $file_name = "GYOJIUKETSUKE_YOYAKUJOHO." . $data->receptionist_date . "." . $data->shop_code . "." . $data->receipt_date . "." . $data->slip_number . "." . $this->getRandomNumber(5);
                            try {
                                $sftp->put('/' . $path . '/' . $file_name . '.tsv', $content);
                                $this->pushLog()->info(" PUT ". $file_name . ".tsv in " . $path );
                                try {
                                    // update customer order
                                    DB::table('customer_orders')
                                        ->where('slip_number', '=', $data->slip_number)
                                        ->update([
                                            'send_status' => 1,
                                            'upd_id' => 999999,
                                            'upd_datetime' => Carbon::now()
                                        ]);
                                    $this->pushLog()->info(" UPDATE customer_orders success");

                                } catch (\Exception $ex) {
                                    $this->pushLog()->error("ERROR ! ERROR when update customer_orders " . $ex);
                                    $sftp->delete('/' . $path . '/BACKUP/' . $file_name . '.tsv');
                                    $this->createFile($model, 'ERR');
                                }
                            } catch (\Exception $exception) {
                                $this->pushLog()->error("ERROR ! Can not PUT file tsv in " . $path);
                                $this->createFile($model, 'ERR');
                            }
                        }
                    }
                }
            }
            if (!$sftp->exists('/' . $path . '/ERR')) {

                $this->pushLog()->info(" COMPLETE export tsv in " . $path);
            }
            $this->removeFile($model, 'RUN');
        }
    }
}

<?php

namespace App\Models;
use App\Scopes\ActiveScope;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $timestamps = false;
    protected $_alias = 'categories';
    protected $table = 'categories';
    protected $fillable = [
        'code',
        'division_code',
        'name',
        'del_flag',
        'ins_datetime',
        'ins_id',
        'upd_datetime',
        'upd_id'
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ActiveScope());
    }
}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ItemNamesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('item_names')->truncate();
        DB::table('item_names')->insert([
            [
                'item_code'=>'0010',
                'name'=>'Name Item1',
                'del_flag'=>0,
                'ins_id'=>1,
                'ins_datetime'=>Carbon::now()
            ],
            [
                'item_code'=>'0012',
                'name'=>'Name Item2',
                'del_flag'=>0,
                'ins_id'=>1,
                'ins_datetime'=>Carbon::now()
            ],
            [
                'item_code'=>'0011',
                'name'=>'Name Item3',
                'del_flag'=>0,
                'ins_id'=>1,
                'ins_datetime'=>Carbon::now()
            ]
        ]);
    }
}

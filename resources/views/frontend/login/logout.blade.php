@extends('frontend.layouts.main')
@section('content')
    <!--content detail-->
    <div id="logout">
        <div class="container">
            <div class="logout">
                <div class="content-logout">
                    <div class="top-text">
                        <div class="content-text">
                            <p>サンエーネット予約サービスのログアウトが完了しました。<br>
                                ご利用ありがとうございました。</p>
                        </div>
                        <div class="logout-middle">
                            <a href="#"><img src="{{asset("images/yoko.png")}}"></a>
                            <a href="#" class="text-link">予約受付中</a>
                        </div>
                        <div class="re-login">
                            <a href="#"><i class="fas fa-shopping-cart"></i>ログインはこちら</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

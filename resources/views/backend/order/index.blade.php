@extends('backend.layouts.main')
@section('content')

    <div id="Contentwrap">
        @include('common.message_notification')
        <div id="PageContent">
            <h2><i class="fas fa-search"></i> 予約検索</h2>
            <form action="{{route('order.index')}}" method="get" id="order_form_list" autocomplete="off">
                <div class="order">
                    <table class="sp_block">
                        <tr>
                            <th style="width: 150px;" scope="row">伝票番号</th>
                            <td style="width: 210px;">
                                <div class="line">
                                    <input type="text" tabindex="1" placeholder="伝票番号" name="slip_number" value="{{isset($slip_number) ? $slip_number : ''}}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row" style="width: 150px;">店舗</th>
                            <td colspan="4">
                                <div class="custom div-area">
                                    <select class="area-select" name="area_id" tabindex="10"
                                            onchange="Backend.getCity()">
                                        <option value="">エリア</option>
                                        @foreach($areas as $area)
                                            <option
                                                value="{{$area->id}}" {{ null !== $area_id && $area_id == $area->id ? 'selected' : '' }}>{{$area->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <br class="sp">
                                <div class="custom div-city">
                                    <select class="city-select" name="city_id" tabindex="10"
                                            onchange="Backend.getShop()">
                                        <option value="">市町村</option>
                                        @foreach($cities as $city)
                                            <option
                                                value="{{$city->id}}" {{ null !== $city_id && $city_id == $city->id ? 'selected' : '' }}>{{$city->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <br class="sp">
                                <div class="custom div-shop">
                                    <select class="shop-select" name="shop_code" tabindex="10">
                                        <option value="">店舗</option>
                                        @foreach($shops as $shop)
                                            <option
                                                value="{{$shop->code}}" {{ null !== $shop_code && $shop_code == $shop->code ? 'selected' : '' }}>{{$shop->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr class="sp-large">
                            <th style="width: 150px;" scope="row">お名前（カナ）</th>
                            <td style="width: 210px;">
                                <div class="line">
                                    <input type="text" tabindex="1" placeholder="お名前（カナ）" name="customer_name" value="{{isset($customer_name) ? $customer_name : ''}}">
                                </div>
                            </td>
                            <th class="TextR sp-TextL  padding-left-5" style="width: 150px;" scope="row" > 電話番号</th>
                            <td width="300">
                                <div class="line">
                                    <input type="text" tabindex="2" placeholder="電話番号" name="customer_tel" value="{{isset($customer_tel) ? $customer_tel : ''}}">
                                </div>
                            </td>
                            <th class="TextR sp-TextL  padding-left-5" style="width: 130px;" scope="row"> サンエーカード番号</th>
                            <td width="300">
                                <div class="line">
                                    <input type="text" tabindex="2" placeholder="サンエーカード番号" name="customer_card_no" value="{{isset($customer_card_no) ? $customer_card_no : ''}}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row" style="width: 150px;">受付日</th>
                            <td colspan="4">
                                <div class="inline_block rel">
                                    <input type="text" id="receptionist_date_start" placeholder="例：2019/01/02" autocomplete="off"
                                           tabindex="5" name="receptionist_date_start" value="{{isset($receptionist_date_start) ? $receptionist_date_start : ''}}">
                                </div>
                                <br class="sp">&nbsp;〜&nbsp;<br class="sp">
                                <div class="inline_block rel">
                                    <input type="text" id="receptionist_date_end" placeholder="例：2019/01/03" autocomplete="off"
                                           tabindex="6" name="receptionist_date_end" value="{{isset($receptionist_date_end) ? $receptionist_date_end : ''}}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row" style="width: 150px;">受取日</th>
                            <td colspan="4">
                                <div class="inline_block rel">
                                    <input type="text" id="receipt_date_start" placeholder="例：2019/01/02" tabindex="7" autocomplete="off"
                                           name="receipt_date_start" value="{{isset($receipt_date_start) ? $receipt_date_start : ''}}">
                                </div>
                                <br class="sp">&nbsp;〜&nbsp;<br class="sp">
                                <div class="inline_block rel">
                                    <input type="text" id="receipt_date_end" placeholder="例：2019/01/03" tabindex="8" autocomplete="off"
                                           name="receipt_date_end" value="{{isset($receipt_date_end) ? $receipt_date_end : ''}}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">状態</th>
                            <td colspan="3">
                                {{--0：通常、1：WEBキャンセル、2：店頭変更、3：店頭キャンセル--}}
                                <div class="rabutton">
                                    <input type="radio" name="status" id="all" value="" checked="" tabindex="48"
                                        {{ isset($status) && $status == null ? "checked" : "" }}>
                                    <label for="all">全て</label>
                                    <input type="radio" name="status" id="status0" value="0" tabindex="48"
                                        {{ isset($status) && $status == 0 ? "checked" : "" }}>
                                    <label for="status0">通常</label>
                                    <input type="radio" name="status" id="status1" value="1" tabindex="48"
                                        {{ isset($status) && $status == 1 ? "checked" : "" }}>
                                    <label for="status1">WEBキャンセル</label>
                                    <input type="radio" name="status" id="status2" value="2" tabindex="48"
                                        {{ isset($status) && $status == 2 ? "checked" : "" }}>
                                    <label for="status2">店頭変更</label>
                                    <input type="radio" name="status" id="status3" value="3" tabindex="48"
                                        {{ isset($status) && $status == 3 ? "checked" : "" }}>
                                    <label for="status3">店頭キャンセル</label>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="contentfoot">
                    <p class="btn Searchclear">
                        <button type="button" onclick="System.redirectLoading('{{ route('order.index') }}')" value="">
                            検索条件をクリア
                        </button>
                    </p>
                    <p class="btn Search">
                        <button type="submit" onclick="System.showLoading()" value="" tabindex="13"><img
                                src="{{ asset('backend/images/common/icon3.png') }}" alt="検索">検索
                        </button>
                    </p>
                </div>
                <!--ContentFoot -->
            </form>
        </div>
        <!--PageContent -->

        <div id="PageContent" class="pt60">
            <h2><i class="fas fa-list"></i> 予約一覧</h2>

            <div class="PageContentNav">
                @include('backend/elements/paging', ['results' => $orders])
                {{ $orders->appends(request()->input())->links() }}
            </div>
            <table class="OrderForm responsive tdcenter Bordertd TextL table-hover">
                <tr>
                    <th scope="col">@include("backend/elements.sortable", ["field" => "slip_number", "title" => "伝票番号"])</th>
                    <th scope="col">@include("backend/elements.sortable", ["field" => "status", "title" => "状態"])</th>
                    <th scope="col">@include("backend/elements.sortable", ["field" => "receptionist_date", "title" => "受付日<br/>受取日時"])</th>
                    <th scope="col">@include("backend/elements.sortable", ["field" => "shop_code", "title" => "店舗"])</th>
                    <th scope="col">@include("backend/elements.sortable", ["field" => "customer_name", "title" => "お名前（カナ）"])</th>
                    <th scope="col">@include("backend/elements.sortable", ["field" => "customer_tel", "title" => "電話番号"])</th>
                    <th scope="col">詳細</th>
                </tr>
                @if(!$orders->isEmpty())
                    @foreach($orders as $order)
                        <tr>
                            <td>{{$order->slip_number}}</td>
                            <td>{{$status_array[$order->status]}}</td>
                            <td>{{$order->receptionist_date != null ? date("Y/m/d", strtotime($order->receptionist_date)) : ''}}
                                <br/>{{$order->receipt_date != null ? date("Y/m/d", strtotime($order->receipt_date)) : ''}}&nbsp;{{$order->receipt_time != null ? $order->receipt_time.':00' : ''}}</td>
                            <td>{{$order->shop !== null ? $order->shop->name:''}}</td>
                            <td>{{$order->customer_name}}</td>
                            <td>{{$order->customer_tel}}</td>
                            <td><p class="btnmini Edit"><a
                                        href="{{route('order.show', $order->slip_number)}}" class="TextC">詳細</a>
                                </p></td>
                        </tr>
                    @endforeach
                @else
                    @include('backend/elements/no_data',['col' => 7])
                @endif

            </table>
            <div class="PageContentNav">
                {{ $orders->appends(request()->input())->links() }}
            </div>
        </div>
        <!--PageContent -->

    </div>
    <!--Contentwrap -->

@endsection
@section('js')
    <script>
        $(document).ready(function () {
            System.setRangeDate($('#receptionist_date_start'), $('#receptionist_date_end'));
            System.setRangeDate($('#receipt_date_start'), $('#receipt_date_end'));
        });
    </script>
@endsection
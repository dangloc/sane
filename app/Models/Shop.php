<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\ActiveScope;

class Shop extends Model
{
    public $timestamps = false;
    protected $_alias = 'shops';
    protected $table = 'shops';
    protected $fillable = [
        'code',
        'name',
        'name_kana',
        'area_id',
        'zip_code',
        'city_id',
        'address',
        'tel',
        'open_time',
        'close_time',
        'latlng',
        'del_flag',
        'ins_datetime',
        'ins_id',
        'upd_datetime',
        'upd_id'
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ActiveScope());
    }

    public function city()
    {
        return $this->belongsTo('App\Models\Cities', 'city_id');
    }

    public function areas()
    {
        return $this->belongsTo('App\Models\Areas', 'area_id');
    }
    public function customerOder(){
        return $this->hasMany(CustomerOrder::class,'shop_code','code');
    }
}

@extends('backend.layouts.main')
@section('content')
<div id="Contentwrap">
    <div id="PageContent" class="no_mar_bottom">
        <h2><i class="fas fa-users-cog"></i> アカウント{{ array_key_exists('id', Session::get('session_data')) ? '編集' : '登録'}}</h2>
        <div class="order">
            <table class="sp_block flowtable">
                <tr>
                    <th scope="row" style="width: 150px;">氏名</th>
                    <td>
                        {{$account['name']}}
                    </td>
                </tr>
                <tr>
                    <th scope="row">ログインID</th>
                    <td>
                        {{$account['login_id']}}
                    </td>
                </tr>
                <tr>
                    <th scope="row">パスワード</th>
                    <td>
                        *********
                    </td>
                </tr>
                <tr>
                    <th scope="row">ステータス</th>
                    <td>
                        {{$account['status'] ? '有効' : '無効'}}
                    </td>
                </tr>
            </table>
        </div>
        <!--ContentFoot -->
    </div>
    <!--PageContent -->
    <form method="post"
          action="{{ array_key_exists('id', Session::get('session_data')) ? route('account.update', $account['id']) : route('account.store')}}"
          id="form_account_{{array_key_exists('id', Session::get('session_data')) ? 'confirm_edit':'confirm_create'}}">
        {{ csrf_field() }}
        @if(array_key_exists('id', Session::get('session_data')))
            @method('PUT')
        @endif
        <div class="contentfoot">
            <p class="btn Searchclear">
                <button type="button"
                        onclick="window.location='{{ array_key_exists('id', Session::get('session_data')) ? route("account.edit", $account['id']):route("account.create") }}'"><i
                        class="fas fa-undo"></i> 戻る
                </button>
            </p>
            <p class="btn register">
                <button type="button" onclick="System.showDialog('account', '{{ array_key_exists('id', Session::get('session_data')) ? 'confirm_edit' : 'confirm_create'}}')"><i
                        class="fas fa-check"></i> {{ array_key_exists('id', Session::get('session_data')) ? '編集' : '登録'}}する
                </button>
            </p>
        </div>
        <!--PageContent -->
    </form>
</div>
<!--Contentwrap -->
@endsection
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_prices', function (Blueprint $table) {
            $table->char('item_code', 13)->comment('商品コード');
            $table->integer('price')->comment('価格');
            $table->char('del_flag', 1)->comment('削除フラグ')->default(0);
            $table->dateTime('ins_datetime')->default(DB::raw('CURRENT_TIMESTAMP'))->comment('登録日時');
            $table->integer('ins_id')->length(11)->comment('登録者ID');
            $table->dateTime('upd_datetime')->comment('更新日時')->nullable();
            $table->integer('upd_id')->length(11)->comment('更新者ID')->nullable();

            $table->primary('item_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_prices');
    }
}

var System = {};
var status_changed = false;

System.convertKana = function (str) {
    var regex = /[Ａ-Ｚａ-ｚ０-９！＂＃＄％＆＇（）＊＋，－．／：；＜＝＞？＠［＼］＾＿｀｛｜｝]/g;
    str = str.replace(regex, function (ch) {
        return String.fromCharCode(ch.charCodeAt(0) - 0xFEE0);
    });
    return str;
};

System.convertNumberTo1Byte = function (str, acceptNegative) {
    var regex = /[０-９]/g;
    var isNegative = false;
    if (str != "") {
        if (str.charAt(0) == '-' || str.charAt(0) == '－') {
            isNegative = true;
        }
        str = str.replace(/[^0-9０-９\.]/g, '');
        str = str.replace(regex, function (ch) {
            return String.fromCharCode(ch.charCodeAt(0) - 0xFEE0);
        });
    }
    // #1580
    if (true == acceptNegative && true === isNegative) {
        str = '-' + str;
    }
    return str;
};
System.changeCity = function(url,e){
    var id_area = $(e).val();
   $.ajax({
       url: url,
       type: 'get',
       data: {
           id : id_area
       },
       success: function(data){
            $('.div-city').html(data.html);
       }
   });
};


System.showDialog = function (model, name, id) {
    $('#dialog_' + name).dialog({
        autoOpen: false,
        modal: true,
        buttons: {
            "OK": function () {
                System.showLoading();
                $(this).dialog('close');
                if (typeof id == "undefined") {
                    id = '';
                }
                $('#form_' + model + '_' + name + id).submit();
            }
        }
    }).dialog("open");
};

System.showDialogDetail = function (name) {
    $('#dialog_' + name).dialog({
        dialogClass: "sp-dialog",
        autoOpen: false,
        modal: true,
        width: 'auto',
        buttons: {
            "閉じる": function () {
                $(this).dialog('close');
            }
        }
    }).dialog("open");
};

System.showDialog2 = function () {
    $("#dialog_change_user_status").dialog({
        autoOpen: false,
        modal: true,
        width: 700,
        buttons: {
            "編集する": function () {
                $(this).dialog('close');
                $('#user_form_change_status').submit();
            }
        }
    }).dialog("open");
};


System.showDialog3 = function (id, url) {
    // open dialog
    $("#dialog_change_invoice_status").dialog({
        autoOpen: false,
        modal: true,
        width: 700,
        buttons: {
            "変更する": function (e) {
                e.preventDefault();
                // call ajax
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('input[name="_token"]').val()
                    },
                    url: url,
                    type: 'POST',
                    crossDomain: true,
                    dataType: 'json',
                    data: {
                        id: id,
                        status: $("input[name='status_change']:checked").val(),
                    },
                    success: function (result) {
                        if (result.status === 200) {
                            $('#status_' + id).html(result.data);
                            status_changed = id;
                        } else {
                            alert(result.error);
                        }
                    },
                    error: function (xhr, status, error) {
                        alert("system error");
                    }
                });
                $('#dialog_change_invoice_status').dialog('close');
            }
        }
    }).dialog("open");
};

System.showDialog4 = function (id, url, old_value) {
    if (status_changed !== id) {
        System.setOldStatus(old_value);
    }
    System.showDialog3(id, url);
};

System.addImage = function (btn) {
    var input = $(btn).closest('.item').find('input[type=file]');
    input.trigger('click');
};

System.loadImage = function (event, e) {
    var fileExtension = ['jpeg', 'jpg', 'png', 'bmp'],
        output = $(e).parents('.item').find('img').first()[0];

    if ($(e).val() == '') {
        $(e).val('');
        output.src = '';
        return;
    }

    if ($.inArray($(e).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
        alert("jpg,jpeg,bmp,pngのみがアップロード出来ます。");
        $(e).val('');
    } else {
        if(e.files[0].size >= 2000000){
            alert('商品画像 2MB以下のファイルを指定してください。');
            $(e).val('');
        }
        if ('undefined' != typeof event.target.files[0]) {
            $('#test-exist').css('display','block');
            output.src = URL.createObjectURL(event.target.files[0]);
            $(e).parents('.item').find('.remove-img').removeClass('hidden');
            $(e).parents('.item').find('img').first().removeClass('hidden');
        } else {
            $(e).parents('.item').find('img').first().removeAttr('src').addClass('hidden');
            $(e).parents('.item').find('.remove-img').addClass('hidden');
        }
    }
};

System.loadFile = function (event, e) {
    var fileExtension = ['pub'],
        output = $(e).parents('.item').find('p').first()[0];

    if ($(e).val() == '') {
        $(e).val('');
        output.textContent = '';
        return;
    }

    if ($.inArray($(e).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
        alert("pubのみがアップロード出来ます。");
        $(e).val('');
    } else {

        if ('undefined' != typeof event.target.files[0]) {
            console.log(output);
            // output.text(URL.createObjectURL(event.target.files[0]));
            output.textContent = event.target.files[0].name;
            $(e).parents('.item').find('.remove-img').removeClass('hidden');
        } else {
            $(e).parents('.item').find('img').first().val('');
            $(e).parents('.item').find('.remove-img').addClass('hidden');
        }
    }
};

System.redirectLoading = function (url) {
    System.showLoading();
    window.location.href = url;
};
System.redirectNoLoading = function (url) {
    window.location.href = url;
};

System.showLoading = function () {
    $('#loading').show();
};

System.hideLoading = function () {
    $('#loading').hide();
};

System.convertAlphanumericTo1Byte = function (str) {
    var regex = /[Ａ-Ｚａ-ｚ０-９！＂＃＄％＆＇（）＊＋，－．／：；＜＝＞？＠［＼］＾＿｀｛｜｝]/g;
    str = str.replace(regex, function (ch) {
        return String.fromCharCode(ch.charCodeAt(0) - 0xFEE0);
    });
    return str;
};

System.setRangeMonth = function (e) {
    var from_month = $('#from_month');
    var to_month = $('#to_month');
    var fromDate = from_month.val();
    var toDate = to_month.val();
    if (fromDate !== '') {
        var dateMin = from_month.datepicker("getDate");
        var rMin = new Date(dateMin.getFullYear(), dateMin.getMonth(), dateMin.getDate());
        to_month.datepicker("option", "minDate", rMin);
    }

    if (toDate !== '') {
        var dateMax = to_month.datepicker("getDate");
        var rMax = new Date(dateMax.getFullYear(), dateMax.getMonth(), dateMax.getDate());
        from_month.datepicker("option", "maxDate", rMax);
    }
};
System.setRangeDate = function (startDate, endDate , format) {
    if (typeof undefined == typeof format) {
        format = 'yy/mm/dd';
    }

    $(startDate).datepicker({
        showOn: "both",
        buttonImageOnly: true,
        closeText: '',
        buttonImage: baseUrl + '/common/images/calendar.png',
        dateFormat: format,
        changeYear: true,
        changeMonth: true,
        onSelect: function () {
            var dateMin = $(startDate).datepicker("getDate");
            var rMin = new Date(dateMin.getFullYear(), dateMin.getMonth(), dateMin.getDate());
            $(endDate).datepicker("option", "minDate", rMin);
        }
    });
    $(endDate).datepicker({
        showOn: "both",
        buttonImageOnly: true,
        buttonImage: baseUrl + '/common/images/calendar.png',
        dateFormat: format,
        changeYear: true,
        changeMonth: true,
        onSelect: function () {
            var dateMax = $(endDate).datepicker("getDate");
            var rMax = new Date(dateMax.getFullYear(), dateMax.getMonth(), dateMax.getDate());
            $(startDate).datepicker("option", "maxDate", rMax);
        }
    });
};

System.setDateMonthYear = function(pewpew) {
    $(pewpew).datepicker(
        {
            dateFormat: "yy/mm",
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            onClose: function(dateText, inst) {


                function isDonePressed(){
                    return ($('#ui-datepicker-div').html().indexOf('ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all ui-state-hover') > -1);
                }

                if (isDonePressed()){
                    var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                    var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                    $(this).datepicker('setDate', new Date(year, month, 1)).trigger('change');

                    $('.date-picker').focusout()//Added to remove focus from datepicker input box on selecting date
                }
            },
            beforeShow : function(input, inst) {

                inst.dpDiv.addClass('month_year_datepicker')

                if ((datestr = $(this).val()).length > 0) {
                    year = datestr.substring(0,4);
                    month = datestr.substring(datestr.length-2, datestr.length);
                    $(this).datepicker('option', 'defaultDate', new Date(year, month-1, 1));
                    $(this).datepicker('setDate', new Date(year, month-1, 1));
                    $(".ui-datepicker-calendar").hide();
                }
            }
        })
};

System.checkTimeSearch = function (start_date, end_date, start_time, end_time, msg_error) {
    var start_date = $('#' + start_date).val();
    var end_date = $('#' + end_date).val();
    var start_time = $('#' + start_time).val();
    var end_time = $('#' + end_time).val();
    if (start_date == end_date && start_time > end_time) {
        $('#' + msg_error).show();
        return false;
    } else {
        $('#' + msg_error).hide();
        return true;
    }
};

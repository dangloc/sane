<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Factory as ValidationFactory;
use Illuminate\Validation\Factory;
use App\Rules\CheckPassword;

class ChangePassEntryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'nullable|sometimes|check_email|unique:customers',
            'confirm_email' => 'required_with:email|same:email',
            'new_password' => ['sometimes','nullable', new CheckPassword],
            'confirm_password' => 'required_with:new_password|same:new_password',
        ];
    }

    public function attributes()
    {
        return [
            'email' => '新しいメールアドレス',
            'confirm_email' => '新しいメールアドレス',
            'new_password' => '新しいパスワード',
            'confirm_password' => '新しいパスワード（再入力）',
        ];
    }
     public function messages()
     {
         return [
             'confirm_email.required_with' => '新しいメールアドレス（再入力）は必須です。',
             'confirm_email.same' => '新しいメールアドレス(再入力)と新しいメールアドレスは同じ値を指定してください。',
             'confirm_password.required_with'=>'新しいパスワード（再入力）は必須です。'
         ];
     }
}

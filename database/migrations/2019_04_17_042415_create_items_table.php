<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->char('shop_code',4)->comment('店舗コード');
            $table->char('code', 13)->comment('商品コード');
            $table->string('item_image_name', 512)->comment('画像名');
            $table->string('item_image_folder_name', 512)->comment('画像管理フォルダ名');
            $table->string('content1', 11)->comment('項目1');
            $table->string('content2', 300)->comment('項目2');
            $table->string('content3', 23)->comment('項目3');
            $table->char('required_confirm_flag', 1)->comment('必須確認事項フラグ');
            $table->char('limited_flag', 1)->comment('限定数フラグ');
            $table->smallInteger('limited_count')->comment('限定数')->nullable();
            $table->smallInteger('additive_code')->comment('添加物コード');
            $table->smallInteger('lead_time')->comment('リードタイム');
            $table->text('reserve_content1')->comment('予備項目1')->nullable();
            $table->text('reserve_content2')->comment('予備項目2')->nullable();
            $table->text('reserve_content3')->comment('予備項目3')->nullable();
            $table->char('del_flag', 1)->comment('削除フラグ')->default(0);
            $table->dateTime('ins_datetime')->default(DB::raw('CURRENT_TIMESTAMP'))->comment('登録日時');
            $table->integer('ins_id')->length(11)->comment('登録者ID');
            $table->dateTime('upd_datetime')->comment('更新日時')->nullable();
            $table->integer('upd_id')->length(11)->comment('更新者ID')->nullable();

            $table->primary(array('shop_code', 'code'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}

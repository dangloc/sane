@extends('backend.layouts.main')
@section('content')
    <div id="Contentwrap">
        <form method="post" action="{{ array_key_exists('id', Session::get('session_data')) ? route('account.edit.confirm', $account['id']) : route('account.create.confirm')}}" >
            {{ csrf_field() }}
            <div class="contenttop detail-btn-group">
                <p class="btn Searchclear">
                    <button type="button" onclick="System.redirectNoLoading('{{ route('account.index') }}')"><i
                            class="fas fa-list"></i> 一覧ページに戻る
                    </button>
                </p>
            </div>
            <div id="PageContent" class="no_mar_bottom">
                <h2><i class="fas fa-users-cog"></i> アカウント{{ array_key_exists('id', Session::get('session_data')) ? '編集' : '登録'}}</h2>
                @if(array_key_exists('id', Session::get('session_data')))
                    <input style="display: none" name="id" value="{{$account['id']}}">
                @endif
                <div class="order">
                    <table class="sp_block flowtable">
                        <tr>
                            <th scope="row" style="width: 150px;">氏名<span class="req"></span></th>
                            <td>
                                <div class="line required medium-input">
                                    <input type="text" tabindex="7"  name="name" placeholder="例）管理 太郎" value="{{ old('name', isset($account) ? $account['name'] : '')}}">
                                </div>
                                @include('backend.elements.validate_error', ['message' => $errors->first('name')])
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">ログインID<span class="req"></span></th>
                            <td>
                                <div class="line required medium-input">
                                    <input type="text" tabindex="8" name="login_id" placeholder="例）kanri" value="{{ old('login_id', isset($account) ? $account['login_id'] : '')}}">
                                </div>
                                @include('backend.elements.validate_error', ['message' => $errors->first('login_id')])
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">パスワード<span class="req"></span></th>
                            <td>
                                <div class="line required medium-input">
                                    <input type="password" tabindex="9" name="password" placeholder="8桁以上の半角英数字">
                                </div>
                                @include('backend.elements.validate_error', ['message' => $errors->first('password')])
                            </td>
                        </tr>
                        <tr>
                            <th width="158" style="width: 150px;" scope="row">ステータス<span class="req"></span></th>
                            <td>
                                <div class="rabutton labelfull">
                                    <input type="radio" name="status" id="cate11" value="1" tabindex="1" checked
                                        {{(isset($account['status']) && $account['status'] == 1) || (null !== old('status') && old('status') == 1)? "checked" : "" }}>
                                    <label for="cate11">有効</label>
                                    <input type="radio" name="status" id="cate12" value="0" tabindex="2"
                                        {{(isset($account['status']) && $account['status'] == 0) || (null !== old('status') && old('status') == 0)? "checked" : "" }}>
                                    <label for="cate12">無効</label>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <!--ContentFoot -->
            </div>
            <!--PageContent -->

            <div class="contentfoot">
                <p class="btn clear">
                    <button type="button" onclick="System.redirectNoLoading('{{ route('account.index') }}')"><i
                            class="fas fa-list"></i> 一覧ページに戻る
                    </button>
                </p>
                <p class="btn register">
                    <button type="submit" onclick="System.showLoading()"><i
                            class="fas fa-check"></i> 確認する
                    </button>
                </p>
            </div>
            <!--PageContent -->
        </form>
    </div>
    <!--Contentwrap -->
@endsection
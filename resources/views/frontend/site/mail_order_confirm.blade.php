<?php $dateBooking = !empty($order['receptionist_date']) ? getDateTime($order['receptionist_date']) : getDateTime(date('Ymd'))?>
<?php $dateReceive = !empty($order['receipt_date']) ? getDateTime($order['receipt_date']) : getDateTime(date('Ymd'))?>
<p>このたびは、「サンエーネット予約サービス」をご利用いただき</p>
<p>誠にありがとうございます。</p>
<p>以下の通り、ご予約のキャンセルを承りました。</p><br>

<p>━━━━━━━━━━━━━━━━━━</p>
<p>ご予約内容</p>
<p>━━━━━━━━━━━━━━━━━━</p>
<p>伝票番号：{{$order['slip_number']}}</p>
<p>受付日：{{$dateBooking['year']}}年{{$dateBooking['month']}}月{{$dateBooking['day']}}日</p>
<p>受取り日時：{{$dateReceive['year']}}年{{$dateReceive['month']}}月{{$dateReceive['day']}}日 {{$order['time']}}時</p>
<p>受取り店舗：{{$order['cityName'].$order['shopName']}}</p>
<p>お名前：{{$order['customer_name']}}様</p>
<p>電話番号：{{$order['customer_tel']}}</p>
<p>サンエーカード番号：{{formatCardNo($order['customer_card_no'])}}</p><br>

<p>━━━━━━━━━━━━━━━━━━</p>
<p>ご予約商品</p>
<?php $totalPrice = 0;?>
@if(!empty($order['items']))
    @foreach($order['items'] as $item)
        <?php $totalPrice += !empty($item['price']) ? $item['item_count'] * $item['price'] : 0;?>
        <p>商品名：{{$item['name']}}</p>
        <p>価格：{{number_format($item['price'])}}円（税抜）</p>
        <p>数量：{{$item['item_count']}}</p>
    @endforeach
@endif
<p>------------------</p>
<p>合計（税抜）：{{number_format($totalPrice)}}円</p><br>

<p>このメールは自動送信です。</p>
<p>返信いただいても回答できませんので、ご了承ください。</p>
<p>なお、ご不明な点がございましたら、</p>
<p>上記受取り店舗に直接お電話にてお問い合わせください。</p>
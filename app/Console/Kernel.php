<?php

namespace App\Console;

use App\Jobs\importTsv;
use App\Models\Category;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Http\Common\logWriter;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\export_tsv::class,
        Commands\import_tsv::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
//         $schedule->command('inspire')
//                  ->everyMinute();
//        $schedule->call(new testCron())->everyMinute();
//        division_code',
//        'name',
//        'del_flag',
//        'ins_datetime',
//        'ins_id',
//        'upd_datetime',
//        'upd_id'
//        $schedule->job(new importTsv())->everyFiveMinutes();

//        $schedule->job(new importTsv())->dailyAt('18:30');


    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}

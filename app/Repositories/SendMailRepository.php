<?php

namespace App\Repositories;

use App\Mail\ConfirmOrderMail;
use App\Jobs\SendEmailBookingCompleteJob;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;

/**
 * class SendMailRepository.
 *
 * @package namespace App\Repositories;
 */
class SendMailRepository extends BaseRepository
{
    public $itemRepository;

    function __construct(ItemRepository $itemRepository)
    {
        $this->itemRepository = $itemRepository;
    }

    public function sendMail(Request $request, $orders, $type = 'complete')
    {
        if (!empty($orders['slip_number'])) {
            $orders['items'] = $this->itemRepository->getItemDataOrder($orders['slip_number']);
            $orders['time'] = ($request->session()->has(config('const.session.time'))) ? $request->session()->get(config('const.session.time')) : "";
            $orders['images']=getBarCode($orders['slip_number']);
        }
        if (Auth::guard(config('const.guard.frontend'))->check()) {
            $email = Auth::guard(config('const.guard.frontend'))->user()->email;
        } else {
            if ($request->session()->has(config('const.session.customer_booking'))) {
                $session = $request->session()->get(config('const.session.customer_booking'));
                if (!empty($session['email'])) {
                    $email = $session['email'];
                }
            }
        }
        if (!empty($email)) {
            try {
                if ($type == 'complete') {
                    dispatch(new SendEmailBookingCompleteJob($email, $orders));
                }else{
                    Mail::to($email)->send(new ConfirmOrderMail($orders));
                }
                return true;
            } catch (\Exception $e) {
                return false;
            }
        }
    }
}
<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdministratorSeeder::class);
        $this->call(AreasTableSeeder::class);
        $this->call(BannerTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(CitiesTableSeeder::class);
        $this->call(CustomerOrderTableSeeder::class);
        $this->call(CustomerTableSeeder::class);
        $this->call(EventItemViewsTableSeeder::class);
        $this->call(EventsTableSeeder::class);
        $this->call(InfomationsTableSeeder::class);
        $this->call(ItemBonusPointTableSeeder::class);
        $this->call(ItemNamesTableSeeder::class);
        $this->call(ItemPricesTableSeeder::class);
        $this->call(ItemsTableSeeder::class);
        $this->call(ItemStatusTableSeeder::class);
        $this->call(ItemTableSeeder::class);
        $this->call(ReserveTableSeeder::class);
        $this->call(ShopTableSeeder::class);
    }
}

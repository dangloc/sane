<?php

namespace App\Http\Controllers\Frontend;

use App\Repositories\InformationsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InformationController extends Controller
{
    private $information;
    public function __construct(InformationsRepository $information)
    {
        $this->information = $information;
    }

    public function index(){
        $data_information=$this->information->getInfoPagination(10);
        return view('frontend.static.information',compact('data_information'));
    }
    public function detail($id){
        $data=$this->information->getInforDetail($id);
        if($data==null){
            abort(404);
        }
        return view('frontend.information.index',compact('data'));
    }
}

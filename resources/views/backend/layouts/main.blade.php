<!DOCTYPE html>
<html lang="ja">
<head>
    @include('backend.elements.meta')
    @yield('css')
</head>
<body class="{{isset($controller) && isset($action) ? $controller.'-'.$action : ''}}">
@if(Auth::guard(config('const.guard.backend'))->check())
    <div id="header">
        @include('backend.elements.header')
    </div>
@endif
@yield('content')
@include('backend.elements.dialog')
@include('backend.elements.footer')
@yield('js')
</body>
</html>

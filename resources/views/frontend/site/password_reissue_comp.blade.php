@extends('frontend.layouts.main')
@section('content')
    <div id="logout" class="password-wrap password_reissue">
        <div class="container">
            <div class="password logout">
                <div class="content-logout">
                    <div class="top-text">
                        <div class="content-text">
                            <p onclick="location.href='{{route('frontend.login')}}'">パスワードの再設定が完了しました。</p>
                        </div>
                        <div class="logout-middle">
                            <a href="#"><img src="{{asset('frontend/images/yoko.png')}}"></a>
                            <a href="#" class="text-link">予約受付中</a>
                        </div>
                        <div class="re-login">
                            <a href="{{route('frontend.login')}}"><i class="fas fa-shopping-cart"></i>ログインはこちら</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
@endsection

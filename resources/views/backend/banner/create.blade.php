@extends('backend.layouts.main')
@section('content')
    <form method="post" action="{{route('banner.create_confirm')}}" enctype="multipart/form-data" name="form_add"
          id="option_form_create">
        {{ csrf_field() }}
        <div id="Contentwrap">

            <div class="contenttop detail-btn-group">
                <p class="btn Searchclear">
                    <button type="button" onclick="System.redirectNoLoading('{{ route('banner.index') }}')"><i
                            class="fas fa-list"></i> 一覧ページに戻る
                    </button>
                </p>
            </div>

            <div id="PageContent" class="no_mar_bottom">
                <h2><i class="fas fa-image"></i> バナー登録</h2>
                <div class="order">
                    <table class="sp_block flowtable">
                        <tr class="item">
                            <th scope="row" style="width: 150px;">バナー画像<span class="req"></span></th>
                            <td>
                                <img src="{{$flag == config('const.flag')['from_confirm'] && Session::has('banner_url_temp') ? asset(Session::get('banner_url_temp')) : ''}}" width="260px" class="{{ $flag == config('const.flag')['from_confirm'] && Session::has('banner_url_temp') ? '' : 'hidden' }} img-preload">
                                <input type="file" name="banner_url" class="hidden" accept="image/*"
                                       onchange="System.loadImage(event, this)">
                                <p class="btn History btn-image-banner">
                                    <button id="btn_add_more_img" type="button" onclick="System.addImage(this)">画像追加</button>
                                    <span class="red-text img-banner-error"><span class="req"></span>縦345×幅700の画像をアップロードしてください。</span>
                                </p>
                                <div class="clear">
                                    <div class="overflow">
                                        @include('backend.elements.validate_error', ['message' => $errors->first('banner_url')])
                                    </div>
                                </div>
                            </td>

                        </tr>
                        <tr>
                            <th scope="row" style="width: 150px;">リンク先URL</th>
                            <td>
                                <div class="line required full_input">
                                    <input type="text" tabindex="7" name="link_url" placeholder="リンク先URL" value="{{old('link_url') ? old('link_url') : ''}}" >
                                </div>
                                @include('backend.elements.validate_error', ['message' => $errors->first('link_url')])
                            </td>
                        </tr>
                        <tr>
                            <th scope="row" style="width: 150px;">公開期間<span class="req"></span></th>
                            <td colspan="4">
                                <div class="inline_block rel">
                                    <input type="text" readonly id="public_start_date" placeholder="例：2019/01/02" autocomplete="off"
                                           tabindex="5" name="public_start_date" value="{{old('public_start_date') ? old('public_start_date') : ''}}">
                                </div>
                                <br class="sp">&nbsp;〜&nbsp;<br class="sp">
                                <div class="inline_block rel">
                                    <input type="text" readonly id="public_end_date" placeholder="例：2019/01/03" autocomplete="off"
                                           tabindex="6" name="public_end_date" value="{{old('public_end_date') ? old('public_end_date') : ''}}">
                                </div>
                                <br>
                                @include('backend.elements.validate_error', ['message' => $errors->first('public_start_date')])
                                @include('backend.elements.validate_error', ['message' => $errors->first('public_end_date')])
                            </td>
                        </tr>
                        <tr>
                            <th width="158" style="width: 150px;" scope="row">ステータス<span class="req"></span></th>
                            <td>
                                <div class="rabutton labelfull">
                                    <input type="radio" name="status" id="cate11" value="1" tabindex="1" {{old('status') !== null ? (old('status') == '1' ? 'checked' : '') : 'checked'}}>
                                    <label for="cate11">有効</label>
                                    <input type="radio" name="status" id="cate12" value="0" tabindex="2" {{old('status') !== null ? (old('status') == '0' ? 'checked' : '') : '' }}>
                                    <label for="cate12">無効</label>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <!--ContentFoot -->
            </div>
            <!--PageContent -->

            <div class="contentfoot">
                <p class="btn clear">
                    <button type="button" onclick="System.redirectNoLoading('{{ route('banner.index',['flag' =>1])}}')"><i
                            class="fas fa-list"></i> 一覧ページに戻る
                    </button>
                </p>
                <p class="btn register">
                    <button type="submit" onclick="System.showLoading()"><i class="fas fa-check" ></i> 確認する</button>
                </p>
            </div>
            <!--PageContent -->

        </div>
    </form>
    <!--Contentwrap -->

@endsection
@section('js')
    <script>
        $(document).ready(function () {
            System.setRangeDate($('#public_start_date'), $('#public_end_date'));
        });
    </script>
@endsection
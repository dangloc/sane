<a id="pagetop"></a>
<div id="header">
    <div id="headerIn">
        <p id="logo"><img src="{{ asset('backend/images/common/logo-pc.png') }}" alt="logo" class="switch"></p>
        <p class="StoreName"><a href="#"> {{Auth::guard(config('const.guard.backend'))->user() != null ? Auth::guard(config('const.guard.backend'))->user()->name : ''}} </a></p>
        <p class="LogOut"><a href="{{ route('logout') }}">ログアウト</a></p>
        <div id="headerNav">
            <ul>
                <li class="nav01 menu__multi"><a href="#" class="{{ isset($controller) && isset($action) && activeMenu($controller, $action) == config('view.active_menu.order') ? 'active' : ''}}">予約管理</a>
                    <ul class="menu__second-level">
                        <li><a href="{{ route('order.index') }}">予約一覧</a></li>
                    </ul>
                </li>
                <li class="nav05 menu__multi"><a href="#" class="{{isset($controller) && isset($action) && activeMenu($controller, $action) == config('view.active_menu.import') ? 'active' : ''}}">取込データ確認</a>
                    <ul class="menu__second-level">
                        <li><a href="{{ route('event.index') }}">行事コード</a></li>
                        <li><a href="{{ route('category.index') }}">カテゴリーコード</a></li>
                        <li><a href="{{ route('event.category') }}">行事カテゴリー</a></li>
                        <li><a href="{{ route('event.view') }}">行事表示</a></li>
                        <li><a href="{{ route('event.item') }}">行事商品表示</a></li>
                        <li><a href="{{ route('item.index') }}">商品</a></li>
                        <li><a href="{{ route('item.name') }}">商品名称</a></li>
                        <li><a href="{{ route('item.price') }}">商品価格</a></li>
                        <li><a href="{{ route('item.point') }}">ボーナスポイント</a></li>
                        <li><a href="{{ route('item.shop_count') }}">店別計画</a></li>
                        <li><a href="{{ route('item.limited') }}">商品別限定</a></li>
                    </ul>
                </li>
                <li class="nav03 menu__multi"><a href="#" class="{{isset($controller) && isset($action) && activeMenu($controller, $action) == config('view.active_menu.product') ? 'active' : ''}}">商品管理</a>
                    <ul class="menu__second-level">
                        <li><a href="{{ route('item.list') }}">商品一覧</a></li>
                    </ul>
                </li>
                <li class="nav04 menu__multi"><a href="#" class="{{isset($controller) && isset($action) && activeMenu($controller, $action) == config('view.active_menu.shop') ? 'active' : ''}}">店舗管理</a>
                    <ul class="menu__second-level">
                        <li><a href="{{ route('shop.index') }}">店舗一覧</a></li>
                        <li><a href="{{ route('shop.create') }}">店舗登録</a></li>
                    </ul>
                </li>
                <li class="nav02 menu__multi"><a href="#" class="{{isset($controller) && isset($action) && activeMenu($controller, $action) == config('view.active_menu.customer') ? 'active' : ''}}">お客様管理</a>
                    <ul class="menu__second-level">
                        <li><a href="{{ route('user.index') }}">お客様一覧</a></li>
                    </ul>
                </li>
                <li class="nav07 menu__multi"><a href="#" class="{{isset($controller) && isset($action) && activeMenu($controller, $action) == config('view.active_menu.notification') ? 'active' : ''}}">お知らせ管理</a>
                    <ul class="menu__second-level">
                        <li><a href="{{ route('information.index') }}">お知らせ一覧</a></li>
                        <li><a href="{{ route('information.create') }}">お知らせ登録</a></li>
                    </ul>
                </li>
                <li class="nav06 menu__multi"><a href="#" class="{{isset($controller) && isset($action) && activeMenu($controller, $action) == config('view.active_menu.setting') ? 'active' : ''}}">各種設定</a>
                    <ul class="menu__second-level">
                        <li><a href="{{ route('banner.index') }}">バナー一覧</a></li>
                        <li><a href="{{ route('banner.create') }}">バナー登録</a></li>
                        <li><a href="{{ route('account.index') }}">アカウント一覧</a></li>
                        <li><a href="{{ route('account.create') }}">アカウント登録</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <!--headerNav -->
    </div>
    <!--headerIn -->
</div>
<!--header -->
<?php

namespace App\Models;

use App\Scopes\ActiveScope;
use Illuminate\Database\Eloquent\Model;

class EventCategory extends Model
{
    public $timestamps=false;
    protected $_alias='event_categories';
    protected $table='event_categories';
    protected $fillable=[
        'event_code',
        'category_code',
        'sort',
        'del_flag',
        'ins_datetime',
        'ins_id',
        'upd_datetime',
        'upd_id'
    ];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ActiveScope());
    }
}

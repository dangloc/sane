<?php

namespace App\Repositories;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * class ItemNameRepository.
 *
 * @package namespace App\Repositories;
 */
class ItemNameRepository extends BaseRepository
{

    function __construct()
    {
        $this->_model = \App\Models\ItemName::class;
    }

    public function paging()
    {
        $sort = request()->get('sort', 'item_code');
        $direction = request()->get('direction', 'desc');
        $params = request()->all();

        return $this->getModel()
            ->when(isset($params['item_code']) && $params['item_code'] != '', function ($q) use ($params) {
                return $q->where('item_code', 'like', '%' . $params['item_code'] . '%');
            })
            ->when(isset($params['name']) && $params['name'] != '', function ($q) use ($params) {
                return $q->where('name', 'like', '%' . $params['name'] . '%');
            })
            ->orderBy($sort, $direction)
            ->paginate(config('const.paging.backend'));
    }
}

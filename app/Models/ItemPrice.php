<?php

namespace App\Models;
use App\Scopes\ActiveScope;

use Illuminate\Database\Eloquent\Model;

class ItemPrice extends Model
{
    public $timestamps = false;
    protected $_alias = 'item_prices';
    protected $table = 'item_prices';
    protected $fillable = [
        'item_code',
        'price',
        'del_flag',
        'ins_datetime',
        'ins_id',
        'upd_datetime',
        'upd_id'
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ActiveScope());
    }
    public function reserves(){
        return $this->hasMany(Reserve::class,'item_code','item_code');
    }
}

@extends('frontend.layouts.main')
@section('content')
    <div id="mypage_cancel_order_complete" class="password-wrap">
        <div class="container">
            <div class="mypage_cancel_order_complete">
                <h1 class="title">マイページ</h1>
                <div class="button_group">
                    <button class="button_back" onclick="location.href='{{route('mypage.index')}}'">マイページTOPへ戻る</button>
                    <button class="button_change" onclick="location.href='{{route('mypage.change_password_entry')}}'">メールアドレス・パスワードの変更</button>
                    @if(!Auth::guard(config('const.guard.frontend'))->check())
                        <button class="button_login" onclick="location.href='{{route('frontend.logout')}}'"><i
                                    class="fa fa-sign-out-alt"></i>ログアウト
                        </button>
                    @endif
                </div>
                <div class="cancel_order">ご注文のキャンセル（確認）</div>
                <button class="button_print" onclick="Frontend.windowPrint()">この画面を印刷する</button>
                <div class="content">
                    <p>キャンセルが完了いたしました。<br>またのご利用をお待ちしております。</p>
                </div>
                <div class="description">
                    <p>ご変更頂いた内容は、後ほどサンエーネット予約サービスよりお送りする <br>「キャンセル確認メール」、または[マイページ]画面にてご確認いただけます。</p>
                </div>
                @if(!empty($bookingData))
                    <?php $sumPrice = $totalQuantity = 0;?>
                    @foreach($bookingData['item'] as $item)
                        <?php $sumPrice += !empty($item['total_price']) ?  $item['total_price'] : 0;?>
                        <?php $totalQuantity += !empty($item['item_count']) ?  $item['item_count'] : 0;?>
                        <div class="cart">
                            <div class="image_product">
                                <img src="{{ ($item['item_image_folder_name'] ==null || $item['item_image_folder_name']=='_') ? asset('/frontend/images/item/noimage.jpg')
                                                    : config('const.url_item_image').$item['item_image_folder_name']}}"
                                     alt="">
                            </div>
                            <div class="info_product">
                                <div class="product_name">{{$item['name']}}
                                    @if($item['required_confirm_flag']!=config('const.not_with'))
                                        <label class="required-confirm" style="<?= ($item['required_flag'] == '0') ? "display:inline-block" : "display:none"?>">

                                            @if($item['required_confirm_flag']== config('const.with_wasabi'))
                                                わさび入り @elseif($item['required_confirm_flag']==config('const.with_kamaboko'))
                                                かまぼこ赤 @elseif($item['required_confirm_flag']==config('const.with_classic')) 定番
                                            @endif
                                        </label>
                                        <label class="required-confirm" style="<?= ($item['required_flag'] == '1') ? "display:inline-block" : "display:none"?>" >
                                            @if($item['required_confirm_flag']==config('const.with_wasabi'))
                                                わさび抜き@elseif($item['required_confirm_flag']==config('const.with_kamaboko'))
                                                かまぼこ白 @elseif($item['required_confirm_flag']==config('const.with_classic')) 法事
                                            @endif
                                        </label>
                                    @endif
                                </div>
                                <div class="detail_product">
                                    <div class="price">
                                        <label>価格</label>
                                        <span>{{number_format($item['price'])}}<span>円+税</span></span>
                                    </div>
                                    <div class="count">
                                        <label>数量</label>
                                        <span>{{number_format($item['item_count'])}}</span>
                                    </div>
                                    <div class="total">
                                        <label>小計</label>
                                        <span>{{number_format($item['total_price'])}}<span>円+税</span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
                <hr>
                <div class="bill">
                            <span class="count_product">
                                <label>商品数</label>
                                <span>{{number_format($totalQuantity)}}品</span>
                            </span>
                    <span class="total_price">
                                <label>合計金額</label>
                                <span>{{number_format($sumPrice)}}<span>円+税</span></span>
                            </span>
                </div>
                <div class="title_time_order">商品の受取り日と受取り店舗</div>
                <div class="date_time_order">
                    <table>
                        <tr class="row-1">
                            <td class="label_td">受取り日</td>
                            <td class="time_order"><span>{{$bookingData['date']['year']}}年{{$bookingData['date']['month']}}月{{$bookingData['date']['day']}}日(水)</span><span>{{$bookingData['receipt_time']}}時</span></td>
                        </tr>
                        <tr class="row-2">
                            <td class="label_td">受取り店舗</td>
                            <td class="address"><span class="text">{{$bookingData['shop_name']}}</span><span
                                        class="tel">TEL : {{$bookingData['customer_tel']}}</span></td>
                        </tr>
                        <tr class="row-3">
                            <td class="label_td"></td>
                            <td class="city">{{$bookingData['city_name'].$bookingData['address']}}</td>
                        </tr>
                    </table>
                </div>
                <div class="customer">
                    <div class="info_customer_title">お客さま情報</div>
                    <div class="info_customer">
                        <div>
                            <label>お名前</label>
                            <span>{{$bookingData['customer_name']}}</span>
                        </div>
                        <div>
                            <label>電話番号</label>
                            <span>TEL : {{$bookingData['customer_tel']}}</span>
                        </div>
                        <div>
                            <label>サンエーカード番号</label>
                            <span>{{formatCardNo($bookingData['customer_card_no'])}}</span>
                        </div>
                    </div>
                </div>
                <div class="button_back">
                    <button class="button_print_footer" onclick="Frontend.windowPrint()">この画面を印刷する</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $(document).ready(function ($) {
            if (window.history && window.history.pushState) {
                window.history.pushState('forward', null, './#forward');
                $(window).on('popstate', function () {
                    location.href = laroute.route('mypage.index');
                });
            }
        });
    </script>
@endsection


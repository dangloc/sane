<?php

namespace App\Repositories;

use App\Models\Customer;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

/**
 * class ItemBonusPointRepository.
 *
 * @package namespace App\Repositories;
 */
class ItemBonusPointRepository extends BaseRepository
{
    function __construct()
    {
        $this->_model = \App\Models\ItemBonusPoint::class;
    }

    /**
     * @param $code
     * @param $date
     * @return bool
     * check point bonus top
     */
    function checkPointBonus($code, $date)
    {
        if (!empty($code) && !empty($date)) {
            $data = $this->getModel()->select('*')
                ->where('item_code', '=', $code)
                ->where(DB::raw('STR_TO_DATE ( receipt_start_date,  "%Y%m%d")'), '<=', Carbon::parse($date)->format('Ymd'))
                ->where(DB::raw('STR_TO_DATE ( receipt_end_date,  "%Y%m%d")'), '>=', Carbon::parse($date)->format('Ymd'))
                ->where(DB::raw('STR_TO_DATE ( receptionist_start_date,  "%Y%m%d")'), '<=', date('Ymd'))
                ->where(DB::raw('STR_TO_DATE ( receptionist_end_date,  "%Y%m%d")'), '>=', date('Ymd'))
                ->get()->toArray();
            return count($data);
        }
        return false;
    }

    function searchItemBonus($item_code, $date_receive)
    {
        $now = Carbon::now()->format('Ymd');
        $data=$this->getModel()->where('item_code', '=', $item_code)
            ->where('receipt_start_date', '<=', $date_receive)
            ->where('receipt_end_date', '>=', $date_receive)
            ->where('receptionist_start_date', '<=', $now)
            ->where('receptionist_end_date','>=',$now)->get();
        return $data;
    }
}

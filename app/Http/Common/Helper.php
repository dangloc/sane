<?php
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

if (!function_exists('pr')) {
    function pr($data)
    {
        echo '<pre>';
        print_r($data);
        echo '</pre>';
    }
}

if (!function_exists('isManager')) {
    function isManager()
    {
        return strlen(strstr(url()->current(), 'management')) > 0;
    }
}

function activeMenu($controller, $action)
{
    switch (true) {
        case $controller == 'order':
            return config('view.active_menu.order');
        case ($controller == 'item' && $action != 'search'):
            return config('view.active_menu.import');
        case $controller == 'category':
            return config('view.active_menu.import');
        case $controller == 'event':
            return config('view.active_menu.import');
        case ($controller == 'item' && $action == 'search'):
            return config('view.active_menu.product');
        case $controller == 'shop':
            return config('view.active_menu.shop');
        case $controller == 'user':
            return config('view.active_menu.customer');
        case $controller == 'information':
            return config('view.active_menu.notification');
        case $controller == 'account':
            return config('view.active_menu.setting');
        case $controller == 'banner':
            return config('view.active_menu.setting');
        default:
            return config('view.active_menu.order');
    }
}

function mergeDateAndTime($date, $time)
{
    if ($date != '' && $time != '') {
        return date('Y-m-d', strtotime($date)) . " " . $time . ":00:00";
    }
    return null;
}

function formatDatetime($datetime)
{
    if (isset($datetime)) {
        return date('Y/m/d', strtotime($datetime));
    }
    return null;
}

function getDateTime($date)
{
    $year = !empty(\Carbon\Carbon::parse($date)->format('Y')) ? \Carbon\Carbon::parse($date)->format('Y') : "";
    $month = !empty(\Carbon\Carbon::parse($date)->format('m')) ? \Carbon\Carbon::parse($date)->format('m') : "";
    $day = !empty(\Carbon\Carbon::parse($date)->format('d')) ? \Carbon\Carbon::parse($date)->format('d') : "";
    return array('year' => $year, 'month' => $month, 'day' => $day);
}

function checkCard($card)
{
    $max = 9;
    $sum = 0;
    for ($i = 0; $i < strlen($card) - 1; $i++) {
        if ($max == 1) {
            $kq = $max;
            $max = 9;
        } else {
            $kq = $max;
            $max--;
        }
        $result = $card[$i] * $kq;
        if (strlen($result) == 2) {
            $array = str_split($result);
            $sum += $array[0] + $array[1];
        } else {
            $sum += $result;
        }
    }
    $check = (int)10 - $sum % 10;
    if ($check == $card[strlen($card) - 1]) {
        return true;
    } else {
        return false;
    }
}

function setSessionTop(\Illuminate\Http\Request $request, $eventCode = '', $shopCode = '', $categoryCode = '', $dateReceive = '', $time = '', $area_id = 0, $city_id = 0, $search_shop_text = '')
{
    $key_check = array(
        config('const.session.event_code') => $eventCode,
        config('const.session.shop_code') => $shopCode,
        config('const.session.category_code') => $categoryCode,
        config('const.session.date_receive') => getDateTime($dateReceive),
        config('const.session.time') => $time,
        config('const.session.date_receive_raw') => $dateReceive,
        config('const.session.area_id') => $area_id,
        config('const.session.city_id') => $city_id,
        config('const.session.search_shop_text') => $search_shop_text
    );
    foreach ($key_check as $key => $value) {
        if ($request->session()->has($key)) {
            $request->session()->pull($key);
        }
        $request->session()->put($key, $value);
    }
}

function checkSessionTopExist(\Illuminate\Http\Request $request)
{
    $key_check = array(
        config('const.session.event_code'),
        config('const.session.shop_code'),
        config('const.session.date_receive'),
        config('const.session.time'),
        config('const.session.date_receive_raw'),
    );
    foreach ($key_check as $key) {
        if (!$request->session()->has($key)) {
            return false;
        }
}
    return true;
}

/**
 * @param \Illuminate\Http\Request $request
 * @param int $itemCode
 * @param int $quantity
 * @param int $requiredFlag
 * Luu session Item addToCart
 */

function saveSessionItem($request, $itemCode = 0, $quantity = 0, $requiredFlag = 0, $shopCode = 0, $requiredConfirmFlag = 0,$dateReceiveRaw =0 , $timeReceive = 0)
{
    $item_list = array(
        'itemCode' => $itemCode,
        'quantity' => $quantity,
        'requiredFlag' => $requiredFlag,
        'requiredConfirmFlag' => $requiredConfirmFlag,
    );
    $eventCode = ($request->session()->has(config('const.session.event_code'))) ? $request->session()->get(config('const.session.event_code')) : "";

    if (empty($requiredConfirmFlag) && empty($requiredFlag)) {
        $keyProduct = $itemCode . '_' . $shopCode . '_' . $requiredConfirmFlag . '_key';
    } else {
        $keyProduct = $itemCode . '_' . $shopCode . '_' . $requiredConfirmFlag . '_' . $requiredFlag . '_key';
    }
    $keyItem = $dateReceive=  '';

    if (!empty($dateReceiveRaw)){
        $keyItem = strtotime($dateReceiveRaw). $shopCode.$eventCode;
        $dateReceive = getDateTime($dateReceiveRaw);
    }else{
        $keyItem = ($request->session()->has(config('const.session.date_receive_raw'))) ? strtotime($request->session()->get(config('const.session.date_receive_raw'))) . $shopCode.$eventCode : '' . $shopCode.$eventCode;
        $dateReceiveRaw = ($request->session()->has(config('const.session.date_receive_raw'))) ? $request->session()->get(config('const.session.date_receive_raw')) : '';
        $dateReceive = ($request->session()->has(config('const.session.date_receive'))) ? $request->session()->get(config('const.session.date_receive')) : '';
    }

    if (empty($timeReceive)){
        $timeReceive = ($request->session()->has(config('const.session.time'))) ? $request->session()->get(config('const.session.time')) : '';
    }

    $newCart = array(
        $keyItem => array(
            'key_item' => $keyItem,
            'shopCode' => $shopCode,
            'eventCode' => $eventCode,
            'timeReceive' => $timeReceive,
            'dateReceiveRaw' => $dateReceiveRaw,
            'dateReceive' => $dateReceive,
            'itemList' => array(
                $keyProduct => $item_list
            ),
        )
    );
    if ($request->session()->has(config('const.session.cart'))) {
        $cartList = $request->session()->get(config('const.session.cart'));
        if (!empty($cartList)) {
            if (!array_key_exists($keyItem, $cartList)) {
                $cartList[$keyItem] = $newCart[$keyItem];
            } else {
                if (!array_key_exists($keyProduct, $cartList[$keyItem]['itemList'])) {
                    $cartList[$keyItem]['itemList'][$keyProduct] = $newCart[$keyItem]['itemList'][$keyProduct];
                } else {
                    $cartList[$keyItem]['itemList'][$keyProduct]['quantity'] += $newCart[$keyItem]['itemList'][$keyProduct]['quantity'];
                }
            }
        }
        $request->session()->put(config('const.session.cart'), $cartList);
    } else {
        $request->session()->put(config('const.session.cart'), $newCart);
    }
}
function formatCardNo($stringNumber){
   $card=str_split($stringNumber,4);
   return $card[0].'-'.$card[1].'-'.$card[2];
}

function updateSession($request, $keyItem, $keyProduct, $attribute, $value)
{
    if ($request->session()->has(config('const.session.cart'))) {
        $cartList = $request->session()->get(config('const.session.cart'));
        if (!empty($keyItem) && !empty($keyProduct) && !empty($attribute) && isset($value)) {
            if (!empty($cartList[$keyItem]['itemList'][$keyProduct])) {
                $cartList[$keyItem]['itemList'][$keyProduct][$attribute] = $value;
            }
        }
        $request->session()->pull(config('const.session.cart'));
        $request->session()->put(config('const.session.cart'), $cartList);
        return $cartList;
    }
    return false;
}

function deleteSession($request){
    $key_check = array(
        config('const.session.event_code'),
        config('const.session.shop_code'),
        config('const.session.date_receive'),
        config('const.session.time'),
        config('const.session.date_receive_raw'),
        config('const.session.cart'),
        config('const.session.area_id'),
        config('const.session.city_id'),
        config('const.session.search_shop_text'),
        config('const.session.customer_booking')
    );
    foreach ($key_check as $key) {
        if ($request->session()->has($key)) {
            $request->session()->pull($key);
        }
    }
    return true;
}

function checkExistSession($request)
{
    $eventCode = $request->session()->has(config('const.session.event_code'));
    $shopCode = $request->session()->has(config('const.session.shop_code'));
    $dateReceiveRaw = $request->session()->has(config('const.session.date_receive_raw'));

    if (!empty($eventCode) && !empty($shopCode) && !empty($dateReceiveRaw)) {
        return true;
    }
    return false;
}

/**
 * @return string
 * Tạo slip number and check unique db
 */
function createSlipNumber()
{
    $slipNumber = '9';
    $data = \App\Models\Reserve::query()
        ->select(DB::raw('MAX(slip_number) as slip_number'))
        ->where('receptionist_date', '=', Carbon::now()->format('Ymd'))
        ->first();
    $rand = 0;
    if (!empty($data)){
        $rand = (strlen($data->slip_number) == 12) ? substr($data->slip_number, 1,2). substr($data->slip_number,9,3) : '00000';

    }
    $rand = (int) $rand +1;
    $arraySlip = array_map('intval', str_split($rand));
    $numberFix = '';
    for ($i = 0; $i < (5 - count($arraySlip)); $i++) {
        $numberFix .= '0';
    }
    $randSlip = $numberFix . $rand;
    if (strlen($randSlip) == 5) {
        $slipNumber .= $randSlip[0] . $randSlip[1] . date('ymd') . $randSlip[2] . $randSlip[3] . $randSlip[4];
        $check = \App\Models\CustomerOrder::query()->where('slip_number', '=', $slipNumber)->first();
        if ($check) {
            return createSlipNumber();
        }
    }
    return $slipNumber;
}

function isJson($string)
{
    json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE);
}

function getBarCode($slip_code){
    require_once  app_path().'/Http/Lib/barcode/barcodephp/jan13.php';
    $lib = new \Jan13();
    $img = $lib->draw($slip_code, 3, 100);
    if ($img == false) {
        return false;
    }
    $url = storage_path(config('const.storage.path_barcode')). $slip_code .'.jpg';
    $check = Imagejpeg($img, $url, 100);
    if ($check == false) {
        return false;
    }
    return "storage/barcode/".$slip_code.'.jpg';
}

function subStringDot($string = '', $length = 32)
{
    if (strlen($string) > $length) {
        return mb_substr($string, 0, $length) . "...";
    }
    return $string;
}

function convertTimeToInt($time)
{
    $time = date('ymd', strtotime($time));
    return (int)$time;
}

function convertDateToChar($string, $isFullString){
    if ($isFullString == false){
        return substr((str_replace('/','',$string)),2);
    }else{
        return str_replace('/','',$string);
    }
}

function convertCharToDate($string){
    if(!empty($string)){
        if(strlen($string) == 6){
            $day = substr($string,4,2);
            $month = substr($string,2,2);
            $year = "20".substr($string,0,2);
        }
        if(strlen($string) == 8){
            $day = substr($string,6,2);
            $month = substr($string,4,2);
            $year = substr($string,0,4);
        }
        return $year."/".$month."/".$day;
    }
}

function formatTime($string){
    if(!empty($string)){
        $hour = str_pad(substr($string,0,2), 2, '0', STR_PAD_LEFT);
        $minute = str_pad(substr($string,2,2), 2, '0', STR_PAD_LEFT);
        return $hour.":".$minute;
    }
}

function moveImage($url,$name){
    $nameImage = str_replace('storage/temp/', '', $url);
    if( $url !== null){
        Storage::move('public/temp/'.$nameImage,'public/'.$name.'/'.$nameImage);
    }
    return 'storage/'.$name.'/'.$nameImage;

}
function deleteImage($url,$name){
    if($url == !null){
        $nameImage = str_replace('storage/'.$name.'/', '', $url);
        Storage::delete('public/'.$name.'/'.$nameImage);
    }
}

function dayJapan($date)
{
    if (!empty($date)) {
        $day = Carbon::parse($date)->format('w');
        $days = array("日", "月", "火", "水", "木", "金", "土");
        return !empty($days[$day]) ? $days[$day] : "";
    }
    return "";
}

function validateDate($date, $format = 'Y-m-d')
{
    $d = DateTime::createFromFormat($format, $date);
    // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
    return $d && $d->format($format) === $date;
}

function forMatPhone($phone){
    if (!empty($phone)){
        return substr($phone,0,4).'-'. substr($phone,4, 2).'-'.substr($phone,6,4);
    }
}

function formatDate($date){
    $year=Carbon::parse($date)->format('Y');
    $month=Carbon::parse($date)->format('m');
    $day=Carbon::parse($date)->format('d');
    return $year."年".$month."月".$day;
}
<?php

namespace App\Models;
use App\Scopes\ActiveScope;

use Illuminate\Database\Eloquent\Model;

class Informations extends Model
{
    public $timestamps = false;
    protected $_alias = 'informations';
    protected $table = 'informations';
    protected $fillable = [
        'id',
        'title',
        'content',
        'public_start_date',
        'public_start_time',
        'public_end_date',
        'public_end_time',
        'del_flag',
        'ins_datetime',
        'ins_id',
        'upd_datetime',
        'upd_id'

    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ActiveScope());
    }
}



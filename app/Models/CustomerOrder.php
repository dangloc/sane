<?php

namespace App\Models;
use App\Scopes\ActiveScope;

use Illuminate\Database\Eloquent\Model;

class CustomerOrder extends Model
{
    const STATUS_CANCE = 1;
    public $timestamps = false;
    protected $_alias = 'customer_orders';
    protected $table = 'customer_orders';
    protected $fillable = [
        'slip_number',
        'customer_id',
        'shop_code',
        'receptionist_date',
        'receipt_date',
        'receipt_time',
        'customer_name',
        'customer_tel',
        'customer_card_no',
        'total_price',
        'status',
        'send_status',
        'web_order_timestamp',
        'web_cancel_timestamp',
        'del_flag',
        'ins_datetime',
        'ins_id',
        'upd_datetime',
        'upd_id',
    ];
    // NOT APPLY global scope with ORDER
//    protected static function boot()
//    {
//        parent::boot();
//        static::addGlobalScope(new ActiveScope());
//    }

    public function customer(){
        return $this->belongsTo(Customer::class);
    }
    public function shop(){
        return $this->belongsTo(Shop::class,'shop_code','code');
    }
    public function reserves(){
        return $this->hasMany(Reserve::class,'slip_number','slip_number');
    }
}

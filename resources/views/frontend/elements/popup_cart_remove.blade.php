<div id="modal-cart-remove" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="group-input">
                <div class="row">
                    <div class="col-xs-12 col-lg-12">
                        <label>カートから削除します。よろしいでしょうか。</label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="Frontend.removeItemCart('{{$dataKeyItem}}','{{$dataKeyProduct}}', {{$boxList}})">OK</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">いいえ</button>
            </div>
        </div>

    </div>
</div>
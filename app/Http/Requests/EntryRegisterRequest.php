<?php

namespace App\Http\Requests;

use App\Http\Common\checkCardOnCustomer;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Factory;
use function PHPSTORM_META\type;
use App\Rules\CheckPassword;

class EntryRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function __construct(Factory $validationFactory)
    {
        $validationFactory->extend('check_card',
            function () {
                $card_on = implode(request()->card);
                if (strlen($card_on) == 12) {
                    $check = checkCard($card_on);
                    return $check;
                }
            }
        );
    }

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule = [
            'email' => 'required|check_email|unique:customers',
            'confirm_email' => 'required_with:email|same:email',
            'my_password' => ['required', new CheckPassword],
            'confirm_password' => 'required_with:my_password|same:my_password',
            'name' => 'required|katakana',
            'tel' => 'required|max:11|regex:/^[0-9]*$/i',
            'card.*' => 'required|numeric|digits:4',
            'card' => 'check_card',
            'policy' => 'accepted'
        ];
        return $rule;
    }

    public function attributes()
    {
        return [
            'email' => 'メールアドレス',
            'confirm_email' => '新しいメールアドレス',
            'my_password' => 'パスワード',
            'confirm_password' => 'パスワード（再入力）',
            'name' => 'お名前',
            'tel' => '電話番号',
            'policy' => '方針',
            'card.*' => 'カード番号',
            'card'=>'カード番号'
        ];
    }
    public function messages()
    {
        return [
            'confirm_email.required_with'=>'メールアドレス(再入力)は必ず指定してください。',
            'confirm_email.same'=>'メールアドレス(再入力)とメールアドレスには同じ値を指定してください。',
            'tel.regex'=>':attributeには半角数字を指定してください。'
        ];
    }
}

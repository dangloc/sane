<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\BackendController;
use App\Http\Requests\AccountCreateRequest;
use App\Repositories\AccountRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AccountController extends BackendController
{
    protected $accountRepository;

    public function __construct(AccountRepository $accountRepository)
    {
        $this->accountRepository = $accountRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $accounts = $this->accountRepository->searchCondition($request)->paginate(config('const.paging.backend'));
        return view('backend.account.index', compact('accounts'))
            ->with([
                'name' => $request->get('name'),
                'status' => $request->get('status'),
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        request()->session()->put('session_data', []);
        return view('backend.account.create');
    }

    /**
     * Show the form for confirm the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function confirm(AccountCreateRequest $request, $id = null)
    {
        $request->flash();
        $request->session()->put('session_data', $request->all());
        return view('backend.account.confirm')->with('account', $request->all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if(!$request->session()->has('session_data')){
            return redirect()->route('account.index')->with('error', config('messages.SYSTEM_ERROR'));
        }
        $data = $request->session()->get('session_data');
        $request->session()->forget('session_data');
        try {
            $this->accountRepository->create($data);
            return redirect()->route('account.index')->with('success', config('messages.DB_INSERT'));
        } catch (\Exception $e) {
            return redirect()->route('account.index')->with('error', config('messages.SYSTEM_ERROR'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $account = $this->accountRepository->find($id);
        if ($account == null) {
            return redirect()->route('account.index')->with('error', config('messages.NO_DATA'));
        } else {
            $account = $account->toArray();
        }
        request()->session()->put('session_data', $account);
        return view('backend.account.create')->with('account', $account);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!$request->session()->has('session_data'))
        {
            return redirect()->route('account.index')->with('error', config('messages.SYSTEM_ERROR'));
        }
        $data = $request->session()->get('session_data');
        $request->session()->forget('session_data');
        try {
            $this->accountRepository->update($id, $data);
            return redirect()->route('account.index')->with('success', config('messages.DB_UPDATE'));
        } catch (\Exception $e) {
            return redirect()->route('account.index')->with('error', config('messages.SYSTEM_ERROR'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->accountRepository->softDelete($id);
            return redirect()->route('account.index')->with('success', config('messages.DB_DELETE'));
        } catch (\Exception $e) {
            return redirect()->route('account.index')->with('error', config('messages.SYSTEM_ERROR'));
        }
    }
}

<p>このたびは、「サンエーネット予約サービス」をご利用いただき</p>
<p>誠にありがとうございます。</p>
<p>以下の通り、お客様情報の変更を承りました。</p>
<p>━━━━━━━━━━━━━━━━━━</p>
<p>お客様情報</p>
<p>━━━━━━━━━━━━━━━━━━</p>
<p>伝票番号：{{$order['slip_number']}}</p>
<p>お名前：{{$order['customer_name']}}様</p>
<p>電話番号：{{$order['customer_tel']}}</p>
<p>サンエーカード番号：{{formatCardNo($order['customer_card_no'])}}</p>
<br>
<p>このメールは自動送信です。</p>
<p>返信いただいても回答できませんので、ご了承ください。</p>
<p>お客様情報の変更は、</p>
<p>ご登録いただいたメールアドレス、パスワードにて</p>
<p>マイページにログインして行ってください。</p>
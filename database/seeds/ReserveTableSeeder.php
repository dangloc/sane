<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ReserveTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('reserves')->truncate();
        DB::table('reserves')->insert([
            [
                'slip_number'=>'00190516000',
                'shop_code'=>'1',
                'receptionist_date'=>'20190517',
                'receptionist_type'=>'3',
                'receipt_date'=>'20190518',
                'receipt_time'=>'12',
                'customer_name' => 'HN',
                'customer_tel' => '0964161702',
                'customer_card_no' => '190802100156',
                'event_code'=>'A',
                'item_code'=>'0010',
                'required_confirm_flag'=>'0',
                'required_flag'=>'1',
                'item_count'=>'2',
                'status_flag'=>'0',
                'update_flag'=>'0',
                'reserve_content1'=>'Customer content 1',
                'reserve_content2'=>'Customer content 2',
                'reserve_del_flag'=>'0',
                'update_timestamp'=>'20190517',
                'del_flag'=>0,
                'ins_id'=>1,
                'ins_datetime'=>'2019-05-13 11:29:17'
            ],
                [
                    'slip_number'=>'00190516000',
                    'shop_code'=>'1',
                    'receptionist_date'=>'20190517',
                    'receptionist_type'=>'3',
                    'receipt_date'=>'20190518',
                    'receipt_time'=>'12',
                    'customer_name' => 'HN',
                    'customer_tel' => '0964161702',
                    'customer_card_no' => '190802100156',
                    'event_code'=>'A',
                    'item_code'=>'0011',
                    'required_confirm_flag'=>'0',
                    'required_flag'=>'1',
                    'item_count'=>'3',
                    'status_flag'=>'0',
                    'update_flag'=>'0',
                    'reserve_content1'=>'Customer content 1',
                    'reserve_content2'=>'Customer content 2',
                    'reserve_del_flag'=>'0',
                    'update_timestamp'=>'20190517',
                    'del_flag'=>0,
                    'ins_id'=>1,
                    'ins_datetime'=>'2019-05-13 11:29:17'
                ],
                [
                    'slip_number'=>'00190516001',
                    'shop_code'=>'1',
                    'receptionist_date'=>'20190517',
                    'receptionist_type'=>'3',
                    'receipt_date'=>'20190518',
                    'receipt_time'=>'12',
                    'customer_name' => 'HN',
                    'customer_tel' => '0964161702',
                    'customer_card_no' => '190802100156',
                    'event_code'=>'B',
                    'item_code'=>'0010',
                    'required_confirm_flag'=>'0',
                    'required_flag'=>'1',
                    'item_count'=>'2',
                    'status_flag'=>'0',
                    'update_flag'=>'0',
                    'reserve_content1'=>'Customer content 1',
                    'reserve_content2'=>'Customer content 2',
                    'reserve_del_flag'=>'0',
                    'update_timestamp'=>'20190517',
                    'del_flag'=>0,
                    'ins_id'=>1,
                    'ins_datetime'=>'2019-05-13 11:29:17'
                ],
            [
                'slip_number'=>'00190516003',
                'shop_code'=>'1',
                'receptionist_date'=>'20190517',
                'receptionist_type'=>'3',
                'receipt_date'=>'20190518',
                'receipt_time'=>'12',
                'customer_name' => 'HN',
                'customer_tel' => '0964161702',
                'customer_card_no' => '190802100156',
                'event_code'=>'C',
                'item_code'=>'0012',
                'required_confirm_flag'=>'0',
                'required_flag'=>'1',
                'item_count'=>'2',
                'status_flag'=>'0',
                'update_flag'=>'0',
                'reserve_content1'=>'Customer content 1',
                'reserve_content2'=>'Customer content 2',
                'reserve_del_flag'=>'0',
                'update_timestamp'=>'20190517',
                'del_flag'=>0,
                'ins_id'=>1,
                'ins_datetime'=>'2019-05-13 11:29:17'
            ],
        ]);
    }
}

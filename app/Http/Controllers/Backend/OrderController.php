<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\BackendController;
use App\Repositories\AreasRepository;
use App\Repositories\CitiesRepository;
use App\Repositories\CustomerOrderRepository;
use App\Repositories\OrderRepository;
use App\Repositories\ReserveRepository;
use App\Repositories\ShopsRepository;
use Illuminate\Http\Request;

class OrderController extends BackendController
{
    protected $customerOrderRepository;
    protected $areaRepository;
    protected $cityRepository;
    protected $shopRepository;
    protected $reserveRepository;

    public function __construct(CustomerOrderRepository $customerOrderRepository, ShopsRepository $shopRepository, AreasRepository $areaRepository, CitiesRepository $cityRepository, ReserveRepository $reserveRepository)
    {
        $this->customerOrderRepository = $customerOrderRepository;
        $this->areaRepository = $areaRepository;
        $this->cityRepository = $cityRepository;
        $this->shopRepository = $shopRepository;
        $this->reserveRepository = $reserveRepository;
    }

    public function index(Request $request)
    {
        $areas = $this->areaRepository->all();
        $cities = [];
        $shops = [];
        if (null != $request->get('area_id')){
            $cities = $this->areaRepository->getCity($request->get('area_id'));
            if (null != $request->get('city_id')){
                $shops = $this->shopRepository->getShopByAreaAndCity($request->get('area_id'), $request->get('city_id'));
            }
        }
        $orders = $this->customerOrderRepository->searchCondition($request)->paginate(config('const.paging.backend'));
        return view('backend.order.index', compact('orders'))
            ->with([
                'areas' => $areas,
                'cities' => $cities,
                'shops' => $shops,
                'slip_number' => $request->get('slip_number'),
                'area_id' => $request->get('area_id'),
                'city_id' => $request->get('city_id'),
                'shop_code' => $request->get('shop_code'),
                'customer_name' => $request->get('customer_name'),
                'customer_tel' => $request->get('customer_tel'),
                'customer_card_no' => $request->get('customer_card_no'),
                'receptionist_date_start' => $request->get('receptionist_date_start'),
                'receptionist_date_end' => $request->get('receptionist_date_end'),
                'receipt_date_start' => $request->get('receipt_date_start'),
                'receipt_date_end' => $request->get('receipt_date_end'),
                'status' => $request->get('status'),
                'status_array' => config('const.status_customer_order')
            ]);
    }
    // this route use for order screen
    public function getShop($area_id, $city_id) {
        $shops = $this->shopRepository->getShopByAreaAndCity($area_id, $city_id);
        return mb_convert_encoding($shops->toArray(), 'UTF-8', 'UTF-8');
    }

    public function show($slip_number)
    {
        $order = $this->customerOrderRepository->find($slip_number);
        if ($order == null) {
            return redirect()->route('order.index')->with('error', config('messages.NO_DATA'));
        }
        $reserves = $this->reserveRepository->getReserveBySlipNo($slip_number);
        return view('backend.order.show', compact('order'))
            ->with([
                'status_array' => config('const.status_customer_order'),
                'reserves' => $reserves
            ]);
    }
}

@extends('backend.layouts.main')
@section('content')

    <div id="Contentwrap">

        <div class="contenttop detail-btn-group">
            <p class="btn Searchclear">
                <button type="button" onclick="System.redirectLoading('{{ route('order.index') }}')" ><i class="fas fa-list"></i> 一覧ページに戻る</button>
            </p>
        </div>
        <!--ContentFoot -->

        <div id="PageContent">
            <h2><span><i class="fas fa-file"></i> 商品詳細</span></h2>
            <table class="listtable">
                <tr>
                    <th width="30%" scope="row">店舗名（店舗コード）</th>
                    <td width="70%">為又シティ（0021）</td>
                </tr>
                <tr>
                    <th scope="row">カテゴリー</th>
                    <td>オードブル</td>
                </tr>
                <tr>
                    <th scope="row">商品名（商品コード）</th>
                    <td>オードブル（1234567890123）</td>
                </tr>
                <tr>
                    <th scope="row">商品画像</th>
                    <td><img src="https://yoyaku-photo.san-a.co.jp/20140930104711_0260913000003.jpg" width="200"></td>
                </tr>
                <tr>
                    <th scope="row">状態</th>
                    <td>販売中
                        <p class="btn register" style="float:right;">
                            <button type="submit" value="" class="opener2"><i class="fas fa-sync-alt my-white"></i> 販売状態変更</button>
                        </p>
                    </td>
                </tr>
                <tr>
                    <th scope="row">項目1</th>
                    <td>テキストテキスト</td>
                </tr>
                <tr>
                    <th scope="row">項目2</th>
                    <td>テキストテキストテキストテキストテキスト<br/>テキストテキストテキストテキストテキストテキストテキスト<br/>テキストテキストテキストテキストテキスト</td>
                </tr>
                <tr>
                    <th scope="row">項目3</th>
                    <td>テキストテキスト</td>
                </tr>
                <tr>
                    <th scope="row">必須確認事項</th>
                    <td>かまぼこ</td>
                </tr>
                <tr>
                    <th scope="row">限定数</th>
                    <td>0</td>
                </tr>
                <tr>
                    <th scope="row">添加物コード</th>
                    <td>4072</td>
                </tr>
                <tr>
                    <th scope="row">リードタイム</th>
                    <td>2日前まで受付可能</td>
                </tr>
                <tr>
                    <th scope="row">予備項目1</th>
                    <td>_</td>
                </tr>
                <tr>
                    <th scope="row">予備項目2</th>
                    <td>_</td>
                </tr>
                <tr>
                    <th scope="row">予備項目3</th>
                    <td>_</td>
                </tr>
                <tr>
                    <th scope="row">受付開始日〜受付終了日</th>
                    <td>2019/01/01〜2019/03/31</td>
                </tr>
                <tr>
                    <th scope="row">受取開始日〜受取終了日</th>
                    <td>2019/01/01〜2019/03/31</td>
                </tr>
                <tr>
                    <th scope="row">価格</th>
                    <td>¥3,500</td>
                </tr>
            </table>
        </div>
        <!--PageContent -->

        <div id="PageContent" class="pt60">
            <h2 id="buy_his"><i class="fas fa-shopping-cart"></i> 予約数</h2>
            <table class="OrderForm responsive tdcenter Bordertd TextL table-hover">
                <tr>
                    <th>行事</th>
                    <th>商品</th>
                    <th>確認事項</th>
                    <th width="180px">数量</th>
                    <th width="200px">金額（税抜）</th>
                </tr>
                <tr>
                    <td style="text-align:left;">清明祭</td>
                    <td style="text-align:left;">3段重セット</td>
                    <td style="text-align:left;">かまぼこ（赤）</td>
                    <td style="text-align:right;">1</td>
                    <td style="text-align:right;"><strong>¥8,800</strong></td>
                </tr>
                <tr>
                    <td style="text-align:left;">清明祭</td>
                    <td style="text-align:left;">寿司盛り合わせ 凛</td>
                    <td style="text-align:left;">わさび（入り）</td>
                    <td style="text-align:right;">1</td>
                    <td style="text-align:right;"><strong>¥4,980</strong></td>
                </tr>
            </table>
        </div>

        <!--PageContent -->
        <div class="contenttop detail-btn-group">
            <p class="btn Searchclear">
                <button type="button" onclick="System.redirectLoading('{{ route('order.index') }}')"><i class="fas fa-list"></i> 一覧ページに戻る</button>
            </p>
        </div>
        <!--ContentFoot -->
    </div>
    <!--Contentwrap -->
    <div class="dialog" id="dialog" title="ポイント付与">
        <table class="OrderForm responsive table-hover">
            <thead>
            <tr>
                <th scope="col" width="100px">選択</th>
                <th scope="col">ポイント種別</th>
                <th scope="col" width="220px">付与ポイント</th>
            </tr>
            </thead>
            <tbody>
            <tr id="item-332" hp-flag="1">
                <td class="check"><input type="checkbox" name="checkbox" id="pt-checkbox01" tabindex="52" checked/>
                    <label for="pt-checkbox01"></label>
                </td>
                <td>来店付与ポイント</td>
                <td class="TextR"><span class="price-pretax">99,999,999 pt</span></td>
            </tr>
            <tr id="item-332" hp-flag="1">
                <td class="check"><input type="checkbox" name="checkbox" id="pt-checkbox02" tabindex="52"/>
                    <label for="pt-checkbox02"></label>
                </td>
                <td>紹介付与ポイント</td>
                <td class="TextR"><span class="price-pretax">99,999,999 pt</span></td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="dialog" id="dialog2" title="変更">
        <table class="OrderForm responsive table-hover">
            <tr>
                <th width="158" style="width: 150px;" scope="row">販売状態変更</th>
                <td colspan="4">
                    <div class="rabutton">
                        <input type="hidden" name="rank" id="MemberRank_" value="">
                        <input type="radio" name="rank" id="MemberRank0" value="0" default-value="0" class="" data-com.agilebits.onepassword.user-edited="yes">
                        <label for="MemberRank0">販売中</label>
                        <input type="radio" name="rank" id="MemberRank1" value="1" default-value="0" class="">
                        <label for="MemberRank1">販売停止</label>
                    </div>
                </td>
            </tr>
        </table>
    </div>
@endsection
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_orders', function (Blueprint $table) {
            $table->char('slip_number',12)->comment('伝票番号');
            $table->integer('customer_id')->comment('お客様情報ID');
            $table->char('shop_code',4)->comment('店舗コード');
            $table->char('receptionist_date',8)->comment('受付日');
            $table->char('receipt_date', 8)->comment('受取日');
            $table->char('receipt_time',2)->comment('受取時間');
            $table->string('customer_name',512)->comment('お名前');
            $table->string('customer_tel',11)->comment('電話番号')->default('00000000000');
            $table->string('customer_card_no',12)->comment('カード番号')->default('000000000000');
            $table->char('status', 1)->comment('状態');
            $table->char('send_status', 1)->comment('送信状態');
            $table->timestamp('web_order_timestamp')->comment('WEB注文日時');
            $table->timestamp('web_cancel_timestamp')->comment('WEBキャンセル日時')->nullable();
            $table->char('del_flag', 1)->comment('削除フラグ')->default(0);
            $table->dateTime('ins_datetime')->default(DB::raw('CURRENT_TIMESTAMP'))->comment('登録日時');
            $table->integer('ins_id')->length(11)->comment('登録者ID');
            $table->dateTime('upd_datetime')->comment('更新日時')->nullable();
            $table->integer('upd_id')->length(11)->comment('更新者ID')->nullable();

            //constraints
//            $table->foreign('customer_id')->references('id')->on('customers');
            $table->primary(array('slip_number'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_orders');
    }
}

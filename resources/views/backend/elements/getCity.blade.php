<select class="city-select" name="city_id" tabindex="10">
    <option value="">市町村</option>
    @if($cities !== [])
        @foreach($cities as $city)
            <option value="{{$city['id']}}" {{old('city_id') !== null ? 'selected' : ''}}>{{$city['name']}}</option>
        @endforeach
    @endif
</select>
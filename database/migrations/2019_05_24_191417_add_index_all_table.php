<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexAllTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach (config('table_map') as $table_name => $value) {
            Schema::table($table_name, function (Blueprint $table) use ($table_name) {
                foreach (config('table_map.' . $table_name . '.keys') as $key)
                {
                    $table->index($key);
                }
                $table->index('del_flag');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}

<?php

namespace App\Models;
use App\Scopes\ActiveScope;

use Illuminate\Database\Eloquent\Model;

class ItemBonusPoint extends Model
{
    public $timestamps = false;
    protected $_alias = 'item_bonus_points';
    protected $table = 'item_bonus_points';
    protected $fillable = [
        'item_code',
        'reception_start_date',
        'reception_end_date',
        'receipt_start_date',
        'receipt_end_date',
        'del_flag',
        'ins_datetime',
        'ins_id',
        'upd_datetime',
        'upd_id'
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ActiveScope());
    }
}

// ----------------------------------------------------
	// hover画像切替
// ----------------------------------------------------
$(function(){
     $('img').hover(function(){
        $(this).attr('src', $(this).attr('src').replace('_off', '_on'));
          }, function(){
             if (!$(this).hasClass('currentPage')) {
             $(this).attr('src', $(this).attr('src').replace('_on', '_off'));
        }
   });
});

//-----------------------------
　// Scroll
//------------------------------
$(function(){
	$('a[href^="#"]').click(function(){
		var speed = 500;
		var href= $(this).attr("href");
		var target = $(href == "#" || href == "" ? 'html' : href);
		var position = target.offset().top;
		$("html, body").animate({scrollTop:position}, speed, "swing");
		return false;
	});
});

//-----------------------------
　//レスポンシブ画像切り替え
//-----------------------------
$(function(){
	var $setElem = $('.switch'),
	pcName = '_pc',
	spName = '_sp',
	replaceWidth = 767;
	
	$setElem.each(function(){
		var $this = $(this);
		function imgSize(){
			var windowWidth = parseInt( window.innerWidth ? window.innerWidth: $(window).width());
			if(windowWidth >= replaceWidth) {
				$this.attr('src',$this.attr('src').replace(spName,pcName)).css({visibility:'visible'});
			} else if(windowWidth < replaceWidth) {
				$this.attr('src',$this.attr('src').replace(pcName,spName)).css({visibility:'visible'});
			}
		}
		$(window).resize(function(){imgSize();});
		imgSize();
	});
});

//-----------------------------
　//タブ切り替え
//-----------------------------

$(function() {
	$('.tab li').click(function() {
		var index = $('.tab li').index(this);
		$('.content .order').css('display','none');
		$('.content .order').eq(index).css('display','block');
		$('.tab li').removeClass('select');
		$(this).addClass('select')
	});
});


$(function(){
    $('#menu li').hover(function(){
        $("ul:not(:animated)", this).slideDown();
    }, function(){
        $("ul.child",this).slideUp();
    });
});

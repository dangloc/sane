<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Factory as ValidationFactory;

class PasswordReissueRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer_id' => 'required',
            'my_password' => 'required|min:8|max:20|regex:/^[a-zA-Z0-9\s]+$/',
            'confirm_password' => 'required|min:8|max:20|required_with:my_password|same:my_password'
        ];
    }

    public function attributes()
    {
        return [
            'customer_id' => '顧客ID',
            'confirm_password' => '新しいパスワード（再入力）',
            'my_password' => '新しいパスワード'
        ];
    }

    public function messages(){
        return [
            'my_password.regex' => 'パスワードは８～２０文字の半角英数字を入力してください。',
            'confirm_password.same' => '新しいパスワードと新しいパスワード（再入力）が一致しません。'
        ];
    }
}

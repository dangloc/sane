@extends('frontend.layouts.main')
@section('content')
    <!-- Start Slider Section -->
    <div id="slider-main">
        <div class="container">
            <div class="row">

                <div class="col-xs-12 col-lg-8">
                    <div class="slider-main">
                        <div class="slider-header owl-carousel owl-theme">
                            @if(!empty($banner_list))
                                @foreach($banner_list as $banner)
                                    <div class="item">
                                        <a href="{{$banner->link_url}}" target="_blank">
                                            @if (filter_var($banner->banner_url, FILTER_VALIDATE_URL) === FALSE)
                                                <img src="{{asset(''.$banner->banner_url)}}" alt=""/>
                                            @else
                                                <img src="{{$banner->banner_url}}" alt=""/>
                                            @endif
                                        </a>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-lg-4">
                    <div class="left-content">
                        <div class="box-bg">
                            <h3 class="title"><p>お知らせ</p> Information</h3>
                            <div class="box-content">
                                @if(!empty($info_list))
                                    @foreach($info_list as $info)
                                        <div class="row-content">
                                            <div class="date"><a
                                                        href="{{route('information.detail',['id' => $info->id])}}">{{date('Y/m/d',strtotime($info->public_start_date))}}</a>
                                            </div>
                                            <div class="content"><a
                                                        href="{{route('information.detail',['id' => $info->id])}}">{{subStringDot($info->title)}}</a>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- End Slider Section -->

    <!-- Start Section Search -->
    <div id="section-search">

        <div class="container">
            <div class="title-top">
                <h3 class="title"><span>3ステップでカンタン商品検索！</span>※店舗または受取り日によってはお取扱いのない商品もございます。予めご了承ください。</h3>
            </div>
        </div>

        <form action="" method="">
            <!-- Start Step 1-->
            <div class="container">
                <div class="step1 bg-gray-blur">
                    <div class="title-step">
                        <h3 class="title">1.受取り店舗を検索</h3>
                    </div>
                    <div class="main-step">
                        <div class="row">
                            <div class="col-xs-12 col-lg-5 hidden-sp">
                                <div class="map-show">
                                    <div class="image-show">
                                        <img src="{{asset('frontend/images/common/map.png')}}">
                                    </div>
                                    <div class="area-show">
                                        <div class="northern-area area-item" ><a href="javascript:Frontend.genCitiesMap(1);"
                                                                                name="area" id="area_1">北部エリア</a>
                                        </div>
                                        <div class="chubu-area area-item"><a href="javascript:Frontend.genCitiesMap(2);"
                                                                             name="area" id="area_2">中部エリア</a></div>
                                        <div class="southern-area area-item"><a href="javascript:Frontend.genCitiesMap(3);"
                                                                                name="area" id="area_3">南部エリア</a>
                                        </div>
                                        <div class="ishigakijima area-item"><a href="javascript:Frontend.genCitiesMap(4);"
                                                                               name="area" id="area_4">石垣島</a></div>
                                        <div class="miyakojima area-item"><a href="javascript:Frontend.genCitiesMap(5);"
                                                                             name="area" id="area_5">宮古島</a></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-lg-7">
                                <div class="content-search">

                                    <div class="group-input">
                                        <div class="row">
                                            <div class="col-xs-12 col-lg-4">
                                                <label>エリア</label>
                                                <div class="area selectItem">
                                                    <select name="area" class="area-form" id="area-form">
                                                        @if(!empty($areas))
                                                            <option value="">エリアを選択</option>
                                                            @foreach($areas as $key => $area)
                                                                @if($areaId != 0 && $area->id == $areaId)
                                                                    <option selected
                                                                            value="{{$areaId}}">{{$area->name}}</option>
                                                                @else
                                                                    <option value="{{$area->id}}">{{$area->name}}</option>
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                    <i class="fas fa-caret-down"></i>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-lg-4">
                                                <label>市町村</label>
                                                <div class="city selectItem">
                                                    <select name="city" class="city-form" id="city-form">
                                                        <option value="">市町村を選択</option>
                                                    </select>
                                                    <i class="fas fa-caret-down"></i>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-lg-4">
                                                <label>店名等</label>
                                                <div class="story selectItem">
                                                    <input type="text" name="shop-search" class="shop-search"
                                                           placeholder="店名など">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <img class="lazy-load" src="{{asset('frontend/images/ajax-loader-event-date.gif')}}">
                                    <div class="searchTime">

                                        <table>
                                            <thead>
                                            <tr>
                                                <th></th>
                                                <th>店舗名</th>
                                                <th>営業時間</th>
                                            </tr>
                                            </thead>
                                            <tbody class="search-shop-body">

                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Step 1-->
            <div class="next-step">
                <div class="container">
                    <ul>
                        <li><i class="fas fa-angle-double-down"></i></li>
                        <li><i class="fas fa-angle-double-down"></i></li>
                        <li><i class="fas fa-angle-double-down"></i></li>
                    </ul>
                </div>
            </div>
            <!-- Start Step 2-->
            <div class="container">
                <div class="step2 bg-gray-blur">
                    <div class="title-step">
                        <h3 class="title">2.カテゴリを選択</h3>
                    </div>
                    <div class="main-step">
                        <img class="lazy-load" src="{{asset('frontend/images/ajax-loader-event-date.gif')}}">
                        <div class="group-btn">
                            <span class="red-text">受取り店舗を選択してください。</span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Step 2-->
            <div class="next-step">
                <div class="container">
                    <ul>
                        <li><i class="fas fa-angle-double-down"></i></li>
                        <li><i class="fas fa-angle-double-down"></i></li>
                        <li><i class="fas fa-angle-double-down"></i></li>
                    </ul>
                </div>
            </div>
            <!-- Start Step 3-->
            <div class="container">
                <div class="step3 bg-gray-blur">
                    <div class="title-step">
                        <h3 class="title">3.受取り日を指定</h3>
                    </div>
                    <div class="main-step">
                        <img class="lazy-load" src="{{asset('frontend/images/ajax-loader-event-date.gif')}}">
                        <div class="date-search">
                            <span class="red-text">カテゴリを選択してください。</span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Step 3-->
            <div class="submit-Form">
                <div class="container">
                    <div class="btn_submit">
                        <button type="button" onclick="Frontend.getProduct();"><i class="fas fa-search"></i>商品一覧を表示する
                            <img class="lazy-load" id="item-load" src="{{asset('frontend/images/ajax-loader.gif')}}">
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- End Section Search -->

    <!-- Start List Products -->
    <div id="list-products">

    </div>

    <div id ="modal-areas">

    </div>
    <!-- End List Products-->

@endsection

@section('js')
    <script type="text/javascript">
        $(document).ready(function () {
            var area_first = $('#area-form').val();
            Frontend.genShopMap(area_first,'{{$cityId}}','{{$searchShopText}}','{{$shopCode}}', '{{$eventCode}}','{{$dateReceive}}','{{$time}}');

            $('#area-form').change(function () {
                $(".shop-search").val('');
                Frontend.genShopMap($(this).val());
            });

            $('#city-form').change(function () {
                $(".shop-search").val();
                Frontend.genShopMap($('#area-form').val(), $(this).val(), '');
            });

            $(".shop-search").keyup(function () {
                Frontend.genShopMap($('#area-form').val(), $('#city-form').val(), $(this).val());
            });
        });

        var $table = $('.step1 .searchTime'),
            $bodyCells = $table.find('tbody tr:first').children(),
            colWidth;

        // Adjust the width of thead cells when window resizes
        $(window).resize(function() {
            // Get the tbody columns width array
            colWidth = $bodyCells.map(function() {
                return $(this).width();
            }).get();

            // Set the width of thead columns
            $table.find('thead tr').children().each(function(i, v) {
                $(v).width(colWidth[i]);
            });

            var width = $('.step1 .searchTime table').width();
            $('.step1 .searchTime table thead tr th').css('width',(width-5)*2/5);
            $('.step1 .searchTime table thead tr th:first').css('width',(width-5)/5);
            $('.step1 .searchTime table thead tr th:last').css('width',(width-1)*2/5);
        }).resize(); // Trigger resize handler

    </script>
@endsection


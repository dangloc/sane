<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Factory;

class InformationCreateRequest extends FormRequest
{
    public function __construct(Factory $validationFactory)
    {
        $validationFactory->extend('check_time',
            function ($attribute) {
            if (null !== request()->get('public_start_date') and null !== request()->get('public_end_date'))
            {
                $start_date = request()->get('public_start_date');
                $end_date = request()->get('public_end_date');
                $start_time = request()->get('public_start_time');
                $end_time = request()->get('public_end_time');

                $start = $start_date . $start_time;
                $end = $end_date . $end_time;
                if($end > $start){
                    return true;
                }else{
                    return false;
                }
            } else {
                return true;
            }
            },'公開期間開始≺公開期間終了になるように入力してください。'

        );

    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $rules  = [
            'content' => 'required',
            'public_start_date' => 'required|date_format:"Y/m/d"',
            'public_end_date' => 'required|date_format:"Y/m/d"|check_time',
            'public_start_time' => 'required',
            'public_end_time' => 'required',
        ];
        if ($request->session()->exists('session_data') && array_key_exists('id', $request->session()->get('session_data'))) {
            $rules['title'] = 'required|max:512|unique:informations,title,' . $request->session()->get('session_data')['id'] . ',id,del_flag,0';
        } else {
            $rules['title'] = 'required|max:512|unique:informations,title,,,del_flag,0';
        }
        return $rules;
    }
    public function attributes()
    {
        return [
            'title' => 'タイトル',
            'content' => '本文',
            'public_start_date' => '公開期間開始',
            'public_end_date' => '公開期間終了',
        ];
    }
    public function messages()
    {
        return [
            'public_start_date.date_format' => '入力された' . request('public_start_date') . 'は不正です。:attributeはYYYY/mm/dd形式で指定してください。',
            'public_end_date.date_format' => '入力された' . request('public_end_date') . 'は不正です。:attributeはYYYY/mm/dd形式で指定してください。'
        ];
    }
}
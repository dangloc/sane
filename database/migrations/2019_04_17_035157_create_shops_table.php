<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {
            $table->increments('id')->comment('ID');
            $table->char('code', 4)->comment('店舗コード')->unique();
            $table->text('name')->comment('店舗名漢字');
            $table->text('name_kana')->comment('店舗名カナ');
            $table->integer('area_id')->comment('エリアID');
            $table->string('zip_code', 10)->comment('郵便番号');
            $table->integer('city_id')->comment('市町村ID');
            $table->text('address')->comment('市町村以降');
            $table->string('tel', 30)->comment('TEL');
            $table->char('open_time',4)->comment('営業時間開始');
            $table->char('close_time',4)->comment('営業時間終了');
            $table->geometry('latlng')->comment('緯度経度');
            $table->char('del_flag', 1)->comment('削除フラグ')->default(0);
            $table->dateTime('ins_datetime')->default(DB::raw('CURRENT_TIMESTAMP'))->comment('登録日時');
            $table->integer('ins_id')->length(11)->comment('登録者ID');
            $table->dateTime('upd_datetime')->comment('更新日時')->nullable();
            $table->integer('upd_id')->length(11)->comment('更新者ID')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shops');
    }
}

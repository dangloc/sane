<?php
return [
    'LOGIN_FAILED' => 'ログインに失敗しました。',
    'TOKEN_TIMEOUT' => 'セッションの有効期限が切れています。 再度ログインを行なってください。',
    'SYSTEM_ERROR' => 'システムエラーが発生しました。',
    'DB_ERROR' => 'DB関連エラーが発生しました。',
    'HTTP401' => 'HTTP401認',
    'URL_INVALID' => 'URLが不正です',
    'NOT_FOUND' => 'URLが不正です',
    'PLEASE_LOG_IN' => '再度ログインしてください。',
    'NO_ORDER_DATA' => '注文発注データがありません。',
    'NO_DATA' => 'データがありません。',
    'ACCESS_DENIED' => '表示権限がありません。',

    'DB_INSERT' => '登録しました。',
    'DB_UPDATE' => '修正しました。',
    'DB_DELETE' => '削除しました。',
];
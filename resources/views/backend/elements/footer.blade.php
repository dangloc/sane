<div id="loading">
    <img src="{{asset('backend/images/common/ajax-loader.gif')}}" alt="">
</div>
<script src="{{ asset('backend/js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('js/laroute.js') }}"></script>
<script src="{{ asset('backend/js/responsive-tables.js') }}"></script>
<script src="{{ asset('backend/js/jquery-ui-i18n.min.js') }}"></script>
<script src="{{ asset('backend/js/jquery.slicknav.js') }}"></script>
<script src="{{ asset('backend/js/common.js') }}"></script>
<script src="{{ asset('backend/js/autoload/BackendReady.js') }}"></script>
<script src="{{ asset('common/system.js') }}"></script>
<script src="{{ asset('backend/js/autoload/Backend.js') }}"></script>
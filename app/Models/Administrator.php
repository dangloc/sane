<?php

namespace App\Models;
use App\Scopes\ActiveScope;

use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Administrator extends Model implements Authenticatable
{
    use AuthenticableTrait;

    public $timestamps = false;
    protected $_alias = 'administrators';
    protected $table = 'administrators';
    protected $fillable = [
        'name',
        'login_id',
        'password',
        'status',
        'del_flag',
        'ins_datetime',
        'ins_id',
        'upd_datetime',
        'upd_id'
    ];

    public function getAuthPassword()
    {
        return $this->password;
    }

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ActiveScope());
    }
}

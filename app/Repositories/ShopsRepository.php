<?php

namespace App\Repositories;

use function foo\func;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

/**
 * class ShopsRepository.
 *
 * @package namespace App\Repositories;
 */
class ShopsRepository extends BaseRepository
{
    function __construct()
    {
        $this->_model = \App\Models\Shop::class;
    }

    public function searchKeyUpShop($text_search = "", $areaId = "", $cityId = "")
    {
        $data = $this->getModel()->select('id', 'code', 'name', 'open_time', 'close_time')
            ->when($areaId, function ($query, $areaId) {
                return $query->where('area_id', '=', $areaId);
            })
            ->when($cityId, function ($query, $cityId) {
                return $query->where('city_id', '=', $cityId);
            })
            ->where(function ($q) use ($text_search) {
                $q->where('name', 'like', '%' . $text_search . '%')
                    ->orWhere('name', 'like', '%' . $text_search);
            })
            ->get()->toArray();
        return $data;
    }

    //  for admin search
    public function searchCondition($data)
    {
        $sort = request()->get('sort', 'code');
        $direction = request()->get('direction', 'desc');
        return $this->getModel('shops')
            ->when($data['name'] !== null, function ($query) use ($data){
                    return $query->where('name','like','%'.$data['name'].'%');
            })
            ->when($data['city_id'] !== null,function ($query) use ($data){
                return $query->where('city_id','=',$data['city_id']);
            })
            ->when($data['area_id'] !== null,function($query) use($data){
                return $query->where('area_id','=',$data['area_id']);
            })
            ->orderBy($sort, $direction)
            ->paginate(config('const.paging.backend'));

    }

    public function getShopByAreas($area_id){
        $data = $this->getModel()
            ->select('shops.id', 'shops.code', 'shops.name', 'shops.open_time', 'shops.close_time')
            ->join('cities','shops.city_id','cities.id')
            ->where('shops.area_id', '=', $area_id)
            ->where('shops.del_flag', '=', 0)
            ->where('cities.del_flag', '=', 0)
            ->withoutGlobalScopes()->distinct()->get()->toArray();
        if (!empty($data)) {
            return $data;
        }
        return array();
    }

    public function getShopByCities($city_id)
    {
        $data = $this->getModel()->select('id', 'code', 'name', 'open_time', 'close_time')->where('city_id', '=', $city_id)->get();
        if (!empty($data)) {
            return $data->toArray();
        }
        return array();
    }

    // this use for admin order screen
    public function getShopByAreaAndCity($area_id, $city_id){
        return $this->getModel()->where('area_id', '=', $area_id)->where('city_id', '=', $city_id)->get();
    }

    public function getShopInfoComplete($code)
    {
        $data = $this->getModel()->select('name','city_id', 'tel', 'address', DB::raw('AsText(latlng) as latlng'))->where('code', '=', $code)->first();
        if (!empty($data)) {
            return $data->toArray();
        }
        return array();
    }

    public function controlGeometryData($latlng)
    {
        if (!empty($latlng)) {
            $polText = substr($latlng, 6, strlen($latlng) - 7);
            $latlng = explode(' ', $polText);
            $data['lat'] = !empty($latlng[0]) ? $latlng[0] : '';
            $data['lng'] = !empty($latlng[1]) ? $latlng[1] : '';
            return $data;
        }
        return array();
    }

    public function find($id)
    {
        return $this->getModel()->select(
            'id',
            'code',
            'name',
            'name_kana',
            'area_id',
            'zip_code',
            'city_id',
            'address',
            'tel',
            'open_time',
            'close_time',
            DB::raw('AsText(latlng) as latlng'),
            'del_flag')
            ->where('id', '=', $id)
            ->where('del_flag', '=', 0)
            ->first();
    }

    public function create($data)
    {
        $data['ins_datetime'] = Carbon::now();
        $data['ins_id'] = Auth::id();
        $data['latlng'] = DB::raw("ST_GeomFromText('POINT(" . $data['latlng'] . ")')");
        $object = new $this->_model($data);
        $object->save();
        return $object->id;
    }

    public function update($id, $data)
    {
        $data['latlng'] = DB::raw("ST_GeomFromText('POINT(" . $data['latlng'] . ")')");
        return parent::update($id, $data); // TODO: Change the autogenerated stub
    }

    public function getShopOperationTime($shopCode){
        return $this->getModel()
            ->select('open_time','close_time')
            ->where('code', $shopCode)
            ->first();
    }
}

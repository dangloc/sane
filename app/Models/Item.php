<?php

namespace App\Models;
use App\Scopes\ActiveScope;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    public $timestamps = false;
    protected $_alias = 'items';
    protected $table = 'items';
    protected $fillable = [
        'shop_code',
        'code',
        'item_image_name',
        'item_image_folder_name',
        'content1',
        'content2',
        'content3',
        'required_confirm_flag',
        'limited_flag',
        'limited_count',
        'additive_code',
        'lead_time',
        'reserve_content1',
        'reserve_content2',
        'reserve_content3',
        'del_flag',
        'ins_datetime',
        'ins_id',
        'upd_datetime',
        'upd_id'
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ActiveScope());
    }
}

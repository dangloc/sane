<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\ActiveScope;

class ReserveUpdate extends Model
{
    public $timestamps = false;
    protected $_alias = 'reserve_updates';
    protected $table = 'reserve_updates';
    protected $fillable = [
        'slip_number',
        'flag',
        'del_flag',
        'ins_datetime',
        'ins_id',
        'upd_datetime',
        'upd_id'
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ActiveScope());
    }
}

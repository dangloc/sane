<?php

return [
    /*
    |--------------------------------------------------------------------------
    | バリデーション言語行
    |--------------------------------------------------------------------------
    |
    | 以下の言語行はバリデタークラスにより使用されるデフォルトのエラー
    | メッセージです。サイズルールのようにいくつかのバリデーションを
    | 持っているものもあります。メッセージはご自由に調整してください。
    |
    */
    'check_email'          => 'メールアドレスのフォーマットが不正です。',
    'check_password'       => ':attribute は８～２０文字の半角英数字を入力してください。',
    'check_card'           => ':attribute が不正です。',
    'accepted'             => ':attributeを承認してください。',
    'active_url'           => ':attributeが有効なURLではありません。',
    'after'                => ':attributeには、:dateより後の日付を指定してください。',
    'after_or_equal'       => ':attributeには、:date以前の日付を指定してください。',
    'alpha'                => ':attributeはアルファベットのみがご利用できます。',
    'alpha_dash'           => ':attributeはアルファベットとダッシュ(-)及び下線(_)がご利用できます。',
    'alpha_num'            => ':attributeはアルファベット数字がご利用できます。',
    'alpha_num_nospace'    => ':attributeは英数字のみがご利用できます。',
    'array'                => ':attributeは配列でなくてはなりません。',
    'before'               => ':attributeには、:dateより前の日付をご利用ください。',
    'before_or_equal'      => ':attributeには、:date以前の日付をご利用ください。',
    'between'              => [
        'numeric' => ':attributeは、:minから:maxの間で指定してください。',
        'file'    => ':attributeは、:min kBから、:max kBの間で指定してください。',
        'string'  => ':attributeは、:min文字から、:max文字の間で指定してください。',
        'array'   => ':attributeは、:min個から:max個の間で指定してください。',
    ],
    'boolean'              => ':attributeは、trueかfalseを指定してください。',
    'confirmed'            => ':attributeと、確認フィールドとが、一致していません。',
    'date'                 => ':attributeには有効な日付を指定してください。',
    'date_format'          => ':attributeは:format形式で指定してください。',
    'date_format_custom'   => ':attributeが日付フォーマット入力してください。（例：2018.11.02）。',
    'datetime_format_custom'   => ':attributeが日付フォーマット入力してください。（例：2018.11.02 18:00:00）。',
    'different'            => ':attributeと:otherには、異なった内容を指定してください。',
    'digits'               => ':attributeは:digits桁で指定してください。',
    'digits_between'       => ':attributeは:min桁から:max桁の間で指定してください。',
    'dimensions'           => ':attributeの図形サイズが正しくありません。',
    'distinct'             => ':attributeには異なった値を指定してください。',
    'email'                => ':attributeには、有効なメールアドレスを指定してください。',
    'exists'               => '選択された:attributeは正しくありません。',
    'file'                 => ':attributeにはファイルを指定してください。',
    'filled'               => ':attributeに値を指定してください。',
    'email_exist'          => ':attribute 未登録',
    'gt'                   => [
        'numeric' => ':attributeには、:valueより大きな値を指定してください。',
        'file'    => ':attributeには、:value kBより大きなファイルを指定してください。',
        'string'  => ':attributeは、:value文字より長く指定してください。',
        'array'   => ':attributeには、:value個より多くのアイテムを指定してください。',
    ],
    'gte'                  => [
        'numeric' => ':attributeには、:value以上の値を指定してください。',
        'file'    => ':attributeには、:value kB以上のファイルを指定してください。',
        'string'  => ':attributeは、:value文字以上で指定してください。',
        'array'   => ':attributeには、:value個以上のアイテムを指定してください。',
    ],
    'image'                => ':attributeには画像ファイルを指定してください。',
    'in'                   => '選択された:attributeは正しくありません。',
    'in_array'             => ':attributeには:otherの値を指定してください。',
    'integer'              => ':attributeは整数で指定してください。',
    'ip'                   => ':attributeには、有効なIPアドレスを指定してください。',
    'ipv4'                 => ':attributeには、有効なIPv4アドレスを指定してください。',
    'ipv6'                 => ':attributeには、有効なIPv6アドレスを指定してください。',
    'json'                 => ':attributeには、有効なJSON文字列を指定してください。',
    'lt'                   => [
        'numeric' => ':attributeには、:valueより小さな値を指定してください。',
        'file'    => ':attributeには、:value kBより小さなファイルを指定してください。',
        'string'  => ':attributeは、:value文字より短く指定してください。',
        'array'   => ':attributeには、:value個より少ないアイテムを指定してください。',
    ],
    'lte'                  => [
        'numeric' => ':attributeには、:value以下の値を指定してください。',
        'file'    => ':attributeには、:value kB以下のファイルを指定してください。',
        'string'  => ':attributeは、:value文字以下で指定してください。',
        'array'   => ':attributeには、:value個以下のアイテムを指定してください。',
    ],
    'max'                  => [
        'numeric' => ':attributeには、:max以下の数字を指定してください。',
        'file'    => ':attributeには、:max kB以下のファイルを指定してください。',
        'string'  => ':attributeは、:max文字以下で指定してください。',
        'array'   => ':attributeは:max個以下指定してください。',
    ],
    'mimes'                => ':attributeには:valuesタイプのファイルを指定してください。',
    'mimetypes'            => ':attributeには:valuesタイプのファイルを指定してください。',
    'min'                  => [
        'numeric' => ':attributeには、:min以上の数字を指定してください。',
        'file'    => ':attributeには、:min kB以上のファイルを指定してください。',
        'string'  => ':attributeは、:min文字以上で指定してください。',
        'array'   => ':attributeは:min個以上指定してください。',
    ],
    'not_in'               => '選択された:attributeは正しくありません。',
    'not_regex'            => ':attributeの形式が正しくありません。',
    'numeric'              => ':attributeには数字を指定してください。',
    'present'              => ':attributeが存在していません。',
    'regex'                => ':attributeに正しい形式を指定してください。',
    'required'             => ':attributeは必須です。',
    'required_if'          => ':otherが:valueの場合、:attributeも指定してください。',
    'required_unless'      => ':otherが:valuesでない場合、:attributeを指定してください。',
    'required_with'        => ':valuesを指定する場合は、:attributeも指定してください。',
    'required_with_all'    => ':valuesを指定する場合は、:attributeも指定してください。',
    'required_without'     => ':valuesを指定しない場合は、:attributeを指定してください。',
    'required_without_all' => ':valuesのどれも指定しない場合は、:attributeを指定してください。',
    'same'                 => ':attributeと:otherには同じ値を指定してください。',
    'size'                 => [
        'numeric' => ':attributeは:sizeを指定してください。',
        'file'    => ':attributeのファイルは、:sizeキロバイトでなくてはなりません。',
        'string'  => ':attributeは:size文字で指定してください。',
        'array'   => ':attributeは:size個指定してください。',
    ],
    'in_valid'             => ':attributeは半角英文字、アンダーバー（ _ ）のみ入力可能です。',
    'invalid'             => ':attributeが不正です。',
    'form_url'             => ':attributeは英字のみ入力可能です。',
    'unique'               => ':attributeは存在しています。',
    'custom_unique'        => ':attributeが存在しています。',
    'url'                  => ':attributeはURLのフォーマットで入力してください。',
    'greater_than_field'   => ':attribute＞:otherとなるように入力してください。',
    'check_date'           => ':attribute≧今日となるように入力してください。',
    'check_datetime'           => ':attribute≧今日となるように入力してください。',
    'greater_than_or_equal_field'   => ':attribute≧:otherとなるように入力してください。',
    'greater_than_or_equal_time_field'   => ':attribute≧:otherとなるように入力してください。',
    'less_than_field'      => ':attribute＜:otherとなるように入力してください。',
    'less_than_or_equal_field'   => ':attribute≦:otherとなるように入力してください。',
    'katakana'             => ':attributeは全角のカタカナで入力してください。',
    'not_full_width'             => ':attributeは半角文字で入力してください。',
    'array_required'             => ':attributeは必須です。',
    'with_app_type'             => ':attributeが不正です。',
    'data_type_condition' => '「メール送信可否フラグ」タイプを使用する場合、必ず「メールアドレス(送信先)」タイプの項目も設定して下さい。',
    'physical_name_unique_in_table' => ':attributeは存在しています。',
    'name_validation' => ':attributeが不正です。',
    'check_value_data_type_validation' => ':attributeを変更しないでください。',
    'no_required'             => ':attributeを入力しないでください。',
    'name_unique_in_db'             => ':attributeは存在しています。',
    'only_number' => ':attributeが数値のみ入力してください。',
    'in_' => ':attributeが０と１のみ入力してください。',
    'in_gender' => ':attributeが１と２のみ入力してください。',
    'hiragana' => ':attributeが全角ひらがなのみ入力してください。',
    'alphanumeric' => ':attributeが英語と数字のみ入力してください。',
    'check_exit_data_type_date_time' => 'Data type＝「カレンダー（枠数制限）」は一つしか登録できません。',
    'check_exit_data_type_date' => 'Data type＝日付」は一つしか登録できません。',
    'check_reserve_num' => ':attributeの受入枠数の値は、予約数以上の値を入力してください。',
    'in_upper_case' => ':attributeが小文字のみ入力してください。',
    'is_valid_physical_name' => ':attributeは使用できない値です。',
    'custom_calendar' =>':attributeが:DATE/TIMEは必須です。',
    'error_multiple_pk' =>'PKは一つしか選択できません。',
    'custom_calendar_date_required' =>':attribute DATE/TIMEの日付は必須です。',
    'table_column_id_required' =>'条件:rowの絞り込み項目は必須です。',
    'where_pattern_type_required' =>'条件:rowの絞り込みパターンは必須です。',
    'where_value_required' =>'条件:rowの絞り込み値は必須です。',
    'only_data_type12' =>'メール送信可否フラグは存在しています。',
    'custom_postal_code_prefix' => '郵便区番号は、3桁で入力してください。',
    'custom_postal_code_postfix' => '町域番号は、4桁で入力してください。',
    'timezone' => ':attributeには、有効なゾーンを指定してください。',
    'uploaded' => ':attributeのアップロードに失敗しました。',

    // validate
    'VALIDATE_IS_NUMERIC_HALF_WIDTH'    => "%sは半角数値で入力してください。",
    'VALIDATE_DATETIME_GREATER_THAN'    => "%sより未来日時の入力をしてください。",
    'VALIDATE_IS_ARRAY'                 => "%sが不正です。",
    'VALIDATE_FORMAT_HOUR'              => "%sは時刻形式で入力してください。",
    'VALIDATE_ITEMS_ARE_NUMERICS'       => "%sは不正です。",
    'VALIDATE_IS_INVALID'               => '%sが不正です。',
    'VALIDATE_RE_PASSWORD'              => '新しいパスワードと一致しません。',
    'VALIDATE_LOGIN_ID_EXISTED'         => 'メールアドレスが存在しています。',
    'VALIDATE_STORE_ID_EXISTED'         => '入力されたメールアドレスは登録済の為、登録できません。詳細はお電話にてお問い合わせください。',
    'VALIDATE_EMAIL_NOT_CORRECT'        => 'メールアドレスが不正です。',
    'VALIDATE_NO_FILE_UPLOAD'           => 'VALIDATE_NO_FILE_UPLOAD。',
    'VALIDATE_FORMAT'                   => '%sの形式が不正です。',
    'VALIDATE_DECIMAL_NUMBER'           => '%sの少数は第%s位まで入力してください。',
    'VALIDATE_KATAKANA'                 => '%sはカタカナで入力してください。',
    'VALIDATE_CANNOT_ALLOW_CHANGE'      => '%sは入力できません。',
    'VALIDATE_URL'                      => "%sはURLのフォーマットで入力してください。",
    'VALIDATE_UNIQUE_POSITION'          => '同じ商品 && 同じ台 && 同じ段(y) && 同じ位置(x) の場合には登録できません。',

    // Validate calendar in frontend
    'VALIDATE_TABLE_COLUMN_INFO_IS_NULL' => 'テーブルカラムデータ無し。',
    'VALIDATE_FORM_CALENDAR_INFO_IS_NULL' => 'フォームカレンダーデータ無し。',
    'VALIDATE_ACCEPTANCE_FLAG_ENABLE' => '受け入れるの曜日が禁止。',
    'VALIDATE_ACCEPTANCE_SPAN_START_DATETIME' => ':attributeが:timeから入力してください。', // time: 2018年01月01日
    'VALIDATE_ACCEPTANCE_SPAN_END_DATETIME' => ':attributeが:timeまで入力してください。',
    'VALIDATE_ACCEPTANCE_POSSIBLE' => '現在、登録を締め切っております。',
    'VALIDATE_ACCEPTANCE_START_DATETIME' => ':attributeが:timeから入力してください。',
    'VALIDATE_ACCEPTANCE_END_DATETIME' => ':attributeが:timeまで入力してください。', // time: 23時30分

    // Validate step mail
    'VALIDATE_WHERE_PATTERN_TYPE' => '条件:indexの選択肢。',
    'VALIDATE_WHERE_VALUE' => '条件:indexの値。',
    'VALIDATE_PHYSICAL_NAME_VALUE' => '条件:indexの値。',
    'VALIDATE_STEP_MAIL_CONDITION_INFO_REQUIRED' => '条件2を登録してください。',
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
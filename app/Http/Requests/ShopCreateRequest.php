<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Factory;

class ShopCreateRequest extends FormRequest
{
    public function __construct(Factory $validationFactory)
    {
        $validationFactory->extend('check_code',
            function ($attribute) {
                $code = request()->get('code');

                if(ctype_digit($code)){
                    return true;
                }else{
                    return false;
                }
            },'店舗コードは不正です。4桁の数字を入力してください。'

        );

    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $rules  = [
            'area_id' => 'required',
            'zip_code' => 'required|zip_code|max:10',
            'city_id' => 'required',
            'address' => 'required',
            'tel' => 'required|phone|max:30',
            'open_time' => 'required',
            'close_time' => 'required|gt:open_time',
            'latlng' => 'required',
        ];
        if ($request->session()->exists('session_data') && array_key_exists('id', $request->session()->get('session_data'))) {
            $rules['code'] = 'required|max:4|check_code|unique:shops,code,' . $request->session()->get('session_data')['id'] . ',id,del_flag,0';
            $rules['name'] = 'required|unique:shops,name,' . $request->session()->get('session_data')['id'] . ',id,del_flag,0';
            $rules['name_kana'] = 'required|unique:shops,name_kana,' . $request->session()->get('session_data')['id'] . ',id,del_flag,0|katakana';

        } else {
            $rules['code'] = 'required|max:4|check_code|unique:shops,code,,,del_flag,0';
            $rules['name'] = 'required|unique:shops,name,,,del_flag,0';
            $rules['name_kana'] = 'required|unique:shops,name_kana,,,del_flag,0|katakana';
        }
        return $rules;
    }
    public function attributes()
    {
        return [
            'code' => '店舗コード',
            'name' => '店舗名漢字',
            'name_kana' => '店舗名カナ',
            'area_id' => 'エリア',
            'zip_code' => '郵便番号',
            'city_id' => '市町村',
            'address' => '市町村以降',
            'tel' => '電話番号',
            'open_time' => '営業時間開始',
            'close_time' => '営業時間終了',
            'latlng' => '緯度経度',
        ];
    }
    public function messages()
    {
        return [
            'code.max' => '店舗コードは不正です。4桁の数字を入力してください。',
            'code.min' => '店舗コードは不正です。4桁の数字を入力してください。',
            'close_time.gt' => ':attributeには、'. formatTime(request('open_time')).'より大きな値を指定してください。'
        ];
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemBonusPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_bonus_points', function (Blueprint $table) {
            $table->char('item_code', 13)->comment('商品コード');
            $table->char('receptionist_start_date',8)->comment('受付開始日');
            $table->char('receptionist_end_date', 8)->comment('受付終了日');
            $table->char('receipt_start_date',8)->comment('受取開始日')->nullable();
            $table->char('receipt_end_date',8)->comment('受取終了日')->nullable();
            $table->char('del_flag', 1)->comment('削除フラグ')->default(0);
            $table->dateTime('ins_datetime')->default(DB::raw('CURRENT_TIMESTAMP'))->comment('登録日時');
            $table->integer('ins_id')->length(11)->comment('登録者ID');
            $table->dateTime('upd_datetime')->comment('更新日時')->nullable();
            $table->integer('upd_id')->length(11)->comment('更新者ID')->nullable();

            $table->primary('item_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_bonus_points');
    }
}

@extends('backend.layouts.main')
@section('content')

    <div id="Contentwrap">
        <div id="PageContent" class="no_mar_bottom">
            <h2><i class="fas fa-image"></i> バナー登録</h2>
            <div class="order">
                <table class="sp_block flowtable">
                    <tr>
                        <th scope="row" style="width: 150px;">バナー画像</th>
                        <td>
                            <img src="{{asset(Session::get('banner_url_temp'))}}" width="260px">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" style="width: 150px;">リンク先URL</th>
                        <td>
                            {{Session::get('data')['link_url']}}
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" style="width: 150px;">公開期間</th>
                        <td>
                            {{Session::get('data')['public_start_date']}}〜{{Session::get('data')['public_end_date']}}
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" style="width: 150px;">ステータス</th>
                        <td>
                            {{config('const.status_banner')[Session::get('data')['status']]}}
                        </td>
                    </tr>
                </table>
            </div>
            <!--ContentFoot -->
        </div>
        <!--PageContent -->
        <form id="form_banner_confirm_create" action="{{ route('banner.store') }}" method="POST">
            {{ csrf_field() }}
            <div class="contentfoot">
                <p class="btn Searchclear">
                    <button type="button" value="" onclick="System.redirectNoLoading('{{ route('banner.create',['flag' => config('const.flag')['from_confirm']])}}')"><i
                            class="fas fa-undo"></i> 戻る
                    </button>
                </p>
                <p class="btn register">
                    <button type="button" onclick="System.showDialog('banner','confirm_create')"
                            class="opener"><i class="fas fa-check"></i> 登録する
                    </button>
                </p>
            </div>
        </form>
        <!--PageContent -->

    </div>
    <!--Contentwrap -->
@endsection
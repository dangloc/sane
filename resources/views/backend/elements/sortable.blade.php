<?php
$sort = request()->get('sort', 'id');
$direction = request()->get('direction', 'desc');

$class = '';
$params = request()->input();
$params['sort'] = $field;

if ($sort == $field) {
    $params['direction'] = $direction == 'asc' ? 'desc' : 'asc';
    $class = 'fa fa-sort-' . ($direction == 'desc' ? 'down' : 'up');
} else {
    $params['direction'] = 'asc';
}

$url = request()->url();
$link = $url . '?' . http_build_query($params);

echo '<a class="' . $class . '" href="'. $link .'">'. $title .'</a>';
?>


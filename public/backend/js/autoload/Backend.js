var Backend = {};

Backend.getItemDetail = function (shopCode, itemCode) {
    $.ajax({
        url: laroute.route('item.detail'),
        data: {shop_code: shopCode, item_code: itemCode},
        type: 'get',
        dataType: 'json',
        success: function (res) {
            $('#dialog_item_detail').html(res.data);
            System.showDialogDetail('item_detail');

            System.hideLoading();
        }
    });

    return false;
};
Backend.showDialog = function (model, name, id) {
    $('#dialog_' + name).dialog({
        autoOpen: false,
        modal: true,
        buttons: {
            "OK": function () {
                $(this).dialog('close');
                if (typeof id == "undefined") {
                    id = '';
                }
                System.showLoading();
                $('#form_' + model + '_' + name + id).submit();
            }
        }
    }).dialog("open");
};


// ajax admin : shop screen + order screen
Backend.getCity = function () {
    $(".div-city").show();
    // $(".shop-select option[value='']").remove();
    let area_id = $(".area-select").val();
    if (area_id === '') {
        $('select[name="shop_code"]').empty();
        $('select[name="shop_code"]').append($("<option value=''>店舗</option>"));
        $('select[name="city_id"]').empty();
        $('select[name="city_id"]').append($("<option value=''>市町村</option>"));
    } else {
        $.support.cors = true;
        $.ajax({
            // headers: {
            //     'X-CSRF-TOKEN': $('input[name="_token"]').val()
            // },
            method: 'GET',
            // dataType: 'json',
            crossDomain: true,
            cache: false,
            url: baseUrl + '/management/shop/' + area_id + '/city' ,
            // data: data,
            success: function (response) {
                $('select[name="shop_code"]').empty();
                $('select[name="shop_code"]').append($("<option value=''>店舗</option>"));
                $('select[name="city_id"]').empty();
                $('select[name="city_id"]').append($("<option value=''>市町村</option>"));
                for (var i = 0; i < response.length; i++) {
                    var option = new Option(response[i]['name'], response[i]['id']);
                    $('select[name="city_id"]').append(option);
                }
            },
            error: function (exception) {
                console.log("Error : ", exception);
            }
        });
    }

};

// ajax admin : order screen
Backend.getShop = function () {
    $(".div-shop").show();
    // $(".shop-select option[value='']").remove();
    let area_id = $(".area-select").val();
    let city_id = $(".city-select").val();
    if (city_id === '') {
        $('select[name="shop_code"]').empty();
        $('select[name="shop_code"]').append($("<option value=''>店舗</option>"));
    } else {
        $.support.cors = true;
        $.ajax({
            // headers: {
            //     'X-CSRF-TOKEN': $('input[name="_token"]').val()
            // },
            method: 'GET',
            // dataType: 'json',
            crossDomain: true,
            cache: false,
            url: baseUrl + '/management/order/' + area_id + '/' + city_id + '/shop',
            // data: data,
            success: function (response) {
                $('select[name="shop_code"]').empty();
                $('select[name="shop_code"]').append($("<option value=''>店舗</option>"));
                for (var i = 0; i < response.length; i++) {
                    var option = new Option(response[i]['name'], response[i]['code']);
                    $('select[name="shop_code"]').append(option);
                }
            },
            error: function (exception) {
                console.log("Error : ", exception);
            }
        });
    }
};
Backend.pickStoreUnit = function () {
      $(".div-select-store").show();
};
Backend.pickItemUnit = function () {
    $(".div-select-store").hide();
};
Backend.getEvent = function () {
    // $(".div-shop").show();
    // $(".shop-select option[value='']").remove();
    let category_code = $(".category-select").val();
    if (category_code === '') {
        $('select[name="event_code"]').empty();
        $('select[name="event_code"]').append($("<option value=''>行事</option>"));
    } else {
        $.support.cors = true;
        $.ajax({
            // headers: {
            //     'X-CSRF-TOKEN': $('input[name="_token"]').val()
            // },
            method: 'GET',
            // dataType: 'json',
            crossDomain: true,
            cache: false,
            url: baseUrl + '/management/item/' + category_code + '/event',
            // data: data,
            success: function (response) {
                console.log(response);
                $('select[name="event_code"]').empty();
                $('select[name="event_code"]').append($("<option value=''>行事</option>"));
                for (var i = 0; i < response.length; i++) {
                    var option = new Option(response[i]['name'], response[i]['code']);
                    $('select[name="event_code"]').append(option);
                }
            },
            error: function (exception) {
                console.log("Error : ", exception);
            }
        });
    }
};
Backend.showDialogUpdateStopFlag = function (item_code, shop_code) {
    var flag = $('.' + item_code + '-' + shop_code).html() == '販売中' ? 1 : 0;
    var dialog_id = flag == 1 ? 'dialog_confirm_stop_selling' : 'dialog_confirm_selling' ;
    $('#' + dialog_id).dialog({
        autoOpen: false,
        modal: true,
        buttons: {
            "OK": function () {
                $(this).dialog('close');
                Backend.updateStopFlag(item_code, shop_code, flag);
                $('#' + dialog_id).dialog("close");
            }
        }
    }).dialog("open");
};
Backend.updateStopFlag = function (item_code, shop_code, flag) {
    $('#loading').show();


    $.support.cors = true;
    $.ajax({
        // headers: {
        //     'X-CSRF-TOKEN': $('input[name="_token"]').val()
        // },
        method: 'GET',
        dataType: 'json',
        crossDomain: true,
        cache: false,
        url: baseUrl + '/management/item/' + item_code + '/' + shop_code + '/' + flag,
        // data: data,
        success: function (response) {
            $('#loading').hide();
            if (response.value == 1) {
                $('.' + item_code + '-' + shop_code).html(flag == 0 ? '販売中' : '販売停止');
                if($('.'+item_code+'-'+shop_code).hasClass('stop-selling')){
                    $('.'+item_code+'-'+shop_code).removeClass('stop-selling');
                }else{
                    $('.'+item_code+'-'+shop_code).addClass('stop-selling');
                }
            } else {
                alert("商品ステータス更新に失敗しました。")
            }
        },
        error: function (exception) {
            console.log("Error : ", exception);
        }
    });

};

Backend.checkRequire = function (array_element, form_element) {
    var result = true;
    array_element.forEach(function (element) {
        if ($('#' + element).val() == '' ){
            $('#' + element + '_error').show();
            result = (result && false)
        } else {
            $('#' + element + '_error').hide();
        }
    });
    if (result){
        System.showLoading();
        $('#' + form_element).submit();
    }

};



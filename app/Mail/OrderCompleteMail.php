<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class OrderCompleteMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The demo object instance.
     *
     * @var Order
     */
    public $order;
    public $email;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order, $email = false)
    {
        $this->order = $order;
        $this->email = $email !== false ? $email : env('MAIL_USERNAME');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->email)
            ->view('frontend.site.mail_order_complete')
            ->subject("【サンエーネット予約サービス】ご予約を受け付けました(伝票番号：".$this->order['slip_number'].")");
    }
}

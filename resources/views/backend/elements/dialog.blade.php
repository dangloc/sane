<div class="dialog" id="dialog_confirm_delete" title="削除確認">
    <p>削除します。よろしいですか？<br />よろしければ、[OK]ボタンをクリックしてください。</p><p class="f14 mt05">削除しない場合は「×」ボタンで閉じてください。</p>
</div>

<div class="dialog" id="dialog_confirm_edit" title="編集確認">
    <p>編集します。よろしいですか？<br />よろしければ、[OK]ボタンをクリックしてください。</p><p class="f14 mt05">編集しない場合は「×」ボタンで閉じてください。</p>
</div>

<div class="dialog" id="dialog_confirm_create" title="登録確認">
    <p>登録します。よろしいですか？<br />よろしければ、[OK]ボタンをクリックしてください。</p><p class="f14 mt05">登録しない場合は「×」ボタンで閉じてください。</p>
</div>

<div class="dialog" id="dialog_confirm_stop_selling" title="変更">
    <p>販売停止にします。よろしいですか？<br />よろしければ、[OK]ボタンをクリックしてください。</p><p class="f14 mt05">変更しない場合は「×」ボタンで閉じてください。</p>
</div>
<div class="dialog" id="dialog_confirm_selling" title="変更">
    <p>販売中にします。よろしいですか？<br />よろしければ、[OK]ボタンをクリックしてください。</p><p class="f14 mt05">変更しない場合は「×」ボタンで閉じてください。</p>
</div>
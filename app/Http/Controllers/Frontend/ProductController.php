<?php

namespace App\Http\Controllers\Frontend;

use App\Repositories\AddToCartRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\EventRepository;
use App\Repositories\ItemBonusPointRepository;
use App\Repositories\ItemRepository;
use App\Repositories\ShopsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class ProductController extends Controller
{
    private $item;
    private $item_bonus_point;
    private $category;
    private $shop;
    private $event;
    public $addToCartRepository;

    public function __construct(AddToCartRepository $addToCartRepository, ItemRepository $item, ItemBonusPointRepository $item_bonus_point, CategoryRepository $category, ShopsRepository $shop, EventRepository $event)
    {
        $this->item = $item;
        $this->item_bonus_point = $item_bonus_point;
        $this->category = $category;
        $this->shop = $shop;
        $this->event = $event;
        $this->addToCartRepository = $addToCartRepository;
    }

    public function detail(Request $request, $id, $shop_code, $keyitem, $keyproduct)
    {
        if (!empty($keyitem) && !empty($keyproduct)){
            if ($request->session()->has(config('const.session.cart'))) {
                $dataCartList = $request->session()->get(config('const.session.cart'));
                $dataCartList = $this->addToCartRepository->getFullDataSessionCart($dataCartList);
                if (!empty($dataCartList[$keyitem]['itemList'][$keyproduct])){
                    $dataProduct = $dataCartList[$keyitem]['itemList'][$keyproduct];
                    $dataItem = $dataCartList[$keyitem];
                    return view('frontend.product.detail_cart', compact('dataItem','dataProduct'));
                }
            }
        }else if (checkSessionTopExist($request)) {
            $shop = $this->shop->findByAttribute('code', $shop_code, 'name');
            $item = $this->item->getItemDetail($id, $shop_code);
            if ($item == null) {
                abort(404);
            }
            $category_code = $request->session()->get( config('const.session.event_code'));
            $category = $this->event->findByAttribute('code', $category_code, 'name');
            $date_receive = Session::get(config('const.session.date_receive'))['year'] . Session::get(config('const.session.date_receive'))['month'] . Session::get(config('const.session.date_receive'))['day'];
            $search_item_bonus = $this->item_bonus_point->searchItemBonus($id, $date_receive);
            return view('frontend.product.detail', compact('id', 'item', 'search_item_bonus', 'category', 'shop','date_receive'));
        }

        abort(404);
    }
}

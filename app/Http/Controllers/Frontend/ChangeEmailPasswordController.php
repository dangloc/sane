<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests\ChangePassEntryRequest;
use App\Repositories\CustomerRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class ChangeEmailPasswordController extends Controller
{
    protected $customerRepository;
    public function __construct(CustomerRepository $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    public function changePasswordEntry()
    {
        if(Session::has('success')){
            return view('frontend.mypage.change_password_entry');
        }
        if(!Auth::guard(\config('const.guard.frontend'))->check()){
            abort(404);
        }
        return view('frontend.mypage.change_password_entry');
    }

    public function changePasswordEntryAction(ChangePassEntryRequest $request)
    {
        if ($request->post()) {
            $input = $request->post();
            $password = $input['new_password'];
            $email = $input['email'];
            $data_update = array();
            if(empty($email) && empty($password)){
                return redirect()->back()->with('error_not_blank','メールアドレス又はパスワードを入力してください。');
            }
            if (!empty($password)) {
                $data_update += array('password' => bcrypt($password));
            }
            if (!empty($email)) {
                $data_update += array('email' => $email);
            }
            try {
                $this->customerRepository->update(Auth::guard(config('const.guard.frontend'))->id(), $data_update);
                Auth::guard(\config('const.guard.frontend'))->logout();
                return redirect()->back()->with('success','');
            } catch (\Exception $e) {

            }
        }
        return redirect()->back()->withInput();
    }

}

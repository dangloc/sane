@extends('backend.layouts.main')
@section('content')
    <div id="Contentwrap">
        @include('common.message_notification')
        <div id="PageContent">
            <form action="{{route('shop.index')}}" method="get">
                <h2><i class="fas fa-search"></i> 店舗検索</h2>
                <div class="order">
                    <table class="sp_block">
                        <tr>
                            <th scope="row" style="width: 150px;">エリア</th>
                            <td colspan="4">
                                <div class="custom div-area">
                                    <select class="area-select" name="area_id" tabindex="10" onchange="System.changeCity('{{route('shop.getCity')}}',this)">
                                        <option value="">エリア</option>
                                        @foreach($areas as $area)
                                        <option value="{{$area->id}}" {{old('area_id') == $area->id ? 'selected' : ''}} onchange="System.changeCity('{{route('shop.getCity')}}',{{$area->id}})">{{$area->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="custom div-city">
                                    <select class="city-select" name="city_id" tabindex="10">
                                        <option value="">市町村</option>
                                           @if($cities !== [])
                                               @foreach($cities as $city)
                                                    <option value="{{$city['id']}}" {{old('city_id') !== null ? 'selected' : ''}}>{{$city['name']}}</option>
                                               @endforeach
                                           @endif
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row" style="width: 150px;">店舗名</th>
                            <td colspan="4">
                                <div class="inline_block rel sp-w45 full_input">
                                    <input type="text" placeholder="店舗名" tabindex="5" name="name" value="{{old('name') !== null ? old('name') : ''}}">
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="contentfoot">
                    <p class="btn Searchclear">
                        <button type="button" onclick="System.showLoading()">検索条件をクリア
                        </button>
                    </p>
                    <p class="btn Search">
                        <button type="submit" value="" tabindex="13" onclick="System.showLoading()"><img
                                src="{{ asset('backend/images/common/icon3.png') }}" alt="検索">検索
                        </button>
                    </p>
                </div>
                <!--ContentFoot -->
            </form>
        </div>
        <!--PageContent -->

        <div id="PageContent" class="pt60">
            <h2><i class="fas fa-shapes"></i> 店舗一覧<span class="btn register btnMedium sp-mt-neg-55" style="margin-top:-40px;"><button
                        type="button" onclick="System.redirectNoLoading('{{ route('shop.create') }}')" tabindex="14"><i
                            class="fas fa-plus"></i> 店舗登録</button></span></h2>

            <div class="PageContentNav">
                @include('backend/elements/paging', ['results' => $listData])
                {{ $listData->appends(request()->input())->links() }}
            </div>
            <table class="OrderForm responsive tdcenter Bordertd TextL table-hover">
                <tr>
                    <th scope="col" width="100px">@include("backend/elements.sortable", ["field" => "code", "title" => "店舗コード"])</th>
                    <th scope="col">@include("backend/elements.sortable", ["field" => "name", "title" => "店舗名漢字"])</th>
                    <th scope="col">@include("backend/elements.sortable", ["field" => "name_kana", "title" => "店舗名カナ"])</th>
                    <th scope="col" width="100px">@include("backend/elements.sortable", ["field" => "area_id", "title" => "エリア"])</th>
                    <th scope="col" width="100px">@include("backend/elements.sortable", ["field" => "city_id", "title" => "市区町村"])</th>
                    <th scope="col" width="130px">@include("backend/elements.sortable", ["field" => "open_time", "title" => "営業時間"])</th>
                    <th scope="col" width="120px">編集</th>
                    <th scope="col" width="120px">削除</th>
                </tr>
                @if(!$listData->isEmpty())
                    @foreach($listData as $shop)
                        <tr>
                            <td style="text-align:left;">{{$shop->code}}</td>
                            <td style="text-align:left;">{{$shop->name}}</td>
                            <td style="text-align:left;">{{$shop->name_kana}}</td>
                            <td style="text-align:left;">{{$shop->area_id}}</td>
                            <td style="text-align:left;">{{$shop->city_id}}</td>
                            <td style="text-align:center;">
                                {{$shop->open_time}}:00~{{$shop->close_time}}:00
                            </td>
                            <td><p class="btnmini Edit"><a href="{{route('shop.edit',$shop->id)}}" class="TextC">編集</a></p></td>
                            <td>
                                <form id="{{'form_shop_confirm_delete'.$shop->id}}" method="post"
                                      action="{{ route('shop.destroy', $shop->id) }}">
                                    {{ csrf_field() }}
                                    @method('DELETE')
                                    <p class="btnmini">
                                        <button type="button"
                                                onclick="Backend.showDialog('shop', 'confirm_delete', {{$shop->id}})">
                                            削除
                                        </button>
                                    </p>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @else
                    @include('backend/elements/no_data',['col' => 8])
                @endif
            </table>
            <div class="PageContentNav">
                {{ $listData->appends(request()->input())->links() }}
            </div>
        </div>
        <!--PageContent -->
    </div>
    <!--Contentwrap -->
@endsection

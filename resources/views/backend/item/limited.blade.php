@extends('backend.layouts.main')
@section('content')

    <div id="Contentwrap">
        <div id="PageContent">
            <h2><i class="fas fa-search"></i> 商品別限定注文可能数マスター検索</h2>
            <form action="{{route('item.limited')}}" method="get">
                <div class="order">
                    <table class="sp_block">
                        <tr>
                            <th scope="row" style="width: 150px;">商品コード</th>
                            <td colspan="4">
                                <div class="inline_block rel sp-w45 line">
                                    <input type="text" placeholder="商品コード" tabindex="5" name="item_code" value="{{isset($params['item_code']) ? $params['item_code'] : ''}}">
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="contentfoot">
                    <p class="btn Searchclear">
                        <button type="button" onclick="System.redirectLoading('{{ route('item.limited') }}')" value="">検索条件をクリア</button>
                    </p>
                    <p class="btn Search">
                        <button type="submit" onclick="System.showLoading()" value="" tabindex="13"><img src="{{ asset('backend/images/common/icon3.png') }}" alt="検索">検索</button>
                    </p>
                </div>
                <!--ContentFoot -->
            </form>
        </div>
        <!--PageContent -->

        <div id="PageContent" class="pt60">
            <h2><i class="fas fa-shapes"></i> 商品別限定注文可能数マスター一覧</h2>

            <div class="PageContentNav">
                @include('backend.elements.paging', ['results' => $listData])
                {{ $listData->appends(request()->input())->links() }}
            </div>
            <table class="OrderForm responsive tdcenter Bordertd TextL table-hover">
                <tr>
                    <th scope="col">@include("backend.elements.sortable", ["field" => "item_code", "title" => "商品コード"])</th>
                    <th scope="col">@include("backend.elements.sortable", ["field" => "order_possible_count", "title" => "限定注文可能数"])</th>
                </tr>
                @if(!$listData->isEmpty())
                    @foreach($listData as $item)
                        <tr>
                            <td>{{$item->item_code}}</td>
                            <td style="text-align:center;">{{number_format($item->order_possible_count)}}</td>
                        </tr>
                    @endforeach
                @else
                    @include('backend.elements.no_data',['col' => 2])
                @endif
            </table>
            <div class="PageContentNav">
                {{ $listData->appends(request()->input())->links() }}
            </div>
        </div>
        <!--PageContent -->

    </div>
    <!--Contentwrap -->
@endsection
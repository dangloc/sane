@extends('backend.layouts.main')
@section('content')

    <div id="Contentwrap">
        <div id="PageContent">
            <h2><i class="fas fa-search"></i> 商品マスター検索</h2>
            <form action="{{route('item.index')}}" method="get">
                <div class="order">
                    <table class="sp_block">
                        <tr>
                            <th scope="row" style="width: 150px;">店舗コード</th>
                            <td colspan="4">
                                <div class="inline_block rel sp-w45 line">
                                    <input type="text" placeholder="店舗コード" tabindex="5" name="shop_code" value="{{isset($params['shop_code']) ? $params['shop_code'] : ''}}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row" style="width: 150px;">商品コード</th>
                            <td colspan="4">
                                <div class="inline_block rel sp-w45 line">
                                    <input type="text" placeholder="商品コード" tabindex="5" name="code" value="{{isset($params['code']) ? $params['code'] : ''}}">
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="contentfoot">
                    <p class="btn Searchclear">
                        <button type="button" value="" onclick="System.redirectLoading('{{ route('item.index') }}')">検索条件をクリア</button>
                    </p>
                    <p class="btn Search">
                        <button type="submit" value="" onclick="System.showLoading()" tabindex="13"><img src="{{ asset('backend/images/common/icon3.png') }}" alt="検索">検索</button>
                    </p>
                </div>
                <!--ContentFoot -->
            </form>
        </div>
        <!--PageContent -->

        <div id="PageContent" class="pt60">
            <h2><i class="fas fa-shapes"></i> 商品マスター一覧</h2>

            <div class="PageContentNav">
                @include('backend.elements.paging', ['results' => $listData])
                {{ $listData->appends(request()->input())->links() }}
            </div>
            <table class="OrderForm responsive tdcenter Bordertd TextL table-hover">
                <tr>
                    <th scope="col" width="100px">@include("backend.elements.sortable", ["field" => "shop_code", "title" => "店舗コード"])</th>
                    <th scope="col" width="100px">@include("backend.elements.sortable", ["field" => "code", "title" => "商品コード"])</th>
                    <th scope="col">@include("backend.elements.sortable", ["field" => "content1", "title" => "項目1"])</th>
                    <th scope="col" width="120px">詳細</th>
                </tr>
                @if(!$listData->isEmpty())
                    @foreach($listData as $item)
                        <tr>
                            <td>{{$item->shop_code}}</td>
                            <td>{{$item->code}}</td>
                            <td style="text-align:left;">{{$item->content1}}</td>
                            <td>
                                <p class="btnmini Edit">
                                    <a onclick="Backend.getItemDetail('{{$item->shop_code}}', '{{$item->code}}')" class="TextC opener label">詳細</a>
                                </p>
                            </td>
                        </tr>
                    @endforeach
                @else
                    @include('backend.elements.no_data',['col' => 4])
                @endif
            </table>
            <div class="PageContentNav">
                {{ $listData->appends(request()->input())->links() }}
            </div>
        </div>
        <!--PageContent -->

    </div>
    <!--Contentwrap -->
    <div class="dialog" id="dialog_item_detail" title="詳細" style="width: auto !important;">

    </div>
@endsection

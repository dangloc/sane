<?php

namespace App\Models;

use App\Scopes\ActiveScope;
use Illuminate\Database\Eloquent\Model;

class EventView extends Model
{
    public $timestamps = false;
    protected $_alias = 'event_views';
    protected $table='event_views';
    protected $fillable=[
        'event_code',
        'sort',
        'del_flag',
        'ins_datetime',
        'ins_id',
        'upd_datetime',
        'upd_id'
    ];
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ActiveScope());
    }
}

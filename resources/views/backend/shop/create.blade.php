@extends('backend.layouts.main')
@section('content')
    <div id="Contentwrap">
        <form method="post"
              action="{{ array_key_exists('id', Session::get('session_data')) ? route('shop.edit.confirm', $shop['id']) : route('shop.create.confirm')}}">
            {{ csrf_field() }}
        <div class="contenttop detail-btn-group">
            <p class="btn Searchclear">
                <button type="button" onclick="window.location='{{ route("shop.index") }}'"><i class="fas fa-list"></i> 一覧ページに戻る</button>
            </p>
        </div>

        <div id="PageContent" class="no_mar_bottom">
            <h2><i class="fas fa-shapes"></i> 店舗{{ array_key_exists('id', Session::get('session_data')) ? '編集' : '登録'}}</h2>
            @if(array_key_exists('id', Session::get('session_data')))
                <input style="display: none" name="id" value="{{$shop['id']}}">
            @endif
            <div class="order">
                <table class="sp_block flowtable">
                    <tr>
                        <th scope="row" style="width: 150px;">店舗コード<span class="req"></span></th>
                        <td>
                            <div class="line required medium_input">
                                <input type="text" tabindex="7" placeholder="店舗コード" name="code" value="{{ old('code', isset($shop) ? $shop['code'] : '') }}">
                            </div>
                            @include('backend.elements.validate_error', ['message' => $errors->first('code')])
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" style="width: 150px;">店舗名漢字<span class="req"></span></th>
                        <td>
                            <div class="line required full_input">
                                <input type="text" tabindex="7" placeholder="店舗名漢字" name="name" value="{{ old('name', isset($shop) ? $shop['name'] : '') }}">
                            </div>
                            @include('backend.elements.validate_error', ['message' => $errors->first('name')])
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" style="width: 150px;">店舗名カナ<span class="req"></span></th>
                        <td>
                            <div class="line required full_input">
                                <input type="text" tabindex="7" placeholder="店舗名カナ" name="name_kana" value="{{ old('name_kana', isset($shop) ? $shop['name_kana'] : '') }}">
                            </div>
                            @include('backend.elements.validate_error', ['message' => $errors->first('name_kana')])
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" style="width: 150px;">エリア<span class="req"></span></th>
                        <td>
                            <div class="line required">
                                <div class="custom">
                                    <select class="area-select" name="area_id" tabindex="10" onchange="Backend.getCity()">
                                        <option value="">エリア</option>
                                        @foreach($areas as $area)
                                            <option value="{{$area->id}}" {{ null !== $area_id && $area_id == $area->id ? 'selected' : '' }}>{{$area->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @include('backend.elements.validate_error', ['message' => $errors->first('area_id')])
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" style="width: 150px;">郵便番号<span class="req"></span></th>
                        <td>
                            <div class="line required medium_input">
                                <input type="text" tabindex="8" placeholder="郵便番号" name="zip_code" value="{{ old('zip_code', isset($shop) ? $shop['zip_code'] : '') }}">
                            </div>
                            @include('backend.elements.validate_error', ['message' => $errors->first('zip_code')])
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" style="width: 150px;">市町村<span class="req"></span></th>
                        <td>
                            <div class="line required">
                                <div class="custom">
                                    <select class="city-select" name="city_id" tabindex="10" id="city">
                                        <option value="">市町村</option>
                                        @foreach($cities as $city)
                                            <option value="{{$city->id}}" {{ null !== $city_id && $city_id == $city->id ? 'selected' : '' }}>{{$city->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @include('backend.elements.validate_error', ['message' => $errors->first('city_id')])
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" style="width: 150px;">市町村以降<span class="req"></span></th>
                        <td>
                            <div class="line required full_input">
                                <input type="text" tabindex="8" id="address" placeholder="市町村以降" name="address"  value="{{ old('address', isset($shop) ? $shop['address'] : '') }}">
                            </div>
                            @include('backend.elements.validate_error', ['message' => $errors->first('address')])
                            <div id="map" style="height: 500px; width: 100%; margin-top: 10px;"></div>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" style="width: 150px;">経度緯度<span class="req"></span></th>
                        <td>
                            <div class="line required medium-input">
                                <input type="text" placeholder="26.344877, 127.873479" name="latlng" id="latlng" readonly="readonly" style="cursor: not-allowed;" value="{{ old('latlng', isset($shop) ? $shop['latlng'] : '') }}">
                            </div>
                            @include('backend.elements.validate_error', ['message' => $errors->first('latlng')])
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" style="width: 150px;">電話番号<span class="req"></span></th>
                        <td>
                            <div class="line required medium_input">
                                <input type="text" tabindex="10" placeholder="電話番号" name="tel" value="{{ old('tel', isset($shop) ? $shop['tel'] : '') }}">
                            </div>
                            @include('backend.elements.validate_error', ['message' => $errors->first('tel')])
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" style="width: 150px;">営業時間<span class="req"></span></th>
                        <td>
                            <div class="line required">
                                @include('backend.elements.hourselection',['id'=>'open_time','name'=>'open_time','hourSelected' => old('open_time', isset($shop) ? $shop['open_time'] : '')])
                                〜
                                @include('backend.elements.hourselection',['id'=>'close_time','name'=>'close_time','hourSelected' => old('close_time', isset($shop) ? $shop['close_time'] : '')])
                            </div>
                            @include('backend.elements.validate_error', ['message' => $errors->first('close_time')])
                        </td>
                    </tr>
                </table>
            </div>
            <!--ContentFoot -->
        </div>
        <!--PageContent -->

        <div class="contentfoot">
            <p class="btn clear">
                <button type="button" onclick="System.redirectNoLoading('{{ route('shop.index') }}')"><i class="fas fa-list"></i> 一覧ページに戻る</button>
            </p>
            <p class="btn register">
                <button type="submit" onclick="System.showLoading()"><i class="fas fa-check"></i> 確認する</button>
            </p>
        </div>
        <!--PageContent -->
        </form>
    </div>
    <!--Contentwrap -->
    <script src="{{asset('backend/js/google-map-api.js')}}"></script>
    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key={{config('const.GOOGLE_API_KEY')}}&callback=initMap&language=ja">
    </script>
@endsection
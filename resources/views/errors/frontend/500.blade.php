@extends('frontend.layouts.main')
@section('css')
    <link rel="stylesheet" href="/frontend/css/error.css">
@endsection
@section('content')
    <div class="error">
        <div class="container">
            <div class="error_title">
                <div class="description">
                    システムエラーが発生しました。
                </div>
            </div>
            <div class="button">
                <button class="button_back" onclick="location.href='{{route('site.index')}}'">トップページへ戻る</button>
            </div>
        </div>
    </div>
@endsection
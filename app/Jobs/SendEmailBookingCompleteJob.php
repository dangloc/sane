<?php

namespace App\Jobs;

use App\Mail\OrderCompleteMail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendEmailBookingCompleteJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $order;
    public $email;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $order)
    {
        $this->order = $order;
        $this->email = $email;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            Mail::to($this->email)->send(new OrderCompleteMail($this->order, env('MAIL_USERNAME')));
            $slipNumber = !empty($this->order['slip_number']) ? $this->order['slip_number'] : "";
            Log::info('Send mail success', array('slip_number' => $slipNumber));
        } catch (\Exception $e) {
            Log::error('SendMail Booking Complete Error: ', ['message' => $e->getMessage()]);
        }
    }
}

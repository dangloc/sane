@extends('backend.layouts.main')
@section('content')

    <div id="Contentwrap">
        <div id="PageContent">
            <h2><i class="fas fa-search"></i> 商品検索</h2>
            <form action="{{route('item.list')}}" method="get" id="item_list" autocomplete="off">
            <div class="order">
                <table class="sp_block">
                    <tr class="div-select-store">
                        <th scope="row" style="width: 150px;">店舗<span class="req"></span></th>
                        <td colspan="4">
                            <div class="custom div-area">
                                <select class="area-select" name="area_id" tabindex="10"
                                        onchange="Backend.getCity()">
                                    <option value="">エリア</option>
                                    @foreach($areas as $area)
                                        <option
                                            value="{{$area->id}}" {{ null !== $area_id && $area_id == $area->id ? 'selected' : '' }}>{{$area->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <br class="sp">
                            <div class="custom div-city">
                                <select class="city-select" name="city_id" tabindex="10"
                                        onchange="Backend.getShop()">
                                    <option value="">市町村</option>
                                    @foreach($cities as $city)
                                        <option
                                            value="{{$city->id}}" {{ null !== $city_id && $city_id == $city->id ? 'selected' : '' }}>{{$city->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <br class="sp">
                            <div class="custom div-shop">
                                <select class="shop-select" name="shop_code" id="shop_code" tabindex="10">
                                    <option value="">店舗</option>
                                    @foreach($shops as $shop)
                                        <option
                                            value="{{$shop->code}}" {{ null !== $shop_code && $shop_code == $shop->code ? 'selected' : '' }}>{{$shop->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div id="shop_code_error" class="display-n red-text">店舗を入力してください。</div>
                        </td>
                    </tr>
                    <tr class="div-select-event">
                        <th scope="row" style="width: 150px;">行事<span class="req"></span></th>
                        <td colspan="4">
                            <div class="custom div-category">
                                <select class="category-select" name="category_code" tabindex="10"
                                onchange="Backend.getEvent()">
                                    <option>カテゴリー</option>
                                    @foreach($categories as $category)
                                        <option
                                            value="{{$category->code}}" {{ null !== $category_code && $category_code == $category->code ? 'selected' : '' }}>{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="custom div-event">
                                <select class="event-select" name="event_code" id="event_code" tabindex="10">
                                    <option value="">行事</option>
                                    @foreach($events as $event)
                                        <option
                                            value="{{$event->code}}" {{ null !== $event_code && $event_code == $event->code ? 'selected' : '' }}>{{$event->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div id="event_code_error" class="display-n red-text">行事を入力してください。</div>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" style="width: 150px;">受取日<span class="req"></span></th>
                        <td colspan="4">
                            <div class="inline_block rel">
                                <input type="text" id="receipt_start_date" placeholder="例：2019/01/02" tabindex="7"
                                       name="receipt_start_date" value="{{isset($receipt_start_date) ? $receipt_start_date : ''}}">
                            </div>
                            <br class="sp">&nbsp;〜&nbsp;<br class="sp">
                            <div class="inline_block rel">
                                <input type="text" id="receipt_end_date" placeholder="例：2019/01/03" tabindex="8"
                                       name="receipt_end_date" value="{{isset($receipt_end_date) ? $receipt_end_date : ''}}">
                            </div>
                            <div id="receipt_start_date_error" class="display-n red-text">受取開始日を入力してください。</div>
                            <div id="receipt_end_date_error" class="display-n red-text">受取終了日を入力してください。</div>
                        </td>
                    </tr>
                    <tr>
                        <th style="width: 150px;" scope="row">商品コード</th>
                        <td style="width: 260px;">
                            <div class="line full_input">
                                <input type="text" tabindex="1" placeholder="商品コード" name="item_code" value="{{isset($item_code) ? $item_code : ''}}">
                            </div>
                        </td>
                        <th class="TextR sp-TextL" style="width: 100px;" scope="row">商品名</th>
                        <td width="300">
                            <div class="line full_input">
                                <input type="text" tabindex="2" placeholder="商品名" name="item_name" value="{{isset($item_name) ? $item_name : ''}}">
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="contentfoot">
                <p class="btn Searchclear">
                    <button type="button" onclick="System.showLoading(); System.redirectLoading('{{ route('item.list') }}')"value="">検索条件をクリア</button>
                </p>
                <p class="btn Search">
                    <button type="button" onclick="Backend.checkRequire(['shop_code', 'event_code', 'receipt_start_date', 'receipt_end_date'] , 'item_list')" tabindex="13"><img src="{{ asset('backend/images/common/icon3.png') }}" alt="検索">検索</button>
                </p>
            </div>
            <!--ContentFoot -->
            </form>
        </div>
        <!--PageContent -->
        <div id="PageContent" class="pt60">
            <h2><i class="fas fa-list"></i> 商品一覧</h2>

            @if(null != $shop_code
            and null != $event_code
            and null != $receipt_start_date and validateDate($receipt_start_date, 'Y/m/d') == true
            and null != $receipt_end_date and validateDate($receipt_end_date, 'Y/m/d') == true)
                <div class="PageContentNav">
                    @include('backend/elements/paging', ['results' => $items])
                    {{ $items->appends(request()->input())->links() }}
                </div>
                <table class="OrderForm responsive tdcenter Bordertd TextL table-hover">
                    <tr>
                        <th scope="col">@include("backend/elements.sortable", ["field" => "items_shop_code", "title" => "店舗コード<br/>店舗名"])</th>
                        <th scope="col">@include("backend/elements.sortable", ["field" => "items_code", "title" => "商品コード<br/>商品名"])</th>
                        <th scope="col">商品画像</th>
                        <th scope="col">@include("backend/elements.sortable", ["field" => "event_item_views_receptionist_start_date", "title" => "受付開始日<br/>受付終了日"])</th>
                        <th scope="col">@include("backend/elements.sortable", ["field" => "event_item_views_receipt_start_date", "title" => "受取開始日<br/>受取終了日"])</th>
                        <th scope="col">@include("backend/elements.sortable", ["field" => "shop_order_possible_count", "title" => "店別計画数"])</th>
                        <th scope="col">@include("backend/elements.sortable", ["field" => "item_order_possible_count", "title" => "限定数"])</th>
                        <th scope="col">@include("backend/elements.sortable", ["field" => "count_reserve_item", "title" => "予約数"])</th>
                        <th scope="col">状態</th>
                    </tr>
                    @if(!$items->isEmpty())
                        @foreach($items as $item)
                        <tr>
                            <td>{{$item->items_shop_code}}<br/>
                                @foreach($shops as $shop)
                                    @if($item->items_shop_code == $shop->code)
                                        {{$shop->name}}
                                    @endif
                                @endforeach
                            </td>
                            <td>{{$item->items_code}}<br/>{{$item->item_names_name}}</td>
                            @if (filter_var(config('const.url_item_image') . $item->items_item_image_folder_name, FILTER_VALIDATE_URL) === FALSE || $item->items_item_image_folder_name == '_')
                                <td><img src="{{ asset('backend/images/common/noimage.jpg') }}" width="100"></td>
                            @else
                                <td><img src="{{ config('const.url_item_image') . $item->items_item_image_folder_name }}" width="100"></td>
                            @endif
                            <td>{{ \Carbon\Carbon::parse('20'.$item->event_item_views_receptionist_start_date)->format('Y/m/d')}}<br/>{{ \Carbon\Carbon::parse('20'.$item->event_item_views_receptionist_end_date)->format('Y/m/d')}}</td>
                            <td>{{ \Carbon\Carbon::parse('20'.$item->event_item_views_receipt_start_date)->format('Y/m/d')}}<br/>{{ \Carbon\Carbon::parse('20'.$item->event_item_views_receipt_end_date)->format('Y/m/d')}}</td>
                            <td>{{null != $item->shop_order_possible_count ? number_format($item->shop_order_possible_count, 0, '.', ',') : 0}}</td>
                            <td>{{null != $item->item_order_possible_count ? number_format($item->item_order_possible_count, 0, '.', ',') : 0}}</td>
                            <td>{{number_format($item->count_reserve_item, 0, '.', ',')}}</td>
                            <td>
                                <p class="btnmini Edit">
                                    <button type="button" class="{{$item->items_code}}-{{$item->items_shop_code}} {{$item->stop_flag == 0 ? 'stop-selling' : ''}}"
                                            onclick="Backend.showDialogUpdateStopFlag('{{$item->items_code}}' , '{{$item->items_shop_code}}')">{{$item->stop_flag == 0 ? '販売中' : '販売停止'}}</button>
                                </p>
                            </td>
                        </tr>
                        @endforeach
                    @else
                        @include('backend.elements.no_data',['col' => 9])
                    @endif
                </table>
                <div class="PageContentNav">
                    {{ $items->appends(request()->input())->links() }}
                </div>
            @else
                <table class="OrderForm responsive tdcenter Bordertd TextL table-hover">
                    <tr>
                        <th scope="col">店舗コード<br/>店舗名</th>
                        <th scope="col">商品コード<br/>商品名</th>
                        <th scope="col">商品画像</th>
                        <th scope="col">受付開始日<br/>受付終了日</th>
                        <th scope="col">受取開始日<br/>受取終了日</th>
                        <th scope="col">店別計画数</th>
                        <th scope="col">限定数</th>
                        <th scope="col">予約数</th>
                        <th scope="col">状態</th>
                    </tr>
                    @include('backend.elements.no_data',['col' => 9])
                </table>
            @endif
        </div>
        <!--PageContent -->

    </div>
    <!--Contentwrap -->
@endsection
@section('js')
    <script>
        $(document).ready(function () {
            System.setRangeDate($('#receipt_start_date'), $('#receipt_end_date'));
        });
    </script>
@endsection
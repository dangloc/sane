<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert([
            [
                'name' => 'test',
                'email' => 'test@gmail.com',
                'tel' => '0886610194',
                'card_no'=>'123456789012',
                'del_flag' => 0,
                'ins_datetime' => date('Y-m-d'),
                'ins_id' => 1,
                'password' => bcrypt('12345678'),
            ],
            [
                'name' => 'duongh',
                'email' => 'duongh.paraline@gmail.com',
                'tel' => '0886610194',
                'card_no'=>'123456789032',
                'del_flag' => 0,
                'ins_datetime' => date('Y-m-d'),
                'ins_id' => 1,
                'password' => bcrypt('12345678'),
            ]
        ]);
    }
}

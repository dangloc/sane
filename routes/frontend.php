<?php

use Illuminate\Support\Facades\Route;


Route::middleware('web')->group(function () {
    Route::get('/', 'SiteController@index')->name('site.index');
    Route::get('/login', 'LoginController@index')->name('frontend.login');
    Route::get('/login-confirm', 'LoginController@loginConfirm')->name('frontend.login.confirm');
    Route::post('login', 'LoginController@authenticate')->name('authenticate_login');
    Route::get('/logout', 'LoginController@logout')->name('frontend.logout');
    Route::get('/password-reissue/{token}', 'SiteController@passwordReissue')->name('site.password_reissue');
    Route::post('/password-reissue', 'SiteController@passwordReissueAction')->name('site.password_reissue_action');
    Route::get('/password-reissue-comp', 'SiteController@passwordReissueComp')->name('site.password_reissue_comp');
    Route::get('/password-reminder', 'SiteController@passwordReminder')->name('site.password_reminder');
    Route::post('password-reminder', 'SiteController@sendMailPasswordReissue')->name('site.send_mail_password_reissue');
    //AJAX
    Route::group(['prefix' => 'ajax'], function () {
        Route::post('/render-by-map', 'AjaxController@renderShopByMap')->name('site.ajax.render_by_map');
        Route::post('/render-shop-by-city', 'AjaxController@renderShopByCity')->name('site.ajax.render_shop_by_city');
        Route::post('/get-event', 'AjaxController@getEvent')->name('site.ajax.get_event');
        Route::post('/get-product', 'AjaxController@getProduct')->name('site.ajax.get_product');
        Route::post('/render-city-popup', 'AjaxController@renderCityPopup')->name('site.render_city_popup');
    });

    Route::group(['prefix' => 'product'], function () {
        Route::get('/detail/{id}/{shopcode}/{keyitem}/{keyprouct}', 'ProductController@detail')->name('product.detail');
    });

    Route::get('/information-detail/{id}', 'InformationController@detail')->name('information.detail');

    Route::group(['prefix' => 'cart'], function () {
        Route::get('/', 'CartController@index')->name('cart.index');
        Route::get('/confirm', 'CartController@confirm')->name('cart.confirm');
        Route::get('/complete', 'CartController@complete')->name('cart.complete');
        Route::post('/add-to-cart', 'CartController@addToCart')->name('cart.add_to_cart');
        Route::post('/remove-item-cart', 'CartController@removeItemCart')->name('cart.remove_item_cart');
        Route::post('/on-load-box-cart', 'CartController@onLoadBoxCart')->name('cart.on_load_box_cart');
        Route::post('/check-item-cart', 'CartController@checkItemCart')->name('cart.check_item_cart');
        Route::post('/change-number-cart', 'CartController@changeNumberCart')->name('cart.change_number_cart');
        Route::post('/insert-cart', 'CartController@insertCart')->name('cart.insert_cart');
        Route::get('/not-registration', 'CartController@notRegistration')->name('cart.notregistration');
        Route::post('/not-registration-action', 'CartController@notRegistrationAction')->name('cart.notregistration.action');
        Route::post('/show-popup-remove', 'CartController@showPopupRemove')->name('cart.show_popup_remove');
    });

    Route::group(['prefix' => 'entry'], function () {
        Route::get('/', 'EntryController@index')->name('entry.index');
        Route::post('/','EntryController@postIndex')->name('entry.submit');
        Route::get('/register', 'EntryController@register')->name('entry.register');
    });

    Route::group(['prefix' => 'mypage','middleware'=>'frontend.auth'], function () {
        Route::get('', 'MypageController@index')->name('mypage.index');
        Route::get('/cancel-order-confirm/{slipNumber}', 'MypageController@cancelOrderConfirm')->name('mypage.cancel_order_confirm');
        Route::get('/cancel-order-complete/{slipNumber}', 'MypageController@cancelOrderComplete')->name('mypage.cancel_order_complete');
        Route::post('/cancel-order-complete-action', 'MypageController@updateCancelOrder')->name('mypage.cancel_order_complete_action');
        Route::get('/change_customer_info/{slip_number}', 'MypageController@changeCustomerInfo')->name('mypage.change_customer_info');
        Route::post('/change_customer_info/{slip_number}', 'MypageController@postChangeInfo')->name('mypage.post.change_customer_info');
        Route::post('/show-popup-cancel', 'MypageController@showPopupCancel')->name('mypage.show_popup_cancel');
    });
    Route::get('/change-password-entry', 'ChangeEmailPasswordController@changePasswordEntry')->name('mypage.change_password_entry');
    Route::post('/change-password-entry', 'ChangeEmailPasswordController@changePasswordEntryAction')->name('mypage.change_password_entry_action');
    Route::group(array('prefix' => 'static'), function () {
        Route::view('/guild', 'frontend/static/guild')->name('guild');
        Route::view('/privacy', 'frontend/static/privacy')->name('privacy');
        Route::view('/tokutei', 'frontend/static/tokutei')->name('tokutei');
        Route::view('/browser', 'frontend/static/browser')->name('browser');
        Route::view('/sitemap', 'frontend/static/sitemap')->name('sitemap');
        Route::view('/rule', 'frontend/static/rule')->name('rule');
        Route::get('/information', 'InformationController@index')->name('information');
        Route::get('/information/detail/{id}', 'InformationController@detail')->name('information.detail');
    });
});


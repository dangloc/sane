<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class AccountCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $rules  = [
            'password' => 'required|min:8',
            'status' => 'required',
        ];
        if ($request->session()->exists('session_data') && array_key_exists('id', $request->session()->get('session_data'))) {
            $rules['login_id'] = 'required|max:256|unique:administrators,login_id,' . $request->session()->get('session_data')['id'] . ',id,del_flag,0| alpha_num_nospace';
            $rules['name'] = 'required|max:256';
            $rules['password'] = 'nullable|min:8';
        } else {
            $rules['login_id'] = 'required|max:256|unique:administrators,login_id,,,del_flag,0|alpha_num_nospace';
            $rules['name'] = 'required|max:256';
        }
        return $rules;
    }
    public function attributes()
    {
        return [
            'name' => '氏名',
            'login_id' => 'ログインID',
            'password' => 'パスワード',
            'status' => 'ステータス',
        ];
    }
//    public function messages()
//    {
//
//    }
}

@extends('frontend.layouts.main')
@section('content')
    <div id="entry_register" class="password-wrap">
        <div id="prog" class="container">
            <div class="prog">
                <ul>
                    <li class="nomal"><span>カートの中</span><strong>1</strong></li>
                    <li class="current"><span>ログイン・入力</span><strong>2</strong></li>
                    <li class="later"><span>入力内容の確認</span><strong>3</strong></li>
                    <li class="later"><span>購入完了</span><strong>4</strong></li>
                </ul>
            </div>
        </div>
        <div class="container">
            <div class="entry_register">
                <form>
                    <h1 class="title"><i class="fas fa-pencil-alt"></i> お客様情報入力</h1>
                    <div class="description">お客さま情報</div>
                    <div class="form_input">
                        <div class="group_input">
                            <label>お名前（カタカナ）<span>必須</span></label>
                            <input type="text" name="email" placeholder="例：ヤマダハナコ">
                        </div>
                        <div class="group_input">
                            <label>電話番号<span>必須</span></label>
                            <input type="text" placeholder="例：0901234567 ハイフン無し">
                        </div>
                        <div class="group_input_phone">
                            <label>サンエーカード番号<span>必須</span></label>
                            <div class="phone">
                                <input type="text">ー
                                <input type="text">ー
                                <input type="text">
                            </div>
                        </div>
                    </div>
                    <div class="policy">サンエーネット予約サービス利用規約</div>
                    <div class="entry_register_scroll">
                        <div class="scroll">
                            <p>ダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキス</p>
                            <p>トダミーテキストダミーテキストダミーテキストダミーテキストダミーテキスト</p>
                            <br>
                            <p><b>第1条</b></p>
                            <p>ダミーテキストダミーテキストダミーテキストダミーテキストダミーテキスト</p>
                            <p>ダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキスト</p>
                            <p>(1)ダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキスト</p>
                            <p>　 ダミーテキストダミーテキストダミーテキストダミーテキストダミーテキスト</p>
                            <p>(２)ダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテ</p>
                            <p>キストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキスト</p>
                            <p>ダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキス </p>
                            <p>ダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキス </p>
                            <p>ダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキス </p>
                            <p>ダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキス </p>
                            <p>ダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキス </p>
                            <br>
                            <p>ダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキス </p>
                            <p>ダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキス </p>
                            <p>ダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキス </p>
                            <br>
                            <p>ダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキス </p>
                            <p>ダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキス </p>

                            <br>
                            <p>ダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキス </p>
                            <p>ダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキス </p>
                            <br>
                            <br>


                        </div>
                    </div>
                    <div class="checkbox">
                        <input id="checkbox_agree" type="checkbox">
                        <label for="checkbox_agree">利用規約に同意する</label>
                    </div>
                    <div class="button">
                        <button type="submit" class="button_submit">入力内容を確認する</button>
                    </div>
                </form>
                <button class="button_back"><i class="fas fa-chevron-left"></i><span>前の画面に戻る</span></button>
            </div>
        </div>

    </div>
@endsection
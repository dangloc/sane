<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class ItemBonusPointTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('item_bonus_points')->truncate();
        DB::table('item_bonus_points')->insert([
            [
                'item_code'=>'0010',
                'receptionist_start_date'=>'20190512',
                'receptionist_end_date'=>'20190515',
                'receipt_start_date'=>'20190511',
                'receipt_end_date'=>'20190515',
                'del_flag'=>0,
                'ins_id'=>1,
                'ins_datetime'=>Carbon::now()
            ],
            [
                'item_code'=>'0011',
                'receptionist_start_date'=>'20190512',
                'receptionist_end_date'=>'20190515',
                'receipt_start_date'=>'20190511',
                'receipt_end_date'=>'20190515',
                'del_flag'=>0,
                'ins_id'=>1,
                'ins_datetime'=>Carbon::now()
            ],
            [
                'item_code'=>'0012',
                'receptionist_start_date'=>'20190512',
                'receptionist_end_date'=>'20190515',
                'receipt_start_date'=>'20190511',
                'receipt_end_date'=>'20190515',
                'del_flag'=>0,
                'ins_id'=>1,
                'ins_datetime'=>Carbon::now()
            ]
        ]);
    }
}

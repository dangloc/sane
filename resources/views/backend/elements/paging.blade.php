<?php
$start = $results->perPage() * ($results->currentPage() - 1) + 1;
$end = $start + $results->count() - 1;
if($results->total() == 0){
    $start = 0;
}
?>
<p>{{$results->total()}}件中<strong>{{$start}}～{{$end}}</strong>件目を表示しています</p>

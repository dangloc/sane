<?php
return [
    // BATCH 2-12
    'Event' => [
        'folder' => 'GYOJIUKETSUKE_GYOJI',
        'keys' => [
            'code'
        ],
        'fields' => [
            'code',
            'name',
            'start_date',
            'end_date',
        ]
    ],
    'Category' => [
        'folder' => 'GYOJIUKETSUKE_KATEGORI',
        'keys' => [
            'code'
        ],
        'fields' => [
            'code',
            'division_code',
            'name',
        ]
    ],
    'EventCategory' => [
        'folder' => 'GYOJIUKETSUKE_TABKATEGORI',
        'keys' => [
            'event_code',
            'category_code'
        ],
        'fields' => [
            'event_code',
            'category_code',
            'sort',
            'del_flag'
        ]
    ],
    'EventView' => [
        'folder' => 'GYOJIUKETSUKE_GYOJI_LIST',
        'keys' => [
            'event_code'
        ],
        'fields' => [
            'sort',
            'event_code'
        ]
    ],
    'EventItemView' => [
        'folder' => 'GYOJIUKETSUKE_SHOHINNO',
        'keys' => [
            'event_code',
            'category_code',
            'item_code'
        ],
        'fields' => [
            'event_code',
            'category_code',
            'item_code',
            'sort',
            'receptionist_start_date',
            'receptionist_end_date',
            'receipt_start_date',
            'receipt_end_date',
            'del_flag'
        ]
    ],
    'Item' => [
        'folder' => 'GYOJIUKETSUKE_SHOHINMASTER',
        'keys' => [
            'shop_code',
            'code'
        ],
        'fields' => [
            'shop_code',
            'code',
            'item_image_name',
            'content1',
            'content2',
            'content3',
            'required_confirm_flag',
            'limited_flag',
            'limited_count',
            'additive_code',
            'lead_time',
            'item_image_folder_name',
            'reserve_content1',
            'reserve_content2',
            'reserve_content3',
            'del_flag'
        ]
    ],
    'ItemName' => [
        'folder' => 'GYOJIUKETSUKE_SHOKUHINITEM_NAME',
        'keys' => [
            'item_code'
        ],
        'fields' => [
            'item_code',
            'name'
        ]
    ],
    'ItemPrice' => [
        'folder' => 'GYOJIUKETSUKE_SHOKUHINITEM_TANKA',
        'keys' => [
            'item_code',
        ],
        'fields' => [
            'item_code',
            'price'
        ]
    ],
    'ItemBonusPoint' => [
        'folder' => 'SOKIYOYAKU_BP_JANCD_TE_MAINTE',
        'keys' => [
            'item_code'
        ],
        'fields' => [
            'item_code',
            'receptionist_start_date',
            'receptionist_end_date',
            'receipt_start_date',
            'receipt_end_date',
        ]
    ],
    'ShopPlanOrderPossibleCount' => [
        'folder' => 'TENBETSU_CHUMON_KANOSU',
        'keys' => [
            'shop_code',
            'receipt_date',
            'item_code'
        ],
        'fields' => [
            'shop_code',
            'receipt_date',
            'item_code',
            'order_possible_count'
        ]
    ],
    'ItemLimitedOrderPossibleCount' => [
        'folder' => 'SHOHIN_CHUMON_KANOSU',
        'keys' => [
            'item_code'
        ],
        'fields' => [
            'item_code',
            'order_possible_count'
        ]
    ],
    // BATCH 14
    'ReserveUpdate' => [
        'folder' => 'GYOJIUKETSUKE_YOYAKUJOHO_HENKO_FLG',
        'keys' => [],
        'fields' => [
            'slip_number',
            'flag'
        ]
    ]
];


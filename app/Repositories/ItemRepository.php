<?php

namespace App\Repositories;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * class ItemRepository.
 *
 * @package namespace App\Repositories;
 */
class ItemRepository extends BaseRepository
{
    protected $option_shop_repository;

    function __construct()
    {
        $this->_model = \App\Models\Item::class;
    }

    public function paging()
    {
        $sort = request()->get('sort', 'shop_code');
        $direction = request()->get('direction', 'desc');
        $params = request()->all();

        return $this->getModel()
            ->when(isset($params['shop_code']) && $params['shop_code'] != '', function ($q) use ($params) {
                return $q->where('shop_code', 'like', '%' . $params['shop_code'] . '%');
            })
            ->when(isset($params['code']) && $params['code'] != '', function ($q) use ($params) {
                return $q->where('code', 'like', '%' . $params['code'] . '%');
            })
            ->orderBy($sort, $direction)
            ->paginate(config('const.paging.backend'));
    }

    /**
     * @param $shop_code
     * @param $event_code
     * @param $date_receive
     * @return mixed
     * getListItem in top
     * $itemCode != true -- check cart list
     * $itemCode = false -- get list item
     */
    public function getListItem($shopCode, $eventCode, $dateReceive, $categoryCode = 0, $itemCode = 0)
    {
        $end = Carbon::parse($dateReceive)->format('Y-m-d');
        $now = Carbon::now()->format('Y-m-d');
        $length = (strtotime($end) - strtotime($now))/86400;
        if (!empty($shopCode) && !empty($eventCode) && !empty($dateReceive)) {
            $data = $this->getModel()
                ->select('items.code',
                    'items.shop_code',
                    'items.item_image_name',
                    'items.item_image_folder_name',
                    'items.content1',
                    'items.content2',
                    'items.content3',
                    'items.required_confirm_flag',
                    'items.limited_flag',
                    'items.limited_count',
                    'items.lead_time',
                    'items.reserve_content1',
                    'items.reserve_content2',
                    'items.reserve_content3',
                    'item_names.name',
                    'item_prices.price',
                    'event_item_views.event_code')
                ->leftJoin('event_item_views', 'items.code', '=', 'event_item_views.item_code')
                ->leftJoin('item_names', 'items.code', '=', 'item_names.item_code')
                ->leftJoin('item_prices', 'items.code', '=', 'item_prices.item_code')
                ->leftJoin('item_status', function ($q) {
                    $q->on('items.code', '=', 'item_status.code')->on('items.shop_code', '=', 'item_status.shop_code');
                })
                ->where('items.shop_code', '=', $shopCode)
                ->when($itemCode, function ($query, $itemCode) {
                    return $query->where('items.code', '=', $itemCode);
                })
                ->where('items.lead_time', '<=', $length)
                ->where('event_item_views.event_code', '=', $eventCode)
                ->when($categoryCode, function ($query, $categoryCode) {
                    return $query->where('event_item_views.category_code', '=', $categoryCode);
                })
                ->where('event_item_views.receipt_start_date', '<=', convertTimeToInt($dateReceive))
                ->where('event_item_views.receipt_end_date', '>=', convertTimeToInt($dateReceive))
                ->where('event_item_views.receptionist_start_date', '<=', convertTimeToInt(now()))
                ->where('event_item_views.receptionist_end_date', '>=', convertTimeToInt(now()))
                ->where('item_status.stop_flag', '=', 0)
                ->where('items.del_flag', '=', 0)
                ->where('event_item_views.del_flag', '=', 0)
                ->where('item_names.del_flag', '=', 0)
                ->where('item_prices.del_flag', '=', 0)
                ->where('item_status.del_flag', '=', 0)
                ->orderBy('event_item_views.sort', 'ASC')->withoutGlobalScopes()->get()->toArray();

            return $data;
        }
        return array();
    }


    public function getOne($params = array())
    {
        if (!isset($params['shop_code']) || !isset($params['item_code'])) {
            return array();
        }
        return $this->getModel()
            ->where('shop_code', $params['shop_code'])
            ->where('code', $params['item_code'])
            ->first();
    }

    public function getItemDetail($code, $shopcode)
    {
        if (!empty($code)) {
            $data = $this->getModel()->select(
                'items.shop_code',
                'items.code',
                'items.item_image_name',
                'items.item_image_folder_name',
                'items.content1',
                'items.content2',
                'items.content3',
                'items.required_confirm_flag',
                'items.limited_flag',
                'items.limited_count',
                'items.lead_time',
                'items.reserve_content1',
                'items.reserve_content2',
                'items.reserve_content3',
                'item_names.name AS item_name',
                'item_prices.price',
                'shops.name AS shop_name'
            )
                ->leftJoin('event_item_views', 'items.code', '=', 'event_item_views.item_code')
                ->leftJoin('item_names', 'items.code', '=', 'item_names.item_code')
                ->leftJoin('item_prices', 'items.code', '=', 'item_prices.item_code')
                ->leftJoin('shops', 'items.shop_code', '=', 'shops.code')
                ->leftJoin('item_status', function ($q) {
                    $q->on('items.code', '=', 'item_status.code')->on('items.shop_code', '=', 'item_status.shop_code');
                })
                ->where('item_status.stop_flag', '=', 0)
                ->where('items.del_flag', '=', 0)
                ->where('items.code', '=', $code)
                ->where('shops.code', '=', $shopcode)
                ->where('shops.del_flag', '=', 0)->withoutGlobalScopes()->first();
            return $data;
        }
        return array();
    }

    /**
     * @param $code
     * @param $shopCode
     * @return array
     * get Item info for order
     */
    public function getItemDataOrder($slipNumnber)
    {
        if (!empty($slipNumnber)) {

            $data = $this->getModel()
                ->select(DB::raw('DISTINCT (items.code)'),'item_names.name', 'item_prices.price'
                    ,'items.item_image_folder_name','items.item_image_name','reserves.item_count','reserves.required_confirm_flag','reserves.required_flag')
                ->where('reserves.slip_number', '=', $slipNumnber)
                ->join('reserves', function ($q) {
                    $q->on('items.code', '=', 'reserves.item_code')->on('items.shop_code', '=', 'reserves.shop_code');
                })
                ->join('item_names', 'item_names.item_code', '=', 'items.code')
                ->join('item_prices', 'item_prices.item_code', '=', 'items.code')
                ->where('items.del_flag', '=', 0)->withoutGlobalScopes()->get()->toArray();
            return $data;
        }
        return array();
    }

    // for admin item-search
    public function searchCondition($request){
        $sort = $request->get('sort', 'items_code');
        $direction = $request->get('direction', 'desc');
        $query = DB::table('items')
            ->leftJoin('event_item_views', function($join) {
                $join->on('items.code', '=', 'event_item_views.item_code');
                $join->on('event_item_views.del_flag', '=', DB::raw("0"));
            })
            ->leftJoin('item_status', function($join) {
                $join->on('items.code', '=', 'item_status.code');
                $join->on('items.shop_code', '=', 'item_status.shop_code');
                $join->on('item_status.del_flag', '=', DB::raw("0"));
            })
            ->leftJoin('item_names', function($join) {
                $join->on('items.code', '=', 'item_names.item_code');
                $join->on('item_names.del_flag', '=', DB::raw("0"));
            })
            ->leftJoin('shop_plan_order_possible_counts', function($join) {
                $join->on('items.code', '=', 'shop_plan_order_possible_counts.item_code');
                $join->on('shop_plan_order_possible_counts.del_flag', '=', DB::raw("0"));
            })
            ->leftJoin('item_limited_order_possible_counts', function($join) {
                $join->on('items.code', '=', 'item_limited_order_possible_counts.item_code');
                $join->on('item_limited_order_possible_counts.del_flag', '=', DB::raw("0"));
            })
            ->select(
                'items.shop_code as items_shop_code',
                'items.code as items_code',
                'item_names.name as item_names_name',
                'items.item_image_folder_name as items_item_image_folder_name',
                'event_item_views.receptionist_start_date as event_item_views_receptionist_start_date',
                'event_item_views.receptionist_end_date as event_item_views_receptionist_end_date',
                'event_item_views.receipt_start_date as event_item_views_receipt_start_date',
                'event_item_views.receipt_end_date as event_item_views_receipt_end_date',
                'shop_plan_order_possible_counts.order_possible_count as shop_order_possible_count',
                'item_limited_order_possible_counts.order_possible_count as item_order_possible_count',
                'item_status.stop_flag')
            ->addSelect(DB::raw('(select count(reserves.item_code) FROM reserves WHERE reserves.item_code = items.code AND reserves.reserve_del_flag = 0 AND reserves.del_flag = 0) as count_reserve_item'))
            ->where('items.shop_code', '=', $request->get('shop_code'))
            ->where('event_item_views.event_code', '=', request()->get('event_code'))
            ->when(null !== $request->get('item_code'), function ($query) {
                return $query->where('items.code', 'like', '%' . request()->get('item_code') . '%');
            })
            ->when(null !== $request->get('item_name') or '' !== trim($request->get('item_name')), function ($query) {
                return $query->where('item_names.name', 'like', '%' . request()->get('item_name') . '%');
            })
            ->where('items.del_flag', '=', 0)
            ->where('event_item_views.receipt_end_date', '>=', $this->convertDateSixChar($request->get('receipt_start_date')))
            ->where('event_item_views.receipt_start_date', '<=', $this->convertDateSixChar($request->get('receipt_end_date')))
            ->groupBy(
                'items.shop_code',
                'items.code',
                'item_names.name',
                'items.item_image_folder_name',
                'event_item_views.receptionist_start_date',
                'event_item_views.receptionist_end_date',
                'event_item_views.receipt_start_date',
                'event_item_views.receipt_end_date',
                'shop_plan_order_possible_counts.order_possible_count',
                'item_limited_order_possible_counts.order_possible_count',
                'item_status.stop_flag');
//            ->distinct();
        return $query->orderBy($sort, $direction);
    }

    public function getRequiredConfirmFlag($itemCode, $shopCode)
    {
        if (!empty($itemCode) && !empty($shopCode)) {
            $data = $this->getModel()->select('required_confirm_flag')
                ->where('code', '=', $itemCode)
                ->where('shop_code', '=', $shopCode)
                ->first();
            if (!empty($data)) {
                return $data->required_confirm_flag;
            }
        }
        return false;
    }
}

<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\BackendController;
use App\Repositories\CategoryRepository;

class CategoryController extends BackendController
{
    public $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function index()
    {
        $data = $this->categoryRepository->paging();
        return view('backend.category.index',
            [
                'listData' => $data,
                'params' => request()->all()
            ]
        );
    }
}

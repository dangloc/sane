<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdministratorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('administrators', function (Blueprint $table) {
            $table->increments('id')->length(11)->comment('ID');
            $table->string('name', 64)->comment('氏名');
            $table->string('login_id', 256)->comment('メールアドレス');
            $table->char('password', 64)->comment('パスワード');
            $table->char('status', 1)->comment('ステータス');
            $table->char('del_flag', 1)->comment('削除フラグ')->default(0);
            $table->dateTime('ins_datetime')->default(DB::raw('CURRENT_TIMESTAMP'))->comment('登録日時');
            $table->integer('ins_id')->length(11)->comment('登録者ID');
            $table->dateTime('upd_datetime')->comment('更新日時')->nullable();
            $table->integer('upd_id')->length(11)->comment('更新者ID')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('administrators');
    }
}

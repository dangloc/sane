<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="format-detection" content="telephone=no">
<meta name=”robots” content=”noindex” />
<title>行事商品ネット予約サービス管理画面</title>
<link rel="icon" href="{{asset('backend/images/common/favicon.ico')}}">
<meta property="og:type" content="website">
<meta property="og:locale" content="ja_JP">
<meta property="og:title" content="サンエーネット予約サービス">
<meta property="og:image" content="{{asset('backend/images/common/ogp.png')}}”>
<meta property="og:description" content="サンエーのオードブルやお寿司やケーキをネットで予約・店舗で受取りできるサービスです。受取り店舗・日にちを選んで予約。マイページの利用でキャンセル、確認も簡単です">
<meta property="og:url" content="https://yoyaku.san-a.co.jp">
<meta property="og:site_name" content="サンエーネット予約サービス"/>
<link rel="apple-touch-icon" sizes="180x180" href="{{asset('backend/images/common/apple-touch-icon-180x180.png')}}">


<link href="{{ asset('backend/css/jquery-ui.min.css') }}" rel="stylesheet">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="{{ asset('backend/css/responsive-tables.css') }}" rel="stylesheet">
<link href="{{ asset('backend/css/slicknav.css') }}" rel="stylesheet">
<link href="{{ asset('backend/css/common.css') }}" rel="stylesheet">
<link href="{{ asset('backend/css/pagestyle.css') }}" rel="stylesheet">
<link href="{{ asset('backend/css/custom.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('backend/css/mobile_style.css') }}">
<script src="{{ asset('backend/js/jquery-1.12.4.min.js') }}"></script>
<link rel="shortcut icon" href="{{ asset('images/common/favicon.ico') }}" type="image/vnd.microsoft.icon">
<link rel="icon" href="{{ asset('images/common/favicon.ico') }}" type="image/vnd.microsoft.icon">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

<!--[if lt IE 9]>
<script src="{{ asset('backend/js/html5shiv-printshiv.js') }}"></script>
<script src="{{ asset('backend/js/selectivizr.js') }}"></script>
<![endif]-->
<script>
    let baseUrl = "{{url('/')}}";
</script>
<!--[if IE 8]>
<html class="ie ie8 lt-ie9">
<![endif]-->
<!--[if IE 7]>
<html class="ie ie7 lt-ie9 lt-ie8">
<![endif]-->
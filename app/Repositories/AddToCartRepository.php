<?php

namespace App\Repositories;

use App\Models\Customer;
use Illuminate\Support\Facades\DB;

/**
 * class AddToCartRepository.
 *
 * @package namespace App\Repositories;
 */
class AddToCartRepository extends BaseRepository
{
    public $itemRepository;
    public $itemBonusPoint;
    public $eventRepository;
    public $shopRepository;

    function __construct(ItemRepository $itemRepository, ItemBonusPointRepository $itemBonusPoint, EventRepository $eventRepository, ShopsRepository $shopRepository)
    {
        $this->itemRepository = $itemRepository;
        $this->itemBonusPoint = $itemBonusPoint;
        $this->eventRepository = $eventRepository;
        $this->shopRepository = $shopRepository;
    }

    public function getFullDataSessionCart($data_session)
    {
        $datas = array();
        if (!empty($data_session)) {
            foreach ($data_session as $key => $itemList) {
                foreach ($itemList['itemList'] as $key_item => $item) {
                    if (!empty($item['itemCode'])) {
                        $data_detail = $this->itemRepository->getItemDetail($item['itemCode'], $itemList['shopCode']);
                        if (!empty($data_detail)) { // Nếu item trên session ko có trong db ko lấy session
                            $datas[$key]['itemList'][$key_item] = $data_detail->toArray();
                            $datas[$key]['itemList'][$key_item]['quantity'] = $item['quantity'];
                            $datas[$key]['itemList'][$key_item]['requiredFlag'] = $item['requiredFlag'];
                            $datas[$key]['itemList'][$key_item]['total_price'] = $item['quantity'] * $datas[$key]['itemList'][$key_item]['price'];
                            $checkBonus = $this->itemBonusPoint->checkPointBonus($item['itemCode'], $itemList['dateReceiveRaw']);
                            $datas[$key]['itemList'][$key_item]['bonus'] = !empty($checkBonus) ? 'ボーナスポイント対象商品' : '';
                            $datas[$key]['eventCode'] = $itemList['eventCode'];
                            $datas[$key]['eventName'] = $this->eventRepository->getEventName($itemList['eventCode']);
                            $datas[$key]['dateReceive'] = $itemList['dateReceive'];
                            $datas[$key]['dateReceiveRaw'] = $itemList['dateReceiveRaw'];
                            $datas[$key]['timeReceive'] = $itemList['timeReceive'];
                            $datas[$key]['shopName'] = $datas[$key]['itemList'][$key_item]['shop_name'];
                            $datas[$key]['keyItem'] = $itemList['key_item'];
                            $datas[$key]['shopCode'] = $itemList['shopCode'];
                            $shopTime = $this->shopRepository->getShopOperationTime($itemList['shopCode']);
                            $datas[$key]['shopOpenTime'] = !empty($shopTime->open_time) ? formatTime($shopTime->open_time) : '9:00';
                            $datas[$key]['shopCloseTime'] = !empty($shopTime->close_time) ? formatTime($shopTime->close_time) : '23:00';
                            if (!empty($datas[$key]['itemList'][$key_item]['shop_code'])) {
                                $datas[$key]['itemList'][$key_item]['keyProduct'] = $key_item;
                            } else {
                                $datas[$key]['itemList'][$key_item]['keyProduct'] = '';
                            }
                        }
                    }
                }
                if (!empty($datas[$key])) {
                    $total_bill = !empty(array_sum(array_column($datas[$key]['itemList'], 'total_price'))) ? array_sum(array_column($datas[$key]['itemList'], 'total_price')) : 0;
                    $total = !empty(array_sum(array_column($datas[$key]['itemList'], 'quantity'))) ? array_sum(array_column($datas[$key]['itemList'], 'quantity')) : 0;
                    $datas[$key]['totalPrice'] = $total_bill;
                    $datas[$key]['total'] = $total;
                }
            }
        }
        return $datas;
    }

    public function controlGetListByItemSession($datas)
    {
        $return = array();
        if (!empty($datas)) {
            $datas = $this->getFullDataSessionCart($datas);
            foreach ($datas as $key => $data) {
                foreach ($data['itemList'] as $item) {
                    if (!array_key_exists($item['keyProduct'], $return)) {
                        $return[$item['keyProduct']] = $item;
                        $return[$item['keyProduct']]['keyItem'] = $data['keyItem'];
                        $return[$item['keyProduct']]['eventCode'] = $data['eventCode'];
                        $return[$item['keyProduct']]['dateReceiveRaw'] = $data['dateReceiveRaw'];
                    } else {
                        $return[$item['keyProduct']]['quantity'] += $item['quantity'];
                        $return[$item['keyProduct']]['total_price'] += $item['quantity'] * $item['price'];
                    }
                }
            }
        }
        return $return;
    }

}

<?php

namespace App\Repositories;

use App\Models\Customer;
use Illuminate\Support\Facades\DB;

/**
 * class ItemLimitedOrderPossibleCountRepository.
 *
 * @package namespace App\Repositories;
 */
class ItemLimitedOrderPossibleCountRepository extends BaseRepository
{
    function __construct()
    {
        $this->_model = \App\Models\ItemLimitedOrderPossibleCount::class;
    }

    /**
     * Check so luong gioi han
     */
    public function checkLimitCount($code)
    {
        $data = $this->getModel()->select(DB::raw('(order_possible_count * 0.9) as order_possible_count'))->where('item_code', '=', $code)->first();
        if (!empty($data)){
            return !empty($data->order_possible_count) ? (int)$data->order_possible_count : false ;
        }
        return false;
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class BackendController extends Controller
{
    public function guard()
    {
        return Auth::guard(config('const.guard.backend'));
    }
}

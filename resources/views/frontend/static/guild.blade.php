@extends('frontend.layouts.main')
@section('css')
    <link rel="stylesheet" href="{{asset('frontend/css/static.css')}}">
@endsection
@section('content')
    <div class="guild">
        <div class="container">
            <div class="row">
                <h1 class="title_page">ご利用ガイド</h1>
                <div class="guild_content">
                    <div class="group_content">
                        <h3 class="title_content">サンエーネット予約サービスとは</h3>
                        <p class="description_content">
                            サンエーネット予約サービスとは、普段お客様にご利用いただいているサンエー食品館の商品を、PCやスマートフォンであらかじめご予約し、お受取り店舗
                            にてお支払い、受取りできるサービスです。</p>
                    </div>
                    <div class="group_content">
                        <h3 class="title_content">ご利用について</h3>
                        <div class="description_content">
                            <p>会員登録されると、マイページがご利用いただけます。</p>
                            <p>マイページが不要の方は、会員登録せずにご利用いただけます</p>
                        </div>
                    </div>
                    <div class="group_content">
                        <h3 class="title_content">ご予約について</h3>
                        <div class="description_content">
                            <p>ご予約はインターネット、スマートフォン・タブレットにて承ります。</p>
                            <p>受取り店舗を選択し、カテゴリ、受取日を選択します。</p>
                            <p>「商品一覧を表示する」ボタンを押下すると、ご予約可能な商品一覧が表示されます。</p>
                            <p>多数の商品が表示されて、絞り込みたい場合には、表示された一覧上部にある、「絞り込み条件」ボタンを押下し表示商品を絞り込む事ができます。</p>
                            <p>※店舗または受取り日によってはお取扱いのない商品もございます。予めご了承ください。</p>
                        </div>
                    </div>
                    <div class="group_content space_top">
                        <h3 class="title_content">商品について</h3>
                        <div class="description_content">
                            <p>[価格や表示について]</p>
                            <p>・商品の価格は全て税抜き価格で表示しております。</p>
                            <p>・サンエーネット予約サービスで使用している写真はイメージです。</p>
                            <p>・生鮮食品は、生育状況や天候により産地が変わる場合がございます。</p>
                            <p>・グラム売りの商品につきましては、商品の重量により売価が違います。実際の価格は、商品を計量してから「100g単価×実際の重量」の計算で決定いた</p>
                            <p>します。また、重量変更による購入金額の変更のご連絡は致しませんのであらかじめご了承ください。</p>
                            <div class="description_product">
                                <div class="">[取扱商品について]</div>
                                <p class="">「よりどりX点」割引や「朝市・夕市」など、一部対象外のサービスや商品がございます。</p>
                            </div>
                        </div>
                    </div>
                    <div class="group_content">
                        <h3 class="title_content">商品のお受取りついて</h3>
                        <div class="description_content">
                            <p>[台風発生時のお受取りについて]</p>
                            <p>台風発生時のお受取りにつきまして、サービスエリアが受取り当日中に暴風域に入る予報の際、当該日の受取りを中止とし、ご注文はキャンセルとなる事が
                                あります。</p>
                            <p>※公共交通機関の運行を参考に判断いたします</p>
                            <p>※サンエーの店舗が営業中であっても、お受取りは中止となる事がございます</p>
                            <p>※上記受取り不可能と判断した場合、既に受けたご注文もキャンセルされます。（お受取り不可能の場合お客様へご連絡をいたしますが、お客様と連絡が取
                                れなくても、ご注文はキャンセルとなります）</p>
                        </div>
                    </div>
                    <div class="group_content space_top1">
                        <h3 class="title_content">お支払いについて</h3>
                        <div class="description_content">
                            <p>商品お受取りの際に、レジにてお支払をお願いします。</p>
                        </div>
                    </div>
                    <div class="group_content">
                        <h3 class="title_content">キャンセルについて</h3>
                        <div class="description_content">
                            <p>受注生産につき、ご予約後のキャンセル・変更は、お受取りの３日前までにお願いします。</p>
                            <p>[キャンセルポリシー]</p>
                            <p>当日のキャンセルや、受取り時間を過ぎてもご連絡が取れない場合などは、下記の注文キャンセル手数料を申し受けますので、予めご了承ください。</p>
                            <p>※受取り日時を過ぎてもご連絡のない場合は、キャンセルとさせていただきます。</p>
                        </div>
                    </div>
                    <div class="group_content">
                        <h3 class="title_content">お問い合わせ・ご要望</h3>
                        <div class="description_content">
                            <p>【経塚シティ】：0120-157-515（午前9時～午後5時、5時以降 098-871-3333）</p>
                            <p>【西原シティ】：0120-852-052（午前9時～午後5時、5時以降 098-882-9100）</p>
                            <p>【那覇メインプレイス】：098-963-9121（午前9時～午後5時、5時以降 098-951-3300）</p>
                        </div>
                    </div>
                </div>
                <div class="footer_back"><a href="{{route('site.index')}}">TOPページへ戻る</a></div>
            </div>
        </div>
    </div>
@endsection
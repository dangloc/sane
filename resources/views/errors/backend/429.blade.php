@extends('backend.layouts.main')
@section('css')
    <link rel="stylesheet" href="/frontend/css/error.css">
@endsection
@section('content')
    <div id="Contentwrap">
        <div class="color-error message-notification">
            リクエスト回数が多すぎます。
        </div>
    </div>
@endsection
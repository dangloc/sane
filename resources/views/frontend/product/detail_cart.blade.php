@extends('frontend.layouts.main')
@section('content')
    <div class="detail">
        <div class="container">
            <div class="row">
                <a href="{{route('site.index')}}" class="button_back_item">一覧ページに戻る
                </a>
            </div>
            <div class="row">
                <h3 class="title_result_search">{{!empty($dataProduct['bonus']) ? 'ボーナスポイント対象商品' : ''}}</h3>
            </div>
            <div class="row">
                <h1 class="name_item_detail">{{!empty($dataProduct['item_name']) ? $dataProduct['item_name'] :''}}</h1>
                <div class="detail-top">
                    <div class="row">
                        <div class="detail-top_left col-md-6">
                            <div class="detail_thumb">
                                <div class="item">
                                    <img src="{{ empty($dataProduct['item_image_folder_name']) ? asset('/frontend/images/item/noimage.jpg')
                                                    : config('const.url_item_image').$dataProduct['item_image_folder_name']}}" alt="">
                                </div>
                            </div>
                            <div class="detail_about">{{!empty($dataProduct['content3']) ? $dataProduct['content3'] : ''}}
                            </div>
                        </div>
                        <div class="detail-top_right col-md-6">
                            <div class="info">
                                <div class="info_top">
                                    <ul>
                                        <li><label>店舗</label><span>{{!empty($dataProduct['shop_name']) ? $dataProduct['shop_name'] : ''}}</span></li>
                                        <li><label>商品カテゴリ</label><span>{{!empty($dataItem['eventName']) ? $dataItem['eventName'] : ''}}</span></li>
                                        <li><label>受取り日</label><span>{{!empty($dataItem['dateReceive']['year']) ? $dataItem['dateReceive']['year'] : ''}}年{{!empty($dataItem['dateReceive']['month']) ? $dataItem['dateReceive']['month'] : ''}}月{{!empty($dataItem['dateReceive']['day']) ? $dataItem['dateReceive']['day'] : ''}}日({{dayJapan($dataItem['dateReceiveRaw'])}})</span><span>{{ !empty($dataItem['timeReceive']) ? $dataItem['timeReceive'].'時' : ''.'時'}}</span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="info_bottom">
                                    <div class="info_des">
                                        「カートへ追加」ボタンをクリックすると、 商品が<br>
                                        「カート」の中に入ります。
                                    </div>
                                    <div class="price">
                                        <span class="money">{{!empty($dataProduct['price']) ? number_format($dataProduct['price']) : ''}}</span><span class="tax">円+税</span>
                                    </div>
                                    <div>
                                        @if(!empty($dataProduct['required_confirm_flag']))
                                            @if($dataProduct['required_confirm_flag']!=config('const.not_with'))
                                                <label class="check_required_confirm">
                                                    <input onclick="Frontend.changeBackgroundButton(event,this)"
                                                           type="radio" hidden value="0"
                                                           id="check_required_true"
                                                           name="check_required_true">
                                                    @if($dataProduct['required_confirm_flag']== config('const.with_wasabi'))
                                                        わさび入り @elseif($dataProduct['required_confirm_flag']==config('const.with_kamaboko'))
                                                        かまぼこ赤 @elseif($dataProduct['required_confirm_flag']==config('const.with_classic'))
                                                        定番
                                                    @endif
                                                </label>
                                                <label class="check_required_confirm">
                                                    <input onclick="Frontend.changeBackgroundButton(event,this)"
                                                           type="radio" hidden value="1"
                                                           id="check_required_false"
                                                           name="check_required_false">
                                                    @if($dataProduct['required_confirm_flag']==config('const.with_wasabi'))
                                                        わさび抜き@elseif($dataProduct['required_confirm_flag']==config('const.with_kamaboko'))
                                                        かまぼこ白 @elseif($dataProduct['required_confirm_flag']==config('const.with_classic'))
                                                        法事
                                                    @endif
                                                </label>
                                            @endif
                                        @endif
                                        <input type="hidden" class="item-code" value="{{!empty($dataProduct['code']) ? $dataProduct['code'] : ''}}">
                                        <input type="hidden" class="shop-code" value="{{!empty($dataProduct['shop_code']) ? $dataProduct['shop_code'] : ''}}">
                                        <input type="hidden" class="event-code" value="{{!empty($dataItem['eventCode']) ? $dataItem['eventCode'] :''}}">
                                        <input type="hidden" class="required-confirm-flag" value="{{!empty($dataProduct['required_confirm_flag'])? $dataProduct['required_confirm_flag'] :''}}">
                                        <input type="hidden" class="date-receive-raw" value="{{!empty($dataItem['dateReceiveRaw']) ? $dataItem['dateReceiveRaw'] : ''}}">
                                        <input type="hidden" class="time-receive" value="{{!empty($dataItem['timeReceive']) ? $dataItem['timeReceive'] : ''}}">
                                    </div>
                                    <div class="quantity" style="cursor: pointer;">
                                        <input onkeyup="Frontend.keyUpQuantity(this)" type="number" value="1" min="1" max="999">
                                    </div>
                                    <button class="add-to-cart" onclick="Frontend.addToCart(this, false)">
                                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>カートへ追加
                                        <img class="lazy-load" src="{{asset('frontend/images/ajax-loader.gif')}}">
                                        <label class="bin-button"></label>
                                    </button>

                                </div>
                            </div>
                            <div class="list-button clearfix">
                                <div class="wrap">
                                    <a href="#" onclick="Frontend.scrollToBox('box_1')" class="button">キャンセルについて</a>
                                </div>
                                <div class="wrap">
                                    <a href="#" onclick="Frontend.scrollToBox('box_2')" class="button">お支払い方法について</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="container-fluid">
                            <div class="detail_content2">{{!empty($dataProduct['content2']) ? $dataProduct['content2'] : ''}}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="container-fluid">
                            <div class="box-content" id="box_1">
                                <h3 class="box-title">キャンセルについて
                                </h3>
                                <div class="box-text">
                                    <p>
                                        <strong>▪️キャンセルについて</strong><br>
                                        受注生産につき、ご予約後のキャンセル・変更は、お受取りの３日前までにお願いします。
                                    </p>
                                    <p>
                                        <strong>▪️キャンセルポリシー</strong><br>
                                        当日のキャンセルや、受取り時間を過ぎてもご連絡が取れない場合などは、下記の注文キャンセル手数料を申し受けますので、予めご了承ください。<br>
                                        ※受取り日時を過ぎてもご連絡のない場合は、キャンセルとさせていただきます。
                                    </p>
                                    <p>
                                         当日及び以降　：注文額の50％<br>
                                        限度額 　 50,000円（税抜
                                    </p>
                                </div>
                            </div>
                            <div class="box-content" id="box_2">
                                <h3 class="box-title">
                                    お支払い方法について
                                </h3>
                                <div class="box-text">
                                    <strong>商品お受取りの際に、レジにてお支払をお願いします。</strong><br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
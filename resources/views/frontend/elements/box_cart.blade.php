<div class="shopping-cart-fix-bottom tCenter">
    <div class="container">
        <div class="basket-popup" style="display: none;">
            <a href="#"><span class="basket-popup-header" onclick="Frontend.closeCart()">カートの中<i
                            class="close-popup"></i></span></a>
            <div class="total-basket">
                <span>合計(<span class="cart-number-item" id="total-all-quantity-cart">0</span>個)</span>
            </div>
            <div class="basket-content">
                <div class="text-description-popup-cart">
                    <p id="total-all-price-cart">0 円＋税</p>
                </div>
                <div class="button-checkout top-checkout tCenter">
                    <a href="{{route('cart.index')}}">ご注文手続きに進む</a>
                </div>
                <div class="list-item-block">
                </div>
                <div class="tCenter">
                    <p class="btn-close-cart">
                        <span onclick="Frontend.closeCart()">カートを閉じる</span>
                    </p>
                </div>
            </div>
            <div class="empty-cart tCenter">
                <p>現在、カートには<br>
                    商品が入っていません。</p>
            </div>
            <div class="loading-cart tCenter">
            </div>
        </div>
        <div class="box-cart" onclick="Frontend.showCartFix()" style="">
            <p class="TextC">
                <span>カートの中</span>
                <span class="no"><i style="font-style: normal" id="box-hide-quantity">0</i>個</span>
                <span class="border-bottom">中を見る</span>
            </p>
            <i class="fas fa-shopping-cart"></i>
        </div>
    </div>
</div>
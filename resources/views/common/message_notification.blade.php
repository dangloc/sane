@if(session()->has('success'))
    <p class="color-success message-notification"  >{{  session('success') }}</p>
@elseif(session()->has('error'))
    <p class="color-error message-notification"  >{{  session('error') }}</p>
@endif
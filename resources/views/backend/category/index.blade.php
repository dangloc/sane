@extends('backend.layouts.main')
@section('content')
    <div id="Contentwrap">
        @include('common.message_notification')
        <div id="PageContent">
            <h2><i class="fas fa-search"></i> カテゴリーマスター検索</h2>
            <form action="{{route('category.index')}}" method="get">
                <div class="order">
                    <table class="sp_block">
                        <tr>
                            <th scope="row" style="width: 150px;">カテゴリーコード</th>
                            <td colspan="4">
                                <div class="inline_block rel sp-w45 line">
                                    <input type="text" placeholder="カテゴリーコード" tabindex="5" name="code" value="{{isset($params['code']) ? $params['code'] : ''}}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row" style="width: 150px;">事業部コード</th>
                            <td colspan="4">
                                <div class="inline_block rel sp-w45 line">
                                    <input type="text" placeholder="事業部コード" tabindex="5" name="division_code" value="{{isset($params['division_code']) ? $params['division_code'] : ''}}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row" style="width: 150px;">カテゴリー名</th>
                            <td colspan="4">
                                <div class="inline_block rel sp-w45 full_input">
                                    <input type="text" placeholder="カテゴリー名" tabindex="5" name="name" value="{{isset($params['name']) ? $params['name'] : ''}}">
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="contentfoot">
                    <p class="btn Searchclear">
                        <button type="button" onclick="System.redirectLoading('{{ route('category.index') }}')" value="">検索条件をクリア</button>
                    </p>
                    <p class="btn Search">
                        <button type="submit" value="" onclick="System.showLoading()" tabindex="13"><img src="{{ asset('backend/images/common/icon3.png') }}" alt="検索">検索</button>
                    </p>
                </div>
                <!--ContentFoot -->
            </form>
        </div>
        <!--PageContent -->

        <div id="PageContent" class="pt60">
            <h2><i class="fas fa-shapes"></i> カテゴリーマスター一覧</h2>

            <div class="PageContentNav">
                @include('backend.elements.paging', ['results' => $listData])
                {{ $listData->appends(request()->input())->links() }}
            </div>
            <table class="OrderForm responsive tdcenter Bordertd TextL table-hover">
                <tr>
                    <th scope="col" width="130px">@include("backend.elements.sortable", ["field" => "code", "title" => "カテゴリーコード"])</th>
                    <th scope="col" width="120px">@include("backend.elements.sortable", ["field" => "division_code", "title" => "事業部コード"])</th>
                    <th scope="col">@include("backend.elements.sortable", ["field" => "name", "title" => "カテゴリー名"])</th>
                </tr>
                @if(!$listData->isEmpty())
                    @foreach($listData as $item)
                        <tr>
                            <td>{{$item->code}}</td>
                            <td>{{$item->division_code}}</td>
                            <td style="text-align:left;">{{$item->name}}</td>
                        </tr>
                    @endforeach
                @else
                    @include('backend.elements.no_data',['col' => 3])
                @endif
            </table>
            <div class="PageContentNav">
                {{ $listData->appends(request()->input())->links() }}
            </div>
        </div>
        <!--PageContent -->

    </div>
    <!--Contentwrap -->
@endsection
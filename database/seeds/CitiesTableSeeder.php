<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cities')->insert([
            [
                'area_id' => 1,
                'name' => '東京1',
                'del_flag' => 0,
                'ins_datetime' => date('Y-m-d H:i:s'),
                'ins_id' => 1,
                'upd_datetime' => date('Y-m-d H:i:s'),
                'upd_id' => 1
            ],
            [
                'area_id' => 1,
                'name' => '京都1',
                'del_flag' => 0,
                'ins_datetime' => date('Y-m-d H:i:s'),
                'ins_id' => 2,
                'upd_datetime' => date('Y-m-d H:i:s'),
                'upd_id' => 2
            ],
            [
                'area_id' => 2,
                'name' => '大阪2',
                'del_flag' => 0,
                'ins_datetime' => date('Y-m-d H:i:s'),
                'ins_id' => 3,
                'upd_datetime' => date('Y-m-d H:i:s'),
                'upd_id' => 3
            ],
            [
                'area_id' => 2,
                'name' => '秋田2',
                'del_flag' => 0,
                'ins_datetime' => date('Y-m-d H:i:s'),
                'ins_id' => 4,
                'upd_datetime' => date('Y-m-d H:i:s'),
                'upd_id' => 4
            ],
            [
                'area_id' => 3,
                'name' => '青森3',
                'del_flag' => 0,
                'ins_datetime' => date('Y-m-d H:i:s'),
                'ins_id' => 5,
                'upd_datetime' => date('Y-m-d H:i:s'),
                'upd_id' => 5
            ],
            [
                'area_id' => 3,
                'name' => '旭川3',
                'del_flag' => 0,
                'ins_datetime' => date('Y-m-d H:i:s'),
                'ins_id' => 6,
                'upd_datetime' => date('Y-m-d H:i:s'),
                'upd_id' => 6
            ],
            [
                'area_id' => 4,
                'name' => '熱海4',
                'del_flag' => 0,
                'ins_datetime' => date('Y-m-d H:i:s'),
                'ins_id' => 7,
                'upd_datetime' => date('Y-m-d H:i:s'),
                'upd_id' => 7
            ],
            [
                'area_id' => 4,
                'name' => '福岡4',
                'del_flag' => 0,
                'ins_datetime' => date('Y-m-d H:i:s'),
                'ins_id' => 8,
                'upd_datetime' => date('Y-m-d H:i:s'),
                'upd_id' => 8
            ],
            [
                'area_id' => 5,
                'name' => '萩5',
                'del_flag' => 0,
                'ins_datetime' => date('Y-m-d H:i:s'),
                'ins_id' => 7,
                'upd_datetime' => date('Y-m-d H:i:s'),
                'upd_id' => 7
            ],
            [
                'area_id' => 5,
                'name' => '箱根5',
                'del_flag' => 0,
                'ins_datetime' => date('Y-m-d H:i:s'),
                'ins_id' => 8,
                'upd_datetime' => date('Y-m-d H:i:s'),
                'upd_id' => 8
            ]
        ]);
    }
}

@extends('frontend.layouts.main')
@section('css')
    <link rel="stylesheet" href="{{asset('frontend/css/static.css')}}">
@endsection
@section('content')
    <div class="tokutei">
        <div class="container">
            <h1 class="title_page">特定商取引に関する法律に基づく表示</h1>
            <div class="tokutei_content">
                <div class="tokutei_description">
                    <p>サンエー（以下、当社）が運営する「サンエーネット予約サービス」の特定商取引法に基づく表示は以下のとおりです。</p>
                    <p>また当社は個人情報の重要性を認識し、個人情報保護法に基づいて適切にこれを取り扱います。</p>
                </div>
            </div>
            <table class="table_content">
                <tr class="table_border">
                    <td class="table_label" data-title="販売業者">販売業者</td>
                    <td class="table_content">株式会社サンエー</td>
                </tr>
                <tr class="table_border">
                    <td class="table_label" data-title="名称">名称</td>
                    <td class="table_content">サンエーネット予約サービス</td>
                </tr>
                <tr class="table_border">
                    <td class="table_label" data-title="運営責任者">運営責任者</td>
                    <td class="table_content">鈴木　優司　（ネット販売部）</td>
                </tr>
                <tr class="table_border">
                    <td class="table_label" data-title="所在地">所在地</td>
                    <td class="table_content">〒901-2733　沖縄県宜野湾市大山7-2-10</td>
                </tr>
                <tr class="table_border">
                    <td class="table_label" data-title="連絡先">連絡先</td>
                    <td class="table_content">
                        <p>電話番号（サンエー本社）：098-898-2230</p>
                        <p>FAX：098-897-2533</p>
                        <p>e-mail: netsuper@san-a.co.jp</p>
                    </td>
                </tr>
                <tr class="table_border">
                    <td class="table_label" data-title="ご予約方法について">ご予約方法について</td>
                    <td class="table_content">サンエーネット予約サービスが指定する入力・操作方法のみ</td>
                </tr>
                <tr class="table_border">
                    <td class="table_label" data-title="お受取り日時について">お受取り日時について</td>
                    <td class="table_content">商品は、当日から翌日のお客様ご指定の日時間帯でお受取りください。</td>
                </tr>
                <tr>
                    <td class="table_label" data-title="お受取り方法について">お受取り方法について</td>
                    <td class="table_content">直接、お客様がご指定した店舗でお受取りください。</td>
                </tr>
                <tr class="table_border">
                    <td class="table_label" data-title="お支払い方法について">お支払い方法について</td>
                    <td class="table_content">商品お受取りの際に、レジにてお支払をお願いします。</td>
                </tr>
                <tr class="table_border">
                    <td class="table_label" data-title="キャンセル・返品について">キャンセル・返品について</td>
                    <td class="table_content">
                        <p>お客さま都合による返品依頼</p>
                        <p>原則としてご購入頂いた商品の返品・交換は出来かねます。万が一返品となる場合は、返送料を頂戴することがご</p>
                        <p>ざいます。あらかじめご了承ください。 </p>
                        <p>商品の瑕疵（欠損・破損）に基づく返品依頼及び、商品の品違いによる返品依頼。商品の交換品・代替品がない</p>
                        <p>場合は、代金を返済いたします。（送料・手数料当社負担） </p>
                        <p>上記いずれの場合にも、</p>
                        <p>・生もの（冷蔵・冷凍品など）：お届けの翌日まで</p>
                        <p>・生もの以外：お届け日から1週間以内</p>
                        <p>に当社までご連絡いただきますようお願いいたします。</p>
                    </td>
                </tr>
            </table>
            <div class="tokutei_footer_back"><a href="{{route('site.index')}}">TOPページへ戻る</a></div>
        </div>
    </div>
@endsection
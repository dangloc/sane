<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests\LoginFrontendRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

class LoginController extends Controller
{
    /**
     * @return mixed
     * set guard customers
     */
    public function guard()
    {
        return Auth::guard(config('const.guard.frontend'));
    }

    /**
     * @param LoginFrontendRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * Check login
     */
    public function index(Request $request)
    {
        if ($this->guard()->check()) {
            return redirect()->route('site.index');
        }
        return view('frontend.login.login');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * Login key confirm
     */
    public function loginConfirm()
    {
        $request = app('router')->getRoutes()->match(app('request')->create(URL::previous()));
        if ($request->getName() == 'cart.index' ||  $request->getName() == 'frontend.login.confirm'
            || $request->getName() == 'cart.notregistration' || $request->getName() == 'entry.index'
        ) {
                return view('frontend.login.index');
        }
        return redirect()->route('site.index');
    }


    public function authenticate(LoginFrontendRequest $request)
    {
        if ($request->post()) {
            $arr = [
                'email' => !empty($request->email) ? $request->email : "",
                'password' => !empty($request->password) ? $request->password : "",
            ];
            $keyConfirm = !empty($request->keyConfirm) ? $request->keyConfirm : false;
            if ($this->guard()->attempt($arr)) {
                if ($keyConfirm) {
                    return redirect()->route('cart.confirm');
                }
                return redirect()->route('site.index');
            }
        }
        return redirect()->back()->withInput()->withErrors(['login_id' => config('messages.LOGIN_FAILED')]);
    }

    public function logout()
    {
        $this->guard()->logout();
//        Session::flush();
        return redirect('login');
    }

}

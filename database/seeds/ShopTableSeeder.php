<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ShopTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shops')->insert([
            [
                'code' => 1,
                'name' => 'A11',
                'name_kana' => 'A11',
                'area_id' => 1,
                'zip_code' => 1000,
                'city_id' => 1,
                'address' => 'A11',
                'tel' => '',
                'open_time' => '9',
                'close_time' => '23',
                'latlng' => '',
                'del_flag' => 0,
                'ins_datetime' => date('Y-m-d H:i:s'),
                'ins_id' => 1,
                'upd_datetime' => date('Y-m-d H:i:s'),
                'upd_id' => 1
            ],
            [
                'code' => 2,
                'name' => 'B11',
                'name_kana' => 'B11',
                'area_id' => 1,
                'zip_code' => 1000,
                'city_id' => 1,
                'address' => 'A11',
                'tel' => '',
                'open_time' => '9',
                'close_time' => '23',
                'latlng' => '',
                'del_flag' => 0,
                'ins_datetime' => date('Y-m-d H:i:s'),
                'ins_id' => 1,
                'upd_datetime' => date('Y-m-d H:i:s'),
                'upd_id' => 1
            ],
            [
                'code' => 3,
                'name' => 'A12',
                'name_kana' => 'A12',
                'area_id' => 1,
                'zip_code' => 1000,
                'city_id' => 2,
                'address' => 'A11',
                'tel' => '',
                'open_time' => '9',
                'close_time' => '23',
                'latlng' => '',
                'del_flag' => 0,
                'ins_datetime' => date('Y-m-d H:i:s'),
                'ins_id' => 1,
                'upd_datetime' => date('Y-m-d H:i:s'),
                'upd_id' => 1
            ],
            [
                'code' => 4,
                'name' => 'B12',
                'name_kana' => 'B12',
                'area_id' => 1,
                'zip_code' => 1000,
                'city_id' => 2,
                'address' => 'A11',
                'tel' => '',
                'open_time' => '9',
                'close_time' => '23',
                'latlng' => '',
                'del_flag' => 0,
                'ins_datetime' => date('Y-m-d H:i:s'),
                'ins_id' => 1,
                'upd_datetime' => date('Y-m-d H:i:s'),
                'upd_id' => 1
            ],
            [
                'code' => 5,
                'name' => 'A13',
                'name_kana' => 'B13',
                'area_id' => 1,
                'zip_code' => 1000,
                'city_id' => 3,
                'address' => 'A11',
                'tel' => '',
                'open_time' => '9',
                'close_time' => '23',
                'latlng' => '',
                'del_flag' => 0,
                'ins_datetime' => date('Y-m-d H:i:s'),
                'ins_id' => 1,
                'upd_datetime' => date('Y-m-d H:i:s'),
                'upd_id' => 1
            ],
            [
                'code' => 6,
                'name' => 'B13',
                'name_kana' => 'B13',
                'area_id' => 1,
                'zip_code' => 1000,
                'city_id' => 3,
                'address' => 'A11',
                'tel' => '',
                'open_time' => '9',
                'close_time' => '23',
                'latlng' => '',
                'del_flag' => 0,
                'ins_datetime' => date('Y-m-d H:i:s'),
                'ins_id' => 1,
                'upd_datetime' => date('Y-m-d H:i:s'),
                'upd_id' => 1
            ],
            [
                'code' => 7,
                'name' => 'A14',
                'name_kana' => 'A14',
                'area_id' => 1,
                'zip_code' => 1000,
                'city_id' => 4,
                'address' => 'A11',
                'tel' => '',
                'open_time' => '9',
                'close_time' => '23',
                'latlng' => '',
                'del_flag' => 0,
                'ins_datetime' => date('Y-m-d H:i:s'),
                'ins_id' => 1,
                'upd_datetime' => date('Y-m-d H:i:s'),
                'upd_id' => 1
            ],
            [
                'code' => 8,
                'name' => 'B14',
                'name_kana' => 'B14',
                'area_id' => 1,
                'zip_code' => 1000,
                'city_id' => 4,
                'address' => 'A11',
                'tel' => '',
                'open_time' => '9',
                'close_time' => '23',
                'latlng' => '',
                'del_flag' => 0,
                'ins_datetime' => date('Y-m-d H:i:s'),
                'ins_id' => 1,
                'upd_datetime' => date('Y-m-d H:i:s'),
                'upd_id' => 1
            ],
            [
                'code' => 9,
                'name' => 'A15',
                'name_kana' => 'A15',
                'area_id' => 1,
                'zip_code' => 1000,
                'city_id' => 5,
                'address' => 'A11',
                'tel' => '',
                'open_time' => '9',
                'close_time' => '23',
                'latlng' => '',
                'del_flag' => 0,
                'ins_datetime' => date('Y-m-d H:i:s'),
                'ins_id' => 1,
                'upd_datetime' => date('Y-m-d H:i:s'),
                'upd_id' => 1
            ],
            [
                'code' => 10,
                'name' => 'B15',
                'name_kana' => 'B15',
                'area_id' => 1,
                'zip_code' => 1000,
                'city_id' => 5,
                'address' => 'A11',
                'tel' => '',
                'open_time' => '9',
                'close_time' => '23',
                'latlng' => '',
                'del_flag' => 0,
                'ins_datetime' => date('Y-m-d H:i:s'),
                'ins_id' => 1,
                'upd_datetime' => date('Y-m-d H:i:s'),
                'upd_id' => 1
            ],

            [
                'code' => 11,
                'name' => 'A21',
                'name_kana' => 'A21',
                'area_id' => 2,
                'zip_code' => 1000,
                'city_id' => 1,
                'address' => 'A11',
                'tel' => '',
                'open_time' => '9',
                'close_time' => '23',
                'latlng' => '',
                'del_flag' => 0,
                'ins_datetime' => date('Y-m-d H:i:s'),
                'ins_id' => 1,
                'upd_datetime' => date('Y-m-d H:i:s'),
                'upd_id' => 1
            ],
            [
                'code' => 12,
                'name' => 'A22',
                'name_kana' => 'A22',
                'area_id' => 2,
                'zip_code' => 1000,
                'city_id' => 2,
                'address' => 'A11',
                'tel' => '',
                'open_time' => '9',
                'close_time' => '23',
                'latlng' => '',
                'del_flag' => 0,
                'ins_datetime' => date('Y-m-d H:i:s'),
                'ins_id' => 1,
                'upd_datetime' => date('Y-m-d H:i:s'),
                'upd_id' => 1
            ],
            [
                'code' => 13,
                'name' => 'A23',
                'name_kana' => 'A23',
                'area_id' => 2,
                'zip_code' => 1000,
                'city_id' => 3,
                'address' => 'A11',
                'tel' => '',
                'open_time' => '9',
                'close_time' => '23',
                'latlng' => '',
                'del_flag' => 0,
                'ins_datetime' => date('Y-m-d H:i:s'),
                'ins_id' => 1,
                'upd_datetime' => date('Y-m-d H:i:s'),
                'upd_id' => 1
            ],
            [
                'code' => 14,
                'name' => 'A24',
                'name_kana' => 'A24',
                'area_id' => 2,
                'zip_code' => 1000,
                'city_id' => 4,
                'address' => 'A11',
                'tel' => '',
                'open_time' => '9',
                'close_time' => '23',
                'latlng' => '',
                'del_flag' => 0,
                'ins_datetime' => date('Y-m-d H:i:s'),
                'ins_id' => 1,
                'upd_datetime' => date('Y-m-d H:i:s'),
                'upd_id' => 1
            ],

            [
                'code' => 15,
                'name' => 'A25',
                'name_kana' => 'A25',
                'area_id' => 2,
                'zip_code' => 1000,
                'city_id' => 5,
                'address' => 'A11',
                'tel' => '',
                'open_time' => '9',
                'close_time' => '23',
                'latlng' => '',
                'del_flag' => 0,
                'ins_datetime' => date('Y-m-d H:i:s'),
                'ins_id' => 1,
                'upd_datetime' => date('Y-m-d H:i:s'),
                'upd_id' => 1
            ],
        ]);
    }
}

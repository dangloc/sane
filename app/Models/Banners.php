<?php

namespace App\Models;

use App\Scopes\ActiveScope;
use Illuminate\Database\Eloquent\Model;

class Banners extends Model
{
    public $timestamps = false;
    protected $_alias = 'banners';
    protected $table = 'banners';
    protected $fillable = [
        'id',
        'banner_url',
        'link_url',
        'public_start_date',
        'public_end_date',
        'status',
        'del_flag',
        'ins_datetime',
        'ins_id',
        'upd_datetime',
        'upd_id'
    ];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ActiveScope());
    }
}

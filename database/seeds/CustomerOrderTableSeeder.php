<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CustomerOrderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customer_orders')->truncate();
        DB::table('customer_orders')->insert(
            [
                [
                    'slip_number' => '00190516000',
                    'customer_id' => 1,
                    'shop_code' => '1',
                    'receptionist_date' => '20190513',
                    'receipt_date' => '20190515',
                    'receipt_time' => '10',
                    'customer_name' => 'HN',
                    'customer_tel' => '0964161702',
                    'customer_card_no' => '190802100156',
                    'total_price'=>5700,
                    'status' => 0,
                    'send_status' => '0',
                    'web_order_timestamp' => '2019-05-16 09:36:49',
                    'ins_datetime' => '2019-05-16 09:36:49',
                    'ins_id' => 1
                ],
                [
                    'slip_number' => '00190516001',
                    'customer_id' => 2,
                    'shop_code' => '1',
                    'receptionist_date' => '20190513',
                    'receipt_date' => '20190515',
                    'receipt_time' => '10',
                    'customer_name' => 'HN',
                    'customer_tel' => '0964161702',
                    'customer_card_no' => '190802100156',
                    'total_price'=>4200,
                    'status' => 1,
                    'send_status' => '1',
                    'web_order_timestamp' => '2019-05-16 09:36:49',
                    'ins_datetime' => '2019-05-16 09:36:49',
                    'ins_id' => 1
                ],
                [
                    'slip_number' => '00190516002',
                    'customer_id' => 1,
                    'shop_code' => '1',
                    'receptionist_date' => '20190513',
                    'receipt_date' => '20190515',
                    'receipt_time' => '10',
                    'customer_name' => 'HN',
                    'customer_tel' => '0964161702',
                    'customer_card_no' => '190802100156',
                    'total_price'=>11020,
                    'status' => 2,
                    'send_status' => '1',
                    'web_order_timestamp' => '2019-05-16 09:36:49',
                    'ins_datetime' => '2019-05-16 09:36:49',
                    'ins_id' => 1
                ],
                [
                    'slip_number' => '00190516003',
                    'customer_id' => 1,
                    'shop_code' => '1',
                    'receptionist_date' => '20190513',
                    'receipt_date' => '20190530',
                    'receipt_time' => '10',
                    'customer_name' => 'HN',
                    'customer_tel' => '0964161702',
                    'customer_card_no' => '190802100156',
                    'total_price'=>12000,
                    'status' => 3,
                    'send_status' => '0',
                    'web_order_timestamp' => '2019-05-16 09:36:49',
                    'ins_datetime' => '2019-05-16 09:36:49',
                    'ins_id' => 1
                ]
            ]
        );
    }
}

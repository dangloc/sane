<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ForgetPassMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The demo object instance.
     *
     * @var Forget
     */
    public $forget;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($forget)
    {
        $this->forget = $forget;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_USERNAME'))
            ->view('frontend.site.mail_password_temp')
            ->subject("【サンエーネット予約サービス】パスワード再設定のご案内");
    }
}

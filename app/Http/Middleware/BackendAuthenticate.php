<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class BackendAuthenticate
{
    public function handle($request, Closure $next, $guards = null)
    {
        if (!Auth::guard($guards)->check()) {
            return redirect()->route('login');
        }

        return $next($request);
    }
}

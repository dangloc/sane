<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id')->length(11)->comment('ID');
            $table->string('email', 128)->comment('メールアドレス');
            $table->char('password', 64)->comment('パスワード')->nullable();
            $table->string('name', 128)->comment('お名前（カタカナ）');
            $table->string('tel', 11)->comment('電話番号');
            $table->char('del_flag', 1)->comment('削除フラグ')->default(0);
            $table->dateTime('ins_datetime')->default(DB::raw('CURRENT_TIMESTAMP'))->comment('登録日時');
            $table->integer('ins_id')->length(11)->comment('登録者ID');
            $table->dateTime('upd_datetime')->comment('更新日時')->nullable();
            $table->integer('upd_id')->length(11)->comment('更新者ID')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}

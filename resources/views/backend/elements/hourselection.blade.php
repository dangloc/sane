
<div class="custom">
    @php
        $start = "00:00";
        $end = "23:30";

        $tStart = strtotime($start);
        $tEnd = strtotime($end);
        $tNow = $tStart;
        $array = [];
        while($tNow <= $tEnd){
          array_push($array, date("H:i",$tNow));
          $tNow = strtotime('+30 minutes',$tNow);
        }
    @endphp
    <select name="{{$name}}" tabindex="10" id="{{isset($id)? $id : ''}}">
        @foreach($array as $time)
            <option {{ str_replace(":","", $time) == $hourSelected ? "selected": ""}} value="{{str_replace(":","", $time)}}">{{ $time }}時</option>
        @endforeach
            <option {{ "2400" == $hourSelected ? "selected": ""}} value="{{"2400"}}">{{ "24:00" }}時</option>
    </select>
</div>
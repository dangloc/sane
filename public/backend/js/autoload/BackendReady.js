var dispflag = 0;
$(function () {
    $('#headerNav ul').slicknav();

    $(".dialog").dialog({
        autoOpen: false,
        modal: true,
        buttons: {
            "OK": function () {
                $(this).dialog('close');
                System.showLoading();
            }
        }
    });
    $(".opener").on("click", function () {
        $("#dialog").dialog("open");
    });
});

$('#selectAll').change(function () {
    var checkboxes = $(this).closest('form').find(':checkbox');
    checkboxes.prop('checked', $(this).is(':checked'));
});

$('.selectAllShop').change(function(){
    var checkboxes = $(this).closest('table').find(':checkbox');
    checkboxes.prop('checked', $(this).is(':checked'));
});
$('.selectAllOption').change(function () {
    var checkboxes = $(this).closest('table').find(':checkbox');
    if($(this).prop("checked") ==  true){
        checkboxes.each(function(){
            if($(this).prop("checked") ==  false){
                $(this).click();
            }
        });
    }else if($(this).prop("checked") ==  false){
        checkboxes.each(function(){
            if($(this).prop("checked") ==  true){
                $(this).click();
            }
        });
    }
});

$(function () {
    $.datepicker.setDefaults($.datepicker.regional['ja']);
});

$('.alphanumeric').change(function () {
    var value = System.convertAlphanumericTo1Byte($(this).val());
    value = value.replace(/[\s]/g, '');
    $(this).val(value);
});

$('.clear-form').on('click', function () {
    $(this).closest('form').find("input, textarea, select").attr('disabled', 'disabled');
});

$('.disable-table').on('change', function () {
    $(this).closest('table').find("input,textarea,select").not(this).not('.active').attr('disabled', 'disabled');
});

$('#information_form_create').submit(function () {
    $('#release_period_start').val(
        $('#start_date').val() + ' ' + $('#start_hour').val() + ':00:00'
    );
    $('#release_period_end').val(
        $('#end_date').val() + ' ' + $('#end_hour').val() + ':00:00'
    );
});

$('#user_form_list').submit(function () {
    if ('' !== $('#start_date').val()) {
        $('#order_date_start').val(
            $('#start_date').val() + ' ' + $('#start_hour').val() + ':00:00'
        );
    } else {
        $('#order_date_start').val(null);
    }
    if ('' !== $('#end_date').val()) {
        $('#order_date_end').val(
            $('#end_date').val() + ' ' + $('#end_hour').val() + ':00:00'
        );
    } else {
        $('#order_date_end').val(null);
    }

});

$('.change-info').on('change', function () {
    $("input[name='_token'], input[name='_method']").attr('disabled', 'disabled');
    $(this).closest('form').attr("method", "get").submit();
});

$('.changeStockStatus').on('click', function () {
    if ($(this).hasClass('label-blue')) {
        $(this).removeClass('label-blue').addClass('label-red').html('売止');
        $(this).prev('input').val('0');
    } else {
        $(this).removeClass('label-red').addClass('label-blue').html('販売中');
        $(this).prev('input').val('1');
    }

});

$('.product-stock-save').on('click', function () {
    $('#form_product_stock').attr("method", "post").submit();
});

$(function () {
    if ($("#shop_id option[value!='']").length === 0) {
        $('#shop_list').hide();
    }
});

$(function () {
    if($.trim($('input[name="product_name"]').html()) !== ''){
        $('#btn_add_product_container').hide();
        $('.selected-product').show();
    }
});

$(document).on('blur', '.smallInt', function (e) {
    if (this.hasAttribute('data-vmax')) {
        var vMax = $(this).attr('data-vmax');
        if (parseInt(vMax) < parseInt(this.value) || this.value === '') {
            $(this).addClass('error-border');
            $('#stockMsg').show();
            $(':input[type="submit"]').prop('disabled', true);
        } else {
            $(this).removeClass('error-border');
        }
    }
    var result = true;
    $( ".smallInt" ).each(function( index ) {
        if (this.hasAttribute('data-vmax')) {
            var vMax = $(this).attr('data-vmax');
            if (parseInt(vMax) < parseInt(this.value) || this.value === '') {
                result = (result && false);
            }
        }
    });
    if (result){
        $('#stockMsg').hide();
        if ($(".shop-select").val()) {
            $(':input[type="submit"]').prop('disabled', false);
        } else {
            $(':input[type="submit"]').prop('disabled', true);
        }

    }
});

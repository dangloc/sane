<div class="container">
    <!-- Start Header on PC -->
    <div class="row hidden-sp">

        <div class="col-xs-12 col-sm-5">
            <div class="logo">
                <a href="{{route('site.index')}}">
                    <img src="{{asset('frontend/images/logo.png')}}">
                </a>
            </div>
        </div>

        <div class="col-xs-12 col-sm-7">

            <div class="main-menu" id="main-menu">
                <nav class="navbar navbar-expand-lg">
                    <div class="collapse navbar-collapse">
                        <div class="navbar-nav">
                            <li><a class="nav-link" href="{{route('guild')}}">ご利用ガイド</a></li>
                            {{--<li><a class="nav-link" href="#">お問い合わせ</a></li>--}}
                            <li><a class="nav-link" href="{{route('privacy')}}">プライバシーポリシー</a></li>
                            @if(Auth::guard(config('const.guard.frontend'))->check())
                                <li><a class="asset($-link nav-link btn" href="{{route('mypage.index')}}"><i class="fas fa-user-alt"></i>マイページ</a></li>
                            @else
                                <li><a class="login-link nav-link btn" href="{{route('frontend.login')}}"><i class="fas fa-sign-in-alt"></i>ログイン</a></li>
                            @endif
                            <li><a class="cart-link nav-link btn" href="{{route('cart.index')}}"><i class="fas fa-shopping-cart"></i>カート<span id="number-product-order" class="number-product-order"></span></a>
                            </li>
                        </div>
                    </div>
                </nav>
            </div>

        </div>
    </div>
    <!-- End Header on PC -->

    <!-- Start Header on SP -->
    <div class="header-sp hidden-md hidden-lg">
        <div class="row">
            <div class="menu-sp">
                <span class="openMenu"><img src="{{asset('frontend/images/openMenu.png')}}"></span>
                <span class="closeMenu"><img src="{{asset('frontend/images/closeMenu.png')}}"></span>
            </div>

            <div class="logo-sp">
                <a href="{{route('site.index')}}">
                    <img src="{{asset('frontend/images/logo-sp.png')}}">
                </a>
            </div>
            <div class="item-cart">
                @if(Auth::guard(config('const.guard.frontend'))->check())
                    <li><a class="login-link nav-link btn" href="{{route('mypage.index')}}"><i class="fas fa-user-alt"></i>マイページ</a>
                @else
                    <li><a class="login-link nav-link btn" href="{{route('frontend.login')}}"><i class="fas fa-sign-in-alt"></i>ログイン</a>
                @endif
                        <a class="cart-link nav-link btn" href="{{route('cart.index')}}"><i class="fas fa-shopping-cart"><span id="number-product-order" class="number-product-order"></span></i>カート</a>
                    </li>
            </div>
        </div>
    </div>
    <!-- End Header on SP -->

</div>

<!--Menu SP-->
<div id="menuSP" class="hidden-md hidden-lg">
    <div class="container">
        <div class="navbar-nav">
            <ul>
                <li><a class="nav-link" href="{{route('site.index')}}">トップページ </a></li>
                <li><a class="nav-link" href="https://www.san-a.co.jp/company/about/" target="_blank">会社概要</a></li>
                <li><a class="nav-link" href="{{route('guild')}}">ご利用ガイド</a></li>
                <li><a class="nav-link" href="{{route('tokutei')}}">特定商取引に関する法律に基づく表示</a></li>
                <li><a class="nav-link" href="{{route('browser')}}">推奨環境</a></li>
                <li><a class="nav-link" href="{{route('sitemap')}}">サイトマップ</a></li>
                <li><a class="nav-link" href="{{route('privacy')}}">プライバシーポリシー</a></li>
                <li><a class="nav-link" href="{{route('information')}}">お知らせ一覧</a></li>
            </ul>
        </div>
    </div>
</div>
<!--End Menu SP -->
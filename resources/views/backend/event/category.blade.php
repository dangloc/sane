@extends('backend.layouts.main')
@section('content')
    <div id="Contentwrap">
        <div id="PageContent">
            <h2><i class="fas fa-search"></i> 行事カテゴリーマスター検索</h2>
            <form action="{{route('event.category')}}" method="get">
                <div class="order">
                    <table class="sp_block">
                        <tr>
                            <th scope="row" style="width: 150px;">行事コード</th>
                            <td colspan="4">
                                <div class="inline_block rel sp-w45 line">
                                    <input type="text" placeholder="行事コード" tabindex="5" name="event_code" value="{{isset($params['event_code']) ? $params['event_code'] : ''}}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row" style="width: 150px;">カテゴリーコード</th>
                            <td colspan="4">
                                <div class="inline_block rel sp-w45 line">
                                    <input type="text" placeholder="カテゴリーコード" tabindex="5" name="category_code" value="{{isset($params['category_code']) ? $params['category_code'] : ''}}">
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="contentfoot">
                    <p class="btn Searchclear">
                        <button type="button" value="" onclick="System.redirectLoading('{{ route('event.category') }}')">検索条件をクリア</button>
                    </p>
                    <p class="btn Search">
                        <button type="submit" value="" onclick="System.showLoading()" tabindex="13"><img src="{{ asset('backend/images/common/icon3.png') }}" alt="検索">検索</button>
                    </p>
                </div>
                <!--ContentFoot -->
            </form>
        </div>
        <!--PageContent -->

        <div id="PageContent" class="pt60">
            <h2><i class="fas fa-shapes"></i> 行事カテゴリーマスター一覧</h2>

            <div class="PageContentNav">
                @include('backend.elements.paging', ['results' => $listData])
                {{ $listData->appends(request()->input())->links() }}
            </div>
            <table class="OrderForm responsive tdcenter Bordertd TextL table-hover">
                <tr>
                    <th scope="col" width="100px">@include("backend.elements.sortable", ["field" => "event_code", "title" => "行事コード"])</th>
                    <th scope="col" width="100px">@include("backend.elements.sortable", ["field" => "category_code", "title" => "カテゴリーコード"])</th>
                    <th scope="col" width="130px">@include("backend.elements.sortable", ["field" => "sort", "title" => "表示順"])</th>
                </tr>
                @if(!$listData->isEmpty())
                    @foreach($listData as $item)
                        <tr>
                            <td>{{$item->event_code}}</td>
                            <td>{{$item->category_code}}</td>
                            <td style="text-align:center;">{{$item->sort}}</td>
                        </tr>
                    @endforeach
                @else
                    @include('backend.elements.no_data',['col' => 3])
                @endif
            </table>
            <div class="PageContentNav">
                {{ $listData->appends(request()->input())->links() }}
            </div>
        </div>
        <!--PageContent -->

    </div>
    <!--Contentwrap -->
@endsection
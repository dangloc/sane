<div id="modal-choose-areas" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="group-input">
                <div class="row">
                    <div class="col-xs-12 col-lg-12">
                        <label>市町村</label>
                        <div class="area selectItem">
                            <select name="area" class="area-form" id="area-form">
                                @if(!empty($datas))
                                    @foreach($datas['cities'] as $data)

                                        <option value="{{$data['id']}}">{{$data['name']}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="Frontend.genShopMapPopup('{{$areaId}}')">OK</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">閉じる</button>
            </div>
        </div>

    </div>
</div>
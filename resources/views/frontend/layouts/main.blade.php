<!DOCTYPE html>
<html lang="ja">
<head>
    @include('frontend.elements.meta')
    @yield('css')
</head>
<body>
<!-- HEADER -->
<div id="main-header">
    @include('frontend.elements.header')
</div>
<div id="main-wrap">
    @yield('content')
    @include('frontend.elements.box_cart')
    @include('frontend.elements.lazy_loading')
</div>
@include('frontend.elements.footer')
</body>
</html>

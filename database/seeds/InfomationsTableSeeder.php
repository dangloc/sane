<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InfomationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('informations')->insert([
                [
                    'title' => 'test not show（ 那覇メインプレイス周辺エリア）の',
                    'content' => 'サンエーネットスーパーエリア拡大（ 那覇メインプレイス周辺エリア）の',
                    'public_start_date' => date('Y-m-d'),
                    'public_start_time' => date('H:i:s'),
                    'public_end_date' => date('Y-m-d'),
                    'public_end_time' => date('H:i:s'),
                    'del_flag' => 0,
                    'ins_datetime' => date('Y-m-d H:i:s'),
                    'ins_id' => 1,
                ],
                [
                    'title' => 'サンエーネットスーパーエリア拡大（ 那覇メインプレイス周辺エリア）の',
                    'content' => 'ダミーテキストダミーテキスト<br>

                                    ダミーテキストダミーテキストダミーテキストダミーテキスト<br>
                                    
                                    ダミーテキストダミーテキストダミーテキストダミーテキスト<br>
                                    
                                    ダミーテキストダミーテキスト',
                    'public_start_date' => date('Y-m-d'),
                    'public_start_time' => date('H:i:s'),
                    'public_end_date' => date('Y-m-d', strtotime(' +1 day')),
                    'public_end_time' => date('H:i:s'),
                    'del_flag' => 0,
                    'ins_datetime' => date('Y-m-d H:i:s'),
                    'ins_id' => 1,
                ],
                [
                    'title' => 'サンエーネットスーパーエリア拡大',
                    'content' => 'サンエーネットスーパーエリア拡大（ 那覇メインプレイス周辺エリア）の',
                    'public_start_date' => date('Y-m-d'),
                    'public_start_time' => date('H:i:s'),
                    'public_end_date' => date('Y-m-d', strtotime(' +1 day')),
                    'public_end_time' => date('H:i:s'),
                    'del_flag' => 0,
                    'ins_datetime' => date('Y-m-d H:i:s'),
                    'ins_id' => 1,
                ],
                [
                    'title' => 'サンエーネットスーパーエリア拡大（ 那覇メインプレイス周辺エリア',
                    'content' => 'サンエーネットスーパーエリア拡大（ 那覇メインプレイス周辺エリア）の',
                    'public_start_date' => date('Y-m-d'),
                    'public_start_time' => date('H:i:s'),
                    'public_end_date' =>date('Y-m-d', strtotime(' +1 day')),
                    'public_end_time' => date('H:i:s'),
                    'del_flag' => 0,
                    'ins_datetime' => date('Y-m-d H:i:s'),
                    'ins_id' => 1,
                ]
            ]
        );
    }
}

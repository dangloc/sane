@if(strlen(strstr(url()->current(), 'management')) > 0)
    @include('errors.backend.401')
@else
    @include('errors.frontend.401')
@endif
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<meta http-equiv="X-UA-Compatible" content="ie=edge"/>
<title>サンエーネット予約サービス</title>
<meta name="description" content=“サンエーのオードブルやお寿司やケーキをネットで予約・店舗で受取りできるサービスです。受取り店舗・日にちを選んで予約。マイページの利用でキャンセル、確認も簡単です”>
<link rel="canonical" href="https://yoyaku.san-a.co.jp/">
<meta property="og:type" content="website">
<meta property="og:locale" content="ja_JP">
<meta property="og:title" content="サンエーネット予約サービス">
<meta property="og:image" content="{{asset('frontend/images/common/ogp.png')}}”>
<meta property="og:description" content="サンエーのオードブルやお寿司やケーキをネットで予約・店舗で受取りできるサービスです。受取り店舗・日にちを選んで予約。マイページの利用でキャンセル、確認も簡単です">
<meta property="og:url" content="https://yoyaku.san-a.co.jp">
<meta property="og:site_name" content="サンエーネット予約サービス"/>
<link rel="icon" href="{{asset('frontend/images/common/favicon.ico')}}">
<link rel="apple-touch-icon" sizes="180x180" href="{{asset('frontend/images/common/apple-touch-icon-180x180.png')}}">

@yield('css')
<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/style.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/main.css')}}"/>
<link rel="stylesheet" type="text/css" media="print" href="{{asset('frontend/css/print.css')}}"/>
<link href="{{asset('frontend/css/datepicker.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('frontend/css/toastr.css')}}" rel="stylesheet" type="text/css"/>
<meta name="csrf-token" content="{{ csrf_token() }}">

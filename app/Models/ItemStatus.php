<?php

namespace App\Models;
use App\Scopes\ActiveScope;

use Illuminate\Database\Eloquent\Model;

class ItemStatus extends Model
{
    public $timestamps = false;
    protected $_alias = 'item_status';
    protected $table = 'item_status';
    protected $fillable = [
        'shop_code',
        'code',
        'stop_flag',
        'del_flag',
        'ins_datetime',
        'ins_id',
        'upd_datetime',
        'upd_id'
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ActiveScope());
    }
}

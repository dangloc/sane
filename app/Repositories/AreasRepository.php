<?php

namespace App\Repositories;


/**
 * class AreasRepository.
 *
 * @package namespace App\Repositories;
 */
class AreasRepository extends BaseRepository
{
    function __construct()
    {
        $this->_model = \App\Models\Areas::class;
    }

    public function getCity($area_id) {
        return $this->getModel()->find($area_id) != null ? $this->getModel()->find($area_id)->cities : [];
    }
}

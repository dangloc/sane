<table class="OrderForm item-detail-popup responsive">
    <tr>
        <th>店舗コード</th>
        <th>商品コード</th>
        <th>画像名</th>
    </tr>
    <tr>
        <td style="text-align:left;">{{isset($data->shop_code) ? $data->shop_code : '_'}}</td>
        <td style="text-align:left;">{{isset($data->code) ? $data->code : '_'}}</td>
        <td style="text-align:left;">{!! !empty($data->item_image_name) ? nl2br(e($data->item_image_name)) : '_' !!}</td>
    </tr>
    <tr>
        <th colspan="3">項目1</th>
    </tr>
    <tr>
        <td colspan="3" style="text-align:left;" class="item-content">{!! isset($data->content1) ? nl2br(e($data->content1)) : '_' !!}</td>
    </tr>
    <tr>
        <th colspan="3">項目2</th>
    </tr>
    <tr>
        <td colspan="3" style="text-align:left;" class="item-content">{!! isset($data->content2) ? nl2br(e($data->content2)) : '_' !!}</td>
    </tr>
    <tr>
        <th colspan="3">項目3</th>
    </tr>
    <tr>
        <td colspan="3" style="text-align:left;" class="item-content">{!! isset($data->content3) ? nl2br(e($data->content3)) : '_' !!}</td>
    </tr>
    <tr>
        <th>必須確認事項</th>
        <th>限定数フラグ</th>
        <th>限定数</th>
    </tr>
    <tr>
        <td style="text-align:center;">{{isset($data->required_confirm_flag) ? $data->required_confirm_flag : '_'}}</td>
        <td style="text-align:center;">{{isset($data->limited_flag) ? $data->limited_flag : '_'}}</td>
        <td style="text-align:center;">{{isset($data->limited_count) ? $data->limited_count : '_'}}</td>
    </tr>
    <tr>
        <th>添加物コード</th>
        <th>リードタイム</th>
        <th>画像ファイル名</th>
    </tr>
    <tr>
        <td style="text-align:left;">{{isset($data->additive_code) ? $data->additive_code : '_'}}</td>
        <td style="text-align:center;">{{isset($data->lead_time) ? $data->lead_time : '_'}}</td>
        <td style="text-align:left;">{{!empty($data->item_image_folder_name) ? $data->item_image_folder_name : '_'}}</td>
    </tr>
    <tr>
        <th>予備項目1</th>
        <th>予備項目2</th>
        <th>予備項目3</th>
    </tr>
    <tr>
        <td style="text-align:center;">{!! isset($data->reserve_content1) ? nl2br(e($data->reserve_content1)) : '_' !!}</td>
        <td style="text-align:center;">{!! isset($data->reserve_content2) ? nl2br(e($data->reserve_content2)) : '_' !!}</td>
        <td style="text-align:center;">{!! isset($data->reserve_content3) ? nl2br(e($data->reserve_content3)) : '_' !!}</td>
    </tr>
</table>
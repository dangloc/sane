<?php

namespace App\Repositories;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * class ItemStatusRepository.
 *
 * @package namespace App\Repositories;
 */
class ItemStatusRepository extends BaseRepository
{

    function __construct()
    {
        $this->_model = \App\Models\ItemStatus::class;
    }

    public function updateStopFlag($item_code, $shop_code, $flag)
    {
        // return code : 1 -> update ok , 0 -> update not ok , 2 -> cant find item_status
        $upd_date = Carbon::now();
        $upd_id = Auth::id();
        $item_status = $this->getModel()::where('code', '=', $item_code)->where('shop_code', '=', $shop_code)->get();
        if (sizeof($item_status) != 0) {
            try {
                $this->getModel()::where('code', '=', $item_code)
                    ->where('shop_code', '=', $shop_code)
                    ->update([
                        'stop_flag' => $flag,
                        'upd_datetime' => $upd_date,
                        'upd_id' => $upd_id
                    ]);
                return 1;
            } catch (\Exception $e) {
                //logging
                return 0;
            }
        } else {
            return 2;
        }

    }
}

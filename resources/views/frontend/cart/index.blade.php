@extends('frontend.layouts.main')
@section('content')
    <div id="cart-page" class="cart-page">
        <div class="container">
            <div class="cart">

                <div class="title-page">
                    <h1 class="title"><i class="fas fa-shopping-cart"></i>カートの中</h1>
                </div>
                <div class="description">
                    <p>ご注文商品</p>
                </div>

                @if (!empty($datas))
                    @foreach($datas as $itemList)
                        <div class="cart-item-content">
                            <div class="cart-list-show">
                                <ul>
                                    @if (!empty($itemList['itemList']))
                                        @foreach($itemList['itemList'] as $data)
                                            <li id="product-{{$itemList['keyItem']}}-{{$data['keyProduct']}}">
                                                <div class="list-items">
                                                    <div class="image-show left">
                                                        <a href="{{route('product.detail',['id'=>$data['code'] ,'shopcode' => $data['shop_code'], 'keyitem'=>$itemList['keyItem'],'keyproduct'=>$data['keyProduct']])}}">
                                                            <img src="{{ ($data['item_image_folder_name'] == null || $data['item_image_folder_name']=='_') ? asset('/frontend/images/item/noimage.jpg')
                                                    : config('const.url_item_image').$data['item_image_folder_name']}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="information-cart right">
                                                        <div class="title-product <?= !empty($data['bonus']) ? 'bonus-fix' : ''?>">
                                                            <h3>
                                                                <a href="{{route('product.detail',['id'=>$data['code'] ,'shopcode' => $data['shop_code'],'keyitem'=>$itemList['keyItem'],'keyproduct'=>$data['keyProduct']])}}">{{$data['item_name']}}</a>
                                                                @if($data['required_confirm_flag']!= config('const.not_with'))
                                                                    <label class="check_required_confirm <?= ($data['requiredFlag'] == '0') ? "confirm_checked" : ""?>">
                                                                        <input onclick="Frontend.changeRequiredFlag(event,this)"
                                                                               type="radio" hidden value="0"
                                                                               id="check_required_true"
                                                                               keyItem ='{{$itemList['keyItem']}}'
                                                                               keyProduct ='{{$data['keyProduct']}}'
                                                                               name="check_required_true_{{$itemList['keyItem']}}_{{$data['code']}}_{{$data['shop_code']}}">
                                                                        @if($data['required_confirm_flag'] == config('const.with_wasabi'))
                                                                            わさび入り @elseif($data['required_confirm_flag'] == config('const.with_kamaboko'))
                                                                            かまぼこ赤 @elseif($data['required_confirm_flag']== config('const.with_classic'))
                                                                            定番
                                                                        @endif
                                                                    </label>
                                                                    <label class="check_required_confirm <?= ($data['requiredFlag'] == '1') ? "confirm_checked" : ""?>">
                                                                        <input onclick="Frontend.changeRequiredFlag(event,this)"
                                                                               type="radio" hidden value="1"
                                                                               id="check_required_false"
                                                                               keyItem ='{{$itemList['keyItem']}}'
                                                                               keyProduct ='{{$data['keyProduct']}}'
                                                                               name="check_required_true_{{$itemList['keyItem']}}_{{$data['code']}}_{{$data['shop_code']}}">
                                                                        @if($data['required_confirm_flag'] == config('const.with_wasabi'))
                                                                            わさび抜き@elseif($data['required_confirm_flag'] == config('const.with_kamaboko'))
                                                                            かまぼこ白 @elseif($data['required_confirm_flag'] == config('const.with_classic'))
                                                                            法事
                                                                        @endif
                                                                    </label>
                                                                @endif
                                                            </h3>

                                                        </div>
                                                        @if(!empty($data['bonus']))
                                                            <span class="red-text">{{$data['bonus']}}</span>
                                                        @endif
                                                        <div class="information-detail <?= !empty($data['bonus']) ? 'bonus-fix' : ''?>">
                                                            <div class="information-text">
                                                                <div class="price price-one-products">
                                                                    <span class="text">価格</span>
                                                                    <span class="number"><span id="price-{{$itemList['keyItem']}}-{{$data['keyProduct']}}">{{number_format($data['price'])}}</span></span>
                                                                    <span class="unit">円+税</span>
                                                                </div>
                                                                <div class="quantity">
                                                                    <div class="quantity-btn">
                                                                        <label>数量</label>
                                                                        <button class="btn minus" onclick="Frontend.changeQuantitySaveSession(this,'sub')"><i class="fas fa-minus"></i></button>
                                                                        <input type="number"  data-keyItem ="{{$itemList['keyItem']}}" data-keyProduct="{{$data['keyProduct']}}" onblur="Frontend.blurQuantity(this)"
                                                                               id="quantity-{{$itemList['keyItem']}}-{{$data['keyProduct']}}"
                                                                               name="stock_{{$data['keyProduct']}}" value="{{$data['quantity']}}" min="1" max="999" step="1" onkeyup="Frontend.keyUpQuantity(this)">
                                                                        <button class="btn minus" onclick="Frontend.changeQuantitySaveSession(this,'up')"><i class="fas fa-plus"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="price price-total-products">
                                                                    <span class="text">小計</span>
                                                                    <span class="number"><span id="total-price-{{$itemList['keyItem']}}-{{$data['keyProduct']}}">{{number_format($data['price'] * $data['quantity'])}}</span></span>
                                                                    <span class="unit">円+税</span>
                                                                </div>
                                                            </div>

                                                            <div class="information-btn">
                                                                <button class="btn_remove" data-keyItem ="{{$itemList['keyItem']}}" data-keyProduct = "{{$data['keyProduct']}}" onclick="Frontend.showPopupRemove(this,false)"><i class="fas fa-trash-alt"></i>削除
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <span class="red-text error {{$data['keyProduct']}}"></span>
                                                </div>
                                            </li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                            <div class="cart-total">
                                <ul>
                                    <li class="total-products">
                                        <div class="total-products-items">
                                            <label>商品数</label>
                                            <span class="total-number" id="total-quantity-{{$itemList['keyItem']}}-{{$data['keyProduct']}}" >{{$itemList['total']}}品</span>
                                        </div>
                                    </li>
                                    <li class="total-price">
                                        <div class="price-items">
                                            <label>合計金額</label>
                                            <span class="total-number-price"><span
                                                        class="number" id="total-quantity-price-{{$itemList['keyItem']}}">{{number_format($itemList['totalPrice'])}}</span><span
                                                        class="unit">円+税</span></span>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="product-bottom">
                                <ul>
                                    <li>
                                        <a href="#">
                                            <label>受取り店舗</label>
                                            <strong>{{$itemList['shopName']}}</strong>
                                            <span class="date">（営業時間 {{$itemList['shopOpenTime']}}〜{{$itemList['shopCloseTime']}}）</span>
                                        </a>

                                    </li>
                                    <li>
                                        <a href="#">
                                            <label>商品カテゴリ</label>
                                            <strong>{{$itemList['eventName']}}</strong>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <label>受取り日</label>
                                            <strong>{{$itemList['dateReceive']['year']}}年{{$itemList['dateReceive']['month']}}月{{$itemList['dateReceive']['day']}}日({{dayJapan($itemList['dateReceiveRaw'])}})</strong>
                                            <strong class="date">{{$itemList['timeReceive']}}時</strong>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    @endforeach
                @else
                    <span class="no-data-cart">お客様のカートに商品はありません。</span>
                @endif
                <div class="group-btn-bottom">
                    <div class="back-to-page">
                        <a class="back-step" href="{{route('site.index')}}"><i class="fas fa-chevron-left" onclick=""></i>お買い物を続ける</a>
                        @if (!empty($datas))
                            <a class="go-order-process" href="javascript:Frontend.checkProduct('confirm')">ご注文手続きへ進む
                                <img class="lazy-load" src="{{asset('frontend/images/ajax-loader.gif')}}"></a>
                        @endif
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('js')
    <script type="text/javascript">
        $('.shopping-cart-fix-bottom').css('display','none');
    </script>
@endsection



<?php

namespace App\Repositories;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
/**
 * class ReserveRepository.
 *
 * @package namespace App\Repositories;
 */
class ReserveRepository extends BaseRepository
{
    function __construct()
    {
        $this->_model = \App\Models\Reserve::class;
    }

    /**
     * @param $dataCartList
     * @param $customerName
     * @param $customerTel
     * @param $customerCardNo
     * @return array
     * insert confirm boook reserve --- man hinh cart list
     */
    public function insertConfirmBookReserve($dataCartList, $customerName, $customerTel, $customerCardNo)
    {
        if (!empty($dataCartList)) {
            $insertArray = array();
            foreach ($dataCartList as $keyItem => $dataList) {
                $receptionistDate = Carbon::now()->format('Ymd');
                $slipNumber = createSlipNumber();
                $receiptDate = $dataList['dateReceive']['year'] . $dataList['dateReceive']['month'] . $dataList['dateReceive']['day'];
                foreach ($dataList['itemList'] as $keyProduct => $item) {
                    $dataReserve = array(
                        'slip_number' => $slipNumber,
                        'shop_code' => $dataList['shopCode'],
                        'receptionist_date' => $receptionistDate,
                        'receptionist_type' => 3,
                        'receptionist_staff_code' => 999999,
                        'receipt_date' => $receiptDate,
                        'receipt_time' => $dataList['timeReceive'],
                        'customer_name' => $customerName,
                        'customer_tel' => $customerTel,
                        'customer_card_no' => $customerCardNo,
                        'event_code' => $dataList['eventCode'],
                        'item_code' => $item['code'],
                        'required_confirm_flag' => $item['required_confirm_flag'],
                        'required_flag' => $item['requiredFlag'],
                        'item_count' => $item['quantity'],
                        'status_flag' => 0,
                        'update_flag' => 0,
                        'reserve_content1' => '',
                        'reserve_content2' => '',
                        'reserve_del_flag' => 0,
                        'update_timestamp' => Carbon::now()->format('Ymd'),
                        'update_staff_code' => 999999,
                        'del_flag' => 0,
                        'ins_datetime' => Carbon::now(),
                        'ins_id' => 999999,
                    );
                    $this->getModel()->insert($dataReserve);
                    $dataReserve['price'] = $item['price'];
                    $insertArray[] = $dataReserve;
                }
            }
            return $insertArray;
        }
        return array();
    }

    public function updateInfoReserve($params=array(),$data){
        $query=$this->getModel();
        foreach ($params as $key=>$value){
            $query=$query->where($key,$value);
        }
        if ($query->update($data)){
            return true;
        }
        return false;
    }

    /**
     * @param string $shopCode
     * @param string $eventCode
     * @param string $itemCode
     * @return string
     * getSlipNumber
     */
    public function getSlipNumber($shopCode = '', $eventCode = '', $itemCode = '', $timeReceive)
    {
        $data = $this->getModel()
            ->select(DB::raw('MAX(slip_number) as slip_number'))
            ->where('shop_code', '=', $shopCode)
            ->where('event_code', '=', $eventCode)
            ->where('item_code', '=', $itemCode)
            ->where('receipt_time', '=', $timeReceive)
            ->first();
        if (!empty($data)) {
            return $data->slip_number;
        }
        return '';
    }

    /**
     * @param int $slipNumber
     * @return array
     * get All data infor
     */
    public function getDataOrderBySlip($slipNumber = 0)
    {
        if (!empty($slipNumber)) {
            $data = $this->getModel()->select('reserves.slip_number', 'reserves.customer_name', 'reserves.customer_tel', 'reserves.customer_card_no', 'c.name as city_name',
                'reserves.shop_code', 's.name as shop_name', 's.address','s.tel', 'reserves.event_code', 'reserves.receipt_date','reserves.receptionist_date','reserves.receipt_time')
                ->join('shops AS s', 's.code', '=', 'reserves.shop_code')
                ->join('cities AS c', 'c.id', '=', 's.city_id')
                ->where('reserves.slip_number', '=', $slipNumber)
                ->where('reserves.del_flag', '=', 0)
                ->where('reserves.status_flag', '=', 0)
                ->withoutGlobalScopes()
                ->first();
            if (!empty($data)){
                return $data->toArray();
            }
        }
        return array();
    }
    // for admin : customer_order detail page
    public function getReserveBySlipNo($slip_number)
    {
        return $this->getModel()->where('slip_number', '=', $slip_number)->get();
    }
}

@extends('frontend.layouts.main')
@section('content')
    <div class="container">
        <div class="change_customer_info">

            <div class="title-page">
                <h1 class="title">マイページ</h1>
            </div>

            <div class="group_button">
                <button class="button_back" onclick="location.href='{{route('mypage.index')}}'">マイページTOPへ戻る</button>
                <button class="change_email" onclick="location.href = '{{route('mypage.change_password_entry')}}'">
                    メールアドレス・パスワードの変更
                </button>
                <button class="login" onclick="location.href = '{{route('frontend.logout')}}'"><i
                            class="fas fa-sign-out-alt"></i>ログアウト
                </button>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="change_customer_info">
            <form id="form_change_customer_info" method="POST" action="{{route('mypage.post.change_customer_info',$slip_number)}}" autocomplete="off">
                @csrf
                <h1 class="title">お客様情報の変更 <span>(伝票番号：{{$data_order_booking->slip_number}})</span></h1>
                <div class="form_input">
                    <div class="group_input">
                        <table>
                            <tr>
                                <td class="td_old"><span class="label_old">お名前（カタカナ）</span></td>
                                <td><span class="old_data">旧<span>{{$data_order_booking->customer_name}}</span></span></td>
                            </tr>
                        </table>
                        <div class="label_new">
                            <span>新</span><input class="new_data" type="text" name="name" placeholder="例：ヤマダハナコ" value="{{ old('name') }}">
                            @include('frontend.elements.validate_error',['message'=>$errors->first('name')])
                        </div>
                    </div>
                    <div class="group_input">
                        <table>
                            <tr>
                                <td class="td_old"><span class="label_old">電話番号</span></td>
                                <td><span class="old_data">旧<span>{{$data_order_booking->customer_tel}}</span></span></td>
                            </tr>
                        </table>
                        <div class="label_new">
                            <span>新</span><input class="new_data" type="text" name="tel" placeholder="例：0901234567 ハイフン無し" value="{{ old('tel') }}">
                            @include('frontend.elements.validate_error',['message'=>$errors->first('tel')])
                        </div>
                    </div>
                    <div class="group_input">
                        <table>
                            <tr>
                                <td class="td_old"><span class="label_old">サンエーカード番号</span></td>
                                <td><span class="old_data">旧<span>{{formatCardNo($data_order_booking->customer_card_no)}}</span></span></td>
                            </tr>
                        </table>
                        <div class="td_card_no">
                            <span class="label_new">新</span>
                                <span class="card_no">
                                        <input name="card[]" onpaste="return false" maxlength="4" onkeypress="Frontend.maxLengthInputNumber(event,this)" type="number" value="{{ old('card.0') }}"> ー
                                        <input name="card[]" onpaste="return false" maxlength="4" onkeypress="Frontend.maxLengthInputNumber(event,this)" type="number" value="{{ old('card.1') }}"> ー
                                        <input name="card[]" onpaste="return false" maxlength="4" onkeypress="Frontend.maxLengthInputNumber(event,this)" type="number" value="{{ old('card.2') }}">
                                 </span>
                            <div class="label_new">
                                @if(null != $errors->first('card.*'))
                                    @include('frontend.elements.validate_error',['message'=>$errors->first('card.*')])
                                @else
                                    @include('frontend.elements.validate_error',['message'=>$errors->first('card')])
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="button">
                    <button type="submit" class="button_submit">入力内容を確認する</button>
                </div>
            </form>
        </div>
    </div>
@endsection
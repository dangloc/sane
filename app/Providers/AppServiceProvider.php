<?php

namespace App\Providers;

use App\Models\Reserve;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        app('view')->composer('backend.layouts.main', function ($view) {
            $action = app('request')->route();
            if (!$action){
                return;
            } else {
                $action = $action->getAction();
            }
            $controller = class_basename($action['controller']);
            $controller = strtolower(str_replace('Controller', '', $controller));

            list($controller, $action) = explode('@', $controller);

            $view->with(compact('controller', 'action'));
        });

        Validator::extend('phone', function($attribute, $value, $parameters, $validator) {
            return (boolean)preg_match('%^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$%i', $value);
        },trans('validation.invalid'));

        Validator::extend('katakana',function ($attribute, $value, $parameters, $validator){
            mb_regex_encoding("UTF-8");
            return (boolean)(preg_match("/^[ァ-ヶーａ-ｚＡ-Ｚ０-９ァ-ンｧ-ﾝﾞﾟ　 ]+$/u", $value));
        },trans('validation.katakana'));

        Validator::extend('check_email',function($attribute, $value, $parameters, $validator){
            return filter_var($value, FILTER_VALIDATE_EMAIL) !== false;
        },trans('validation.check_email'));

        Validator::extend('alpha_num_latin',function ($attribute, $value, $parameters, $validator){
            return (boolean)(preg_match("/^[a-zA-Z0-9 ]+$/u", $value));
        },trans('validation.alpha'));
        Validator::extend('alpha_num_nospace',function ($attribute, $value, $parameters, $validator){
            return ctype_alnum($value);
        },trans('validation.alpha_num_nospace'));
        Validator::extend('zip_code',function ($attribute, $value, $parameters, $validator){
            return (boolean)(preg_match("/^[0-9 -]+$/u", $value));
        },trans('validation.invalid'));
    }
}

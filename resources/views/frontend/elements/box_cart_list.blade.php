<?php $sum = $sumQuantity = 0;?>
@if(!empty($dataCartList))
    @foreach($dataCartList as $dataList)
        <div class="product-bottom" id="product-{{$dataList['keyItem']}}">
            <ul>
                <li>
                    <a href="#">
                        <label>受取り店舗</label>
                        <strong>{{$dataList['shopName']}}</strong>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <label>受取り日</label>
                        <strong>{{$dataList['dateReceive']['year']}}年{{$dataList['dateReceive']['month']}}月{{$dataList['dateReceive']['day']}}日({{dayJapan($dataList['dateReceiveRaw'])}}) {{$dataList['timeReceive']}}時</strong>
                    </a>
                </li>
            </ul>
        </div>
        @foreach($dataList['itemList'] as $data)
            <?php
            $sum += $data['total_price'];
            $sumQuantity += $data['quantity'];
            ?>
            <div class="item-block cart-product-{{$data['code']}} only-show" id="product-{{$dataList['keyItem']}}-{{$data['keyProduct']}}">
                <div class="item-info">
                    <img src="{{ ($data['item_image_folder_name'] ==null || $data['item_image_folder_name']=='_') ? asset('/frontend/images/item/noimage.jpg')
                                                    : config('const.url_item_image').$data['item_image_folder_name']}}" alt="">
                    <a href="#" class="link-product-item"><span>{{$data['item_name']}}</span>
                        @if($data['required_confirm_flag']!=config('const.not_with'))
                            <label class="required-confirm" style="<?= ($data['requiredFlag'] == '0') ? "display:inline-block" : "display:none"?>">

                                @if($data['required_confirm_flag']== config('const.with_wasabi'))
                                    (わさび入り) @elseif($data['required_confirm_flag']==config('const.with_kamaboko'))
                                    (かまぼこ赤) @elseif($data['required_confirm_flag']==config('const.with_classic')) (定番)
                                @endif
                            </label>
                            <label class="required-confirm" style="<?= ($data['requiredFlag'] == '1') ? "display:inline-block" : "display:none"?>" >
                                @if($data['required_confirm_flag']==config('const.with_wasabi'))
                                    (わさび抜き)@elseif($data['required_confirm_flag']==config('const.with_kamaboko'))
                                    (かまぼこ白) @elseif($data['required_confirm_flag']==config('const.with_classic')) (法事)
                                @endif
                            </label>
                        @endif
                    </a>
                </div>
                <div class="item-quantity">
                    <span onclick="Frontend.changeQuantitySaveSession(this,'sub');"><i class="fas fa-minus"></i></span>
                    <input class="quantity-number tCenter number" data-keyItem="{{$dataList['keyItem']}}"
                           data-keyProduct="{{$data['keyProduct']}}" value="{{$data['quantity']}}" type="number"
                           step="1" min="1" max="999" onkeyup="Frontend.keyUpQuantity(this)" onblur="Frontend.blurQuantity(this)">
                    <input type="hidden" id = "quantity-old-{{$dataList['keyItem']}}-{{$data['keyProduct']}}" value="{{$data['quantity']}}">
                    <span onclick="Frontend.changeQuantitySaveSession(this,'up');"><i class="fas fa-plus"></i></span>
                </div>
                <div class="item-price">
                    <div class="item-delete">
                        <a href="#" data-toggle="modal" onclick="Frontend.showPopupRemove(this,true);" data-keyItem="{{$dataList['keyItem']}}" data-keyProduct = "{{$data['keyProduct']}}"><font
                                    style="vertical-align: inherit;"><font
                                        style="vertical-align: inherit;">削除</font></font></a>
                    </div>
                    <div class="item-price-info">
                        <strong><strong class="money-text" id="price-{{$dataList['keyItem']}}-{{$data['keyProduct']}}">{{number_format($data['price'])}}</strong>円＋税</strong>
                        <span><span class="money-text total-price-item" id="total-price-{{$dataList['keyItem']}}-{{$data['keyProduct']}}">{{number_format($data['total_price'])}}</span>円＋税</span>
                    </div>
                    <input type="hidden" id="item-price" value="{{$data['price']}}"/>
                    <input type="hidden" id="item-code" value="{{$data['code']}}"/>
                    <input type="hidden" id="item-quantity" value="{{$data['quantity']}}"/>
                    <input type="hidden" class="update-cart" value="0">
                </div>
            </div>
        @endforeach
    @endforeach
@endif
<input type="hidden" value="{{$sum}}" class="sum-price-box-cart">
<input type="hidden" value="{{$sumQuantity}}" class="sum-quantity-box-cart">
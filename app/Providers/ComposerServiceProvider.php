<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        app('view')->composer('backend.layouts.main',function ($view){
            $shop = DB::table('shops')->where('id',1)->first();
             $view->with(compact('shop'));
        });
        view()->composer(
            ['backend.layouts.main'],
            'App\Http\ViewComposers\MovieComposer'
        );
    }

}

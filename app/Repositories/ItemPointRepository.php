<?php

namespace App\Repositories;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * class ItemPointRepository.
 *
 * @package namespace App\Repositories;
 */
class ItemPointRepository extends BaseRepository
{
    function __construct()
    {
        $this->_model = \App\Models\ItemBonusPoint::class;
    }

    public function paging()
    {
        $sort = request()->get('sort', 'item_code');
        $direction = request()->get('direction', 'desc');
        $params = request()->all();

        return $this->getModel()
            ->when(isset($params['item_code']) && $params['item_code'] != '', function ($q) use ($params) {
                return $q->where('item_code', 'like', '%' . $params['item_code'] . '%');
            })
            // refix issuse 5680
            ->when(isset($params['receptionist_start_date']) && $params['receptionist_start_date'] != '', function ($q) use ($params) {
                return $q->where('receptionist_end_date', '>=', date('Ymd', strtotime($params['receptionist_start_date'])));
            })
            ->when(isset($params['receptionist_end_date']) && $params['receptionist_end_date'] != '', function ($q) use ($params) {
                return $q->where('receptionist_start_date', '<=', date('Ymd', strtotime($params['receptionist_end_date'])));
            })
            ->when(isset($params['receipt_start_date']) && $params['receipt_start_date'] != '', function ($q) use ($params) {
                return $q->where('receipt_end_date', '>=', date('Ymd', strtotime($params['receipt_start_date'])));
            })
            ->when(isset($params['receipt_end_date']) && $params['receipt_end_date'] != '', function ($q) use ($params) {
                return $q->where('receipt_start_date', '<=', date('Ymd', strtotime($params['receipt_end_date'])));
            })
            ->orderBy($sort, $direction)
            ->paginate(config('const.paging.backend'));
    }
}

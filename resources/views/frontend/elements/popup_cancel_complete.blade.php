<div id="modal-cancel-complete" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="group-input">
                <div class="row">
                    <div class="col-xs-12 col-lg-12">
                        <label>注文をキャンセルします。よろしいですか？</label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="Frontend.cancelOrder('{{$slipNumber}}')">OK</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">閉じる</button>
            </div>
        </div>

    </div>
</div>
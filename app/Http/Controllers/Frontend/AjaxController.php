<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Repositories\AddToCartRepository;
use App\Repositories\AjaxRepository;
use App\Repositories\AreasRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\CitiesRepository;
use App\Repositories\EventRepository;
use App\Repositories\ItemBonusPointRepository;
use App\Repositories\ItemLimitedOrderPossibleCountRepository;
use App\Repositories\ItemRepository;
use App\Repositories\ShopPlanOrderPossibleCountRepository;
use App\Repositories\ShopsRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    public $citiesRepository;
    public $shopsRepository;
    public $areasRepository;
    public $eventRepository;
    public $categoryRepository;
    public $itemRepository;
    public $ajaxRepositoty;

    public function __construct(CitiesRepository $citiesRepository,
                                ShopsRepository $shopsRepository,
                                AreasRepository $areasRepository,
                                EventRepository $eventRepository,
                                ItemRepository $itemRepository,
                                CategoryRepository $categoryRepository,
                                AjaxRepository $ajaxRepositoty
    )
    {
        $this->citiesRepository = $citiesRepository;
        $this->shopsRepository = $shopsRepository;
        $this->areasRepository = $areasRepository;
        $this->eventRepository = $eventRepository;
        $this->itemRepository = $itemRepository;
        $this->categoryRepository = $categoryRepository;
        $this->ajaxRepositoty = $ajaxRepositoty;
    }

    /**
     * @param Request $request
     * render value when click button in map
     */
    public function renderShopByMap(Request $request)
    {
        $areaId = $request->input('area_id', false);
        $cityId = $request->input('city_id', false);
        $textShop = $request->input('text_shop', false);
        $data = $this->ajaxRepositoty->controlDataRenderShopMap($areaId, $cityId, $textShop);
        return response()->json(['success' => true, 'data' =>$data],200);
    }

    /**
     * Filter event in top
     */
    public function getEvent()
    {
        $name = $this->eventRepository->getEvent();

        return response()->json($name);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     * Filter product in top
     */
    public function getProduct(Request $request)
    {
        $shopCode = $request->input('shop_code', false);
        $eventCode = $request->input('event_code', false);
        $categoryCode = $request->input('category_code', 0);
        $area_id = $request->input('area_id', 0);
        $city_id = $request->input('city_id', 0);
        $date = $request->input('date', false);
        $time = $request->input('time', false);
        $search_shop_text = $request->input('search_shop_text', false);

        if ($shopCode && $eventCode && $date && $time) {
            $dateConvert = getDateTime($date);
            $dateReceive = Carbon::parse($date)->format('Y-m-d') . ' ' . $time . ':00:00';
            $datas = $this->itemRepository->getListItem($shopCode, $eventCode, $dateReceive, $categoryCode);
            $datas = $this->ajaxRepositoty->controlDataGetProductTop($datas, $dateReceive);
            $shopName = $this->shopsRepository->findByAttribute('code', $shopCode, 'name');
            $eventName = $this->eventRepository->findByAttribute('code', $eventCode, 'name');
            $categoryFilter = $this->categoryRepository->getTopFilter($eventCode, $dateReceive);
            setSessionTop($request, $eventCode, $shopCode, $categoryCode, $dateReceive, $time, $area_id, $city_id, $search_shop_text);

            $view = view("frontend.elements.site_list_product",
                compact('datas', 'eventName', 'shopName', 'dateConvert', 'categoryFilter', 'time', 'dateReceive', 'categoryCode'))->render();
            return response()->json(['html' => $view]);
        }
        return response()->json(['html' => '']);
    }

    public function renderCityPopup(Request $request)
    {
        $areaId = $request->input('areas_id', false);
        $datas['cities'] = array();
        if ($areaId) {
            $datas['cities'] = $this->areasRepository->find($areaId)->cities->toArray();
        }
        $view = view("frontend.elements.popup_areas",
            compact('datas', 'areaId'))->render();

        return response()->json(['html' => $view]);
    }
}

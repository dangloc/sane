@if(strlen(strstr(url()->current(), 'management')) > 0)
    @include('errors.backend.429')
@else
    @include('errors.frontend.404')
@endif

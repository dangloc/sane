<?php

namespace App\Repositories;


use Illuminate\Http\Request;

/**
 * class EventCategoryRepository.
 *
 * @package namespace App\Repositories;
 */
class EventCategoryRepository extends BaseRepository
{
    function __construct()
    {
        $this->_model = \App\Models\EventCategory::class;
    }

    public function paging()
    {
        $sort = request()->get('sort', 'sort');
        $direction = request()->get('direction', 'asc');
        $params = request()->all();

        return $this->getModel()
            ->when(isset($params['event_code']) && $params['event_code'] != '', function($q) use ($params) {
                return $q->where('event_code', 'like', '%' . $params['event_code'] . '%');
            })
            ->when(isset($params['category_code']) && $params['category_code'] != '', function($q) use ($params) {
                return $q->where('category_code', 'like', '%' . $params['category_code'] . '%');
            })
            ->when(isset($params['order']) && $params['order'] != '', function($q) use ($params) {
                return $q->where('sort', '=', $params['sort']);
            })
            ->orderBy($sort, $direction)
            ->paginate(config('const.paging.backend'));
    }
}

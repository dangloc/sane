<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\BackendController;
use App\Http\Requests\LoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends BackendController
{
    public function index()
    {
        if ($this->guard()->check()){
            return redirect()->route('order.index');
        }

        return view('backend.login.index');
    }

    public function auth(LoginRequest $request)
    {
        $info = $request->only(['login_id', 'password']);
        if ($this->guard()->attempt($info) ) {
            if($this->guard()->user()->status == config('const.status.enable')){
                return redirect()->route('order.index');
            }
            Auth::logout();
        }
        return redirect()->back()->withInput()->withErrors(['login_id' => config('messages.LOGIN_FAILED')]);
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect()->route('login');
    }
}

<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\BackendController;
use App\Repositories\CategoryRepository;
use App\Repositories\EventRepository;
use App\Repositories\ItemLimitedRepository;
use App\Repositories\ItemNameRepository;
use App\Repositories\ItemPointRepository;
use App\Repositories\ItemPriceRepository;
use App\Repositories\ItemRepository;
use App\Repositories\ItemShopRepository;
use App\Repositories\AreasRepository;
use App\Repositories\CitiesRepository;
use App\Repositories\ItemStatusRepository;
use App\Repositories\ShopsRepository;
use Illuminate\Http\Request;
use Carbon\Carbon;
use \Datetime;

class ItemController extends BackendController
{
    public $itemRepository;
    public $itemNameRepository;
    public $itemPriceRepository;
    public $itemPointRepository;
    public $itemShopRepository;
    public $itemLimitedRepository;
    protected $areaRepository;
    protected $cityRepository;
    protected $shopRepository;
    protected $categoryRepository;
    protected $eventRepository;
    protected $itemStatusRepository;

    public function __construct(
        ItemRepository $itemRepository,
        ItemNameRepository $itemNameRepository,
        ItemPriceRepository $itemPriceRepository,
        ItemPointRepository $itemPointRepository,
        ItemLimitedRepository $itemLimitedRepository,
        ItemShopRepository $itemShopRepository,
        ShopsRepository $shopRepository,
        AreasRepository $areaRepository,
        CitiesRepository $cityRepository,
        CategoryRepository $categoryRepository,
        EventRepository $eventRepository,
        ItemStatusRepository $itemStatusRepository
    ) {
        $this->itemRepository = $itemRepository;
        $this->itemNameRepository = $itemNameRepository;
        $this->itemPriceRepository = $itemPriceRepository;
        $this->itemPointRepository = $itemPointRepository;
        $this->itemShopRepository = $itemShopRepository;
        $this->itemLimitedRepository = $itemLimitedRepository;
        $this->areaRepository = $areaRepository;
        $this->cityRepository = $cityRepository;
        $this->shopRepository = $shopRepository;
        $this->categoryRepository = $categoryRepository;
        $this->eventRepository = $eventRepository;
        $this->itemStatusRepository = $itemStatusRepository;
    }

    private function _paginate($repository, $viewName)
    {
        $data = $repository->paging();
        return view('backend.item.' . $viewName,
            [
                'listData' => $data,
                'params' => request()->all()
            ]
        );
    }

    public function index()
    {
        return $this->_paginate($this->itemRepository, 'index');
    }

    public function detail()
    {
        $params = request()->only(['shop_code', 'item_code']);
        $data = $this->itemRepository->getOne($params);

        if (request()->ajax()) {
            return response()->json(
                ['data' => view('backend.item._detail')->with(['data' => $data])->render()]
            );
        } else {
            return view('backend.item.detail');
        }
    }

    function validateDate($date, $format = 'Y-m-d')
    {
        $d = DateTime::createFromFormat($format, $date);
        // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
        return $d && $d->format($format) === $date;
    }

    public function search(Request $request)
    {
        $areas = $this->areaRepository->all();
        $categories = $this->categoryRepository->getCategory();
        $cities = [];
        $shops = [];
        $events = [];
        if (null != $request->get('area_id')){
            $cities = $this->areaRepository->getCity($request->get('area_id'));
            if (null != $request->get('city_id')){
                $shops = $this->shopRepository->getShopByAreaAndCity($request->get('area_id'), $request->get('city_id'));
            }
        }
        if (null != $request->get('category_code')){
            $events = $this->eventRepository->getEventByCategory($request->get('category_code'));
        }
        if (null != $request->get('shop_code')
            and null != $request->get('category_code')
            and null != $request->get('receipt_start_date') and $this->validateDate($request->get('receipt_start_date'), 'Y/m/d') == true
            and null != $request->get('receipt_end_date') and $this->validateDate($request->get('receipt_end_date'), 'Y/m/d') == true)
        {
            $items = $this->itemRepository->searchCondition($request)->paginate(config('const.paging.backend'));
        } else {
            $items = [];
        }
        return view('backend.item.list', compact('items'))
            ->with([
            'areas' => $areas,
            'cities' => $cities,
            'shops' => $shops,
            'categories' => $categories,
            'events' => $events,
            'area_id' => $request->get('area_id'),
            'city_id' => $request->get('city_id'),
            'shop_code' => $request->get('shop_code'),
            'category_code' => $request->get('category_code'),
            'event_code' => $request->get('event_code'),
            'item_code' => $request->get('item_code'),
            'item_name' => $request->get('item_name'),
            'receipt_start_date' => $request->get('receipt_start_date'),
            'receipt_end_date' => $request->get('receipt_end_date')
        ]);
    }

    public function limited()
    {
        return $this->_paginate($this->itemLimitedRepository, 'limited');
    }

    public function name()
    {
        return $this->_paginate($this->itemNameRepository, 'name');
    }

    public function point()
    {
        return $this->_paginate($this->itemPointRepository, 'point');
    }

    public function price()
    {
        return $this->_paginate($this->itemPriceRepository, 'price');
    }

    public function shopCount()
    {
        return $this->_paginate($this->itemShopRepository, 'shop_count');
    }

    public function getEventByCategory($category_code)
    {
        $events = $this->eventRepository->getEventByCategory($category_code);
        return $events->toArray();
    }

    public function updateStopFlag($item_code, $shop_code, $flag)
    {
        $return_code = $this->itemStatusRepository->updateStopFlag($item_code, $shop_code, $flag);
        return response()->json([
            'value' => $return_code,
        ]);
    }
}

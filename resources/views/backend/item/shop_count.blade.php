@extends('backend.layouts.main')
@section('content')

    <div id="Contentwrap">
        <div id="PageContent">
            <h2><i class="fas fa-search"></i> 店別計画注文可能数マスター検索</h2>
            <form action="{{route('item.shop_count')}}" method="get">
                <div class="order">
                    <table class="sp_block">
                        <tr>
                            <th scope="row" style="width: 150px;">店舗コード</th>
                            <td colspan="4">
                                <div class="inline_block rel sp-w45 line">
                                    <input type="text" placeholder="店舗コード" tabindex="5" name="shop_code" value="{{isset($params['shop_code']) ? $params['shop_code'] : ''}}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row" style="width: 150px;">商品コード</th>
                            <td colspan="4">
                                <div class="inline_block rel sp-w45 line">
                                    <input type="text" placeholder="商品コード" tabindex="5" name="item_code" value="{{isset($params['item_code']) ? $params['item_code'] : ''}}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row" style="width: 150px;">受取日</th>
                            <td colspan="4">
                                <div class="inline_block rel">
                                    <input type="text" id="receipt_start_date" placeholder="例：2019/01/02" tabindex="7" autocomplete="off"
                                           name="receipt_start_date" value="{{isset($params['receipt_start_date']) ? $params['receipt_start_date'] : ''}}">
                                </div>
                                <br class="sp">&nbsp;〜&nbsp;<br class="sp">
                                <div class="inline_block rel">
                                    <input type="text" id="receipt_end_date" placeholder="例：2019/01/03" tabindex="8" autocomplete="off"
                                           name="receipt_end_date" value="{{isset($params['receipt_end_date']) ? $params['receipt_end_date'] : ''}}">
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="contentfoot">
                    <p class="btn Searchclear">
                        <button type="button" value="" onclick="System.redirectLoading('{{ route('item.shop_count') }}')">検索条件をクリア</button>
                    </p>
                    <p class="btn Search">
                        <button type="submit" value="" onclick="System.showLoading()" tabindex="13"><img src="{{ asset('backend/images/common/icon3.png') }}" alt="検索">検索</button>
                    </p>
                </div>
                <!--ContentFoot -->
            </form>
        </div>
        <!--PageContent -->

        <div id="PageContent" class="pt60">
            <h2><i class="fas fa-shapes"></i> 店別計画注文可能数マスター一覧</h2>

            <div class="PageContentNav">
                @include('backend.elements.paging', ['results' => $listData])
                {{ $listData->appends(request()->input())->links() }}
            </div>
            <table class="OrderForm responsive tdcenter Bordertd TextL table-hover">
                <tr>
                    <th scope="col">@include("backend.elements.sortable", ["field" => "shop_code", "title" => "店舗コード"])</th>
                    <th scope="col">@include("backend.elements.sortable", ["field" => "item_code", "title" => "商品コード"])</th>
                    <th scope="col">@include("backend.elements.sortable", ["field" => "receipt_date", "title" => "受取日"])</th>
                    <th scope="col">@include("backend.elements.sortable", ["field" => "order_possible_count", "title" => "計画注文可能数"])</th>
                </tr>
                @if(!$listData->isEmpty())
                    @foreach($listData as $item)
                        <tr>
                            <td>{{$item->shop_code}}</td>
                            <td>{{$item->item_code}}</td>
                            <td>{{convertCharToDate($item->receipt_date)}}</td>
                            <td style="text-align:center;">{{number_format($item->order_possible_count,0,'.',',')}}</td>
                        </tr>
                    @endforeach
                @else
                    @include('backend.elements.no_data',['col' => 4])
                @endif
            </table>
            <div class="PageContentNav">
                {{ $listData->appends(request()->input())->links() }}
            </div>
        </div>
        <!--PageContent -->

    </div>
    <!--Contentwrap -->
@endsection
@section('js')
    <script>
        $(document).ready(function () {
            System.setRangeDate($('#receipt_start_date'), $('#receipt_end_date'));
        });
    </script>
@endsection
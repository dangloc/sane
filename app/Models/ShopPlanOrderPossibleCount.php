<?php

namespace App\Models;
use App\Scopes\ActiveScope;

use Illuminate\Database\Eloquent\Model;

class ShopPlanOrderPossibleCount extends Model
{
    public $timestamps = false;
    protected $_alias = 'shop_plan_order_possible_counts';
    protected $table = 'shop_plan_order_possible_counts';
    protected $fillable = [
        'shop_code',
        'item_code',
        'order_possible_count',
        'del_flag',
        'ins_datetime',
        'ins_id',
        'upd_datetime',
        'upd_id'
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ActiveScope());
    }
}

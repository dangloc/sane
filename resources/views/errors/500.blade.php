@if(strlen(strstr(url()->current(), 'management')) > 0)
    @include('errors.backend.500')
@else
    @include('errors.frontend.500')
@endif
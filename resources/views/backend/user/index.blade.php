@extends('backend.layouts.main')
@section('content')
    <div id="Contentwrap">
        @include('common.message_notification')
        <div id="PageContent">
            <form action="{{route('user.index')}}" method="get">
                <h2><i class="fas fa-search"></i> お客様検索</h2>
                <div class="order">
                    <table class="sp_block">
                        <tr>
                            <th style="width: 150px;" scope="row">サンエーカード番号</th>
                            <td style="width: 210px;">
                                <div class="line">
                                    <input type="text" tabindex="1" placeholder="サンエーカード番号" name="card_no"
                                           value="{{isset($card_no) ? $card_no : ''}}">
                                </div>
                            </td>
                            <th class="TextR sp-TextL" style="width: 150px;" scope="row">電話番号</th>
                            <td width="422">
                                <div class="line">
                                    <input type="text" tabindex="2" placeholder="電話番号（ハイフンなし）" name="tel"
                                           value="{{isset($tel) ? $tel : ''}}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th style="width: 150px;" scope="row">お名前（カナ）</th>
                            <td style="width: 210px;">
                                <div class="line">
                                    <input type="text" tabindex="3" placeholder="お名前（カナ）" name="name"
                                           value="{{isset($name) ? $name : ''}}">
                                </div>
                            </td>
                            <th class="TextR sp-TextL" style="width: 150px;" scope="row">メールアドレス</th>
                            <td width="422">
                                <div class="line">
                                    <input type="text" tabindex="4" placeholder="メールアドレス" name="email"
                                           value="{{isset($email) ? $email : ''}}">
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="contentfoot">
                    <p class="btn Searchclear">
                        <button type="button" value="" onclick="System.redirectLoading('{{ route('user.index') }}')">
                            検索条件をクリア
                        </button>
                    </p>
                    <p class="btn Search">
                        <button type="submit" value="" onclick="System.showLoading()" tabindex="13"><img
                                src="{{ asset('backend/images/common/icon3.png') }}" alt="検索">検索
                        </button>
                    </p>
                </div>
                <!--ContentFoot -->
            </form>
        </div>
        <!--PageContent -->

        <div id="PageContent" class="pt60">
            <h2><i class="fas fa-users"></i> お客様一覧
            </h2>

            <div class="PageContentNav">
                @include('backend/elements/paging', ['results' => $customers])
                {{ $customers->appends(request()->input())->links() }}
            </div>
            <table class="OrderForm responsive tdcenter Bordertd TextL table-hover">
                <tr>
                    <th scope="col" width="100px">@include("backend/elements.sortable", ["field" => "id", "title" => "ID"])</th>
                    <th scope="col">@include("backend/elements.sortable", ["field" => "name", "title" => "お名前（カナ）<br/>電話番号"])</th>
                    <th scope="col" width="160px">@include("backend/elements.sortable", ["field" => "card_no", "title" => "サンエーカード番号"])</th>
                    <th scope="col" width="120px">@include("backend/elements.sortable", ["field" => "has_password", "title" => "会員/非会員"])</th>
                    <th scope="col">@include("backend/elements.sortable", ["field" => "email", "title" => "メールアドレス"])</th>
                    <th scope="col">詳細</th>
                </tr>
                @if(!$customers->isEmpty())
                    @foreach($customers as $customer)
                        <tr>
                            <td>{{$customer->id}}</td>
                            <td>{{$customer->name}}<br/>{{$customer->tel}}</td>
                            <td>{{$customer->card_no}}</td>
                            <td>{{$customer->has_password == 'true' ? '会員 ' : '非会員'}}</td>
                            <td>{{$customer->email}}</td>
                            <td><p class="btnmini Edit"><a
                                        onclick="System.redirectNoLoading('{{ route('user.show', $customer->id) }}')"
                                        class="TextC">詳細</a></p></td>
                        </tr>
                    @endforeach
                @else
                    @include('backend/elements/no_data',['col' => 6])
                @endif
            </table>
            <div class="PageContentNav">
                {{ $customers->appends(request()->input())->links() }}
            </div>
        </div>
        <!--PageContent -->

    </div>
    <!--Contentwrap -->
@endsection
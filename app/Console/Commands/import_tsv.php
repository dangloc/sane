<?php

namespace App\Console\Commands;

use App\Jobs\importTsv;
use Illuminate\Console\Command;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Http\Common\logWriter;

class import_tsv extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'batch:import_tsv {no}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Batch 2-12 and 14 for TSV file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        ini_set("memory_limit", "512M");
        $job =  new importTsv($this->argument('no'));
        $job->handle();

    }
}

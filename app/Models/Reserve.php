<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\ActiveScope;

class Reserve extends Model
{
    const FLAG_CANCEL = 1;
    const FLAG_DEL_CANCEL = 1;
    public $timestamps = false;
    protected $_alias = 'reserves';
    protected $table = 'reserves';
    protected $fillable = [
        'slip_number',
        'shop_code',
        'receptionist_date',
        'receptionist_type',
        'receptionist_staff_code',
        'receipt_date',
        'receipt_time',
        'customer_name',
        'customer_tel',
        'customer_card_no',
        'event_code',
        'item_code',
        'required_confirm_flag',
        'required_flag',
        'item_count',
        'status_flag',
        'update_flag',
        'reserve_content1',
        'reserve_content2',
        'reserve_del_flag',
        'update_timestamp',
        'update_staff_code',
        'del_flag',
        'ins_datetime',
        'ins_id',
        'upd_datetime',
        'upd_id'
    ];
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ActiveScope());
    }

    public function itemName()
    {
        return $this->belongsTo(ItemName::class, 'item_code', 'item_code')->where('item_names.del_flag',config('const.delete_off'));
    }

    public function itemPrice()
    {
        return $this->belongsTo(ItemPrice::class, 'item_code', 'item_code');
    }

    public function customerOder()
    {
        return $this->belongsTo(CustomerOrder::class, 'slip_number', 'slip_number');
    }

    public function event()
    {
        return $this->belongsTo(Event::class, 'event_code', 'code');
    }
}

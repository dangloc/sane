<?php
return [
    'Reserve' => [
        'folder' => 'GYOJIUKETSUKE_YOYAKUJOHO',
        'keys' => [
            'slip_number',
            'shop_code',
            'item_code'
        ],
        'fields' => [
            'slip_number',
            'shop_code',
            'receptionist_date',
            'receptionist_type',
            'receptionist_staff_code',
            'receipt_date',
            'receipt_time',
            'customer_name',
            'customer_tel',
            'customer_card_no',
            'event_code',
            'item_code',
            'required_confirm_flag',
            'required_flag',
            'item_count',
            'status_flag',
            'update_flag',
            'reserve_content1',
            'reserve_content2',
            'reserve_del_flag',
            'update_timestamp',
            'update_staff_code',
        ]
    ]
];


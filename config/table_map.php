<?php
return [
    'administrators' => [
        'keys' => [
            'id'
        ]
    ],
    'areas' => [
        'keys' => [
            'id'
        ]
    ],
    'banners' => [
        'keys' => [
            'id'
        ]
    ],
    'categories' => [
        'keys' => [
            'code'
        ]
    ],
    'cities' => [
        'keys' => [
            'id'
        ]
    ],
    'customer_orders' => [
        'keys' => [
            'slip_number'
        ]
    ],
    'customers' => [
        'keys' => [
            'id'
        ]
    ],
    'event_categories' => [
        'keys' => [
            'event_code',
            'category_code'
        ]
    ],
    'event_item_views' => [
        'keys' => [
            'event_code',
            'category_code',
            'item_code'
        ]
    ],
    'event_views' => [
        'keys' => [
            'event_code'
        ]
    ],
    'events' => [
        'keys' => [
            'code'
        ]
    ],
    'informations' => [
        'keys' => [
            'id'
        ]
    ],
    'item_bonus_points' => [
        'keys' => [
            'item_code'
        ]
    ],
    'item_limited_order_possible_counts' => [
        'keys' => [
            'item_code'
        ]
    ],
    'item_names' => [
        'keys' => [
            'item_code'
        ]
    ],
    'item_prices' => [
        'keys' => [
            'item_code'
        ]
    ],
    'item_status' => [
        'keys' => [
            'code',
            'shop_code'
        ]
    ],
    'items' => [
        'keys' => [
            'shop_code',
            'code'
        ]
    ],
    'reserve_updates' => [
        'keys' => [
            'id'
        ]
    ],
    'reserves' => [
        'keys' => [
            'slip_number',
            'shop_code',
            'item_code'
        ]
    ],
    'shop_plan_order_possible_counts' => [
        'keys' => [
            'shop_code',
            'receipt_date',
            'item_code'
        ]
    ],
    'shops' => [
        'keys' => [
            'id'
        ]
    ]
];
<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests\EntryRegisterRequest;
use App\Repositories\CustomerRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

class EntryController extends Controller
{
    private $customer;

    public function __construct(CustomerRepository $customer)
    {
        $this->customer = $customer;
    }

    public function index(Request $request)
    {
        if (Auth::guard(config('const.guard.frontend'))->check()) {
            return redirect()->route('site.index');
        }
        $request_url = app('router')->getRoutes()->match(app('request')->create(URL::previous()));
        if ($request_url->getName() == 'cart.confirm' || $request_url->getName() == 'frontend.login.confirm'
            || $request_url->getName() == 'entry.index' || $request_url->getName() == 'cart.notregistration') {
            $customers = ($request->session()->has(config('const.session.customer_booking'))) ? $request->session()->get(config('const.session.customer_booking')) : array();
            if (!empty($customers['card_no'])) {
                $customers['card_no'] = explode('-', formatCardNo($customers['card_no']));
            }
            return view('frontend.entry.index',compact('customers'));
        }
        return redirect()->route('site.index');
    }

    public function postIndex(EntryRegisterRequest $request)
    {
        $data_create = array(
            'name'=>$request->name,
            'email'=>$request->email,
            'password'=>bcrypt($request->my_password),
            'tel'=>$request->tel,
            'card_no'=>implode(request()->card),
            'del_flag'=>config('const.delete_off'),
            'ins_datetime'=>Carbon::now(),
            'ins_id'=>1,
            'password_raw' =>$request->my_password,
            'policy'=>$request->policy
        );
        if ($request->session()->has(config('const.session.customer_booking'))) {
            $request->session()->pull(config('const.session.customer_booking'));
        }
        $request->session()->put(config('const.session.customer_booking'), $data_create);
        return redirect(route('cart.confirm'));
    }

    public function register()
    {
        return view('frontend.entry.register');
    }

    public function notregistration()
    {
        return view('frontend.entry.notregistration');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\ActiveScope;
class Cities extends Model
{
    public $timestamps = false;
    protected $_alias = 'cities';
    protected $table = 'cities';
    protected $fillable = [
        'area_id',
        'name',
        'del_flag',
        'ins_datetime',
        'ins_id',
        'upd_datetime',
        'upd_id',
    ];

    public function area()
    {
        return $this->belongsTo('App\Models\Areas','city_id');
    }

    public function shops(){
        return $this->hasMany('App\Models\Shop','city_id');
    }

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ActiveScope());
    }

}

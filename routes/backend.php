<?php

use Illuminate\Support\Facades\Route;

Route::get('/login', 'AuthController@index')->name('login');
Route::post('/login', 'AuthController@auth')->name('auth_login');
Route::get('/logout', 'AuthController@logout')->name('logout');

Route::group(['middleware' => 'backend.auth:backend'], function() {
    Route::get('/', 'OrderController@index');

    Route::group(['prefix' => 'account'], function () {
        Route::post('/confirm', 'AccountController@confirm')->name('account.create.confirm');
        Route::post('/{id}/confirm', 'AccountController@confirm')->name('account.edit.confirm');
    });
    Route::resource('account', 'AccountController')->except(['show']);

    Route::group(['prefix' => 'banner'], function () {
        Route::post('/create/confirm', 'BannerController@createConfirm')->name('banner.create_confirm');
        Route::post('/edit/{id}/confirm', 'BannerController@editConfirm')->name('banner.edit_confirm');
    });
    Route::resource('banner', 'BannerController')->except(['show']);
    Route::resource('category', 'CategoryController')->only(['index']);

    Route::group(['prefix' => 'information'], function () {
        Route::post('/create/confirm', 'InformationController@confirm')->name('information.create.confirm');
        Route::post('/{id}/confirm', 'InformationController@confirm')->name('information.edit.confirm');
    });
    Route::resource('information', 'InformationController')->except(['show']);


    Route::resource('order', 'OrderController')->only(['index','show']);
    Route::get('order/{area_id}/{city_id}/shop', 'OrderController@getShop')->name('order.get.shop');

    Route::group(['prefix' => 'shop'], function () {
        Route::post('/confirm', 'ShopController@confirm')->name('shop.create.confirm');
        Route::post('/{id}/confirm', 'ShopController@confirm')->name('shop.edit.confirm');
        Route::get('/{area_id}/city', 'ShopController@getCity')->name('shop.get.city');
        Route::get('/getCity',"ShopController@getCityTest")->name('shop.getCity');

    });

    Route::resource('shop', 'ShopController')->except(['show']);
    Route::resource('user', 'UserController')->only(['index','show']);

    Route::group(['prefix' => 'event'], function () {
        Route::get('/index', 'EventController@index')->name('event.index');
        Route::get('/category', 'EventController@category')->name('event.category');
        Route::get('/item', 'EventController@item')->name('event.item');
        Route::get('/view', 'EventController@view')->name('event.view');
    });
    Route::group(['prefix' => 'item'], function () {
        Route::get('/index', 'ItemController@index')->name('item.index');
        Route::get('/detail', 'ItemController@detail')->name('item.detail');
        Route::get('/limited', 'ItemController@limited')->name('item.limited');
        Route::get('/list', 'ItemController@search')->name('item.list');
        Route::get('/name', 'ItemController@name')->name('item.name');
        Route::get('/point', 'ItemController@point')->name('item.point');
        Route::get('/price', 'ItemController@price')->name('item.price');
        Route::get('/shopCount', 'ItemController@shopCount')->name('item.shop_count');
        Route::get('/{category_code}/event', 'ItemController@getEventByCategory')->name('item.get.event');
        Route::get('/{item_code}/{shop_code}/{flag}', 'ItemController@updateStopFlag')->name('item.update.flag');

    });
});

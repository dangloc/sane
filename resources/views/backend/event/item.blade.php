@extends('backend.layouts.main')
@section('content')
    <div id="Contentwrap">
        <div id="PageContent">
            <h2><i class="fas fa-search"></i> 行事商品表示マスター検索</h2>
            <form action="{{route('event.item')}}" method="get">
                <div class="order">
                    <table class="sp_block">
                        <tr>
                            <th scope="row" style="width: 150px;">行事コード</th>
                            <td colspan="4">
                                <div class="inline_block rel sp-w45 line">
                                    <input type="text" placeholder="行事コード" tabindex="5" name="event_code" value="{{isset($params['event_code']) ? $params['event_code'] : ''}}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row" style="width: 150px;">カテゴリーコード</th>
                            <td colspan="4">
                                <div class="inline_block rel sp-w45 line">
                                    <input type="text" placeholder="カテゴリーコード" tabindex="5" name="category_code" value="{{isset($params['category_code']) ? $params['category_code'] : ''}}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row" style="width: 150px;">商品コード</th>
                            <td colspan="4">
                                <div class="inline_block rel sp-w45 line">
                                    <input type="text" placeholder="商品コード" tabindex="5" name="item_code" value="{{isset($params['item_code']) ? $params['item_code'] : ''}}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row" style="width: 150px;">受付日</th>
                            <td colspan="4">
                                <div class="inline_block rel">
                                    <input type="text" id="receptionist_start_date" placeholder="例：2019/01/02" autocomplete="off"
                                           tabindex="5" name="receptionist_start_date" value="{{isset($params['receptionist_start_date']) ? $params['receptionist_start_date'] : ''}}">
                                </div>
                                <br class="sp">&nbsp;〜&nbsp;<br class="sp">
                                <div class="inline_block rel">
                                    <input type="text" id="receptionist_end_date" placeholder="例：2019/01/03" autocomplete="off"
                                           tabindex="6" name="receptionist_end_date" value="{{isset($params['receptionist_end_date']) ? $params['receptionist_end_date'] : ''}}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row" style="width: 150px;">受取日</th>
                            <td colspan="4">
                                <div class="inline_block rel">
                                    <input type="text" id="receipt_start_date" placeholder="例：2019/01/02" autocomplete="off"
                                           tabindex="7" name="receipt_start_date" value="{{isset($params['receipt_start_date']) ? $params['receipt_start_date'] : ''}}">
                                </div>
                                <br class="sp">&nbsp;〜&nbsp;<br class="sp">
                                <div class="inline_block rel">
                                    <input type="text" id="receipt_end_date" placeholder="例：2019/01/03" autocomplete="off"
                                           tabindex="8" name="receipt_end_date" value="{{isset($params['receipt_end_date']) ? $params['receipt_end_date'] : ''}}">
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="contentfoot">
                    <p class="btn Searchclear">
                        <button type="button" onclick="System.redirectLoading('{{ route('event.item') }}')" value="">検索条件をクリア</button>
                    </p>
                    <p class="btn Search">
                        <button type="submit" value="" onclick="System.showLoading()" tabindex="13"><img src="{{ asset('backend/images/common/icon3.png') }}" alt="検索">検索</button>
                    </p>
                </div>
                <!--ContentFoot -->
            </form>
        </div>
        <!--PageContent -->

        <div id="PageContent" class="pt60">
            <h2><i class="fas fa-shapes"></i> 行事商品表示マスター一覧</h2>

            <div class="PageContentNav">
                @include('backend.elements.paging', ['results' => $listData])
                {{ $listData->appends(request()->input())->links() }}
            </div>
            <table class="OrderForm responsive tdcenter Bordertd TextL table-hover">
                <tr>
                    <th scope="col" width="100px">@include("backend.elements.sortable", ["field" => "event_code", "title" => "行事コード"])</th>
                    <th scope="col" width="100px">@include("backend.elements.sortable", ["field" => "category_code", "title" => "カテゴリーコード"])</th>
                    <th scope="col" width="100px">@include("backend.elements.sortable", ["field" => "item_code", "title" => "商品コード"])</th>
                    <th scope="col">@include("backend.elements.sortable", ["field" => "sort", "title" => "表示順"])</th>
                    <th scope="col" width="130px">@include("backend.elements.sortable", ["field" => "receptionist_start_date", "title" => "受付開始日"])</th>
                    <th scope="col" width="130px">@include("backend.elements.sortable", ["field" => "receptionist_end_date", "title" => "受付終了日"])</th>
                    <th scope="col" width="130px">@include("backend.elements.sortable", ["field" => "receipt_start_date", "title" => "受取開始日"])</th>
                    <th scope="col" width="130px">@include("backend.elements.sortable", ["field" => "receipt_end_date", "title" => "受取終了日"])</th>
                </tr>
                @if(!$listData->isEmpty())
                    @foreach($listData as $item)
                        <tr>
                            <td>{{$item->event_code}}</td>
                            <td>{{$item->category_code}}</td>
                            <td>{{$item->item_code}}</td>
                            <td>{{$item->sort}}</td>
                            <td style="text-align:center;">{{!empty($item->receptionist_start_date) ? convertCharToDate($item->receptionist_start_date) : ''}}</td>
                            <td style="text-align:center;">{{!empty($item->receptionist_end_date) ? convertCharToDate($item->receptionist_end_date) : ''}}</td>
                            <td style="text-align:center;">{{!empty($item->receipt_start_date) ? convertCharToDate($item->receipt_start_date) : ''}}</td>
                            <td style="text-align:center;">{{!empty($item->receipt_end_date) ? convertCharToDate($item->receipt_end_date) : ''}}</td>
                        </tr>
                    @endforeach
                @else
                    @include('backend.elements.no_data',['col' => 8])
                @endif
            </table>
            <div class="PageContentNav">
                {{ $listData->appends(request()->input())->links() }}
            </div>
        </div>
        <!--PageContent -->

    </div>
    <!--Contentwrap -->
@endsection
@section('js')
    <script>
        $(document).ready(function() {
            System.setRangeDate($('#receptionist_start_date'), $('#receptionist_end_date'));
            System.setRangeDate($('#receipt_start_date'), $('#receipt_end_date'));
        });
    </script>
@endsection
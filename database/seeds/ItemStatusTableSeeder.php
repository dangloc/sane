<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ItemStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('item_status')->truncate();
        DB::table('item_status')->insert([
            [
                'code'=>'0010',
                'shop_code'=>'1',
                'stop_flag'=>0,
                'ins_id'=>1,
                'ins_datetime'=>Carbon::now()
            ],
            [
                'code'=>'0011',
                'shop_code'=>'2',
                'stop_flag'=>0,
                'ins_id'=>1,
                'ins_datetime'=>Carbon::now()
            ],
            [
                'code'=>'0012',
                'shop_code'=>'3',
                'stop_flag'=>0,
                'ins_id'=>1,
                'ins_datetime'=>Carbon::now()
            ]
        ]);
    }
}

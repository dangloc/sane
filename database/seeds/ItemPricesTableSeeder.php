<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ItemPricesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('item_prices')->truncate();
        DB::table('item_prices')->insert([
            [
                'item_code'=>'0010',
                'price'=>3400,
                'del_flag'=>0,
                'ins_id'=>1,
                'ins_datetime'=>Carbon::now()
            ],
            [
                'item_code'=>'0011',
                'price'=>3200,
                'del_flag'=>0,
                'ins_id'=>1,
                'ins_datetime'=>Carbon::now()
            ],
            [
                'item_code'=>'0012',
                'price'=>3500,
                'del_flag'=>0,
                'ins_id'=>1,
                'ins_datetime'=>Carbon::now()
            ]
        ]);
    }
}

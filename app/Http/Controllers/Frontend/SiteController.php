<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\PasswordReissueRequest;
use App\Http\Requests\PasswordReminderRequest;
use App\Mail\ForgetPassMail;
use App\Repositories\AddToCartRepository;
use App\Repositories\AreasRepository;
use App\Repositories\BannersRepository;
use App\Repositories\CustomerRepository;
use App\Repositories\InformationsRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

class SiteController extends Controller
{
    public $informationsRepository;
    public $customerRepository;
    public $bannersRepository;
    public $areasRepository;
    public $addToCartRepository;

    public function __construct(InformationsRepository $informationsRepository,
                                CustomerRepository $customerRepository,
                                BannersRepository $bannersRepository,
                                AreasRepository $areasRepository,
                                AddToCartRepository $addToCartRepository
    )
    {
        $this->informationsRepository = $informationsRepository;
        $this->customerRepository = $customerRepository;
        $this->bannersRepository = $bannersRepository;
        $this->areasRepository = $areasRepository;
        $this->addToCartRepository = $addToCartRepository;
    }

    /**
     * @return mixed
     * set guard customers
     */
    public function guard()
    {
        return Auth::guard(config('const.guard.frontend'));
    }

    public function index(Request $request)
    {
        $info_list = $this->informationsRepository->getListInfo();
        $banner_list = $this->bannersRepository->getListBanner();
        $areas = $this->areasRepository->all();
        $shopCode = Session::has(config('const.session.shop_code')) ? Session::get(config('const.session.shop_code')) : '';
        $eventCode = Session::has(config('const.session.event_code')) ? Session::get(config('const.session.event_code')) : '';
        $dateReceive = Session::has(config('const.session.date_receive_raw')) ? Session::get(config('const.session.date_receive_raw')) : '';
        $time = Session::has(config('const.session.time')) ? Session::get(config('const.session.time')) : '';
        $areaId = Session::has(config('const.session.area_id')) ? Session::get(config('const.session.area_id')) : 0;
        $cityId = Session::has(config('const.session.city_id')) ? Session::get(config('const.session.city_id')) : 0;
        $searchShopText = Session::has(config('const.session.search_shop_text')) ? Session::get(config('const.session.search_shop_text')) : '';

        if (!empty($dateReceive)) {
            $dateReceive = Carbon::parse($dateReceive)->format('d/m/Y');
        }
        return view('frontend.site.index',
            compact(
                'info_list',
                'banner_list',
                'areas',
                'eventCode',
                'shopCode',
                'dateReceive',
                'time',
                'areaId',
                'cityId',
                'searchShopText'
            )
        );
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * Quen mat khau
     */
    public function passwordReissue($token)
    {
        if (!empty($token)) {
            $tokenDecrypt = decrypt($token);
            parse_str($tokenDecrypt, $tokenArray);
            $date_check = Carbon::now()->subDays(1)->toDateTimeString();
            if ($tokenArray['customer_id'] && $tokenArray['email_token']) {
                $id = $tokenArray['customer_id'];
                $customer = $this->customerRepository->getCustomerAttribute('id', $id);
                if (!empty($customer['email_time_valid']) && !empty($customer['email_token'])) {
                    if (strtotime($date_check) < strtotime($customer['email_time_valid']) && ($customer['email_token'] == $tokenArray['email_token'])) {
                        return view('frontend.site.password_reissue', compact('id'));
                    }
                }
            }
        }
        return redirect(route('site.index'));
    }

    /**
     * @param PasswordReminderRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * send Mail password reissue
     */
    public function sendMailPasswordReissue(PasswordReminderRequest $request)
    {
        if ($request->post()) {
            $message = '';
            $input = $request->post();
            //GET INFO CUSTOMER
            $customer = $this->customerRepository->getCustomerAttribute('email', $input['email']);
            if ($customer) {

                //CREATE OBJECT MAIL
                $forget = new \stdClass();
                $forget->customers = $customer;

                //UPDATE PASSWORD
                $emailToken = $this->customerRepository->genRandKey(false, 32);
                $dataUpdate = array(
                    'email_time_valid' => date('Y-m-d H:i:s'),
                    'email_token' => $emailToken,
                );
                $update = $this->customerRepository->update($customer['id'], $dataUpdate);
                //SEND MAIL PASSWORD
                if ($update) {
                    $token = encrypt(http_build_query(array('customer_id' => $customer['id'], 'email_token' => $emailToken)));
                    $url = URL::route('site.password_reissue', ['token' => $token]);
                    $forget->url = $url; // gen password new
                    try {
                        Mail::to($input['email'])->send(new ForgetPassMail($forget));
                        $message = 'パスワード再設定URLを送信しました。メールをご確認ください。';
                    } catch (\Exception $e) {
                        $message = '';
                    }

                    return view('frontend.site.password_reminder',compact('message'));
                }
            }
        }
        return redirect()->back()->withInput();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * Thông báo password thành công
     */
    public function passwordReissueComp()
    {
        return view('frontend.site.password_reissue_comp');
    }

    /**
     * @param PasswordReissueRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * Change password
     */
    public function passwordReissueAction(PasswordReissueRequest $request)
    {
        if ($request->post()) {
            $input = $request->post();
            //UPDATE PASSWORD
            $dataUpdate = array(
                'password' => bcrypt($input['my_password'])
            );
            try {
                $this->customerRepository->update($input['customer_id'], $dataUpdate);
                return redirect(route('site.password_reissue_comp'));
            } catch (\Exception $e) {
                $request->session()->flash('update', 'エラーが発生しました');
            }
        }
        return redirect()->back()->withInput();
    }

    public function passwordReminder(Request $request)
    {
        $message = '';
        if ($this->guard()->check()) {
            return redirect()->route('site.index');
        }
        return view('frontend.site.password_reminder', compact('message'));
    }
}

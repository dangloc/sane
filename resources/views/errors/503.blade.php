@if(strlen(strstr(url()->current(), 'management')) > 0)
    @include('errors.backend.503')
@else
    @include('errors.frontend.503')
@endif
@extends('backend.layouts.main')
@section('content')
    <div id="Contentwrap">
        @include('common.message_notification')
        <div id="PageContent">
            <form action="{{route('account.index')}}" method="get">
                <h2><i class="fas fa-search"></i> アカウント検索</h2>
                <div class="order">
                    <table class="sp_block">
                        <tr>
                            <th scope="row" style="width: 150px;">氏名</th>
                            <td colspan="4">
                                <div class="inline_block rel sp-w45 line">
                                    <input type="text" name="name" placeholder="氏名" tabindex="5" value="{{isset($name) ? $name : ''}}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">ステータス</th>
                            <td colspan="3">
                                <div class="rabutton">
                                    <input type="radio" name="status" id="rank1" value="" checked="" tabindex="48"
                                        {{ isset($status) && $status == null ? "checked" : "" }}>
                                    <label for="rank1">全て</label>
                                    <input type="radio" name="status" id="rank2" value="1" tabindex="48"
                                        {{ isset($status) && $status == 1 ? "checked" : "" }}>
                                    <label for="rank2">有効</label>
                                    <input type="radio" name="status" id="rank3" value="0" tabindex="48"
                                        {{ isset($status) && $status == 0 ? "checked" : "" }}>
                                    <label for="rank3">無効</label>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="contentfoot">
                    <p class="btn Searchclear">
                        <button type="button" onclick="System.redirectLoading('{{ route('account.index') }}')" value="">
                            検索条件をクリア
                        </button>
                    </p>
                    <p class="btn Search">
                        <button type="submit" value="" onclick="System.showLoading()" tabindex="13"><img
                                src="{{ asset('backend/images/common/icon3.png') }}" alt="検索">検索
                        </button>
                    </p>
                </div>
                <!--ContentFoot -->
            </form>
        </div>
        <!--PageContent -->

        <div id="PageContent" class="pt60">
            <h2><i class="fas fa-users-cog"></i> アカウント一覧<span class="btn register btnMedium sp-mt-neg-85" style="margin-top:-40px;">
                    <button type="button" onclick="System.redirectNoLoading('{{ route('account.create') }}')"
                            tabindex="14"><i class="fas fa-user-plus"></i> アカウント登録</button>
                </span>
            </h2>

            <div class="PageContentNav">
                @include('backend/elements/paging', ['results' => $accounts])
                {{ $accounts->appends(request()->input())->links() }}
            </div>
            <table class="OrderForm responsive tdcenter Bordertd TextL table-hover">
                <tr>
                    <th scope="col">@include("backend/elements.sortable", ["field" => "id", "title" => "ID"])</th>
                    <th scope="col">@include("backend/elements.sortable", ["field" => "name", "title" => "氏名"])</th>
                    <th scope="col">@include("backend/elements.sortable", ["field" => "status", "title" => "ステータス"])</th>
                    <th scope="col">@include("backend/elements.sortable", ["field" => "login_id", "title" => "ログインID"])</th>
                    <th scope="col">編集</th>
                    <th scope="col">削除</th>
                </tr>
                @if(!$accounts->isEmpty())
                    @foreach($accounts as $account)
                        <tr>
                            <td>{{$account->id}}</td>
                            <td>{{$account->name}}</td>
                            <td>{{$account->status == 0 ? '無効' : '有効'}}</td>
                            <td>{{$account->login_id}}</td>
                            <td><p class="btnmini Edit"><a onclick="System.redirectNoLoading('{{ route('account.edit', $account->id) }}')"
                                                           class="TextC">編集</a></p></td>
                            <td>
                                @if ($account->id != \Illuminate\Support\Facades\Auth::id())
                                <form id="{{'form_account_confirm_delete'.$account->id}}" method="post"
                                      action="{{ route('account.destroy', $account->id) }}">
                                    {{ csrf_field() }}
                                    @method('DELETE')
                                    <p class="btnmini">
                                        <button type="button"
                                                onclick="Backend.showDialog('account', 'confirm_delete', {{$account->id}})">削除
                                        </button>
                                    </p>
                                </form>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    @include('backend/elements/no_data',['col' => 7])
                @endif
            </table>
            <div class="PageContentNav">
                {{ $accounts->appends(request()->input())->links() }}
            </div>
        </div>
        <!--PageContent -->
    </div>
    <!--Contentwrap -->
@endsection
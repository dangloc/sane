@extends('frontend.layouts.main')
@section('css')
    <link rel="stylesheet" href="{{asset('frontend/css/static.css')}}">
@endsection
@section('content')
    <div class="sitemap">
        <div class="container">
            <h1 class="title_page">サイトマップ</h1>
            <div class="sitemap_content">
                <div class="sitemap_description">サンエーネット予約サービスのサイトマップ</div>
                <p class="content"><a href="{{route('site.index')}}">サンエーネット予約サービストップページ</a></p>
                <p class="content"><a href="{{route('frontend.login')}}">ログイン・会員登録（無料）</a></p>
                <p class="content"><a href="https://www.san-a.co.jp/company/about/" target="_blank">会社概要</a></p>
                <p class="content"><a href="{{route('guild')}}">ご利用ガイド</a></p>
                <p class="content"><a href="{{route('tokutei')}}">特定商取引に関する法律に基づく表示</a></p>
                <p class="content"><a href="{{route('browser')}}">推奨環境</a></p>
                <p class="content"><a href="{{route('sitemap')}}">サイトマップ</a></p>
                <p class="content"><a href="{{route('privacy')}}">プライバシーポリシー</a></p>
                <p class="content"><a href="{{route('rule')}}">会員規約</a></p>
                <p class="content"><a href="{{route('information')}}">お知らせ一覧</a></p>
            </div>
            <div class="sitemap_back">
                <a href="{{route('site.index')}}">TOPページへ戻る</a>
            </div>
        </div>
    </div>
@endsection
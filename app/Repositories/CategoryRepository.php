<?php

namespace App\Repositories;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * class CategoryRepository.
 *
 * @package namespace App\Repositories;
 */
class CategoryRepository extends BaseRepository
{
    function __construct()
    {
        $this->_model = \App\Models\Category::class;
    }

    public function paging()
    {
        $sort = request()->get('sort', 'code');
        $direction = request()->get('direction', 'desc');
        $params = request()->all();

        return $this->getModel()
            ->when(isset($params['code']) && $params['code'] != '', function($q) use ($params) {
                return $q->where('code', 'like', '%' . $params['code'] . '%');
            })
            ->when(isset($params['division_code']) && $params['division_code'] != '', function($q) use ($params) {
                return $q->where('division_code', 'like', '%' . $params['division_code'] . '%');
            })
            ->when(isset($params['name']) && $params['name'] != '', function($q) use ($params) {
                return $q->where('name', 'like', '%' . $params['name'] . '%');
            })
            ->orderBy($sort, $direction)
            ->paginate(config('const.paging.backend'));
    }

    /**
     * @param $event_code
     * @param $date
     * @return bool
     * Lay dieu kien filter o top
     */
    public function getTopFilter($event_code, $date)
    {
        if (!empty($event_code) && !empty($date)) {
            $data = $this->getModel()
                ->select(DB::raw('DISTINCT categories.code'),'categories.name')
                ->leftJoin('event_item_views', 'categories.code', '=', 'event_item_views.category_code')
                ->where('event_item_views.event_code', '=', $event_code)
                ->where('event_item_views.receipt_start_date', '<=', convertTimeToInt($date))
                ->where('event_item_views.receipt_end_date', '>=', convertTimeToInt($date))
                ->where('event_item_views.receptionist_start_date', '<=', convertTimeToInt(now()))
                ->where('event_item_views.receptionist_end_date', '>=', convertTimeToInt(now()))
                ->where('event_item_views.del_flag', '=', 0)
                ->where('categories.del_flag', '=', 0)
                ->withoutGlobalScopes()->get()->toArray();
            return $data;
        }
        return false;
    }

    //SELECT categories.code, categories.name
    //FROM categories
    //LEFT JOIN event_categories ON categories.code = event_categories.category_code
    //WHERE event_categories.del_flag = 0
    public function getCategory(){
        $query = DB::table('categories')
            ->select('categories.code', 'categories.name')
            ->leftJoin('event_categories', 'categories.code', '=', 'event_categories.category_code')
            ->where('event_categories.del_flag', '=', 0)
            ->where('categories.del_flag', '=', 0);
        return $query->distinct()->get();
    }

}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AreasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('areas')->insert([
            [
                'name' => '北部エリア',
                'del_flag' => 0,
                'ins_datetime' => date('Y-m-d H:i:s'),
                'ins_id' => 1,
                'upd_datetime' => date('Y-m-d H:i:s'),
                'upd_id' => 1
            ],
            [
                'name' => '中部エリア',
                'del_flag' => 0,
                'ins_datetime' => date('Y-m-d H:i:s'),
                'ins_id' => 2,
                'upd_datetime' => date('Y-m-d H:i:s'),
                'upd_id' => 2
            ],
            [
                'name' => '南部エリア',
                'del_flag' => 0,
                'ins_datetime' => date('Y-m-d H:i:s'),
                'ins_id' => 3,
                'upd_datetime' => date('Y-m-d H:i:s'),
                'upd_id' => 3
            ],
            [
                'name' => '石垣島',
                'del_flag' => 0,
                'ins_datetime' => date('Y-m-d H:i:s'),
                'ins_id' => 4,
                'upd_datetime' => date('Y-m-d H:i:s'),
                'upd_id' => 4
            ],
            [
                'name' => '宮古島',
                'del_flag' => 0,
                'ins_datetime' => date('Y-m-d H:i:s'),
                'ins_id' => 4,
                'upd_datetime' => date('Y-m-d H:i:s'),
                'upd_id' => 4
            ]
        ]);
    }
}

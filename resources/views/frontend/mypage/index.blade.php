@extends('frontend.layouts.main')
@section('content')
    <div id="my-page" class="my-page">
        <div class="container">
            <div class="mypage">

                <div class="title-page">
                    <h1 class="title">マイページ</h1>
                </div>

                <div class="group_button">
                    <button class="change_email" onclick="location.href = '{{route('mypage.change_password_entry')}}'">
                        メールアドレス・パスワードの変更
                    </button>
                    <button class="login" onclick="location.href = '{{route('frontend.logout')}}'"><i
                                class="fas fa-sign-out-alt"></i>ログアウト
                    </button>
                </div>
                <div class="list">注文一覧</div>
                <div class="note">
                    <p>※「キャンセル」のボタンが押せない場合、納品間近の為マイページからの受付ができないようになっております。<span onclick="Frontend.scrollToBox('cancellation')">（キャンセルについて）</span>
                    </p>
                    <p>納品間近のキャンセルは、直接お受取り店舗へお問い合わせ下さい。
                    </p>
                </div>
                @if(!$data_booking->isEmpty())
                    @foreach($data_booking as $booking)
                        <div class="my_order_group">
                            <div class="my_order_infomation">
                                <div class="date_order">
                                    <div class="time_order">
                                        <label>注文日</label>
                                        <span>{{formatDatetime($booking->receptionist_date)}}</span>
                                    </div>
                                    <div class="order_id">
                                        <label>伝票番号</label>
                                        <span>{{$booking->slip_number}}</span>
                                    </div>
                                    <div class="button_cancel">
                                        <button onclick="location.href='{{route('mypage.cancel_order_confirm',['slipNumber' => $booking->slip_number])}}'">
                                            注文をキャンセル
                                        </button>
                                    </div>
                                </div>
                                <div class="order_details">
                                    <table>
                                        <tr>
                                            <td class="order_label">商品名</td>
                                            <td class="text_order">
                                                @if(!$booking->reserves->isEmpty())
                                                    @foreach($booking->reserves as $item)
                                                        @if($item->itemName==null)
                                                            <p>商品が存在しません。</p>
                                                        @else
                                                            <p>{{$item->itemName->name}}
                                                                × {{$item->item_count}}
                                                                @if($item->required_confirm_flag !=config('const.not_with'))
                                                                    <label class="required-confirm" style="<?= ($item->required_flag == '0') ? "display:inline-block" : "display:none"?>">

                                                                        @if($item->required_confirm_flag== config('const.with_wasabi'))
                                                                            わさび入り @elseif($item->required_confirm_flag==config('const.with_kamaboko'))
                                                                            かまぼこ赤 @elseif($item->required_confirm_flag==config('const.with_classic')) 定番
                                                                        @endif
                                                                    </label>
                                                                    <label class="required-confirm" style="<?= ($item->required_flag == '1') ? "display:inline-block" : "display:none"?>" >
                                                                        @if($item->required_confirm_flag==config('const.with_wasabi'))
                                                                            わさび抜き@elseif($item->required_confirm_flag==config('const.with_kamaboko'))
                                                                            かまぼこ白 @elseif($item->required_confirm_flag==config('const.with_classic')) 法事
                                                                        @endif
                                                                    </label>
                                                                @endif
                                                            </p>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="order_label">受取り日</td>
                                            <td class="text_order">
                                                <div>{{formatDatetime($booking->receipt_date)}}&nbsp;&nbsp;<span>{{$booking->receipt_time}}
                                                        時</span></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="order_label">受取り店舗</td>
                                            <td class="text_order">
                                                @if(null == $booking->shop)
                                                    <div>店舗が存在しません。</div>
                                                @else
                                                    <div class="content_order">{{$booking->shop->name}}</div>
                                                    <div class="tel">TEL：{{$booking->shop->tel}}</div>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="order_label">合計金額</td>
                                            @if($booking->reserves->isEmpty())
                                                <td class="text_order">商品が存在しません。</td>
                                            @else
                                                <td class="text_order">{{number_format($booking->total_price)}}円+税</td>
                                            @endif
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="my_order_status">
                                <div class="infomation_label">
                                    <div class="title_info">お客様情報</div>
                                    <button onclick="location.href='{{route('mypage.change_customer_info',$booking->slip_number)}}'">
                                        お客様情報の変更
                                    </button>
                                </div>
                                <div class="infomation_customer">
                                    <table>
                                        <tr>
                                            <td class="infomation_customer_label">お名前</td>
                                            <td class="infomation_customer_content">{{$booking->customer_name}}</td>
                                        </tr>
                                        <tr>
                                            <td class="infomation_customer_label">電話番号</td>
                                            <td class="infomation_customer_content">{{$booking->customer_tel}}</td>
                                        </tr>
                                        <tr>
                                            <td class="infomation_customer_label">サンエーカード番号</td>
                                            <td class="infomation_customer_content">{{formatCardNo($booking->customer_card_no)}}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
                @if(!$data_customer_cancel_order->isEmpty())
                    @foreach($data_customer_cancel_order as $customer_cancel)
                        <div class="my_order_group">
                            <div class="my_order_infomation">
                                <div class="date_order customer_cancel">
                                    <div class="time_order">
                                        <label>注文日</label>
                                        <span>{{formatDatetime($customer_cancel->receptionist_date)}}</span>
                                    </div>
                                    <div class="order_id">
                                        <label>伝票番号</label>
                                        <span>{{$customer_cancel->slip_number}}</span>
                                    </div>
                                    <div class="customer_cancel_booking">キャンセル済み</div>
                                </div>
                                <div class="order_details">
                                    <table>
                                        <tr>
                                            <td class="order_label">商品名</td>
                                            <td class="text_order">
                                                @if($customer_cancel->reserves->isEmpty())
                                                    <p>商品が存在しません。</p>
                                                @else
                                                    @foreach($customer_cancel->reserves as $item)
                                                        <p>{{ null==$item->itemName ? '' : $item->itemName->name}}
                                                            × {{$item->item_count}}
                                                        @if($item->required_confirm_flag !=config('const.not_with'))
                                                            <label class="required-confirm" style="<?= ($item->required_flag == '0') ? "display:inline-block" : "display:none"?>">

                                                                @if($item->required_confirm_flag== config('const.with_wasabi'))
                                                                    わさび入り @elseif($item->required_confirm_flag==config('const.with_kamaboko'))
                                                                    かまぼこ赤 @elseif($item->required_confirm_flag==config('const.with_classic')) 定番
                                                                @endif
                                                            </label>
                                                            <label class="required-confirm" style="<?= ($item->required_flag == '1') ? "display:inline-block" : "display:none"?>" >
                                                                @if($item->required_confirm_flag==config('const.with_wasabi'))
                                                                    わさび抜き@elseif($item->required_confirm_flag==config('const.with_kamaboko'))
                                                                    かまぼこ白 @elseif($item->required_confirm_flag==config('const.with_classic')) 法事
                                                                @endif
                                                            </label>
                                                        @endif
                                                        </p>
                                                    @endforeach
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="order_label">受取り日</td>
                                            <td class="text_order">{{formatDatetime($customer_cancel->receipt_date)}}
                                                &nbsp;&nbsp;<span>{{$customer_cancel->receipt_time}}時</span></td>
                                        </tr>
                                        <tr>
                                            <td class="order_label">受取り店舗</td>
                                            <td class="text_order">
                                                @if(null == $customer_cancel->shop)
                                                    <div>店舗が存在しません。</div>
                                                @else
                                                    <div class="content_order">{{$customer_cancel->shop->name}}</div>
                                                    <div class="tel">TEL：{{$customer_cancel->shop->tel}}</div>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="order_label">合計金額</td>
                                            @if(is_null($customer_cancel->reserves))
                                                <td class="text_order">商品が存在しません。</td>
                                            @else
                                                <td class="text_order">{{number_format($customer_cancel->total_price)}}
                                                    円+税
                                                </td>
                                            @endif
                                        </tr>
                                    </table>

                                </div>
                            </div>
                            <div class="my_order_status">
                                <div class="infomation_label customer_cancel_label">
                                    <div class="title_info">お客様情報</div>
                                </div>
                                <div class="infomation_customer">
                                    <table>
                                        <tr>
                                            <td class="infomation_customer_label">お名前</td>
                                            <td class="infomation_customer_content">{{$customer_cancel->customer_name}}</td>
                                        </tr>
                                        <tr>
                                            <td class="infomation_customer_label">電話番号</td>
                                            <td class="infomation_customer_content">{{$customer_cancel->customer_tel}}</td>
                                        </tr>
                                        <tr>
                                            <td class="infomation_customer_label">サンエーカード番号</td>
                                            <td class="infomation_customer_content">{{formatCardNo($customer_cancel->customer_card_no)}}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
                @if(!$data_customer_change_order->isEmpty())
                    @foreach($data_customer_change_order as $customer_change)
                        <div class="my_order_group">
                            <div class="my_order_infomation">
                                <div class="date_order shop_cancel">
                                    <div class="time_order">
                                        <label>注文日</label>
                                        <span>{{formatDatetime($customer_change->receptionist_date)}}</span>
                                    </div>
                                    <div class="order_id">
                                        <label>伝票番号</label>
                                        <span>{{$customer_change->slip_number}}</span>
                                    </div>
                                </div>
                                <div class="order_details">
                                    <table>
                                        <tr><td colspan="2"><div class="title_customer_change_order">店頭で予約変更されました。不明な点は受取り店舗へ確認してください。</div></td></tr>
                                        <tr>
                                            <td class="order_label">受取り日</td>
                                            <td class="text_order">{{formatDatetime($customer_change->receipt_date)}}
                                                &nbsp;&nbsp;<span>{{$customer_change->receipt_time}}時</span></td>
                                        </tr>
                                        <tr>
                                            <td class="order_label">受取り店舗</td>
                                            <td class="text_order">
                                                @if(null == $customer_change->shop)
                                                    <div>店舗が存在しません。</div>
                                                @else
                                                    <div class="content_order">{{$customer_change->shop->name}}</div>
                                                    <div class="tel">TEL：{{$customer_change->shop->tel}}</div>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="order_label">合計金額</td>
                                            @if(is_null($customer_change->reserves))
                                                <td class="text_order">商品が存在しません。</td>
                                            @else
                                                <td class="text_order">{{number_format($customer_change->total_price)}}
                                                    円+税
                                                </td>
                                            @endif
                                        </tr>
                                    </table>

                                </div>
                            </div>
                            <div class="my_order_status">
                                <div class="infomation_label customer_cancel_label">
                                    <div class="title_info">お客様情報</div>
                                </div>
                                <div class="infomation_customer">
                                    <table>
                                        <tr>
                                            <td class="infomation_customer_label">お名前</td>
                                            <td class="infomation_customer_content">{{$customer_change->customer_name}}</td>
                                        </tr>
                                        <tr>
                                            <td class="infomation_customer_label">電話番号</td>
                                            <td class="infomation_customer_content">{{$customer_change->customer_tel}}</td>
                                        </tr>
                                        <tr>
                                            <td class="infomation_customer_label">サンエーカード番号</td>
                                            <td class="infomation_customer_content">{{formatCardNo($customer_change->customer_card_no)}}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
                @if(!$data_shop_cancel_order->isEmpty())
                    @foreach($data_shop_cancel_order as $shop_cancel)
                        <div class="my_order_group">
                            <div class="my_order_infomation">
                                <div class="date_order shop_cancel">
                                    <div class="time_order">
                                        <label>注文日</label>
                                        <span>{{formatDatetime($shop_cancel->receptionist_date)}}</span>
                                    </div>
                                    <div class="order_id">
                                        <label>伝票番号</label>
                                        <span>{{$shop_cancel->slip_number}}</span>
                                    </div>
                                </div>
                                <div class="order_details">
                                    <table>
                                        <tr><td colspan="2"><div class="title_customer_change_order">店頭で予約キャンセルされました。不明な点は受取り店舗へ確認してください。</div></td></tr>
                                        <tr>
                                            <td class="order_label">受取り日</td>
                                            <td class="text_order">{{formatDatetime($shop_cancel->receipt_date)}}&nbsp;&nbsp;<span>{{$shop_cancel->receipt_time}}
                                                    時</span></td>
                                        </tr>
                                        <tr>
                                            <td class="order_label">受取り店舗</td>
                                            <td class="text_order">
                                                @if(null == $shop_cancel->shop)
                                                    <div>店舗が存在しません。</div>
                                                @else
                                                    <div class="content_order">{{$shop_cancel->shop->name}}</div>
                                                    <div class="tel">TEL：{{$shop_cancel->shop->tel}}</div>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="order_label">合計金額</td>
                                            @if(is_null($shop_cancel->reserves))
                                                <td class="text_order">商品が存在しません。</td>
                                            @else
                                                <td class="text_order">{{number_format($shop_cancel->total_price)}}円+税
                                                </td>
                                            @endif
                                        </tr>
                                    </table>

                                </div>
                            </div>
                            <div class="my_order_status">
                                <div class="infomation_label customer_cancel_label">
                                    <div class="title_info">お客様情報</div>
                                </div>
                                <div class="infomation_customer">
                                    <table>
                                        <tr>
                                            <td class="infomation_customer_label">お名前</td>
                                            <td class="infomation_customer_content">{{$shop_cancel->customer_name}}</td>
                                        </tr>
                                        <tr>
                                            <td class="infomation_customer_label">電話番号</td>
                                            <td class="infomation_customer_content">{{$shop_cancel->customer_tel}}</td>
                                        </tr>
                                        <tr>
                                            <td class="infomation_customer_label">サンエーカード番号</td>
                                            <td class="infomation_customer_content">{{formatCardNo($shop_cancel->customer_card_no)}}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
                <div id="cancellation" class="cancellation">
                    <div class="cancellation_title">キャンセルについて</div>
                    <div class="cancellation_content">
                        <p class="bold">▪️キャンセルについて</p>
                        <p>受注生産につき、ご予約後のキャンセル・変更は、お受取りの３日前までにお願いします。</p>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

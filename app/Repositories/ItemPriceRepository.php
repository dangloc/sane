<?php

namespace App\Repositories;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * class ItemPriceRepository.
 *
 * @package namespace App\Repositories;
 */
class ItemPriceRepository extends BaseRepository
{

    function __construct()
    {
        $this->_model = \App\Models\ItemPrice::class;
    }

    public function paging()
    {
        $sort = request()->get('sort', 'item_code');
        $direction = request()->get('direction', 'desc');
        $params = request()->all();

        return $this->getModel()
            ->when(isset($params['item_code']) && $params['item_code'] != '', function ($q) use ($params) {
                return $q->where('item_code', 'like', '%' . $params['item_code'] . '%');
            })
            ->orderBy($sort, $direction)
            ->paginate(config('const.paging.backend'));
    }
}

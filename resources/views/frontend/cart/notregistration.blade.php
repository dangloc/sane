@extends('frontend.layouts.main')
@section('content')
    <div id="entry_notregister" class="password-wrap">
        <div id="prog" class="container">
            <div class="prog">
                <ul>
                    <li class="nomal"><span>カートの中</span><strong>1</strong></li>
                    <li class="current"><span>ログイン・入力</span><strong>2</strong></li>
                    <li class="later"><span>入力内容の確認</span><strong>3</strong></li>
                    <li class="later"><span>購入完了</span><strong>4</strong></li>
                </ul>
            </div>
        </div>
        <div class="container">
            <div class="entry_notregistration">
                <h1 class="title"><i class="fas fa-pencil-alt"></i> 登録しないで注文手続き（入力）</h1>

                <div class="button_group">
                    <div class="button_register">
                        <div class="label_register">新規登録して注文希望の方</div>
                        <button onclick="location.href='{{route('entry.index')}}'"><i></i> 新規登録(無料)</button>
                    </div>
                    <div class="button_login">
                        <div class="label_login">マイページ登録済みの方</div>
                        <button type="submit" onclick="location.href='{{route('frontend.login.confirm')}}'"><i class="fas fa-sign-in-alt"></i> ログイン</button>
                    </div>
                </div>
                <div class="description">受取り店舗と受取り日</div>
                <div class="description_card">
                    @if(!empty($dataCartList))
                        @foreach($dataCartList as $data)
                            <div class="received">
                                <label>受取り店舗</label>
                                <span class="received_place">{{ $data['shopName'] }}</span>
                                <span class="received_time">（営業時間 {{$data['shopOpenTime']}}〜{{$data['shopCloseTime']}}）</span>
                            </div>
                            <div class="event_name">
                                <label>商品カテゴリ</label>
                                <span>{{$data['eventName']}}</span>
                            </div>
                            <div class="received_time">
                                <label>受取り日</label>
                                <span>{{$data['dateReceive']['year']}}年{{$data['dateReceive']['month']}}月{{$data['dateReceive']['day']}}日({{dayJapan($data['dateReceiveRaw'])}})
                                    {{$data['timeReceive']}}時</span>
                            </div>
                        @endforeach
                    @endif
                </div>

                <div class="infomation">お客さま情報</div>
                <form method="POST" action="{{route('cart.notregistration.action')}}" id="form-not-registration">
                    @csrf
                    <div class="form_data">
                        <div class="group_input">
                            <label>メールアドレス<span>必須</span></label>
                            @if (!empty($customer['email']) && empty($errors->first('email')) && empty($errors->first('confirm_email')))
                                <input type="text" name="email" placeholder="半角でご入力ください" value="{{ $customer['email']}}">
                            @else
                                <input type="text" name="email" placeholder="半角でご入力ください" value="{{old('email')}}">
                            @endif
                            <div class="pc-ml-250">
                                @include('frontend.elements.validate_error',['message'=>$errors->first('email')])
                            </div>
                        </div>
                        <div class="group_input">
                            <label>メールアドレス(再入力)<span>必須</span></label>
                            @if (!empty($customer['email']) && empty($errors->first('email')) && empty($errors->first('confirm_email')))
                                <input type="text" name="confirm_email" placeholder="半角でご入力ください"
                                       value="{{ $customer['email']}}">
                            @else
                                <input type="text" name="confirm_email" placeholder="確認のためもう一度ご入力ください" value="{{old('confirm_email')}}">
                            @endif
                            @if(!$errors->first('email'))
                                <div class="pc-ml-250">
                                    @include('frontend.elements.validate_error',['message'=>$errors->first('confirm_email')])
                                </div>
                            @endif
                        </div>
                        <div class="group_input">
                            <label>お名前（カタカナ）<span>必須</span></label>
                            @if (!empty($customer['name']) && empty($errors->first('name')) && empty($errors->first('name')))
                                <input type="text" name="name" placeholder="例：ヤマダハナコ" value="{{$customer['name']}}">
                            @else
                                <input type="text" name="name" placeholder="例：ヤマダハナコ" value="{{old('name')}}">
                            @endif
                            <div class="pc-ml-250">
                                @include('frontend.elements.validate_error',['message'=>$errors->first('name')])
                            </div>
                        </div>
                        <div class="group_input">
                            <label>電話番号<span>必須</span></label>
                            @if (!empty($customer['tel'])&& empty($errors->first('tel')) && empty($errors->first('tel')) )
                                <input type="text" name="tel" placeholder="例：0901234567 ハイフン無し"
                                       value="{{ $customer['tel']}}">
                            @else
                                <input type="text" name="tel" placeholder="例：0901234567 ハイフン無し" value="{{old('tel')}}">
                            @endif
                            <span>※携帯電話可</span>
                            <div class="pc-ml-250">
                                @include('frontend.elements.validate_error',['message'=>$errors->first('tel')])
                            </div>
                        </div>
                        <div class="group_input_phone">
                            <label>サンエーカード番号<span>必須</span></label>
                            <div class="phone" style="float: unset;">
                                @if(!empty($customer['card_no'])&& empty($errors->first('card')) && empty($errors->first('card.*')))
                                    <input maxlength="4" id="test_a"
                                           onkeypress="Frontend.maxLengthInputNumber(event,this)"
                                           name="card[]" type="number" value="{{$customer['card_no'][0]}}"> ー
                                    <input maxlength="4" onkeypress="Frontend.maxLengthInputNumber(event,this)"
                                           name="card[]" type="number" value="{{$customer['card_no'][1]}}"> ー
                                    <input maxlength="4" onkeypress="Frontend.maxLengthInputNumber(event,this)"
                                           name="card[]" type="number" value="{{$customer['card_no'][2]}}">
                                @else
                                    <input maxlength="4" id="test_a"
                                           onkeypress="Frontend.maxLengthInputNumber(event,this)"
                                           name="card[]" type="number" value="{{old('card')[0]}}"> ー
                                    <input maxlength="4" onkeypress="Frontend.maxLengthInputNumber(event,this)"
                                           name="card[]" type="number" value="{{old('card')[1]}}"> ー
                                    <input maxlength="4" onkeypress="Frontend.maxLengthInputNumber(event,this)"
                                           name="card[]" type="number" value="{{old('card')[2]}}">
                                @endif
                            </div>
                            @if(null != $errors->first('card.*'))
                                <div class="pc-ml-250">
                                    @include('frontend.elements.validate_error',['message'=>$errors->first('card.*')])
                                </div>
                            @else
                                <div class="pc-ml-250">
                                    @include('frontend.elements.validate_error',['message'=>$errors->first('card')])
                                </div>
                            @endif
                        </div>
                        <div class="button">
                            <button type="submit" class="button_submit">入力内容を確認する</button>
                        </div>
                    </div>
                </form>
                <button class="button_back" onclick="location.href='{{route('frontend.login.confirm')}}'"><i class="fas fa-chevron-left"></i><span>前の画面に戻る</span></button>
            </div>
        </div>

    </div>
@endsection
@section('js')
    <script type="text/javascript">
        $('.shopping-cart-fix-bottom').css('display','none');
    </script>
@endsection
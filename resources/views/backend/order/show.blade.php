@extends('backend.layouts.main')
@section('content')

    <div id="Contentwrap">

        <div class="contenttop detail-btn-group">
            <p class="btn Searchclear">
                <button type="button" onclick="System.redirectLoading('{{ url()->previous() }}')"><i class="fas fa-list"></i> 一覧ページに戻る</button>
            </p>
        </div>
        <!--ContentFoot -->

        <div id="PageContent">
            <h2><span><i class="fas fa-file"></i> 予約詳細</span></h2>
            <table class="listtable">
                <tr>
                    <th width="30%" scope="row">伝票番号</th>
                    <td width="70%">{{$order->slip_number}}</td>
                </tr>
                <tr>
                    <th scope="row">店舗</th>
                    <td>{{$order->shop !== null ? $order->shop->name:''}}</td>
                </tr>
                <tr>
                    <th scope="row">状態</th>
                    <td>{{$status_array[$order->status]}}</td>
                </tr>
                <tr>
                    <th scope="row">受付日</th>
                    <td>{{$order->receptionist_date != null ? date("Y/m/d", strtotime($order->receptionist_date)) : ''}}</td>
                </tr>
                <tr>
                    <th scope="row">受取日時</th>
                    <td>{{$order->receipt_date != null ? date("Y/m/d", strtotime($order->receipt_date)) : ''}}&nbsp;{{$order->receipt_time != null ? $order->receipt_time.':00' : ''}}</td>
                </tr>
                <tr>
                    <th scope="row">WEB受注日時</th>
                    <td>{{date_format(date_create($order->web_order_timestamp),"Y/m/d H:i")}}</td>
                </tr>
                <tr>
                    <th scope="row">WEBキャンセル日時</th>
                    <td>{{ $order->web_cancel_timestamp != null ? date_format(date_create($order->web_cancel_timestamp),"Y/m/d H:i") : ''}}</td>
                </tr>
            </table>
            <p>&nbsp;</p>
            <table class="listtable">
                <tr>
                    <th scope="row">お名前（カナ）</th>
                    <td>{{$order->customer_name}}</td>
                </tr>
                <tr>
                    <th scope="row">電話番号</th>
                    <td>{{$order->customer_tel}}</td>
                </tr>
                <tr>
                    <th scope="row">サンエーカード番号</th>
                    <td>{{$order->customer_card_no}}</td>
                </tr>
                <tr>
                    <th scope="row">メールアドレス</th>
                    <td>{{$order->customer !== null ? $order->customer->email:''}}</td>
                </tr>
            </table>
        </div>
        <!--PageContent -->

        <div id="PageContent" class="pt60">
            <h2 id="buy_his"><i class="fas fa-shopping-cart"></i> 予約内容</h2>
            <table class="OrderForm responsive tdcenter Bordertd TextL table-hover">
                <tr>
                    <th>行事</th>
                    <th>商品</th>
                    <th>確認事項</th>
                    <th width="180px">数量</th>
                    <th width="200px">金額（税抜）</th>
                </tr>
            @if(!$reserves->isEmpty())
                    @foreach($reserves as $reserve)
                        <tr>
                            <td style="text-align:left;">{{$reserve->event != null ? $reserve->event->name : ''}}</td>
                            <td style="text-align:left;">{{$reserve->itemName != null ? $reserve->itemName->name : ''}}</td>
                            <td style="text-align:left;">
                                @if ($reserve->required_confirm_flag == 1 and $reserve->required_flag == 0)
                                    {{'わさび入り'}}
                                @elseif($reserve->required_confirm_flag == 1 and $reserve->required_flag == 1)
                                    {{'わさび抜き'}}
                                @elseif($reserve->required_confirm_flag == 2 and $reserve->required_flag == 0)
                                    {{'かまぼこ赤'}}
                                @elseif($reserve->required_confirm_flag == 2 and $reserve->required_flag == 1)
                                    {{'かまぼこ白'}}
                                @elseif($reserve->required_confirm_flag == 3 and $reserve->required_flag == 0)
                                    {{'定番'}}
                                @elseif($reserve->required_confirm_flag == 3 and $reserve->required_flag == 1)
                                    {{'法事'}}
                                @else
                                @endif
                            </td>
                            <td style="text-align:right;">{{$reserve->item_count}}</td>
                            <td style="text-align:right;"><strong>¥{{$reserve->itemPrice !== null ? number_format((int)($reserve->itemPrice->price), 0, '.', ',') : 0}}</strong></td>
                        </tr>
                    @endforeach
                @else
                    @include('backend/elements/no_data',['col' => 7])
                @endif
            </table>
        </div>

        <!--PageContent -->
        <div class="contenttop detail-btn-group">
            <p class="btn Searchclear">
                <button type="button" onclick="System.redirectLoading('{{ url()->previous() }}')"><i class="fas fa-list"></i> 一覧ページに戻る</button>
            </p>
        </div>
        <!--ContentFoot -->
    </div>
    <!--Contentwrap -->
    {{--<div class="dialog" id="dialog" title="ポイント付与">--}}
        {{--<table class="OrderForm responsive table-hover">--}}
            {{--<thead>--}}
            {{--<tr>--}}
                {{--<th scope="col" width="100px">選択</th>--}}
                {{--<th scope="col">ポイント種別</th>--}}
                {{--<th scope="col" width="220px">付与ポイント</th>--}}
            {{--</tr>--}}
            {{--</thead>--}}
            {{--<tbody>--}}
            {{--<tr id="item-332" hp-flag="1">--}}
                {{--<td class="check"><input type="checkbox" name="checkbox" id="pt-checkbox01" tabindex="52" checked/>--}}
                    {{--<label for="pt-checkbox01"></label>--}}
                {{--</td>--}}
                {{--<td>来店付与ポイント</td>--}}
                {{--<td class="TextR"><span class="price-pretax">99,999,999 pt</span></td>--}}
            {{--</tr>--}}
            {{--<tr id="item-332" hp-flag="1">--}}
                {{--<td class="check"><input type="checkbox" name="checkbox" id="pt-checkbox02" tabindex="52"/>--}}
                    {{--<label for="pt-checkbox02"></label>--}}
                {{--</td>--}}
                {{--<td>紹介付与ポイント</td>--}}
                {{--<td class="TextR"><span class="price-pretax">99,999,999 pt</span></td>--}}
            {{--</tr>--}}
            {{--</tbody>--}}
        {{--</table>--}}
    {{--</div>--}}

    {{--<div class="dialog" id="dialog2" title="ステータス変更">--}}
        {{--<table class="OrderForm responsive table-hover">--}}
            {{--<tr>--}}
                {{--<th width="158" style="width: 150px;" scope="row">ステータス</th>--}}
                {{--<td colspan="4">--}}
                    {{--<div class="rabutton">--}}
                        {{--<input type="hidden" name="rank" id="MemberRank_" value="">--}}
                        {{--<input type="radio" name="rank" id="MemberRank0" value="0" default-value="0" class="" data-com.agilebits.onepassword.user-edited="yes">--}}
                        {{--<label for="MemberRank0">受注</label>--}}
                        {{--<input type="radio" name="rank" id="MemberRank1" value="1" default-value="0" class="">--}}
                        {{--<label for="MemberRank1">製造済</label>--}}
                        {{--<input type="radio" name="rank" id="MemberRank2" value="2" default-value="0" class="" checked>--}}
                        {{--<label for="MemberRank2">引渡済</label>--}}
                        {{--<input type="radio" name="rank" id="MemberRank3" value="3" default-value="0" class="">--}}
                        {{--<label for="MemberRank3">キャンセル</label>--}}
                    {{--</div>--}}
                {{--</td>--}}
            {{--</tr>--}}
        {{--</table>--}}
    {{--</div>--}}
@endsection
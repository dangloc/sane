@extends('frontend.layouts.main')
@section('content')
    <div id="password-reissue" class="password-wrap">
        <div class="container">
            <div class="password">

                <div class="title-page">
                    <h1 class="title">パスワード再発行</h1>
                </div>
                <div class="description">
                    <p class="text">パスワードの再設定を行います。新たなパスワードを２回入力してください。</p>
                </div>

                <div id="formPasswordReissue" class="formPassword">
                    <form method="POST" action="{{route('site.password_reissue_action')}}">
                        @csrf
                        <div class="group-text">
                            <p>メールアドレス</p>
                            <p>XXXX@XXXX.jp</p>
                        </div>
                        <div class="group-input">
                            <label>新しいパスワード<span>必須</span></label>
                            <input name="my_password" type="password" value="{{old('my_password')}}" autocomplete= "off"
                                   placeholder="8〜20文字の半角英数字">
                            @include('frontend.elements.validate_error', ['message' => !empty($errors->first('my_password')) ?$errors->first('my_password') :""])
                        </div>
                        <div class="group-input">
                            <label>新しいパスワード（再入力）<span>必須</span></label>
                            <input name="confirm_password" type="password" value="{{old('confirm_password')}}"
                                   placeholder="確認のためもう一度ご入力ください">
                            @include('frontend.elements.validate_error', ['message' => !empty($errors->first('confirm_password')) ?$errors->first('confirm_password') :""])
                            <input type="hidden" name="customer_id" value="{{$id}}">
                        </div>
                        <div class="group-input">
                            <button type="submit">再設定する</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection

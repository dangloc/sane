@extends('frontend.layouts.main')
@section('css')
    <link rel="stylesheet" href="{{asset('frontend/css/static.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/css/pagination.css')}}">
@endsection
@section('content')
    <div class="information">
        <div class="container">
            <h1 class="title_page">お知らせ一覧</h1>
            <div class="information_content">
                @if(!$data_information->isEmpty())
                    @foreach($data_information as $info)
                        <div class="information_data">
                            <div class="info_label">{{$info->public_start_date}}</div>
                            <div class="info_data"><a
                                        href="{{route('information.detail',$info->id)}}">{{$info->title}}</a>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
            {{ $data_information->render('vendor.pagination.default') }}
            <div class="information_back">
                <a href="{{route('site.index')}}">TOPページへ戻る</a>
            </div>
        </div>
    </div>
@endsection
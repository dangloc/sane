@extends('frontend.layouts.main')
@section('content')
    <div id="entry" class="password-wrap">
        <div id="prog" class="container">
            <div class="prog">
                <ul>
                    <li class="nomal"><span>カートの中</span><strong>1</strong></li>
                    <li class="current"><span>ログイン・入力</span><strong>2</strong></li>
                    <li class="later"><span>入力内容の確認</span><strong>3</strong></li>
                    <li class="nomal"><span>購入完了</span><strong>4</strong></li>
                </ul>
            </div>
        </div>
        <div class="container">
            <div class="entry">
                <form method="POST" id="entry_form" action="{{route('entry.submit')}}" autocomplete="off">
                    @csrf
                    <h1 class="title"><i class="fas fa-pencil-alt"></i> マイページ新規登録</h1>
                    <div class="description">お客さま情報</div>
                    <div class="form">
                        <div class="group_input">
                            <label>メールアドレス <span>必須</span></label>
                            @if (!empty($customers['email']) && empty($errors->first('email')) && empty($errors->first('confirm_email')))
                                <input type="text" name="email" placeholder="半角でご入力ください" value="{{ $customers['email']}}">
                            @else
                                <input type="text" name="email" placeholder="半角でご入力ください" value="{{ old('email')  }}">
                            @endif
                            <div class="pc-ml-250">
                                @include('frontend.elements.validate_error',['message'=>$errors->first('email')])
                            </div>
                        </div>
                        <div class="group_input">
                            <label>メールアドレス(再入力) <span>必須</span></label>
                            @if (!empty($customers['email']) && empty($errors->first('confirm_email')) && empty($errors->first('email')))
                                <input type="text" name="confirm_email" placeholder="確認のためもう一度ご入力ください"
                                       value="{{$customers['email']}}">
                            @else
                                <input type="text" name="confirm_email" placeholder="確認のためもう一度ご入力ください"
                                       value="{{old('confirm_email')}}">
                            @endif
                            @if(empty($errors->first('email')))
                                <div class="pc-ml-250">
                                    @include('frontend.elements.validate_error',['message'=>$errors->first('confirm_email')])
                                </div>
                            @endif
                        </div>
                        <div class="group_input">
                            <label>パスワード <span>必須</span></label>
                            @if (!empty($customers['password_raw']) && empty($errors->first('my_password')))
                                <input type="password" autocomplete="new-password" maxlength="20" name="my_password" placeholder="8〜20文字の半角英数字"
                                       value="{{$customers['password_raw']}}">
                            @else
                                <input type="password" autocomplete="new-password" maxlength="20" name="my_password" placeholder="8〜20文字の半角英数字"
                                   value="{{old('my_password')}}">
                            @endif
                            <div class="pc-ml-250">
                                @include('frontend.elements.validate_error',['message'=>$errors->first('my_password')])
                            </div>
                        </div>
                        <div class="group_input">
                            <label>パスワード（再入力) <span>必須</span></label>
                            @if (!empty($customers['password_raw']) && empty($errors->first('confirm_password')))
                                <input type="password" maxlength="20" name="confirm_password"
                                       placeholder="確認のためもう一度ご入力ください"
                                       value="{{$customers['password_raw']}}">
                            @else
                                <input type="password" maxlength="20" name="confirm_password"
                                       placeholder="確認のためもう一度ご入力ください"
                                       value="{{old('confirm_password')}}">
                            @endif
                            <div class="pc-ml-250">
                                @include('frontend.elements.validate_error',['message'=>$errors->first('confirm_password')])
                            </div>
                        </div>
                        <div class="group_input">
                            <label>お名前（カタカナ) <span>必須</span></label>
                            @if (!empty($customers['name']) && empty($errors->first('name')))
                                <input type="text" name="name" placeholder="例：ヤマダハナコ"
                                       value="{{ $customers['name'] }}">
                            @else
                                <input type="text" name="name" placeholder="例：ヤマダハナコ"
                                       value="{{ old('name') }}">
                            @endif
                            <div class="pc-ml-250">
                                @include('frontend.elements.validate_error',['message'=>$errors->first('name')])
                            </div>
                        </div>
                        <div class="group_input">
                            <label>電話番号 <span>必須</span></label>
                            @if (!empty($customers['tel']) && empty($errors->first('tel')))
                                <input type="text" name="tel" placeholder="例：0901234567 ハイフン無し" value="{{ $customers['tel'] }}">
                            @else
                                <input type="text" name="tel" placeholder="例：0901234567 ハイフン無し" value="{{ old('tel') }}">
                            @endif
                            <span>※携帯電話可</span>
                            <div class="pc-ml-250">
                                @include('frontend.elements.validate_error',['message'=>$errors->first('tel')])
                            </div>
                        </div>
                        <div class="group_input_footer">
                            <label>サンエーカード番号 <span>必須</span></label>
                            <div class="phone">
                                @if(!empty($customers['card_no']) && empty($errors->first('card')) &&  empty($errors->first('card.*')))
                                    <input name="card[]" onpaste="return false" maxlength="4" onkeypress="Frontend.maxLengthInputNumber(event,this)" type="number" value="{{ $customers['card_no'][0] }}"> ー
                                    <input name="card[]" onpaste="return false" maxlength="4" onkeypress="Frontend.maxLengthInputNumber(event,this)" type="number" value="{{ $customers['card_no'][1] }}"> ー
                                    <input name="card[]" onpaste="return false" maxlength="4" onkeypress="Frontend.maxLengthInputNumber(event,this)" type="number" value="{{ $customers['card_no'][2] }}">
                                @else
                                    <input name="card[]" onpaste="return false" maxlength="4" onkeypress="Frontend.maxLengthInputNumber(event,this)" type="number" value="{{ old('card.0') }}"> ー
                                    <input name="card[]" onpaste="return false" maxlength="4" onkeypress="Frontend.maxLengthInputNumber(event,this)" type="number" value="{{ old('card.1') }}"> ー
                                    <input name="card[]" onpaste="return false" maxlength="4" onkeypress="Frontend.maxLengthInputNumber(event,this)" type="number" value="{{ old('card.2') }}">
                                @endif
                            </div>
                            @if(null != $errors->first('card.*'))
                                <div class="pc-ml-250">
                                    @include('frontend.elements.validate_error',['message'=>$errors->first('card.*')])
                                </div>
                            @else
                                <div class="pc-ml-250">
                                    @include('frontend.elements.validate_error',['message'=>$errors->first('card')])
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="policy">サンエーネット予約サービス利用規約</div>
                    <div class="container_scroll">
                        <div class="scroll policy_entry">
                            <h1 class="title_page">ご利用ガイド</h1>
                            <div class="guild_content">
                                <div class="group_content">
                                    <h3 class="title_content">サンエーネット予約サービスとは</h3>
                                    <p class="description_content">
                                        サンエーネット予約サービスとは、普段お客様にご利用いただいているサンエー食品館の商品を、PCやスマートフォンであらかじめご予約し、お受取り店舗
                                        にてお支払い、受取りできるサービスです。</p>
                                </div>
                                <div class="group_content">
                                    <h3 class="title_content">ご利用について</h3>
                                    <div class="description_content">
                                        <p>会員登録されると、マイページがご利用いただけます。</p>
                                        <p>マイページが不要の方は、会員登録せずにご利用いただけます</p>
                                    </div>
                                </div>
                                <div class="group_content">
                                    <h3 class="title_content">ご予約について</h3>
                                    <div class="description_content">
                                        <p>ご予約はインターネット、スマートフォン・タブレットにて承ります。</p>
                                        <p>受取り店舗を選択し、カテゴリ、受取日を選択します。</p>
                                        <p>「商品一覧を表示する」ボタンを押下すると、ご予約可能な商品一覧が表示されます。</p>
                                        <p>多数の商品が表示されて、絞り込みたい場合には、表示された一覧上部にある、「絞り込み条件」ボタンを押下し表示商品を絞り込む事ができます。</p>
                                        <p>※店舗または受取り日によってはお取扱いのない商品もございます。予めご了承ください。</p>
                                    </div>
                                </div>
                                <div class="group_content space_top">
                                    <h3 class="title_content">商品について</h3>
                                    <div class="description_content">
                                        <p>[価格や表示について]</p>
                                        <p>・商品の価格は全て税抜き価格で表示しております。</p>
                                        <p>・サンエーネット予約サービスで使用している写真はイメージです。</p>
                                        <p>・生鮮食品は、生育状況や天候により産地が変わる場合がございます。</p>
                                        <p>・グラム売りの商品につきましては、商品の重量により売価が違います。実際の価格は、商品を計量してから「100g単価×実際の重量」の計算で決定いた</p>
                                        <p>します。また、重量変更による購入金額の変更のご連絡は致しませんのであらかじめご了承ください。</p>
                                        <div class="description_product">
                                            <div class="">[取扱商品について]</div>
                                            <p class="">「よりどりX点」割引や「朝市・夕市」など、一部対象外のサービスや商品がございます。</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="group_content">
                                    <h3 class="title_content">商品のお受取りついて</h3>
                                    <div class="description_content">
                                        <p>[台風発生時のお受取りについて]</p>
                                        <p>台風発生時のお受取りにつきまして、サービスエリアが受取り当日中に暴風域に入る予報の際、当該日の受取りを中止とし、ご注文はキャンセルとなる事が
                                            あります。</p>
                                        <p>※公共交通機関の運行を参考に判断いたします</p>
                                        <p>※サンエーの店舗が営業中であっても、お受取りは中止となる事がございます</p>
                                        <p>※上記受取り不可能と判断した場合、既に受けたご注文もキャンセルされます。（お受取り不可能の場合お客様へご連絡をいたしますが、お客様と連絡が取
                                            れなくても、ご注文はキャンセルとなります）</p>
                                    </div>
                                </div>
                                <div class="group_content space_top1">
                                    <h3 class="title_content">お支払いについて</h3>
                                    <div class="description_content">
                                        <p>お支払いは、お受取り店舗にてお願いいたします。</p>
                                    </div>
                                </div>
                                <div class="group_content">
                                    <h3 class="title_content">キャンセルについて</h3>
                                    <div class="description_content">
                                        <p>受注生産につき、ご予約後のキャンセル・変更は、お受取りの３日前までにお願いします。</p>
                                        <p>[キャンセルポリシー]</p>
                                        <p>当日のキャンセルや、受取り時間を過ぎてもご連絡が取れない場合などは、下記の注文キャンセル手数料を申し受けますので、予めご了承ください。</p>
                                        <p>※受取り日時を過ぎてもご連絡のない場合は、キャンセルとさせていただきます。</p>
                                    </div>
                                </div>
                                <div class="group_content">
                                    <h3 class="title_content">お問い合わせ・ご要望</h3>
                                    <div class="description_content">
                                        <p>【経塚シティ】：0120-157-515<br class="sp">（午前9時～午後5時、5時以降 098-871-3333）</p>
                                        <p>【西原シティ】：0120-852-052<br class="sp">（午前9時～午後5時、5時以降 098-882-9100）</p>
                                        <p>【那覇メインプレイス】：098-963-9121<br class="sp">（午前9時～午後5時、5時以降 098-951-3300）</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="checkbox">
                        <input id="checkbox_agree" name="policy" type="checkbox" {{(!empty($customers['policy']) || !empty($errors->all())? 'checked' : '')}}>
                        <label for="checkbox_agree">利用規約に同意する</label>
                    </div>
                    <div class="checkbox-error">@include('frontend.elements.validate_error',['message'=>$errors->first('policy')])</div>
                    <div class="button">
                        <div class="button_submit">
                            <button type="button" onclick="redirect()">入力内容を確認する</button>
                        </div>
                        <div class="button_back">
                            <button type="button" onclick="location.href='{{ route('frontend.login.confirm') }}'"><i class="fas fa-chevron-left"></i><span>前の画面に戻る</span></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="policy_modal_popup" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body policy_modal_popup">
                    <p class="text-center">利用規約に同意してください。</p>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-primary">OK</button>
                </div>
            </div>

        </div>
    </div>
@endsection
@section('js')
    <script type="text/javascript">
        $('.shopping-cart-fix-bottom').css('display','none');

        function redirect(){
            if($('#checkbox_agree').is(":checked")){
                $("#entry_form").submit();
            }else{
                $("#policy_modal_popup").modal('show');
            }
        }
    </script>
@endsection
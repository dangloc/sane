<?php

namespace App\Http\Requests;

use App\Models\Customer;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Factory as ValidationFactory;

class PasswordReminderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * PasswordReminderRequest constructor.
     * @param ValidationFactory $validationFactory
     * Customer validate
     */
    public function __construct(ValidationFactory $validationFactory)
    {

        $validationFactory->extend(
            'email_exist',
            function ($attribute, $value, $parameters) {
                $data = Customer::where('email', '=', $this->$attribute)->where('del_flag', '=', 0)->get()->toArray();
                if (!empty($data)) {
                    return true;
                }
            }
        );
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|check_email|email_exist',
        ];
    }

    public function attributes()
    {
        return[
            'email' => 'メールアドレス',
        ];
    }

    public function messages()
    {
        return [
            'email.email_exist' => 'メールアドレスは存在していません。',
        ];
    }
}

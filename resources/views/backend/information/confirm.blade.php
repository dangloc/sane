@extends('backend.layouts.main')
@section('content')
    <div id="Contentwrap">
        <div id="PageContent" class="no_mar_bottom">
            <h2><i class="fas fa-tag"></i> お知らせ{{ array_key_exists('id', Session::get('session_data')) ? '編集' : '登録'}}</h2>

            <div class="order">
                <table class="sp_block flowtable">
                    <tr>
                        <th scope="row" style="width: 150px;">タイトル</th>
                        <td>
                            {{$information['title']}}
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" style="width: 150px;">本文</th>
                        <td>
                            {!! nl2br(e($information['content'])) !!}
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" style="width: 150px;">公開期間</th>
                        <td>
                            {{date_format(date_create($information['public_start_date']),"Y/m/d")}}
                            {{substr_replace($information['public_start_time'], ":", 2, 0)}}時
                            <br class="sp">〜<br class="sp">{{date_format(date_create($information['public_end_date']),"Y/m/d")}}
                            {{substr_replace($information['public_end_time'], ":", 2, 0)}}時
                        </td>
                    </tr>
                </table>

            </div>
            <!--ContentFoot -->
        </div>
        <!--PageContent -->
        <form method="post"
              action="{{ array_key_exists('id', Session::get('session_data')) ? route('information.update', $information['id']) : route('information.store')}}"
              id="form_information_{{array_key_exists('id', Session::get('session_data')) ? 'confirm_edit':'confirm_create'}}">
            {{ csrf_field() }}
            @if(array_key_exists('id', Session::get('session_data')))
                @method('PUT')
            @endif
            <div class="contentfoot">
                <p class="btn Searchclear">
                    <button type="button"
                            onclick="window.location='{{ array_key_exists('id', Session::get('session_data')) ? route("information.edit", $information['id']):route("information.create") }}'">
                        <i class="fas fa-undo"></i> 戻る
                    </button>
                </p>
                <p class="btn register">
                    <button type="button" onclick="System.showDialog('information', '{{ array_key_exists('id', Session::get('session_data')) ? 'confirm_edit' : 'confirm_create'}}')">
                        <i class="fas fa-check"></i> {{ array_key_exists('id', Session::get('session_data')) ? '編集' : '登録'}}する</button>
                </p>
            </div>
            <!--PageContent -->
        </form>
    </div>
    <!--Contentwrap -->
@endsection
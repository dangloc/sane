@extends('frontend.layouts.main')
@section('content')
    <!--content detail-->
    <div id="complete-wrap" class="complete-wrap">
        <div id="prog" class="container">
            <div class="prog">
                <ul>
                    <li class="nomal"><span>カートの中</span><strong>1</strong></li>
                    <li class="nomal"><span>ログイン・入力</span><strong>2</strong></li>
                    <li class="nomal"><span>入力内容の確認</span><strong>3</strong></li>
                    <li class="current"><span>購入完了</span><strong>4</strong></li>
                </ul>
            </div>
        </div>
        <div class="container">
            <div class="complete confirm cart">
                @if(!empty($dataCartList))
                    @foreach($dataCartList as $data)
                        <div class="complete-confirm-cart-item">
                            <div class="title-page">
                                <h1 class="title"><i class="fas fa-pencil-alt"></i>マイページ新規登録<span>（注文完了）</span></h1>
                            </div>
                            <div class="back-to-page">
                                <button type="button" onclick="Frontend.windowPrint()">
                                    この画面を印刷する
                                </button>
                            </div>

                            <div class="thankYou text-center">
                                <div class="textTop groupText">
                                    <h2>ご注文ありがとうございました。 <br>伝票番号：{{$data['slipNumber']}}</h2>
                                </div>
                                <div class="textMiddle groupText">
                                    <p>ご注文頂いた内容は、後ほどサンエーネット予約サービスよりお送りする「ご注文確認メール」、<br>または[マイページ(※ご利用にはログインが必要です)]画面にてご確認いただけます。
                                    </p>
                                </div>
                                <div class="textBtn groupText">
                                    <div class="back-to-page">
                                        @if(\Illuminate\Support\Facades\Auth::guard('customers')->check())
                                            <button type="button" onclick="location.href='{{route('mypage.index')}}'">
                                                マイページへ
                                            </button>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div id="mapShow">
                                <div class="titleMap">
                                    <h3>商品の受取り日と受取り店舗は下記の通り。</h3>
                                </div>
                                <div class="information-order tableInfo">
                                    <div class="tableContent">
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td>受取り日</td>
                                                <td>{{$data['dateReceive']['year']}}年{{$data['dateReceive']['month']}}月{{$data['dateReceive']['day']}}日({{dayJapan($data['dateReceiveRaw'])}}) <strong>{{$data['timeReceive']}}時</strong></td>
                                            </tr>
                                            <tr>
                                                <td>商品カテゴリ</td>
                                                <td>{{$data['eventName']}}</td>
                                            </tr>
                                            <tr>
                                                <td>受取り店舗</td>
                                                <td>{{$data['shopName']}}　<span>TEL：{{$data['shopTel']}}</span></td>
                                            </tr>

                                            <tr>
                                                <td></td>
                                                <td><span>{{$data['cityName'].$data['address']}}</span></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="complete-map-chart" id = "map-chart-{{$data['slipNumber']}}" data-lat="{{$data['latlng']['lat']}}" data-lng="{{$data['latlng']['lng']}}">

                                </div>
                            </div>

                            <div class="description">
                                <p>ご注文商品</p>
                            </div>

                            <div class="cart-list-show">
                                <ul>
                                    @foreach($data['itemList'] as $item)
                                        <li>
                                            <div class="list-items">

                                                <div class="image-show left">
                                                    <a href="{{route('product.detail',['id'=>$item['code'] ,'shopcode' => $item['shop_code'],'keyitem'=>$data['keyItem'],'keyproduct'=>$item['keyProduct']])}}">
                                                        <img src="{{ ($item['item_image_folder_name'] ==null || $item['item_image_folder_name']=='_') ? asset('/frontend/images/item/noimage.jpg')
                                                    : config('const.url_item_image').$item['item_image_folder_name']}}"
                                                             alt="">
                                                    </a>
                                                </div>

                                                <div class="information-cart right">
                                                    <div class="title-product">
                                                        <h3><a href="{{route('product.detail',['id'=>$item['code'] ,'shopcode' => $item['shop_code'],'keyitem'=>$data['keyItem'],'keyproduct'=>$item['keyProduct']])}}">{{$item['item_name']}}</a>
                                                            @if($item['required_confirm_flag']!=config('const.not_with'))
                                                                <label class="required-confirm" style="<?= ($item['requiredFlag'] == '0') ? "display:inline-block" : "display:none"?>">

                                                                    @if($item['required_confirm_flag']== config('const.with_wasabi'))
                                                                        わさび入り @elseif($item['required_confirm_flag']==config('const.with_kamaboko'))
                                                                        かまぼこ赤 @elseif($item['required_confirm_flag']==config('const.with_classic')) 定番
                                                                    @endif
                                                                </label>
                                                                <label class="required-confirm" style="<?= ($item['requiredFlag'] == '1') ? "display:inline-block" : "display:none"?>" >
                                                                    @if($item['required_confirm_flag']==config('const.with_wasabi'))
                                                                        わさび抜き@elseif($item['required_confirm_flag']==config('const.with_kamaboko'))
                                                                        かまぼこ白 @elseif($item['required_confirm_flag']==config('const.with_classic')) 法事
                                                                    @endif
                                                                </label>
                                                            @endif
                                                        </h3>
                                                    </div>
                                                    <div class="information-detail">

                                                        <div class="information-text">
                                                            <div class="price price-one-products">
                                                                <span class="text">価格</span>
                                                                <span class="number"><span>{{number_format($item['price'])}}</span></span>
                                                                <span class="unit">円+税</span>
                                                            </div>
                                                            <div class="quantity">
                                                                <div class="quantity-btn">
                                                                    <label>数量</label>
                                                                    <span class="number-products"><span
                                                                                class="number">{{number_format($item['quantity'])}}</span></span>
                                                                </div>
                                                            </div>
                                                            <div class="price price-total-products">
                                                                <span class="text">価格</span>
                                                                <span class="number"><span>{{number_format($item['total_price'])}}</span></span>
                                                                <span class="unit">円+税</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>

                            <div class="cart-total">
                                <ul>
                                    <li class="total-products">
                                        <div class="total-products-items">
                                            <label>商品数</label>
                                            <span class="total-number">{{number_format($data['total'])}}品</span>
                                        </div>
                                    </li>
                                    <li class="total-price">
                                        <div class="price-items">
                                            <label>合計金額</label>
                                            <span class="total-number-price"><span class="number">{{number_format($data['totalPrice'])}}</span><span
                                                        class="unit">円+税</span></span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    @endforeach
                @endif
                @if(!empty($customers))
                    <div class="information-order tableInfo">
                        <h3>お客さま情報</h3>
                        <div class="tableContent">
                            <table>
                                <tr>
                                    <td>お名前</td>
                                    <td>{{$customers['name']}}</td>
                                </tr>
                                <tr>
                                    <td>電話番号</td>
                                    <td>{{$customers['tel']}}</td>
                                </tr>
                                <tr>
                                    <td>サンエーカード番号</td>
                                    <td>{{!empty($customers['card_no']) ? formatCardNo($customers['card_no']) :''}}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                @endif
                <div class="group-btn-bottom setFloat">
                    <div class="back-to-page">
                        <button type="button" onclick="Frontend.windowPrint()">この画面を印刷する</button>
                    </div>
                </div>

            </div>
        </div>

    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function ($) {
            if (window.history && window.history.pushState) {
                window.history.pushState('forward', null, './#forward');
                $(window).on('popstate', function () {
                    location.href = laroute.route('site.index');
                });
            }
        });
    </script>
    <script type="text/javascript">
        $('.shopping-cart-fix-bottom').css('display', 'none');
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key={{config('const.GOOGLE_API_KEY')}}&callback=drawMap&language=ja">
    </script>
@endsection



<?php

namespace App\Models;

use App\Scopes\ActiveScope;
use Illuminate\Database\Eloquent\Model;

class EventItemView extends Model
{
    public $timestamps=false;
    protected $_alias='event_item_views';
    protected $table='event_item_views';
    protected $fillable=[
        'event_code',
        'category_code',
        'item_code',
        'sort',
        'receptionist_start_date',
        'receptionist_end_date',
        'receipt_start_date',
        'receipt_end_date',
        'del_flag',
        'ins_datetime',
        'ins_id',
        'upd_datetime',
        'upd_id'
    ];
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ActiveScope());
    }
}

<?php

namespace App\Jobs;

use App\Models\Category;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\Filesystem;
use App\Http\Common\logWriter;
use mysql_xdevapi\Exception;

class importTsv implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $batch_name;

    public function __construct($batch_name)
    {

        $this->batch_name = $batch_name;
    }

    function pushLog()
    {
        return new logWriter('storage/logs/import-tsv-' . Carbon::now()->format('Y-m-d') . '.log');
    }

    /**
     * Load data from tsv file
     * @param $model : name of model
     * @param $content : content of tsv file
     * @return array
     */
    function load_tsv_file($model, $content, $file_path)
    {
        $list_field = config('tsv_import.' . $model . '.fields');
        $array = array();
        $line_array = explode("\n", trim($content));
        for ($n = 0; $n < sizeof($line_array); $n++) {
            $line_output = array();
            if (trim($line_array[$n]) != '') {
                $line = explode("\t", trim($line_array[$n]));
                if (sizeof($line) == sizeof($list_field)) {
                    for ($m = 0; $m < sizeof($list_field); $m++) {
                        $line_output[$list_field[$m]] = $line[$m] != '""' ? trim($line[$m]) : '';
                    }
                    // only for BATCH 6
                    if ($model == 'EventItemView') {
                        if ($line_output['receptionist_start_date'] == '_'){
                            $line_output['receptionist_start_date'] = Carbon::now()->format('ymd');
                        }
                        if ($line_output['receptionist_end_date'] == '_'){
                            $line_output['receptionist_end_date'] = $line_output['receipt_end_date'];
                        }
                    }
                    // only for BATCH item
                    if ($model == 'Item') {
                        if ($line_output['limited_count'] == '_'){
                            $line_output['limited_count'] = 0;
                        }
                        if ($line_output['additive_code'] == '_'){
                            $line_output['additive_code'] = 0;
                        }
                        if ($line_output['lead_time'] == '_'){
                            $line_output['lead_time'] = 0;
                        }
                        // replace one or more "@" become new line character
                        $line_output['content2'] = preg_replace('/(@)+/', chr(0x0A), $line_output['content2']);
                    }
                    $line_output['ins_id'] = 999999;
                } else {
                    $this->pushLog()->error("ERROR ! Error not enough attribute in line " . ($n + 1). ' '. $file_path);
                    return false;
                }
            }
            array_push($array, $line_output);
        }
        return $array;
    }

    /**
     * Handling data, split data to insert_array and update array, execute
     * @param $model : name of model
     * @param $data : array from function load_tsv_file
     * @param $file_path : path to file tsv ( for logging)
     */
    function processData($model, $data, $file_path)
    {

        // BATCH 14
        if ($model == 'ReserveUpdate') {
            DB::beginTransaction();
            // insert into reserve_update
            try {
                $this->insertData('ReserveUpdate', $data);
                // update into customer_orders
                $update_array = [];
                foreach ($data as $record) {
                    $update_record = [];
                    $target_model_update = 'App\Models\CustomerOrder';
                    $query = $target_model_update::where('del_flag', '=', 0)->where('slip_number', '=', $record['slip_number']);
                    if (sizeof($query->withoutGlobalScopes()->get()) > 0 and in_array($record['flag'], [1, 2])) {
                        $update_record = [
                            'slip_number' => $record['slip_number'],
                            'status' => ($record['flag'] + 1)
                        ];
                        array_push($update_array, $update_record);
                    }
                }
                $this->updateData('CustomerOrder', $update_array);
                Storage::disk('sftp')->getAdapter()->disconnect();
                $this->pushLog()->info("COMPLETE ! Insert/Update " . $model . ' in ' . $file_path);
                DB::commit();
            } catch (\Exception $exception) {
                DB::rollback();
                $this->createFile($model, 'ERR');
                Storage::disk('sftp')->getAdapter()->disconnect();
                $this->pushLog()->error("FAIL ! Error when insert/update " . $model . ' in ' . $file_path . " Error ". $exception->getMessage());
                $this->pushLog()->info("ROLLBACK ". $model );
            }
        } else {
            // BATCH 2-12
            $target_model = 'App\Models\\' . $model;
            DB::beginTransaction();
            if (strpos($file_path, '_ALL')) {
                $target_model::query()->withoutGlobalScopes()->delete();
                $this->pushLog()->info("DELETE ALL " . $model);
                if ($model == 'Item'){
                    $target_model_status = 'App\Models\ItemStatus';
                    $target_model_status::query()->withoutGlobalScopes()->delete();
                    $this->pushLog()->info("DELETE ALL ItemStatus");
                }
            }
            $insert_array = [];
            $update_array = [];
            $target_model = 'App\Models\\' . $model;
            $key_array = config('tsv_import.' . $model . '.keys');
            foreach ($data as $record) {
                $query = $target_model::query();
                foreach ($key_array as $key) {
                    $query->where($key, '=', $record[$key]);
                }
                if (!sizeof($query->withoutGlobalScopes()->get())) {
                    array_push($insert_array, $record);
                } else {
                    array_push($update_array, $record);
                }
            }
            try {
                $this->insertData($model, $insert_array);
                $this->updateData($model, $update_array);
                // only with item => insert into ItemStatus
                if ($model == 'Item') {
                    $insert_array_status = [];
                    $key_array_status = ['code', 'shop_code'];
                    $target_model_status = 'App\Models\ItemStatus';
                    foreach ($insert_array as $item) {
                        $query_status = $target_model_status::where('del_flag', '=', 0);
                        $record_status = [];
                        foreach ($key_array_status as $key) {
                            $query_status->where($key, '=', $item[$key]);
                            $record_status[$key] = $item[$key];
                        }
                        $record_status['stop_flag'] = '0';
                        $record_status['ins_id'] = 999999;
                        if (!sizeof($query_status->withoutGlobalScopes()->get())) {
                            array_push($insert_array_status, $record_status);
                        }
                    }
                    $this->insertData('ItemStatus', $insert_array_status);
                }
                Storage::disk('sftp')->getAdapter()->disconnect();
                $this->pushLog()->info("COMPLETE ! Insert/Update " . $model . ' in ' . $file_path);
                DB::commit();
            } catch (\Exception $exception) {
                DB::rollback();
                $this->createFile($model, 'ERR');
                Storage::disk('sftp')->getAdapter()->disconnect();
                $this->pushLog()->error("FAIL ! Error when insert/update " . $model . ' in ' . $file_path . " Error ". $exception->getMessage());
                $this->pushLog()->info("ROLLBACK ". $model );
            }
        }
    }

    /**
     * Insert data to database, disconnect sftp, push log
     * @param $model : name of model
     * @param $values : list records which will be inserted
     * @param $file_path : path to file tsv ( for logging)
     */
    function insertData($model, $values)
    {
        if (sizeof($values) != 0) {
            $target_model = 'App\Models\\' . $model;
            $values_split = array_chunk($values, config('const.batch.number_of_record'));
            foreach ($values_split as $value) {
                $target_model::insert($value);
            }
        }
    }

    /**
     * Update data to database, disconnect sftp, push log
     * @param $model : name of model
     * @param $datas : list records which will be updated
     * @param $file_path : path to file tsv ( for logging)
     */
    function updateData($model, $values)
    {
        if (sizeof($values) != 0) {
            $target_model = 'App\Models\\' . $model;
            $table = $target_model::getModel()->getTable();
            if ($model == 'CustomerOrder') {
                // only for batch 14 , need to update into customer_orders
                $list_keys = ['slip_number'];
                $list_fields = ['slip_number', 'status'];
            } else {
                $list_keys = config('tsv_import.' . $model . '.keys');
                $list_fields = config('tsv_import.' . $model . '.fields');
            }
            $values_split = array_chunk($values, config('const.batch.number_of_record'));
            foreach ($values_split as $value) {
                $keys = implode(',', $list_keys);
                $update_query = "UPDATE {$table} SET ";
                $array_key_value_tail = [];
                foreach ($list_fields as $field) {
                    if (!in_array($field, $list_keys)) {
                        $update_query .= $field . " = CASE concat(" . $keys . ")";
                        foreach ($value as $data) {
                            $value_keys = [];
                            foreach ($list_keys as $key) {
                                array_push($value_keys, $data[$key]);
                            }
                            $field_value = "'" . $data[$field] . "'";
                            $update_query .= " WHEN concat('" . implode("','", $value_keys) . "') THEN " . $field_value;
                            array_push($array_key_value_tail, "concat('" . implode("','", $value_keys) . "')");
                        }
                        $update_query .= " ELSE " . $field . " END,";
                    }
                }
                $update_query .= "upd_id = 999999, upd_datetime = CURRENT_TIMESTAMP";
                $update_query .= " WHERE concat(" . $keys . ") IN (" . implode(",", $array_key_value_tail) . ");";
                DB::update($update_query);
            }
        }
    }

    /**
     * Check file exist or not
     * @param $model : name of model
     * @param $files : list files will be checked
     * @param $type : type will be accepted. RUN or ERR
     * @return boolean
     */
    function checkFile($model, $files, $type)
    {
        $path = config('tsv_import.' . $model . '.folder');
        $inputs = preg_grep('/^(' . $path . '\/' . $type . ')$/', $files);
        return !(sizeof($inputs) == 0);
    }
    /**
     * Create file
     * @param $model : name of model
     * @param $type : type of file will be create. RUN or ERR
     */
    function createFile($model, $type)
    {
        $path = config('tsv_import.' . $model . '.folder');
        Storage::disk('sftp')->put($path . '/' . $type, '');
    }

    /**
     * Remove file
     * @param $model : name of model
     * @param $type : type of file will be remove. RUN or ERR
     */
    function removeFile($model, $type)
    {
        $path = config('tsv_import.' . $model . '.folder');
        Storage::disk('sftp')->delete($path . '/' . $type);
    }

    /**
     * Move file to BACKUP folder
     * @param $file : path to file
     */
    function movingBackup($file)
    {
        if (Storage::disk('sftp')->exists(str_replace('/', '/BACKUP/', $file))) {
            Storage::disk('sftp')->delete(str_replace('/', '/BACKUP/', $file));
        }
        Storage::disk('sftp')->move($file, str_replace('/', '/BACKUP/', $file));
    }

    /**
     * Get the files that meet the conditions
     * @param $model : name of model
     * @param $files : list all files
     * @return array
     */
    function sortFile($model, $files)
    {
        $path = config('tsv_import.' . $model . '.folder');
        $regex = '/^((' . $path . '\/' . $path . '\.)\d{14}(_ALL)?\.tsv)$/';
        // only for BATCH 14
        if ($model == 'ReserveUpdate'){
            $regex = '/^((' . $path . '\/' . $path . '\.)\d{12}\.\d{14}\.tsv)$/';
        }
        $list_files = preg_grep($regex, $files);
        sort($list_files);
        return $list_files;
    }


    function connectSftp($model)
    {
        $path = config('tsv_import.' . $model . '.folder');
        $target_model = 'App\Models\\' . $model;
        $sftp = Storage::disk('sftp');
        $this->pushLog()->info("START IMPORT " . $model . " IN " . $path);

        for ($i = 0; $i < 3; $i++) {
            try {
                $sftp->getAdapter()->getConnection();
            } catch (\Exception $exception) {
                $this->pushLog()->error($exception->getMessage());
                $sftp->getAdapter()->disconnect();
            }
        }
        $path = config('tsv_import.' . $model . '.folder');
        $list_file = $sftp->allFiles($path);

        if ($this->checkFile($model, $list_file, 'ERR')) {
            // push log
            $sftp->getAdapter()->disconnect();
            $this->pushLog()->warning("COMPLETE ! ERR exist " . $path);
        } elseif ($this->checkFile($model, $list_file, 'RUN')) {
            // push log
            $sftp->getAdapter()->disconnect();
            $this->pushLog()->info("COMPLETE ! is Running " . $path);
        } else {
            // start run process

            $list_file_sort = $this->sortFile($model, $list_file);
            if (sizeof($list_file_sort) == 0){
                $this->pushLog()->info("COMPLETE ! No tsv file in  " . $path);
            } else {
                $this->createFile($model, 'RUN');
                for ($m = 0; $m < sizeof($list_file_sort); $m++) {
                    if ($sftp->exists('/' . $path . '/ERR')) {
                        // stop if have ERR becuz previus files
                        $sftp->getAdapter()->disconnect();
                    } elseif ($sftp->size($list_file_sort[$m]) == 0) {
                        $this->pushLog()->info("COMPLETE ! No data in file " . $list_file_sort[$m]);
                        $this->movingBackup($list_file_sort[$m]);
                    } else {
                        $content = $sftp->get($list_file_sort[$m]);
                        $data = $this->load_tsv_file($model, $content, $list_file_sort[$m]);
                        if ($data == false) {
                            $this->createFile($model, 'ERR');
                        } else {
                            $this->processData($model, $data, $list_file_sort[$m]);
                        }
                        if (!$sftp->exists('/' . $path . '/ERR')) {
                            $this->movingBackup($list_file_sort[$m]);
                        }
                    }

                }
                $this->pushLog()->info("END IMPORT " . $model . " IN " . $path);
                $this->removeFile($model, 'RUN');
            }
            // end process
        }
    }

    public function handle()
    {
        ini_set("memory_limit", "512M");
        $batch_array = [
            'GYOJIUKETSUKE_GYOJI' => 'Event',
            'GYOJIUKETSUKE_KATEGORI' => 'Category',
            'GYOJIUKETSUKE_TABKATEGORI' => 'EventCategory',
            'GYOJIUKETSUKE_GYOJI_LIST' => 'EventView',
            'GYOJIUKETSUKE_SHOHINNO' => 'EventItemView',
            'GYOJIUKETSUKE_SHOHINMASTER' => 'Item',
            'GYOJIUKETSUKE_SHOKUHINITEM_NAME' => 'ItemName',
            'GYOJIUKETSUKE_SHOKUHINITEM_TANKA' => 'ItemPrice',
            'SOKIYOYAKU_BP_JANCD_TE_MAINTE' => 'ItemBonusPoint',
            'TENBETSU_CHUMON_KANOSU' => 'ShopPlanOrderPossibleCount',
            'SHOHIN_CHUMON_KANOSU' => 'ItemLimitedOrderPossibleCount',
            'GYOJIUKETSUKE_YOYAKUJOHO_HENKO_FLG' => 'ReserveUpdate',
        ];
        try {
            $this->connectSftp($batch_array[$this->batch_name]);
        } catch (\Exception $exception) {
            $this->pushLog()->error( "ERROR IMPORT " . $this->batch_name . " ".$exception->getMessage());
            $this->removeFile($batch_array[$this->batch_name], 'RUN');
            $this->createFile($batch_array[$this->batch_name], 'ERR');
        }
    }
}

<?php

namespace App\Models;

use App\Scopes\ActiveScope;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;

class Customer extends Model implements Authenticatable
{
    use AuthenticableTrait;
    public $timestamps = false;
    protected $_alias = 'customers';
    protected $table = 'customers';
    protected $guard = 'customers';
    protected $fillable = [
        'name',
        'email',
        'password',
        'tel',
        'del_flag',
        'card_no',
        'ins_datetime',
        'ins_id',
        'upd_datetime',
        'upd_id',
        'email_time_valid',
        'email_token',
        'card_no'
    ];
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ActiveScope());
    }

    protected $hidden = ['password'];
    public function customer_orders(){
        return $this->hasMany(CustomerOrder::class);
    }
}

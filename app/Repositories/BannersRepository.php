<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;

/**
 * class BannersRepository.
 *
 * @package namespace App\Repositories;
 */
class BannersRepository extends BaseRepository
{
    function __construct()
    {
        $this->_model = \App\Models\Banners::class;
    }

    /**
     * @return \Illuminate\Support\Collection
     *  Lấy danh sách banner
     */
    public function getListBanner()
    {
        $data = DB::table('banners')
            ->select('banner_url', 'link_url', 'id','status','public_start_date','public_end_date')
            ->where('del_flag', '=', 0)
            ->orderBy('public_start_date', 'DESC')
            ->where('public_start_date', '<=', date('Y-m-d'))
            ->where('public_end_date', '>=', date('Y-m-d'))
            ->get();
        return $data;
    }
    public function getValueAttribute($id){
        $valueArray =  $this->getModel()->select('id','banner_url','link_url','public_start_date','public_end_date','status')->where('id',$id)->get()->toArray();
        return $valueArray[0];
    }
    public function getAllListBanner(){
        $sort = request()->get('sort', 'id');
        $direction = request()->get('direction', 'desc');
        $data = DB::table('banners')
            ->select('banner_url', 'link_url', 'id','status','public_start_date','public_end_date')
            ->where('del_flag', '=', 0)
            ->orderBy($sort, $direction)
            ->paginate(config('const.paging.backend'));
        return $data;
    }
}

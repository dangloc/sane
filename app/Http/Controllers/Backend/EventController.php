<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\BackendController;
use App\Repositories\EventCategoryRepository;
use App\Repositories\EventItemRepository;
use App\Repositories\EventRepository;
use App\Repositories\EventViewRepository;

class EventController extends BackendController
{
    public $eventRepository;
    public $eventViewRepository;
    public $eventItemRepository;
    public $eventCategoryRepository;

    public function __construct(
        EventRepository $eventRepository,
        EventCategoryRepository $eventCategoryRepository,
        EventItemRepository $evenItemRepository,
        EventViewRepository $eventViewRepository)
    {
        $this->eventRepository = $eventRepository;
        $this->eventViewRepository = $eventViewRepository;
        $this->eventItemRepository = $evenItemRepository;
        $this->eventCategoryRepository = $eventCategoryRepository;
    }

    public function index()
    {
        $data = $this->eventRepository->paging();
        return view('backend.event.index',
            [
                'listData' => $data,
                'params' => request()->all()
            ]
        );
    }

    public function item()
    {
        $data = $this->eventItemRepository->paging();
        return view('backend.event.item',
            [
                'listData' => $data,
                'params' => request()->all()
            ]
        );
    }

    public function category()
    {
        $data = $this->eventCategoryRepository->paging();
        return view('backend.event.category',
            [
                'listData' => $data,
                'params' => request()->all()
            ]
        );
    }

    public function view()
    {
        $data = $this->eventViewRepository->paging();
        return view('backend.event.view',
            [
                'listData' => $data,
                'params' => request()->all()
            ]
        );
    }
}

<?php

namespace App\Repositories;

use App\Models\Customer;
use Illuminate\Support\Facades\DB;

/**
 * class CustomerRepository.
 *
 * @package namespace App\Repositories;
 */
class CustomerRepository extends BaseRepository
{
    function __construct()
    {
        $this->_model = Customer::class;
    }

    /**
     * @param $email
     * @return bool
     * get customer by email
     */
    public function getCustomerAttribute($attribute, $value)
    {
        $data = $this->getModel()->where($attribute, '=', $value)->first();
        if (!empty($data)) {
            return $data->toArray();
        }
        return false;
    }

    /**
     * @param bool $is_number
     * @param int $length
     * @return bool|string
     * Gen token key
     */
    public function genRandKey($is_number = TRUE, $length = 15)
    {
        $randStr = '';
        if ($is_number) {
            $timeREQ = date('YmdHis', time());
            $endREQ = rand(1000, 9999);
            $randStr = $timeREQ . $endREQ;
        } else {
            $randStr = substr(md5(rand()), 0, $length);
        }
        $check = $this->getModel()->where('email_token', '=', $randStr)->first();
        if ($check) {
            return $this->genRandKey($is_number, $length);
        }
        return $randStr;
    }

    public function create($data_create)
    {
        $object = new $this->_model($data_create);
        $object->save();
        return $object->id;
    }

    //  for admin search
    public function searchCondition($request)
    {
        $sort = $request->get('sort', 'id');
        $direction = $request->get('direction', 'desc');
        $query = $this->getModel()
            ->select('*')
            ->where('card_no', 'like', '%'. $request->get('card_no') . '%')
            ->where('tel', 'like', '%' . $request->get('tel') . '%')
            ->where('name', 'like', '%' . $request->get('name') . '%')
            ->where('email', 'like', '%' . $request->get('email') . '%')
            ->addSelect(DB::raw('IF( `password` != "", "true", "false") as has_password'));
        return $query->orderBy($sort, $direction);
    }
}

function initMap() {
    var pos_lat = 26.380596;
    var pos_lgn = 127.85684300000003;
    if ($('#latlng').val() != "") {
        pos_lat = $('#latlng').val().split(" ")[0];
        pos_lgn = $('#latlng').val().split(" ")[1];
    }
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 15,
        center: {lat: Number(pos_lat), lng: Number(pos_lgn)}
    });
    latlng = new google.maps.LatLng(Number(pos_lat), Number(pos_lgn));
    var marker = new google.maps.Marker({
        map: map,
        position: latlng,
        draggable: true
    });
    $('input[name="latlng"]').val(Number(pos_lat) + " " + Number(pos_lgn));
    google.maps.event.addListener(marker, 'dragend', function(evt){
        $('input[name="latlng"]').val(evt.latLng.lat() + " " + evt.latLng.lng());
    });
    google.maps.event.addListener(map, 'click', function(event) {
        placeMarker(event.latLng, marker);
    });
}
function placeMarker(location, marker) {
    marker.setPosition(location);
    $('input[name="latlng"]').val(location.lat() + " " + location.lng());
}

function geocodeAddress(geocoder, resultsMap) {
    var e = document.getElementById("city");
    var city = '' !== e.options[e.selectedIndex].value ? e.options[e.selectedIndex].text : '';
    var address = document.getElementById('address').value ? document.getElementById('address').value : 'Japan';
    geocoder.geocode({'address': (city + ' ' + address)}, function(results, status) {
        if (status === 'OK') {
            resultsMap.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
                map: resultsMap,
                position: results[0].geometry.location,
                draggable: true
            });
            google.maps.event.addListener(marker, 'dragend', function(evt){
                $('input[name="latlng"]').val(evt.latLng.lat() + " " + evt.latLng.lng());
            });
            google.maps.event.addListener(resultsMap, 'click', function(event) {
                placeMarker(event.latLng, marker);
            });
            console.log(results[0].geometry.location.lat());
            console.log(results[0].geometry.location.lng());
            $('input[name="latlng"]').val(results[0].geometry.location.lat() + " " + results[0].geometry.location.lng());
        } else {
            alert('次の理由により、ジオコードが成功しませんでした: ' + status);
        }
    });
}

$('#address').on('change', function () {
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 15,
    });
    var geocoder = new google.maps.Geocoder();
    geocodeAddress(geocoder, map);
});
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reserves', function (Blueprint $table) {
            $table->char('slip_number', 12)->comment('伝票番号');
            $table->char('shop_code', 4)->comment('店舗コード');
            $table->char('receptionist_date', 8)->comment('受付日');
            $table->char('receptionist_type', 1)->comment('受付方法')->default(3);
            $table->integer('receptionist_staff_code')->comment('受付社員コード')->default(999999);
            $table->char('receipt_date',8)->comment('受取日');
            $table->char('receipt_time',2)->comment('受取時間');
            $table->string('customer_name', 512)->comment('お名前');
            $table->string('customer_tel', 11)->comment('電話番号');
            $table->string('customer_card_no', 12)->comment('カード番号');
            $table->char('event_code', 4)->comment('行事コード');
            $table->char('item_code', 13)->comment('商品コード');
            $table->char('required_confirm_flag', 1)->comment('必須確認事項フラグ');
            $table->char('required_flag', 1)->comment('必須フラグ');
            $table->smallInteger('item_count')->comment('数量');
            $table->char('status_flag', 1)->comment('ステータスフラグ')->default(0);
            $table->char('update_flag', 1)->comment('予約変更フラグ');
            $table->text('reserve_content1')->comment('予備項目1');
            $table->text('reserve_content2')->comment('予備項目2');
            $table->char('reserve_del_flag', 1)->comment('削除フラグ');
            $table->char('update_timestamp',14)->comment('更新日時分秒'); // yyyymmddHHMMSS
            $table->integer('update_staff_code')->comment('更新社員番号')->default(999999);
            $table->char('del_flag', 1)->comment('削除フラグ')->default(0);
            $table->dateTime('ins_datetime')->default(DB::raw('CURRENT_TIMESTAMP'))->comment('登録日時');
            $table->integer('ins_id')->length(11)->comment('登録者ID');
            $table->dateTime('upd_datetime')->comment('更新日時')->nullable();
            $table->integer('upd_id')->length(11)->comment('更新者ID')->nullable();

            $table->primary(array('shop_code', 'slip_number', 'item_code','required_flag'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reserves');
    }
}

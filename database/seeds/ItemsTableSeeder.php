<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Items')->truncate();
        DB::table('items')->insert([
            [
                'shop_code'=>'1',
                'code'=>'0010',
                'item_image_name'=>'item1.jpg',
                'item_image_folder_name'=>'/frontend/images/item/',
                'content1'=>'サンエーネッ',
                'content2'=>'サンエーネットスーパーエリア拡大（ 那覇メインプレイス周辺エリア）の2',
                'content3'=>'サンエーネットスーパー',
                'required_confirm_flag'=>rand(1,3),
                'limited_flag'=>'0',
                'additive_code'=>'1',
                'lead_time'=>'7',
                'ins_id'=>'1',
                'del_flag' => 0,
                'ins_datetime'=>Carbon::now()
            ],[
                'shop_code'=>'2',
                'code'=>'0011',
                'item_image_name'=>'item1.jpg',
                'item_image_folder_name'=>'/frontend/images/item/',
                'content1'=>'サンエーネッ',
                'content2'=>'サンエーネットスーパーエリア拡大（ 那覇メインプレイス周辺エリア）の2',
                'content3'=>'サンエーネットスーパ',
                'required_confirm_flag'=>rand(1,3),
                'limited_flag'=>'0',
                'additive_code'=>'2',
                'lead_time'=>'6',
                'ins_id'=>'1',
                'del_flag' => 0,
                'ins_datetime'=>Carbon::now()
            ],[
                'shop_code'=>'3',
                'code'=>'0012',
                'item_image_name'=>'item1.jpg',
                'item_image_folder_name'=>'/frontend/images/item/',
                'content1'=>'サンエーネットス',
                'content2'=>'サンエーネットスーパーエリア拡大（ 那覇メインプレイス周辺エリア）の2',
                'content3'=>'サンエーネッ',
                'required_confirm_flag'=>rand(1,3),
                'limited_flag'=>'0',
                'additive_code'=>'3',
                'lead_time'=>'5',
                'ins_id'=>'1',
                'del_flag' => 0,
                'ins_datetime'=>Carbon::now()
            ]
        ]);
    }
}

<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\NotRegistrationRequest;
use App\Models\Reserve;
use App\Repositories\AddToCartRepository;
use App\Repositories\AjaxRepository;
use App\Repositories\CitiesRepository;
use App\Repositories\CustomerOrderRepository;
use App\Repositories\CustomerRepository;
use App\Repositories\ItemRepository;
use App\Repositories\ReserveRepository;
use App\Repositories\SendMailRepository;
use App\Repositories\ShopsRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

class CartController extends Controller
{
    public $addToCartRepository;
    public $itemRepository;
    public $ajaxRepository;
    public $customerRepository;
    public $reserveRepository;
    public $customerOrderRepository;
    public $shopRepository;
    public $citiesRepository;
    public $sendMailRepository;

    public function __construct(AddToCartRepository $addToCartRepository,
                                ItemRepository $itemRepository,
                                AjaxRepository $ajaxRepository,
                                CustomerRepository $customerRepository,
                                ReserveRepository $reserveRepository,
                                CustomerOrderRepository $customerOrderRepository,
                                ShopsRepository $shopRepository,
                                CitiesRepository $citiesRepository,
                                SendMailRepository $sendMailRepository
    )
    {
        $this->addToCartRepository = $addToCartRepository;
        $this->itemRepository = $itemRepository;
        $this->ajaxRepository = $ajaxRepository;
        $this->customerRepository = $customerRepository;
        $this->reserveRepository = $reserveRepository;
        $this->customerOrderRepository = $customerOrderRepository;
        $this->shopRepository = $shopRepository;
        $this->citiesRepository = $citiesRepository;
        $this->sendMailRepository = $sendMailRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * List cart session
     */
    public function index(Request $request)
    {
        $datas = array();
        $total_bill = 0;

        if ($request->session()->has(config('const.session.cart'))) {
            $data_session = $request->session()->get(config('const.session.cart'));
            if (!empty($data_session)) {
                $datas = $this->addToCartRepository->getFullDataSessionCart($data_session);
            }
        }
        return view('frontend.cart.index', compact('datas', 'total_bill'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     * Load box cart
     */
    public function onLoadBoxCart(Request $request)
    {
        $data_list_cart = array();
        if ($request->session()->has(config('const.session.cart'))) {
            $dataCartList = $request->session()->get(config('const.session.cart'));
            $dataCartList = $this->addToCartRepository->getFullDataSessionCart($dataCartList);
            if (!empty($dataCartList)) {
                $view = view("frontend.elements.box_cart_list",
                    compact('dataCartList'))->render();
                return response()->json(['success' => true, 'data' => $view]);
            }
        }
        return response()->json(['success' => false, 'data' => $data_list_cart]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     * addToCart
     */
    public function addToCart(Request $request)
    {
        if ($request->ajax()) {
            $itemCode = $request->input('itemCode', 0);
            $quantity = $request->input('quantity', 0);
            $requiredFlag = $request->input('requiredFlag', 0);
            $shopCode = $request->input('shopCode', 0);
            $dataReceiveRaw = $request->input('dateReceiveRaw', 0);
            $timeReceive = $request->input('timeReceive', 0);
            if (checkExistSession($request)) {
                $requiredConfirmFlag = $this->itemRepository->getRequiredConfirmFlag($itemCode, $shopCode);
                saveSessionItem($request, $itemCode, $quantity, $requiredFlag, $shopCode, $requiredConfirmFlag, $dataReceiveRaw, $timeReceive);
                if ($request->session()->has(config('const.session.cart'))) {
                    $dataCartList = $request->session()->get(config('const.session.cart'));
                    $dataCartList = $this->addToCartRepository->getFullDataSessionCart($dataCartList);
                    $view = view("frontend.elements.box_cart_list",
                        compact('dataCartList'))->render();
                    return response()->json(['success' => true, 'data' => $view]);
                }
            }
        }
        return response()->json(['success' => false, 'data' => '']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Remove one item in cart list session
     */
    public function removeItemCart(Request $request)
    {
        $dataKeyItem = $request->input('dataKeyItem', 0);
        $dataKeyProduct = $request->input('dataKeyProduct', 0);
        $checkEmptyItem = false;
        if (!empty($dataKeyItem) && !empty($dataKeyProduct)) {
            if ($request->session()->has(config('const.session.cart'))) {
                $datas = $request->session()->get(config('const.session.cart'));
                foreach ($datas as $key => $data) {
                    if ($data['key_item'] == $dataKeyItem) {
                        foreach ($data['itemList'] as $key_item => $item) {
                            if ($key_item == $dataKeyProduct) {
                                unset($datas[$key]['itemList'][$key_item]);
                                if (empty($datas[$key]['itemList'])) {
                                    unset($datas[$key]);
                                    $checkEmptyItem = true;
                                }
                            }
                        }
                    }
                }
                if (!empty($datas)) {
                    $request->session()->put(config('const.session.cart'), $datas);
                } else {
                    $request->session()->pull(config('const.session.cart'));
                }
            }
        }
        return response()->json(['success' => 1, 'data' => $checkEmptyItem]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * Check stock
     */
    public function checkItemCart(Request $request)
    {
        $dataRequired = $request->input('dataRequired');
        $flag_check = true;
        $updateDataRequired = self::updateDataRequired($request, $dataRequired);
        if ($request->session()->has(config('const.session.cart')) && $updateDataRequired) {
            $dataCartList = $request->session()->get(config('const.session.cart'));
            $dataCartList = $this->addToCartRepository->controlGetListByItemSession($dataCartList);
            if (!empty($dataCartList)) {
                foreach ($dataCartList as $key => $item) {
                    $stock = 0;
                    foreach ($dataCartList as $key_check => $item_check) {
                        if ($item_check['shop_code'].$item_check['code'] == $item['shop_code'].$item['code']){
                            $stock += $item_check['quantity'];
                        }
                    }

                    $check = $this->ajaxRepository->checkCartStockExist($item['shop_code'], $item['eventCode'], $item['dateReceiveRaw'], $item['code'], $stock, $item['item_name']);
                    if ($check['success'] == false) {
                        $flag_check = false;
                    }
                    $dataCartList[$key]['errorMessage'] = $check['message'];
                    $dataCartList[$key]['stock'] = $check['stock'];
                }
                return response()->json(['success' => $flag_check, 'data' => $dataCartList]);
            }
        }
        return response()->json(['success' => $flag_check, 'data' => '']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * update number cart
     */
    public function changeNumberCart(Request $request)
    {
        $keyItem = $request->input('keyItem', 0);
        $keyProduct = $request->input('keyProduct', 0);
        $value = $request->input('value', 0);
        $cartList = updateSession($request, $keyItem, $keyProduct, 'quantity', $value);
        if ($cartList) {
            $cartList = $this->addToCartRepository->getFullDataSessionCart($cartList);
            return response()->json(['success' => true, 'data' => $cartList]);
        }
        return response()->json(['success' => false, 'data' => '']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * Booking confirm
     */
    public function confirm(Request $request)
    {
        $dataCartList = array();
        $checkSession = false;
        $request_url = app('router')->getRoutes()->match(app('request')->create(URL::previous()));

        if ($request->session()->has(config('const.session.cart'))) {
            $data_session = $request->session()->get(config('const.session.cart'));
            if (!empty($data_session)) {
                $checkSession = true;
                $dataCartList = $this->addToCartRepository->getFullDataSessionCart($data_session);
            }
        }
        if (!Auth::guard(config('const.guard.frontend'))->check() && $request_url->getName() != 'entry.index' && $request_url->getName() != 'cart.notregistration') {
            return redirect()->route('frontend.login.confirm');
        }
        if (Auth::guard(config('const.guard.frontend'))->check()) {
            $customers = $this->customerRepository->find(Auth::guard(config('const.guard.frontend'))->id());
            $customers = !empty($customers) ? $customers->toArray() : array();
        } else {
            $customers = ($request->session()->has(config('const.session.customer_booking'))) ? $request->session()->get(config('const.session.customer_booking')) : array();
        }
        if ($checkSession) {
            return view('frontend.cart.confirm', compact('dataCartList', 'customers'));
        }

        return redirect(route('cart.index'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * insert Order and Reserve
     */
    public function insertCart(Request $request)
    {
        $customerTel = $request->input('customerTel', 0);
        $customerName = $request->input('customerName', 0);
        $customerCardNo = $request->input('customerCardNo', 0);
        $customerId = $request->input('customerId', 0);
        $flagSend = true;
        if ($request->session()->has(config('const.session.cart'))) {
            $dataSession = $request->session()->get(config('const.session.cart'));
            $dataCartList = $this->addToCartRepository->getFullDataSessionCart($dataSession);
            $reserveData = $this->reserveRepository->insertConfirmBookReserve($dataCartList, $customerName, $customerTel, $customerCardNo);
            if (empty($customerId)){
                if ($request->session()->has(config('const.session.customer_booking'))) {
                    $customers = $request->session()->get(config('const.session.customer_booking'));
                    $customerId = $this->customerRepository->create($customers);
                }
            }
            $customerOrder = $this->customerOrderRepository->insertCustomerOrder($reserveData, $customerId);
            if ($customerOrder) {
                foreach ($customerOrder as $order) {
                    $shopInfo = $this->shopRepository->getShopInfoComplete($order['shop_code']);
                    $order['shopTel'] = !empty($shopInfo['tel']) ? $shopInfo['tel'] : '';
                    $order['shopName'] = !empty($shopInfo['name']) ? $shopInfo['name'] : '';
                    $order['address'] = !empty($shopInfo['address']) ? $shopInfo['address'] : '';
                    $order['cityName'] = !empty($shopInfo['city_id']) ? $this->citiesRepository->findByAttribute('id', $shopInfo['city_id'], 'name') : "";
                    $sendMail = $this->sendMailRepository->sendMail($request, $order, 'complete');
                    if (!$sendMail) {
                        $flagSend = false;
                    }
                }
                if (!empty($customers['password'])) {
                    Auth::guard(config('const.guard.frontend'))->loginUsingId($customerId);
                }
                return response()->json(['success' => true, 'data' => $flagSend]);
            }
        }
        return response()->json(['success' => true, 'data' => '']);
    }

    public function complete(Request $request)
    {
        $request_url = app('router')->getRoutes()->match(app('request')->create(URL::previous()));
        if ($request_url->getName() == 'cart.confirm') {
            Log::emergency('--------------------LOG CART COMPLETE-------------------------');
            if ($request->session()->has(config('const.session.cart'))) {
                $dataSession = $request->session()->get(config('const.session.cart'));
                $dataCartList = $this->addToCartRepository->getFullDataSessionCart($dataSession);
                if (!empty($dataCartList)) {
                    foreach ($dataCartList as $key => $data) {
                        $shopInfo = $this->shopRepository->getShopInfoComplete($data['shopCode']);
                        $dataCartList[$key]['latlng'] = !empty($shopInfo['latlng']) ? $this->shopRepository->controlGeometryData($shopInfo['latlng']) : array('lat' => '', 'lng' => '');
                        $dataCartList[$key]['shopTel'] = !empty($shopInfo['tel']) ? $shopInfo['tel'] : '';
                        $dataCartList[$key]['address'] = !empty($shopInfo['address']) ? $shopInfo['address'] : '';
                        $dataCartList[$key]['cityName'] = !empty($shopInfo['city_id']) ? $this->citiesRepository->findByAttribute('id', $shopInfo['city_id'], 'name') : "";
                        foreach ($data['itemList'] as $keyItem => $item) {
                            if (empty($dataCartList[$key]['slipNumber'])) {
                                $dataCartList[$key]['slipNumber'] = $this->reserveRepository->getSlipNumber($item['shop_code'], $data['eventCode'], $item['code'], $data['timeReceive']);
                            }
                        }
                    }
                    Log::emergency('The data cart confirm: ');
                    Log::info('DATA_CART_LIST', $dataCartList);
                    if (Auth::guard(config('const.guard.frontend'))->check()) {
                        $customers = $this->customerRepository->find(Auth::guard(config('const.guard.frontend'))->id());
                        $customers = !empty($customers) ? $customers->toArray() : array();
                    } else {
                        $customers = ($request->session()->has(config('const.session.customer_booking'))) ? $request->session()->get(config('const.session.customer_booking')) : array();
                    }
                    Log::emergency('The customer complete: ');
                    Log::info('CUSTOMER INFO', !empty($customers) ? $customers : array());
                    deleteSession($request);
                    return view('frontend.cart.complete', compact('dataCartList', 'customers'));
                }
                Log::error('ERROR', 'FORMAT SESSION FAIL: ' . $dataCartList);
            };
        }
        return redirect(route('site.index'));
    }

    public function notRegistration(Request $request)
    {
        $request_previous = app('router')->getRoutes()->match(app('request')->create(URL::previous()));
        if ($request_previous->getName() == 'cart.confirm' || $request_previous->getName() == 'frontend.login.confirm' || $request_previous->getName() == 'cart.notregistration') {
            $customer = [];
            if ($request->session()->has(config('const.session.customer_booking'))) {
                $customer = $request->session()->get(config('const.session.customer_booking'));
                if (!empty($customer['card_no'])) {
                    $customer['card_no'] = explode('-', formatCardNo($customer['card_no']));
                }
            }
            $dataSession = $request->session()->get(config('const.session.cart'));
            if(!isset($dataSession)){
                return redirect()->route('frontend.login');
            }
            $dataCartList = $this->addToCartRepository->getFullDataSessionCart($dataSession);
            return view('frontend.cart.notregistration',compact('dataCartList','customer'));

        }
        return redirect()->route('site.index');
    }

    public function notRegistrationAction(NotRegistrationRequest $request)
    {
        if ($request->post()) {
            $input = $request->post();
            $customers = array(
                'email' => $input['email'],
                'name' => $input['name'],
                'tel' => $input['tel'],
                'card_no' => implode($input['card']),
                'password' => NULL,
                'del_flag' => config('const.delete_off'),
                'ins_datetime' => Carbon::now(),
                'ins_id' => 1
            );
            if ($request->session()->has(config('const.session.customer_booking'))) {
                $request->session()->pull(config('const.session.customer_booking'));
            }
            $request->session()->put(config('const.session.customer_booking'), $customers);
            return redirect(route('cart.confirm'));
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     * Popup confirm remove cart
     */
    public function showPopupRemove(Request $request)
    {
        $dataKeyItem = $request->input('dataKeyItem', 0);
        $dataKeyProduct = $request->input('dataKeyProduct', 0);
        $boxList = $request->input('boxList', false);
        if (!empty($dataKeyItem) && !empty($dataKeyProduct)) {
            $view = view("frontend.elements.popup_cart_remove",
                compact('dataKeyItem', 'dataKeyProduct', 'boxList'))->render();
            return response()->json(['success' => true, 'data' => $view]);
        }
        return response()->json(['success' => false, 'data' => '']);
    }

    public static function updateDataRequired($request, $dataRequired)
    {
        $dataCartList = [];
        if (!empty($dataRequired) && is_array($dataRequired)) {
            foreach ($dataRequired as $data) {
                $dataCartList = updateSession($request, $data['keyItem'], $data['keyProduct'], 'requiredFlag', $data['value']);
                if (!$dataCartList) {
                    return response()->json(['success' => false, 'data' => '']);
                }
            }
        }
        if (!empty($dataCartList) && is_array($dataCartList)) {
            foreach ($dataCartList as $key => $data) {
                foreach ($data['itemList'] as $key_item => $item) {
                    if (!empty($item['requiredConfirmFlag'])) {
                        if ($item['requiredConfirmFlag'] != "0") {
                            $keyCheck = $item['itemCode'] . '_' . $data['shopCode'] . '_' . $item['requiredConfirmFlag'] . '_' . $item['requiredFlag'] . '_key';
                            if ($key_item != $keyCheck) {
                                if (empty($dataCartList[$key]['itemList'][$keyCheck])) {
                                    $dataCartList[$key]['itemList'][$keyCheck] = $item;
                                } else {
                                    $dataCartList[$key]['itemList'][$keyCheck]['quantity'] += (int)$item['quantity'];
                                }
                                unset($dataCartList[$key]['itemList'][$key_item]);
                            }
                        }
                    }
                }
            }
        }
        if ($request->session()->has(config('const.session.cart')) && !empty($dataCartList)) {
            $request->session()->pull(config('const.session.cart'));
            $request->session()->put(config('const.session.cart'), $dataCartList);
        }
        return $dataCartList;
    }
}

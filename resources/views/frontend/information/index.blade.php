@extends('frontend.layouts.main')
@section('css')
    <link rel="stylesheet" href="{{asset('frontend/css/static.css')}}">
@endsection
@section('content')
    <div id="information-detail" class="password-wrap">
        <div class="container">
            <div class="information-detail">

                <div class="title-page">
                    <div class="title">お知らせ</div>
                    <div class="content">
                        <div class="time">{{ !empty($data->public_start_date) ? date('Y/m/d', strtotime($data->public_start_date)) : $data->public_start_date}}</div>
                        <div class="text">
                            <span class="title-info">{{$data->title}}</span><br><br>
                            {!! nl2br($data->content) !!}
                        </div>
                    </div>
                </div>
                <div class="link"><a href="{{route('site.index')}}">TOPページへ戻る</a></div>
            </div>
        </div>

    </div>
@endsection
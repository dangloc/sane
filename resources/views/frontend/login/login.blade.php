@extends('frontend.layouts.main')
@section('content')
    <!--content detail-->
    <div id="login-wrap">

        <div class="container">
            <div class="login">
                <!--Start - Main Content-->
                <div class="main-content">
                    <div class="row">
                        <div class="offset-lg-3 col-xs-12 col-lg-6 login-left-content">
                            <div class="title-page">
                                <h1 class="title"><i class="fas fa-sign-in-alt"></i>ログイン</h1>
                            </div>
                            <div class="login-form bg-gray-box">
                                <h3>
                                    会員登録がお済みの方は、<br>
                                    下記の情報を入力してログインしてください。
                                </h3>
                                <div class="form-content bg-content-box">
                                    <form id="login" method="POST" action="{{route('authenticate_login')}}" novalidate>
                                        @csrf
                                        <div class="group-input email-check">
                                            <label>メールアドレス</label>
                                            <input type="text" name="email" value= "{{ old('email')}}" placeholder="半角でご入力ください">
                                            @include('frontend.elements.validate_error',
                                            ['message' => !empty($errors->first('email')) ?$errors->first('email') : (!empty($errors->first('login_id')) ?$errors->first('login_id') :"")])
                                        </div>
                                        <div class="group-input">
                                            <label>パスワード</label>
                                            <input type="password" name="password" value= "{{old('password')}}" placeholder="パスワードを入力">
                                            @include('frontend.elements.validate_error', ['message' => !empty($errors->first('password')) ?$errors->first('password') :""])
                                        </div>
                                        <div class="group-input">
                                            <button type="submit"><i class="fas fa-sign-in-alt"></i>ログイン</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="forget-password">
                                    <a href="{{route('site.password_reminder')}}">パスワードをお忘れの方はこちら</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('js')
    <script type="text/javascript">
        $('.shopping-cart-fix-bottom').css('display','none');
    </script>
@endsection

@extends('backend.layouts.main')
@section('content')
    <div id="Contentwrap">
        <div id="PageContent">
            <h2><i class="fas fa-search"></i> 行事コードマスター検索</h2>
            <form action="{{route('event.index')}}" method="get">
                <div class="order">
                    <table class="sp_block">
                        <tr>
                            <th scope="row" style="width: 150px;">行事コード</th>
                            <td colspan="4">
                                <div class="inline_block rel sp-w45 line">
                                    <input type="text" placeholder="行事コード" tabindex="5" name="code" value="{{isset($params['code']) ? $params['code'] : ''}}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row" style="width: 150px;">行事名</th>
                            <td colspan="4">
                                <div class="inline_block rel sp-w45 full_input">
                                    <input type="text" placeholder="行事名" tabindex="5" name="name" value="{{isset($params['name']) ? $params['name'] : ''}}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row" style="width: 150px;">期間</th>
                            <td colspan="4">
                                <div class="inline_block rel">
                                    <input type="text" id="start_date" placeholder="例：2019/01/02" autocomplete="off" tabindex="5" name="start_date" value="{{isset($params['start_date']) ? $params['start_date'] : ''}}">
                                </div>
                                <br class="sp">&nbsp;〜&nbsp;<br class="sp">
                                <div class="inline_block rel">
                                    <input type="text"  id="end_date" placeholder="例：2019/01/03" autocomplete="off" tabindex="6" name="end_date" value="{{isset($params['end_date']) ? $params['end_date'] : ''}}">
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="contentfoot">
                    <p class="btn Searchclear">
                        <button type="button" value="" onclick="System.redirectLoading('{{ route('event.index') }}')">検索条件をクリア</button>
                    </p>
                    <p class="btn Search">
                        <button type="submit" value="" onclick="System.showLoading()" tabindex="13"><img src="{{ asset('backend/images/common/icon3.png') }}" alt="検索">検索</button>
                    </p>
                </div>
            </form>
            <!--ContentFoot -->
        </div>
        <!--PageContent -->

        <div id="PageContent" class="pt60">
            <h2><i class="fas fa-shapes"></i> 行事コードマスター一覧</h2>

            <div class="PageContentNav">
                @include('backend.elements.paging', ['results' => $listData])
                {{ $listData->appends(request()->input())->links() }}
            </div>
            <table class="OrderForm responsive tdcenter Bordertd TextL table-hover">
                <tr>
                    <th scope="col" width="100px">@include("backend.elements.sortable", ["field" => "code", "title" => "行事コード"])</th>
                    <th scope="col">@include("backend.elements.sortable", ["field" => "name", "title" => "行事名"])</th>
                    <th scope="col" width="130px">@include("backend.elements.sortable", ["field" => "start_date", "title" => "開始日"])</th>
                    <th scope="col" width="130px">@include("backend.elements.sortable", ["field" => "end_date", "title" => "終了日"])</th>
                </tr>
                @if(!$listData->isEmpty())
                    @foreach($listData as $item)
                        <tr>
                            <td>{{$item->code}}</td>
                            <td style="text-align:left;">{{$item->name}}</td>
                            <td style="text-align:center;">{{convertCharToDate($item->start_date)}}</td>
                            <td style="text-align:center;">{{convertCharToDate($item->end_date)}}</td>
                        </tr>
                    @endforeach
                @else
                    @include('backend.elements.no_data',['col' => 4])
                @endif
            </table>
            <div class="PageContentNav">
                {{ $listData->appends(request()->input())->links() }}
            </div>
        </div>
        <!--PageContent -->
    </div>
    <!--Contentwrap -->
@endsection
@section('js')
    <script>
        $(document).ready(function() {
            System.setRangeDate($('#start_date'), $('#end_date'));
        });
    </script>
@endsection
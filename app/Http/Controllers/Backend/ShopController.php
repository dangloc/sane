<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\BackendController;
use App\Http\Requests\ShopCreateRequest;
use App\Repositories\AreasRepository;
use App\Repositories\CitiesRepository;
use App\Repositories\ShopsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShopController extends BackendController
{
    protected $shopRepository;
    protected $areaRepository;
    protected $cityRepository;

    public function __construct(ShopsRepository $shopRepository, AreasRepository $areaRepository, CitiesRepository $cityRepository)
    {
        $this->shopRepository = $shopRepository;
        $this->areaRepository = $areaRepository;
        $this->cityRepository = $cityRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $areas = $this->areaRepository->all();
        $cities = [];
        if (null != $request->get('area_id')){
            $cities = $this->cityRepository->getCityByAreas($request->get('area_id'));
        }
        $dataDefault = ['area_id' => null,'city_id' => null,'name' => null];
        $dataGetValue = $request->only('area_id','city_id','name');
        $dataSearch = array_merge($dataDefault,$dataGetValue);
        $listData = $this->shopRepository->searchCondition($dataSearch);
        $request->flash();
        return view('backend.shop.index',compact('listData','cities','areas'));
    }
    public function getCityTest(Request $request){
        if($request->ajax()){
            $idAread = $request->id;
            $cities = $this->areaRepository->getCity($idAread);
            return response()->json(['html' => view('backend.elements.getCity')->with(['cities'=>$cities])->render()]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        request()->session()->put('session_data', []);
        $areas = $this->areaRepository->all();
        $cities = [];
        if (null != $request->old('area_id')){
            $cities = $this->areaRepository->getCity($request->old('area_id'));
        }
        return view('backend.shop.create')
            ->with([
                'areas' => $areas,
                'cities' => $cities,
                'area_id' => $request->old('area_id'),
                'city_id' => $request->old('city_id')
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$request->session()->has('session_data')){
            return redirect()->route('shop.index')->with('error', config('messages.SYSTEM_ERROR'));
        }
        $data = $request->session()->get('session_data');
        $request->session()->forget('session_data');
        try {
            $this->shopRepository->create($data);
            return redirect()->route('shop.index')->with('success', config('messages.DB_INSERT'));
        } catch (\Exception $e) {
            return redirect()->route('shop.index')->with('error', config('messages.SYSTEM_ERROR'));
        }
    }

    public function confirm(ShopCreateRequest $request, $id = null)
    {
        $request->flash();
        $request->session()->put('session_data', $request->all());
        $area_name = $this->areaRepository->find($request->get('area_id'))->name;
        $city_name = $this->cityRepository->find($request->get('city_id'))->name;
        return view('backend.shop.confirm')->with(
            [
                'area_name' => $area_name,
                'city_name' => $city_name,
                'shop' => $request->all()]);
    }

    public function getCity($area_id) {
        $cities = $this->areaRepository->getCity($area_id);
        return mb_convert_encoding($cities->toArray(), 'UTF-8', 'UTF-8');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $shop = $this->shopRepository->find($id);
        if ($shop == null) {
            return redirect()->route('shop.index')->with('error', config('messages.NO_DATA'));
        } else {
            $shop = $shop->toArray();
        }
        $shop['latlng'] = substr($shop['latlng'], 6, strlen($shop['latlng']) - 7);
        $areas = $this->areaRepository->all();
        request()->session()->put('session_data', $shop);
        $cities = [];
        if (null != $request->old('area_id')){
            $cities = $this->areaRepository->getCity($request->old('area_id'));
        } else {
            $cities = $this->areaRepository->getCity($shop['area_id']);
        }
        return view('backend.shop.create')
            ->with([
                'shop' => $shop,
                'areas' => $areas,
                'cities' => $cities,
                'area_id' => null != $request->old('area_id') ? $request->old('area_id') : $shop['area_id'],
                'city_id' => null != $request->old('city_id') ? $request->old('city_id') : $shop['city_id']
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!$request->session()->has('session_data'))
        {
            return redirect()->route('shop.index')->with('error', config('messages.SYSTEM_ERROR'));
        }
        $data = $request->session()->get('session_data');
        $request->session()->forget('session_data');
        try {
            $this->shopRepository->update($id, $data);
            return redirect()->route('shop.index')->with('success', config('messages.DB_UPDATE'));
        } catch (\Exception $e) {
            return redirect()->route('shop.index')->with('error', config('messages.SYSTEM_ERROR'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->shopRepository->softDelete($id);
            return redirect()->route('shop.index')->with('success', config('messages.DB_DELETE'));
        } catch (\Exception $e) {
            return redirect()->route('shop.index')->with('error', config('messages.SYSTEM_ERROR'));
        }
    }
}

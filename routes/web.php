<?php
use Illuminate\Support\Facades\Route;

Route::get('/tsv', 'TsvController@index')->name('test.tsv');
Route::get('/tsv_all', 'TsvController@index_all')->name('test.tsv_all');
Route::get('/tsv_export', 'TsvExportController@export_tsv')->name('test.tsv_export');

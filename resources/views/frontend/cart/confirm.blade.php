@extends('frontend.layouts.main')
@section('content')
    <!--content detail-->
    <div id="confirm-wrap" class="confirm-wrap">
        <div id="prog" class="container">
            <div class="prog">
                <ul>
                    <li class="nomal"><span>カートの中</span><strong>1</strong></li>
                    <li class="nomal"><span>ログイン・入力</span><strong>2</strong></li>
                    <li class="current"><span>入力内容の確認</span><strong>3</strong></li>
                    <li class="later"><span>購入完了</span><strong>4</strong></li>
                </ul>
            </div>
        </div>
        <div class="container">
            <div class="confirm cart">
                <div class="title-page">
                    <h1 class="title"><i class="fas fa-pencil-alt"></i>マイページ新規登録<span>（入力内容の確認）</span></h1>
                </div>
                <div class="description">
                    <p>ご注文商品</p>
                </div>
                @if (!empty($dataCartList))
                    @foreach($dataCartList as $itemList)
                        <div class="cart-item-content">
                            <div class="cart-list-show">
                                <ul>
                                    @if (!empty($itemList['itemList']))
                                        @foreach($itemList['itemList'] as $data)
                                            <li>
                                                <div class="list-items">

                                                    <div class="image-show left">
                                                            <img src="{{ ($data['item_image_folder_name'] ==null || $data['item_image_folder_name']=='_') ? asset('/frontend/images/item/noimage.jpg')
                                                    : config('const.url_item_image').$data['item_image_folder_name']}}" alt="">
                                                    </div>

                                                    <div class="information-cart right">
                                                        <div class="title-product <?= !empty($data['bonus']) ? 'bonus-fix' : ''?>">
                                                            <h3>{{$data['item_name']}}
                                                                @if($data['required_confirm_flag']!=config('const.not_with'))
                                                                    <label class="required-confirm" style="<?= ($data['requiredFlag'] == '0') ? "display:inline-block" : "display:none"?>">
                                                                        @if($data['required_confirm_flag']== config('const.with_wasabi'))
                                                                            わさび入り @elseif($data['required_confirm_flag']==config('const.with_kamaboko'))
                                                                            かまぼこ赤 @elseif($data['required_confirm_flag']==config('const.with_classic')) 定番
                                                                        @endif
                                                                            <input type="hidden" class="hidden-requiredFlag" value="{{$data['requiredFlag']}}" keyItem ='{{$itemList['keyItem']}}'
                                                                                   keyProduct ='{{$data['keyProduct']}}'>
                                                                    </label>
                                                                    <label class="required-confirm" class="hidden-requiredFlag" style="<?= ($data['requiredFlag'] == '1') ? "display:inline-block" : "display:none"?>" >
                                                                        @if($data['required_confirm_flag']==config('const.with_wasabi'))
                                                                            わさび抜き@elseif($data['required_confirm_flag']==config('const.with_kamaboko'))
                                                                            かまぼこ白 @elseif($data['required_confirm_flag']==config('const.with_classic')) 法事
                                                                        @endif
                                                                            <input type="hidden" value="{{$data['requiredFlag']}}" keyItem ='{{$itemList['keyItem']}}'
                                                                                   keyProduct ='{{$data['keyProduct']}}'>
                                                                    </label>
                                                                @endif
                                                            </h3>
                                                        </div>
                                                        <span class="red-text">{{$data['bonus']}}</span>
                                                        <div class="information-detail">

                                                            <div class="information-text">
                                                                <div class="price price-one-products">
                                                                    <span class="text">価格</span>
                                                                    <span class="number"><span>{{number_format($data['price'])}}</span></span>
                                                                    <span class="unit">円+税</span>
                                                                </div>
                                                                <div class="quantity">
                                                                    <div class="quantity-btn">
                                                                        <label>数量</label>
                                                                        <span class="number-products"><span class="number">{{$data['quantity']}}</span></span>
                                                                        <input type="hidden"  data-keyItem ="{{$itemList['keyItem']}}" data-keyProduct="{{$data['keyProduct']}}"
                                                                               name="stock_{{$data['keyProduct']}}" value="{{$data['quantity']}}">
                                                                    </div>
                                                                </div>
                                                                <div class="price price-total-products">
                                                                    <span class="text">小計</span>
                                                                    <span class="number"><span>{{number_format($data['price'] * $data['quantity'])}}</span></span>
                                                                    <span class="unit">円+税</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <span class="red-text error {{$data['keyProduct']}}"></span>
                                                </div>
                                            </li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>

                            <div class="cart-total">
                                <ul>
                                    <li class="total-products">
                                        <div class="total-products-items">
                                            <label>商品数</label>
                                            <span class="total-number">{{$itemList['total']}}品</span>
                                        </div>
                                    </li>
                                    <li class="total-price">
                                        <div class="price-items">
                                            <label>合計金額</label>
                                            <span class="total-number-price"><span class="number">{{number_format($itemList['totalPrice'])}}</span><span
                                                        class="unit">円+税</span></span>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="information-order tableInfo">
                                <h3>受取り店舗と受取り日</h3>
                                <div class="tableContent">
                                    <table>
                                        <tr>
                                            <td>受取り店舗</td>
                                            <td>{{$itemList['shopName']}}　（営業時間 {{$itemList['shopOpenTime']}}〜{{$itemList['shopCloseTime']}}）</td>
                                        </tr>
                                        <tr>
                                            <td>商品カテゴリ</td>
                                            <td>{{$itemList['eventName']}}</td>
                                        </tr>
                                        <tr>
                                            <td>受取り日</td>
                                            <td>{{$itemList['dateReceive']['year']}}年{{$itemList['dateReceive']['month']}}月{{$itemList['dateReceive']['day']}}日({{dayJapan($itemList['dateReceiveRaw'])}})<span class="date">{{$itemList['timeReceive']}}時</span></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
                @if(!empty($customers))
                    <div class="information-order tableInfo">
                            <h3>お客さま情報</h3>
                            <div class="tableContent">
                                <table>
                                    <tr>
                                        <td>お名前</td>
                                        <td>{{$customers['name']}}</td>
                                    </tr>
                                    <tr>
                                        <td>電話番号</td>
                                        <td>{{$customers['tel']}}</td>
                                    </tr>
                                    <tr>
                                        <td>サンエーカード番号</td>
                                        <td>{{formatCardNo($customers['card_no'])}}</td>
                                    </tr>
                                </table>
                            </div>
                            <input class="customer-cart" type="hidden" value="{{$customers['name']}}" name="customer-name">
                            <input class="customer-cart" type="hidden" value="{{$customers['tel']}}" name="customer-tel">
                            <input class="customer-cart" type="hidden" value="{{$customers['card_no']}}" name="customer-card-no">
                        @if(!empty($customers['id']))
                            <input class="customer-id" type="hidden" value="{{$customers['id']}}" name="customer-id">
                        @endif
                        </div>
                @endif
                <div class="group-btn-bottom">
                    <div class="back-to-page">
                        @if(\Illuminate\Support\Facades\Auth::guard('customers')->check())
                            <a href="{{route('cart.index')}}" class="back-step"><i class="fas fa-chevron-left"></i>前の画面に戻る</a>
                        @else
                            <a href="{{ URL::previous() }}" class="back-step"><i class="fas fa-chevron-left"></i>前の画面に戻る</a>
                        @endif
                        <div class="groupBtnText">
                            <a class="go-order-process" href="javascript:Frontend.checkProduct('complete')">注文を確定する  <img class="lazy-load" src="{{asset('frontend/images/ajax-loader.gif')}}"></a>
                            <p class="text-feedback">注文後も3日前までならキャンセルが可能です。</p>
                        </div>
                    </div>
                </div>


            </div>
        </div>

    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $('.shopping-cart-fix-bottom').css('display','none');
    </script>
@endsection

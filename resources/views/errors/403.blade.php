@if(strlen(strstr(url()->current(), 'management')) > 0)
    @include('errors.backend.403')
@else
    @include('errors.frontend.403')
@endif

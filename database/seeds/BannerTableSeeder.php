<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BannerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('banners')->insert([
            [
                'banner_url' => 'uploads/banner1.png',
                'link_url' => 'https://theculturetrip.com/asia/japan/articles/the-10-best-traditional-japanese-dishes/',
                'public_start_date' => date('Y-m-d'),
                'public_end_date' => date('Y-m-d', strtotime(' +3 day')),
                'status' => 1,
                'del_flag' => 0,
                'ins_datetime' => date('Y-m-d'),
                'ins_id' => 1,
                'upd_datetime' => date('Y-m-d'),
                'upd_id' => 1,
            ],
            [
                'banner_url' => 'uploads/banner2.png',
                'link_url' => 'https://theculturetrip.com/asia/japan/articles/the-10-best-traditional-japanese-dishes/',
                'public_start_date' => date('Y-m-d'),
                'public_end_date' => date('Y-m-d', strtotime(' +3 day')),
                'status' => 1,
                'del_flag' => 0,
                'ins_datetime' => date('Y-m-d'),
                'ins_id' => 1,
                'upd_datetime' => date('Y-m-d'),
                'upd_id' => 1,
            ],
            [
                'banner_url' => 'uploads/banner3.png',
                'link_url' => 'https://theculturetrip.com/asia/japan/articles/the-10-best-traditional-japanese-dishes/',
                'public_start_date' => date('Y-m-d'),
                'public_end_date' => date('Y-m-d', strtotime(' +3 day')),
                'status' => 1,
                'del_flag' => 0,
                'ins_datetime' => date('Y-m-d'),
                'ins_id' => 1,
                'upd_datetime' => date('Y-m-d'),
                'upd_id' => 1,
            ]
        ]);
    }
}

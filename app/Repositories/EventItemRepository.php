<?php

namespace App\Repositories;


use Illuminate\Http\Request;

/**
 * class EventItemRepository.
 *
 * @package namespace App\Repositories;
 */
class EventItemRepository extends BaseRepository
{
    function __construct()
    {
        $this->_model = \App\Models\EventItemView::class;
    }

    public function paging()
    {
        $sort = request()->get('sort', 'event_code');
        $direction = request()->get('direction', 'asc');
        $params = request()->all();

        return $this->getModel()
            ->when(isset($params['event_code']) && $params['event_code'] != '', function($q) use ($params) {
                return $q->where('event_code', 'like', '%' . $params['event_code'] . '%');
            })
            ->when(isset($params['category_code']) && $params['category_code'] != '', function($q) use ($params) {
                return $q->where('category_code', 'like', '%' . $params['category_code'] . '%');
            })
            ->when(isset($params['item_code']) && $params['item_code'] != '', function($q) use ($params) {
                return $q->where('item_code', 'like', '%' . $params['item_code'] . '%');
            })
            // refix issuse 5680
            ->when(isset($params['receptionist_start_date']) && $params['receptionist_start_date'] != '', function ($q) use ($params) {
                return $q->where('receptionist_end_date', '>=',   substr(date('Ymd', strtotime($params['receptionist_start_date'])),2));
            })
            ->when(isset($params['receptionist_end_date']) && $params['receptionist_end_date'] != '', function ($q) use ($params) {
                return $q->where('receptionist_start_date', '<=', substr(date('Ymd', strtotime($params['receptionist_end_date'])),2));
            })
            ->when(isset($params['receipt_start_date']) && $params['receipt_start_date'] != '', function ($q) use ($params) {
                return $q->where('receipt_end_date', '>=', substr(date('Ymd', strtotime($params['receipt_start_date'])),2));
            })
            ->when(isset($params['receipt_end_date']) && $params['receipt_end_date'] != '', function ($q) use ($params) {
                return $q->where('receipt_start_date', '<=', substr(date('Ymd', strtotime($params['receipt_end_date'])),2));
            })
            ->orderBy($sort, $direction)
            ->paginate(config('const.paging.backend'));
    }
}

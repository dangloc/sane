<?php
return [
    'paging' => array(
        'backend' => 20,
    ),
    'batch' => array(
        'number_of_record' => 1000
    ),
    'delete_on' => 1,
    'delete_off' => 0,
    'guard' => array(
        'frontend' => 'customers',
        'backend' => 'backend',
    ),
    'not_with'=>0,
    'with_wasabi' => 1,
    'with_kamaboko' => 2,
    'with_classic' => 3,
    'GOOGLE_API_KEY' =>'AIzaSyBc_z85PWzLEimF7jVXDGeJV6JmOB5JIaA',
    'session' => array(
        'event_code' => 'session_event_code',
        'shop_code' => 'session_shop_code',
        'category_code' => 'session_category_code',
        'date_receive' => 'session_date_receive',
        'time' => 'session_time',
        'area_id' => 'session_area_id',
        'city_id' => 'session_city_id',
        'cart' => 'session_cart',
        'date_receive_raw' => 'session_date_receive_raw',
        'search_shop_text' => 'session_search_shop_text',
        'customer_booking' => 'customer_booking'
    ),
    'status_order'=>array(
        'booking'=>0,
        'customer_cancel'=>1,
        'shop_change_confirm'=>2,
        'shop_cancel'=>3
    ),
    'update_on'=>1,
    'send_status_on'=>0,
    'storage' => array(
        'path_barcode' => 'app/public/barcode/'
    ),
    'status_banner' => array(
        '0' => '無効',
        '1' => '有効'
    ),
    'flag' => array(
        'from_confirm' => 1,
        'from_new' => 0
    ),
    'status_customer_order' => array(
        0 => '通常',
        1 => 'WEBキャンセル',
        2 => '店頭変更',
        3 => '店頭キャンセル'
    ),
    'url_item_image' =>'https://yoyaku-photo.san-a.co.jp/',
    'status' => array(
        'enable' => 1,
        'disable' => 0,
    ),
    'stock' => array(
        'not_exist_product' => 'はご指定の受取り店舗または受取り日でのお受取りができない商品です。 カートから削除してください。',
        'stock_extra_start' => 'この商品のご入力いただいた数量の総計が在庫の数量を超えています。',
        'stock_extra_end' => '以下ご入力ください。',
        'stock_required' => 'この商品は在庫がございません。カートから削除してください。'
    ),
];

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class EventItemViewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('event_item_views')->truncate();
       DB::table('event_item_views')->insert([
           [
               'event_code'=>'A',
               'category_code'=>'123',
               'item_code'=>'0010',
               'sort'=>1,
               'receptionist_start_date'=>'190509',
               'receptionist_end_date'=>'190510',
               'receipt_start_date'=>'190512',
               'receipt_end_date'=>'190515',
               'ins_id'=>1,
               'ins_datetime'=>Carbon::now()
           ],
           [
               'event_code'=>'A',
               'category_code'=>'123',
               'item_code'=>'0011',
               'sort'=>1,
               'receptionist_start_date'=>'190509',
               'receptionist_end_date'=>'190510',
               'receipt_start_date'=>'190512',
               'receipt_end_date'=>'190515',
               'ins_id'=>1,
               'ins_datetime'=>Carbon::now()
           ],
           [
               'event_code'=>'A',
               'category_code'=>'123',
               'item_code'=>'0012',
               'sort'=>1,
               'receptionist_start_date'=>'190509',
               'receptionist_end_date'=>'190510',
               'receipt_start_date'=>'190512',
               'receipt_end_date'=>'190515',
               'ins_id'=>1,
               'ins_datetime'=>Carbon::now()
           ]
       ]);
    }
}

@extends('backend.layouts.main')
@section('css')
    <link rel="stylesheet" href="/frontend/css/error.css">
@endsection
@section('content')
    <div id="Contentwrap">
        <div class="color-error message-notification">
            ページの有効期限が切れています。
        </div>
    </div>
@endsection
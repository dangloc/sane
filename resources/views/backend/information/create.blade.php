@extends('backend.layouts.main')
@section('content')
    <div id="Contentwrap">
        <form method="post"
              action="{{ array_key_exists('id', Session::get('session_data')) ? route('information.edit.confirm', $information['id']) : route('information.create.confirm')}}">
            {{ csrf_field() }}
            <div class="contenttop detail-btn-group">
                <p class="btn Searchclear">
                    <button type="button" onclick="System.redirectNoLoading('{{route('information.index')}}')"><i class="fas fa-list"></i>
                        一覧ページに戻る
                    </button>
                </p>
            </div>

            <div id="PageContent" class="no_mar_bottom">
                <h2><i class="fas fa-tag"></i> お知らせ{{ array_key_exists('id', Session::get('session_data')) ? '編集' : '登録'}}</h2>
                @if(array_key_exists('id', Session::get('session_data')))
                    <input style="display: none" name="id" value="{{$information['id']}}">
                @endif
                <div class="order">
                    <table class="sp_block flowtable">
                        <tr>
                            <th scope="row" style="width: 150px;">タイトル<span class="req"></span></th>
                            <td>
                                <div class="line required full_input">
                                    <input type="text" tabindex="7" placeholder="タイトル" name="title" value="{{ old('title', isset($information) ? $information['title'] : '') }}">
                                </div>
                                @include('backend.elements.validate_error', ['message' => $errors->first('title')])
                            </td>
                        </tr>
                        <tr>
                            <th scope="row" style="width: 150px;">本文<span class="req"></span></th>
                            <td>
                                <textarea name="content" cols="40">{{ old('content', isset($information) ? $information['content'] : '') }}</textarea>
                                @include('backend.elements.validate_error', ['message' => $errors->first('content')])
                            </td>
                        </tr>
                        <tr>
                            <th scope="row" style="width: 150px;">公開期間<span class="req"></span></th>
                            <td colspan="4">
                                <div class="line required">
                                    <div class="inline_block rel">
                                        <input type="text" id="start_date" name="public_start_date" placeholder="例：2019/01/02" tabindex="5" autocomplete="off"></div>
                                    @include('backend.elements.hourselection',['id'=>'public_start_time','name'=>'public_start_time','hourSelected' => old('public_start_time', isset($information) ? date('Hi',strtotime($information['public_start_time'])) : '')])

                                    <br class="sp">&nbsp;〜&nbsp;<br class="sp">
                                    <div class="inline_block rel">
                                        <input type="text" id="end_date" name="public_end_date" placeholder="例：2019/01/02" tabindex="6" autocomplete="off">
                                    </div>
                                    @include('backend.elements.hourselection',['id'=>'public_end_time','name'=>'public_end_time','hourSelected' => old('public_end_time', isset($information) ? date('Hi',strtotime($information['public_end_time'])) : '')])
                                </div>
                                @include('backend.elements.validate_error', ['message' => $errors->first('public_start_date')])
                                @include('backend.elements.validate_error', ['message' => $errors->first('public_start_time')])
                                @include('backend.elements.validate_error', ['message' => $errors->first('public_end_date')])
                                @include('backend.elements.validate_error', ['message' => $errors->first('public_end_time')])
                            </td>
                        </tr>
                    </table>
                </div>
                <!--ContentFoot -->
            </div>
            <!--PageContent -->

            <div class="contentfoot">
                <p class="btn clear">
                    <button type="button" onclick="System.redirectNoLoading('{{route('information.index')}}')"><i class="fas fa-list"></i>
                        一覧ページに戻る
                    </button>
                </p>
                <p class="btn register">
                    <button type="submit" onclick="System.showLoading()"><i
                            class="fas fa-check"></i> 確認する
                    </button>
                </p>
            </div>
            <!--PageContent -->
        </form>
    </div>
    <!--Contentwrap -->
@endsection
@section('js')
    <script>
        $(document).ready(function () {
            System.setRangeDate($('#start_date'), $('#end_date'));
            $('#start_date').datepicker('setDate', '{{ $start_date_wrong_format ? '' : (null !== old('public_start_date') ? old('public_start_date') : (null == old('public_start_date') && isset($information) ? date_format(date_create($information['public_start_date']),"Y/m/d") : '')) }}');
            $('#end_date').datepicker('setDate', '{{ $end_date_wrong_format ? '' : (null !== old('public_end_date') ? old('public_end_date') : (null == old('public_end_date') && isset($information) ? date_format(date_create($information['public_end_date']),"Y/m/d") : '')) }}');
        });
    </script>
@endsection
<?php

namespace App\Console\Commands;

use App\Jobs\exportTsv;
use Illuminate\Console\Command;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Http\Common\logWriter;

class export_tsv extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'batch:export_tsv';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Batch 13 , export tsv file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set("memory_limit", "512M");
        $job = new exportTsv();
        $job->handle();
    }
}

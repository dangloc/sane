<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 6/6/2019
 * Time: 9:56 AM
 */

namespace App\Http\ViewComposers;
use Illuminate\View\View;


class MovieComposer
{
    protected $list = null;
    public function __construct()
    {
        $this->list = [1,2,3,4,5];
    }
    public function compose(View $view)
    {
        $view->with('list', $this->list);
    }
}
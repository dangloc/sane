<?php

namespace App\Repositories;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use phpDocumentor\Reflection\Types\Integer;
use \Datetime;

class BaseRepository
{
    protected $_model;

    public function getModel()
    {
        return app()->make($this->_model);
    }

    public function all()
    {
        return $this->getModel()->all();
    }

    public function create($data)
    {
        $data['ins_datetime'] = Carbon::now();
        $data['ins_id'] = Auth::id();
        $object = new $this->_model($data);
        $object->save();
        return $object->id;
    }

    public function update($id, $data)
    {
        foreach ($data as $field => $value) {
            if (!in_array($field, $this->getModel()->getFillable())) {
                unset($data[$field]);
            }
        }
        $data['upd_datetime'] = Carbon::now();
        $data['upd_id'] = Auth::id();
        $object = $this->_model::find($id);
        foreach ($data as $field => $value) {
            if ($field != 'id' && in_array($field, $this->getModel()->getFillable())) {
                $object->$field = $value;
            }
        }
        $object->save();
        return true;
    }

    public function find($id)
    {
        return $this->getModel()->find($id);
    }

    public function softDelete($id)
    {
        $upd_date = Carbon::now();
        $upd_id = Auth::id();
        if (gettype($id) !== 'array'){
            $id = [$id];
        }
        return $this->getModel()::whereIn('id', $id)->update([
            'del_flag' => config('const.delete_on'),
            'upd_datetime' => $upd_date,
            'upd_id' => $upd_id
        ]);
    }

    /**
     * @param $attribute
     * @param $value
     * @param $attribute_return
     * @return string
     * Tim kiem attribute
     */
    public function findByAttribute($attribute, $value, $attribute_return)
    {
        $data = "";
        if (!empty($attribute) && !empty($value) && !empty($attribute_return)) {
            $data = $this->getModel()->select($attribute_return)->where($attribute, '=', $value)->first();
            if (!empty($data)){
                $data = $data->toArray();
                if (!empty($data[$attribute_return])) {
                    return $data[$attribute_return];
                }
            }
        }

        return $data;
    }

    function validateDate($date, $format = 'Y-m-d')
    {
        $d = DateTime::createFromFormat($format, $date);
        // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
        return $d && $d->format($format) === $date;
    }

    // number_of_record = number of record will be inserted in 1 query, if null, all record will be inserted into 1 query.
    function createMulti($array, $number_of_record=null){
        if (sizeof($array) > 0) {
            if (null == $number_of_record) {
                $number_of_record = sizeof($array);
            }
            $array_split = array_chunk($array, $number_of_record);
            foreach ($array_split as $value) {
                $this->getModel()->insert($value);
            }
        }
    }
    // $table = table name in database
    // $values_array = data will be updated . Ex : [ ['field1' => 'value1', 'field2' => 'value2;] , ['field1' => 'value3', 'field2' => 'value4']]
    // $keys_array = list keys of table, only accept array . Ex : ['field1']
    // $fields_array = list field of table in $values_array ( all fields will be updated )
    // number_of_record = number of record will be updated in 1 query, if null, all record will be updated into 1 query.
    function updateMulti($table, $values_array, $keys_array, $fields_array, $number_of_record=null){
        if (sizeof($values_array) > 0) {
            if (null == $number_of_record) {
                $number_of_record = sizeof($values_array);
            }
            $values_split = array_chunk($values_array, $number_of_record);
            $update_id = Auth::id();
            foreach ($values_split as $value) {
                $keys = implode(',', $keys_array);
                $update_query = "UPDATE {$table} SET ";
                $array_key_value_tail = [];
                foreach ($fields_array as $field) {
                    if (!in_array($field, $keys_array)) {
                        $update_query .= $field . " = CASE concat(" . $keys . ")";
                        foreach ($value as $data) {
                            $value_keys = [];
                            foreach ($keys_array as $key) {
                                array_push($value_keys, $data[$key]);
                            }
                            $field_value = "'" . $data[$field] . "'";
                            $update_query .= " WHEN concat('" . implode("','", $value_keys) . "') THEN " . $field_value;
                            array_push($array_key_value_tail, "concat('" . implode("','", $value_keys) . "')");
                        }
                        $update_query .= " ELSE " . $field . " END,";
                    }
                }
                $update_query .= "upd_id = " . $update_id . ", upd_date = CURRENT_TIMESTAMP";
                $update_query .= " WHERE concat(" . $keys . ") IN (" . implode(",", $array_key_value_tail) . ");";
                DB::update($update_query);
            }
        }
    }

    // convert format YYYY/mm/dm -> YYmmdm
    public function convertDateSixChar($date){
        return substr(str_replace("/","",$date),2);
    }
}

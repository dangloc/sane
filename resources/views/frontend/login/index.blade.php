@extends('frontend.layouts.main')
@section('content')
    <!--content detail-->
    <div id="login-wrap">
        <div id="prog" class="container">
            <div class="prog">
                <ul>
                    <li class="nomal"><span>カートの中</span><strong>1</strong></li>
                    <li class="current"><span>ログイン・入力</span><strong>2</strong></li>
                    <li class="later"><span>入力内容の確認</span><strong>3</strong></li>
                    <li class="nomal"><span>購入完了</span><strong>4</strong></li>
                </ul>
            </div>
        </div>
        <div class="container">
            <div class="login">
                <div class="title-page">
                    <h1 class="title"><i class="fas fa-sign-in-alt"></i>ログイン</h1>
                </div>
                <!--Start - Main Content-->
                <div class="main-content">
                    <div class="row">
                        <div class="col-xs-12 col-lg-6 login-left-content">
                            <div class="login-form bg-gray-box">
                                <h3>
                                    会員登録がお済みの方は、<br>
                                    下記の情報を入力してログインしてください。
                                </h3>
                                <div class="form-content bg-content-box">
                                    <form id="login" method="POST" action="{{route('authenticate_login')}}">
                                        @csrf
                                        <div class="group-input">
                                            <label>メールアドレス</label>
                                            <input type="text" name="email" value= "{{ old('email')}}" placeholder="半角でご入力ください">
                                            @include('frontend.elements.validate_error',
                                            ['message' => !empty($errors->first('email')) ?$errors->first('email') : (!empty($errors->first('login_id')) ?$errors->first('login_id') :"")])
                                        </div>
                                        <div class="group-input">
                                            <label>パスワード</label>
                                            <input type="password" name="password" value= "{{old('password')}}" placeholder="パスワードを入力">
                                            @include('frontend.elements.validate_error', ['message' => !empty($errors->first('password')) ?$errors->first('password') :""])
                                        </div>
                                        <input type="hidden"  value="1" name="keyConfirm">
                                        <div class="group-input">
                                            <button type="submit"><i class="fas fa-sign-in-alt"></i>ログイン</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="forget-password">
                                    <a href="{{route('site.password_reminder')}}">パスワードをお忘れの方はこちら</a>
                                </div>
                            </div>
                            <div class="back-to-page hidden-sp">
                                <button type="button" onclick="location.href='{{url()->previous()}}'"><i class="fas fa-chevron-left"></i>前の画面に戻る</button>
                            </div>
                        </div>

                        <div class="col-xs-12 col-lg-6 login-right-content">
                            <div class="login-text bg-gray-box">
                                <h3>
                                    会員登録がお済みでない方は、<br>
                                    下のボタンからマイページ登録（無料）してください。
                                </h3>
                                <div class="text-content bg-content-box">
                                    <div class="content-middle">
                                        <a href="{{route('entry.index')}}" class="reg-free">新規登録(無料)</a>
                                        <h4><span class="textLeft">マイページ登録でもっと便利に</span><span class="line"></span></h4>
                                        <i class="fas fa-check"></i>
                                        <p>
                                            <strong>キャンセルもマイページから簡単に行えます</strong>
                                            お受取りの3日前までならマイページからキャンセルが可
                                                能です。以降は受取り店舗でお電話にて承ります。
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="content-bottom">
                                <div class="bg-content bg-gray-box">
                                    <div class="title">
                                        <h3>マイページが不要の方は、<br>
                                            下のボタンから購入手続きへお進みください</h3>
                                    </div>
                                    <div class="box bg-content-box">
                                        <a href="{{route('cart.notregistration')}}">登録しないで購入手続き</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End - Main Content-->
                <div class="login-content">
                    <h4><strong>個人情報に関して</strong></h4>
                    <p>
                        入力されました個人情報は、ご本人の承諾なしに第三者（業務委託先を除く）に開示するようなことはいたしません。<br>
                        株式会社サンエーの商品・情報提供のために利用させていただきます。詳しくは <a href="{{route('privacy')}}">プライバシーポリシー </a>をご覧ください。
                    </p>
                </div>

                <div class="back-to-page back-btn-for-sp text-center hidden-sm hidden-md hidden-lg">
                    <button type="button"><i class="fas fa-chevron-left"></i>前の画面に戻る</button>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('js')
    <script type="text/javascript">
        $('.shopping-cart-fix-bottom').css('display','none');
    </script>
@endsection
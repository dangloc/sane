@extends('frontend.layouts.main')
@section('content')
    <div id="password-reminder" class="password-wrap">
        <div class="container">
            <div class="password">

                <div class="title-page">
                    <h1 class="title">パスワードをお忘れになった方へ</h1>
                </div>
                <div class="description">
                    <p class="text">
                        「サンエーネット予約サービス」のパスワードをお忘れになった場合は、再発行が必要となります。<br>
                        以下の項目を入力して「送信する」ボタンをクリックしてください。<br>
                        ご入力頂いたメールアドレスと合致するデータがあった場合、仮パスワードを送信いたします。
                    </p>
                    <p class="text">
                        ※仮パスワードでログイン後、パスワードの変更を行ってください。<br>
                        ※パスワードはお客様の重要な情報であるため、お電話口での案内は行っておりませんのであらかじめご了承ください。
                    </p>
                    <p class="text">
                        ※仮パスワードでログイン後、パスワードの変更を行ってください。<br>
                        ※パスワードはお客様の重要な情報であるため、お電話口での案内は行っておりませんのであらかじめご了承ください。
                    </p>
                </div>

                <div id="formPasswordReissue" class="formPassword">
                    <form method="POST" action="{{route('site.send_mail_password_reissue')}}">
                        @csrf
                        <div class="group-input">
                            <label>メールアドレス<span>必須</span></label>
                            <input name="email" placeholder="半角でご入力ください" value="{{old('email')}}">
                            @include('frontend.elements.validate_error', ['message' => !empty($errors->first('email')) ?$errors->first('email') :''])
                        </div>
                        <div class="group-input text-center">
                            <span class="message-mail">{{$message}}</span>
                            <button type="submit">送信する</button>
                        </div>
                        <p>ご入力内容をご確認の上、「送信する」をクリックしてください。</p>
                    </form>
                    <div class="text-bottom">
                        <p>
                            <strong>エラーが表示されてしまう場合</strong><br>
                            ・メールアドレスは半角英数字でご入力ください。<br>
                            ・ご登録のメールアドレスをご入力ください。
                        </p>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

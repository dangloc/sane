var Frontend = {};
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$(document).ready(function () {
    Frontend.onLoadBoxCart();
});

//LOAD CART BOX
Frontend.onLoadBoxCart = function () {
    $.ajax({
        url: laroute.route('cart.on_load_box_cart'),
        type: "post",
        dataType: "JSON",
        data: {},
        success: function (data) {
            if (data.success === true) {
                $('.shopping-cart-fix-bottom .box-cart').css('display','block');
                $('.basket-content .list-item-block').html(data.data);
                var total_money = $('.sum-price-box-cart').val();
                var total_quantity = $('.sum-quantity-box-cart').val();
                if (total_money !== undefined && total_money != '') {
                    total_money = total_money.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
                }
                if (total_quantity !== undefined && total_quantity != '') {
                    total_quantity = total_quantity.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
                }
                $('.basket-content .text-description-popup-cart p')
                    .text(total_money)
                    .append('<span style="font-size: 12px;margin-left: 5px;">円＋税 </span>');
                $('.total-basket .cart-number-item').text(total_quantity);
                $('#box-hide-quantity').text(total_quantity);
                $('.number-product-order').text(total_quantity);
                if (parseInt(total_quantity) > 0){
                    $('.number-product-order').css('display','inline-block');
                }
            }
        }
    });
};

Frontend.showCartFix = function () {
    $('.shopping-cart-fix-bottom .basket-popup').css('display', 'block');
    $('.shopping-cart-fix-bottom .box-cart').css('display', 'none');
};

Frontend.closeCart = function () {
    $('.shopping-cart-fix-bottom .box-cart').css('display', 'block');
    $('.shopping-cart-fix-bottom .basket-popup').css('display', 'none');
};

Frontend.removeItemCart = function (dataKeyItem , dataKeyProduct, boxList = false) {
    $('.loading-main').css('display', 'block');
    $('#modal-cart-remove').modal('hide');
    //Box cart
    var priceKey = $('#total-price-' + dataKeyItem + '-' + dataKeyProduct);
    var totalPriceAll = $('#total-all-price-cart');
    var quantityKey = $('#quantity-old-' + dataKeyItem + '-' + dataKeyProduct);
    var totalQuantityKey = $('#total-all-quantity-cart');

    //List cart
    var listTotalPrice = $('#total-quantity-price-' + dataKeyItem);
    var listTotalQuantity = $('#total-quantity-' + dataKeyItem + '-' + dataKeyProduct);
    var listCurrentQuantity = $('#quantity-' + dataKeyItem + '-' + dataKeyProduct);

    $.ajax({
        url: laroute.route('cart.remove_item_cart'),
        type: "post",
        dataType: "JSON",
        data: {
            dataKeyItem: dataKeyItem,
            dataKeyProduct: dataKeyProduct
        },
        success: function (data) {
            $('#modal-cart-remove').remove();
            if (data.success === 1) {
                if (boxList) {
                    if (data.data === true) {
                        $('.list-item-block #product-'+dataKeyItem).remove();
                    }
                    $('#product-' + dataKeyItem + '-' + dataKeyProduct).remove();
                    var totalPrice = Frontend.formatToNumber(totalPriceAll.text()) - Frontend.formatToNumber(priceKey.text());
                    var totalQuantity = Frontend.formatToNumber(totalQuantityKey.text()) - Frontend.formatToNumber(quantityKey.val());
                    totalPriceAll.text(Frontend.formatNumber(totalPrice)+' 円＋税');
                    totalQuantityKey.text(Frontend.formatNumber(totalQuantity));
                    $('#box-hide-quantity').text(Frontend.formatNumber(totalQuantity));
                    if (totalQuantity > 0) {
                        $('#number-product-order').text(Frontend.formatNumber(totalQuantity));
                    }else{
                        $('#number-product-order').css('display','none');
                    }
                    if (Frontend.formatNumber(totalQuantity) === '0'){
                        $('.shopping-cart-fix-bottom').css('display', 'none');
                        $('.shopping-cart-fix-bottom .basket-popup').css('display', 'none');
                    }
                } else {
                    var totalPrice = Frontend.formatToNumber(listTotalPrice.text()) - Frontend.formatToNumber(priceKey.text());
                    listTotalPrice.text(Frontend.formatNumber(totalPrice));
                    listTotalQuantity.text(Frontend.formatNumber(Frontend.formatToNumber(listTotalQuantity.text()) - Frontend.formatToNumber(listCurrentQuantity.val()))+'品');
                    if (data.data === true) {
                        $('#product-' + dataKeyItem + '-' + dataKeyProduct).parents('.cart-item-content').first().remove();
                    }else{
                        $('#product-' + dataKeyItem + '-' + dataKeyProduct).remove();
                    }
                    var total = 0;
                    $( ".total-number" ).each(function() {
                        total += Frontend.formatToNumber($(this).text());
                    });
                    $('#number-product-order').text(total);
                    console.log(total);
                    if (total > 0){
                        console.log(total);
                        $('#number-product-order').css('display','inline-block');
                    }else{
                        $('#number-product-order').css('display','none');
                    }
                    if ($('.cart .cart-item-content').length === 0){
                        $('.cart .description').append('<span class="no-data-cart">お客様のカートに商品はありません。</span>');
                        $('.go-order-process').css('display', 'none');
                    }
                }
                toastr.success('カートから削除しました。');
                $('.toast-success').css('background-color', 'red');
                $('.loading-main').css('display', 'none');
            }
        }
    });
};
//TOP -- GEN SHOP MAP
Frontend.genShopMap = function (area_id = 0, city_id = 0, text_shop = '', shop_id = 0, event_code = 0, date_receive = '', time = '', popup = false) {
    $('.step1 .content-search img.lazy-load').css('display', 'inline-block');
    $.ajax({
        url: laroute.route('site.ajax.render_by_map'),
        type: "post",
        dataType: "JSON",
        data: {
            area_id: area_id,
            city_id: city_id,
            text_shop: text_shop
        },
        success: function (result) {
            if (result.success === true) {
                var data = result.data;
                $('.step1 .content-search img.lazy-load').css('display', 'none');
                $('#area-form').val(data.area_id);
                $('#city-form option').remove();  ;
                if (data.cities.length !==0) {
                    $('#city-form').append($('<option>', {value: 0, text: '市町村を選択'}));
                    $.each(data.cities, function (id, value) {
                        if (city_id !== 0 && city_id == value.id) {
                            $('#city-form').append('<option value="' + value.id + '" selected="selected">' + value.name + '</option>');
                        }else{
                            $('#city-form').append($('<option>', {value: value.id, text: value.name}));
                        }
                    });
                }else{
                    $('#city-form').append($('<option>', {value: "", text:"市町村を選択"}));
                }
                var firstLoad = true;
                if (data.shops) {
                    $('.search-shop-body').children().remove();
                    $.each(data.shops, function (id, value) {
                        if (value.code == shop_id) {
                            firstLoad = false;
                            $('.search-shop-body').append('<tr for="shop-' + value.code + '" class="selected"><td class="input" > ' +
                                '<input type="radio" name="shops" class="shops" id="shop-' + value.code + '" value="' + value.code + '" checked>' +
                                '<label for="shop-' + value.code + '" class="checkmark" ></label>\n' +
                                '</td>\n' +
                                '<td class="text">' + value.name + '</td>\n' +
                                '<td class="time">' + Frontend.convertTimeShop(value.open_time) + ' 〜 ' +Frontend.convertTimeShop(value.close_time)+ '</td></tr>');
                        } else {
                            $('.search-shop-body').append('<tr for="shop-' + value.code + '"><td class="input" > ' +
                                '<input type="radio" name="shops" class="shops" id="shop-' + value.code + '" value="' + value.code + '">' +
                                '<label for="shop-' + value.code + '" class="checkmark" ></label>\n' +
                                '</td>\n' +
                                '<td class="text">' + value.name + '</td>\n' +
                                '<td class="time">' + Frontend.convertTimeShop(value.open_time) + ' 〜 ' + Frontend.convertTimeShop(value.close_time) + '</td></tr>');
                        }
                    });
                    $('.search-shop-body tr').click(function (event) {
                        if (event.target.type !== 'radio') {
                            $(':radio', this).trigger('click');
                            $(".search-shop-body tr.selected").removeClass('selected');
                            $(this).addClass('selected');
                            Frontend.eventSelectShop();
                        }
                    });
                    if (data.shops.length === 0){
                        for (var i = 0; i < 7; i++) {
                            $('.search-shop-body').append('<tr for="shop"><td class="input" > ' +
                                '</td>\n' +
                                '<td class="text"></td>\n' +
                                '<td class="time"></td></tr>');
                        }
                    }
                    if (shop_id !== '' && shop_id !== 0) {
                        Frontend.eventSelectShop(firstLoad, event_code, date_receive, time, popup);
                    }
                }

                var width = $('.step1 .searchTime table').width();
                $('.step1 .searchTime table tbody tr td').css('width',((width-5)*2/5));
                $('.step1 .searchTime table tbody tr td.input').css('width',((width-5)/5));
            }
        }
    });
};

Frontend.genCitiesMap = function (areas_id) {
    $('.loading-main').css('display', 'block');
    $.ajax({
        url: laroute.route('site.render_city_popup'),
        type: "post",
        dataType: "JSON",
        data: {
            areas_id: areas_id,
        },
        success: function (data) {
            $('.loading-main').css('display', 'none');
            $('#modal-areas').html(data.html);
            $('#modal-choose-areas').modal('show');
        }
    });
};

Frontend.genShopMapPopup = function (areas_id) {
    var cities = $("#modal-choose-areas .area-form option:selected").val();
    Frontend.genShopMap(areas_id, cities,'',0,0,'','',true);
    $('#modal-choose-areas').modal('hide');
};

//TOP -- GEN DATE RECEIVE
Frontend.showDateReceive = function (id, onLoad = true, date_receive = '', time = '') {
    var start_date = new Date();
    var flag_exit_date = true;
    $(".step2 .group-btn .selected").removeClass('selected');
    $('#' + id).addClass('selected');
    if ($('.date-search').find('#datepicker').length === 0){
        $('.date-search').html(' <input id="datepicker" type="text" placeholder="受取り日を指定する">\n' +
            '<i class="fas fa-calendar-alt"></i>' +
            '<select class="time-receive">\n' +
            '<option value="9">9時</option>\n' +
            '<option value="10">10時</option>\n' +
            '<option value="11">11時</option>\n' +
            '<option value="12">12時</option>\n' +
            '<option value="13">13時</option>\n' +
            '<option value="14">14時</option>\n' +
            '<option value="15">15時</option>\n' +
            '<option value="16">16時</option>\n' +
            '<option value="17">17時</option>\n' +
            '<option value="18">18時</option>\n' +
            '<option value="19">19時</option>\n' +
            '<option value="20">20時</option>\n' +
            '<option value="21">21時</option>\n' +
            '<option value="22">22時</option>\n' +
            '</select>');
        flag_exit_date = false;
    }
    if ($('.date-search').find('span.red-text').length === 0 && onLoad && !flag_exit_date) {
        $("#datepicker").datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'yyyy/mm/dd',
            startDate: start_date,
            language: 'ja',
            disableTouchKeyboard: true
        });
        $('.submit-Form').css('display', 'block');
    } else {
        if (date_receive !=='') {
            var numbers = date_receive.match(/\d+/g);
            date_receive = numbers[2] + '/' + numbers[1] + '/' + numbers[0];
            $("#datepicker").datepicker({
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy/mm/dd',
                startDate: start_date,
                language: 'ja',
                disableTouchKeyboard: true
            }).datepicker("update", date_receive);
            $('.time-receive').val(time);
            $('.submit-Form').css('display', 'block');
            $('.step3 .main-step img.lazy-load').css('display', 'none');
            Frontend.getProduct();
        }
    }

    $('.date-search .fa-calendar-alt').click(function(){
        $('#datepicker').focus();
    });
};

//TOP -- EVENT SELECT SHOP
Frontend.eventSelectShop = function (firstLoad = true, event_code = 0, date_receive = '', time = '', popup =false) {
    if (firstLoad === true) {
        Frontend.callGetEvent(0,'','',popup);
    } else {
        Frontend.callGetEvent(event_code, date_receive, time);
    }
};

Frontend.callGetEvent = function (event_code = 0, date_receive = '', time = '' ,popup = false) {
    if (popup){
        $('.main-step .group-btn').html('<span class="red-text">受取り店舗を選択してください。</span>');
        $('.step3 .date-search').html('<span class="red-text">カテゴリを選択してください。</span>');
    }else {
        $('.step2 .main-step img.lazy-load').css('display', 'inline-block');
        $.ajax({
            url: laroute.route('site.ajax.get_event'),
            type: "post",
            dataType: "JSON",
            data: {},
            success: function (data) {
                $('.step2 .main-step img.lazy-load').css('display', 'none');
                if (data.length > 0) {
                    $('.main-step .group-btn').children().remove();
                    var flag = true;
                    var id_session = 0;
                    $.each(data, function (id, value) {
                        if (value.code === event_code) {
                            flag = false;
                            id_session = "event_" + value.code;
                            $('.main-step .group-btn').append('<button type=button class="btn-choose selected" onclick="Frontend.showDateReceive(this.id, true)" id="event_' + value.code + '" name="events">' +
                                '<i class="fa fa-circle" aria-hidden="true"></i>' + value.name + '</button>');
                        } else {
                            $('.main-step .group-btn').append('<button type=button class="btn-choose" onclick="Frontend.showDateReceive(this.id, true)" id="event_' + value.code + '" name="events">' +
                                '<i class="fa fa-circle" aria-hidden="true"></i>' + value.name + '</button>');
                        }
                        $('.step3 .date-search').html('<span class="red-text">カテゴリを選択してください。</span>');
                        $('.submit-Form').css('display', 'none');
                        $('#list-products').css('display', 'none');
                    });
                    if (!flag) {
                        Frontend.showDateReceive(id_session, flag, date_receive, time);
                    }
                }
            }
        });
    }
};

//TOP -- SHOW LIST PRODUCT
Frontend.getProduct = function (category_code = '') {
    if (category_code !==''){
        if ($('#list-product-cate-'+category_code).hasClass( "category-submit" )){
            $(this).removeClass('category-submit');
            category_code ='';
        }else {
            $('.list-cate-product').removeClass('category-submit');
            $('#list-product-cate-' + category_code).addClass('category-submit');
        }
    }
    $('#list-products').css('display','block');
    $('#list-products img#item-load').css('display', 'inline-block');
    $('.btn_submit img#item-load').css('display', 'inline-block');
    $('.btn_submit button').addClass('active');
    var shop_code = $('.searchTime .selected .shops').val();
    var event_code = $('.step2 .group-btn .selected').attr('id').split("_")[1];
    var date = $('#datepicker').val();
    var time = $('.time-receive').val();
    var area_id = $('#area-form').val();
    var city_id = $('#city-form').val();
    var search_shop_text = $('.selectItem .shop-search').val();
    if (date !=='') {
        $('.required-datesearch').remove();
        $.ajax({
            url: laroute.route('site.ajax.get_product'),
            type: "post",
            dataType: "JSON",
            data: {
                shop_code: shop_code,
                event_code: event_code,
                date: date,
                time: time,
                category_code: category_code,
                search_shop_text: search_shop_text,
                city_id: city_id,
                area_id: area_id
            },
            success: function (data) {
                $('#list-products').html(data.html);
                $('#list-products img#item-load').css('display', 'none');
                $('.btn_submit img#item-load').css('display', 'none');
                $('.btn_submit button').removeClass('active');
            }
        });
    }else{
        $('#list-products').html('');
        if ($('.date-search').find('.required-datesearch').length !== 1) {
            $('.date-search').append(' <span class="red-text required-datesearch">受取り日を選択してください。</span>');
        }
        $('.btn_submit img#item-load').css('display', 'none');
        $('.btn_submit button').removeClass('active');
    }
};

Frontend.checkProduct = function (type = 'confirm') {
    $('.go-order-process').addClass('active');
    $(".go-order-process img.lazy-load").css('display', 'inline-block');
    $('.loading-main').css('display', 'block');

    var dataRequired = [];
    if (type === 'confirm') {
        $(".confirm_checked input[type=radio]").each(function () {
            var item = {};
            item.keyItem = $(this).attr('keyItem');
            item.keyProduct = $(this).attr('keyProduct');
            item.value = $(this).val();
            dataRequired.push(item);
        });
    }else if (type ==='complete'){
        $(".hidden-requiredFlag").each(function () {
            var item = {};
            item.keyItem = $(this).attr('keyItem');
            item.keyProduct = $(this).attr('keyProduct');
            item.value = $(this).val();
            dataRequired.push(item);
        });
    }
    $.ajax({
        url: laroute.route('cart.check_item_cart'),
        type: "post",
        dataType: "JSON",
        data: {
            dataRequired: dataRequired
        },
        success: function (data) {
            if (data.success === false) {
                $.each(data.data, function (key, item) {
                    if (item.errorMessage !=='') {
                        $('.error.' + key).text(item.errorMessage).css('display', 'inline-block');
                    }
                });
                $('.loading-main').css('display', 'none');
                $(".go-order-process img.lazy-load").css('display', 'none');
            } else {
                if (type === 'confirm') {
                    window.location.href = laroute.route('cart.confirm');
                }else{
                    Frontend.insertCart();
                }
            }
        }
    });
};

Frontend.insertCart = function () {
    var customerName = $('input[name=customer-name]').val();
    var customerTel = $('input[name=customer-tel]').val();
    var customerCardNo = $('input[name=customer-card-no]').val();
    var customerId = $('input[name=customer-id]').val();
    $.ajax({
        url: laroute.route('cart.insert_cart'),
        type: "post",
        dataType: "JSON",
        data: {
            customerTel :customerTel,
            customerName : customerName,
            customerCardNo : customerCardNo,
            customerId : customerId
        },
        success: function (data) {
            if (data.success === true){
                $(".go-order-process img.lazy-load").css('display', 'none');
                $('.go-order-process').removeClass('active');
                $('.loading-main').css('display', 'none');
            }
            window.location.href = laroute.route('cart.complete');
        }
    });
};

Frontend.scrollToBox = function (id) {
    $('html, body').animate({
        scrollTop: $("#" + id).offset().top
    }, 500)
};

Frontend.changeBackgroundButton = function (event, e) {
    $('label').removeClass('confirm_checked');
    event.stopPropagation();
    $(e).closest('label').addClass('confirm_checked');
};

Frontend.changeRequiredFlag = function (event, e) {
        $(e).parents('.title-product').first()
            .find('label.check_required_confirm').removeClass('confirm_checked');
        event.stopPropagation();
        event.stopPropagation();
        $(e).closest('label').addClass('confirm_checked');
};

Frontend.changeQuantity = function (element, type = 'up') {

    var number = $(element).parent().find('input[type="number"]');
    var oldValue = parseFloat(number.val());
    var step = parseFloat(number.attr('step'));
    var newVal = '';
    if (type === 'up') {
        if (oldValue >= number.attr('max')) {
            newVal = oldValue;
        } else {
            newVal = oldValue + step;
        }
    } else if (type === 'sub') {

        if (oldValue <= number.attr('min')) {
            newVal = oldValue;
        } else {
            newVal = oldValue - step;
        }
    }
    number.val(newVal);
    number.trigger("change");
};

Frontend.changeQuantitySaveSession = function (element, type = 'up') {
    var number = $(element).parent().find('input[type="number"]');
    var oldValue = parseFloat(number.val());
    var step = parseFloat(number.attr('step'));
    var newVal = '';

    var keyItem = number.attr('data-keyItem');
    var keyProduct = number.attr('data-keyProduct');

    var priceKey = $('#total-price-' + keyItem + '-' + keyProduct);
    var price = $('#price-' + keyItem + '-' + keyProduct);
    var totalQuantityKey = $('#total-quantity-' + keyItem + '-' + keyProduct);
    var totalPriceKey = $('#total-quantity-price-' + keyItem);

    var totalPriceAll = $('#total-all-price-cart');
    var totalQuanOld = $('#total-all-quantity-cart');
    var quanOld = $('#quantity-old-' + keyItem + '-' + keyProduct);

    if (type === 'up') {
        if (oldValue >= number.attr('max')) {
            newVal = oldValue;
        } else {
            newVal = oldValue + step;
        }
    } else if (type === 'sub') {

        if (oldValue <= number.attr('min')) {
            newVal = oldValue;
        } else {
            newVal = oldValue - step;
        }
    }

    number.val(newVal);
    number.trigger("change");
    priceKey.text(Frontend.formatNumber(parseFloat(newVal) * Frontend.formatToNumber(price.text())));

    $.ajax({
        url: laroute.route('cart.change_number_cart'),
        type: "post",
        dataType: "JSON",
        data: {
            keyItem: keyItem,
            keyProduct: keyProduct,
            value: newVal
        },
        success: function (data) {
            if (data.success === true) {
                var dataList = data.data;
                var totalPrice = dataList[keyItem]['totalPrice'];
                var total = dataList[keyItem]['total'];
                totalQuantityKey.text(Frontend.formatNumber(parseFloat(total)) + '品');
                totalPriceKey.text(Frontend.formatNumber(parseFloat(totalPrice)));

                var priceAll = 0;
                $( ".item-price .total-price-item" ).each(function() {
                    priceAll += Frontend.formatToNumber($(this).text());
                });
                totalPriceAll.text(Frontend.formatNumber(priceAll)).append('<span style="font-size: 12px;margin-left: 5px;">円＋税 </span>');

                var totalALL = Frontend.formatToNumber(totalQuanOld.text()) - Frontend.formatToNumber(quanOld.val()) + parseFloat(newVal);
                if (totalALL === undefined) {
                    totalALL = 0;
                }
                $('#box-hide-quantity').text(Frontend.formatNumber(parseFloat(totalALL)));
                if (totalALL > 0) {
                    $('#number-product-order').text(Frontend.formatNumber(parseFloat(totalALL)));
                }else{
                    $('#number-product-order').css('display','none');
                }
                totalQuanOld.text(Frontend.formatNumber(parseFloat(totalALL)));
                quanOld.val(Frontend.formatNumber(parseFloat(newVal)));

            }
        }
    });
};

Frontend.blurQuantityTop = function(element){
    var currentVal = $(element).val();
    if (currentVal > $(element).attr('max')) {
        currentVal = $(element).attr('max');
    }
    if (currentVal < $(element).attr('min')) {
        currentVal = $(element).attr('min');
    }
    $(element).val(currentVal);
    $(element).trigger("change");
};

Frontend.blurQuantity = function (element) {

    var min = $(element).attr('min');
    var max = $(element).attr('max');
    var currentVal = $(element).val();
    var keyItem = $(element).attr('data-keyItem');
    var keyProduct = $(element).attr('data-keyProduct');

    var priceKey = $('#total-price-' + keyItem + '-' + keyProduct);
    var price = $('#price-' + keyItem + '-' + keyProduct);
    var totalQuantityKey = $('#total-quantity-' + keyItem + '-' + keyProduct);
    var totalPriceKey = $('#total-quantity-price-' + keyItem);
    var priceKeyOld = priceKey.text();

    var totalPriceAll = $('#total-all-price-cart');
    var totalQuanOld = $('#total-all-quantity-cart');
    var quanOld = $('#quantity-old-' + keyItem + '-' + keyProduct);

    var flag_ajax = true;

    if (currentVal > $(element).attr('max')) {
        currentVal = $(element).attr('max');
    }
    if (currentVal < $(element).attr('min')) {
        currentVal = $(element).attr('min');
    }

    $(element).val(currentVal);
    $(element).trigger("change");

    if (parseFloat(currentVal) < parseFloat(min) || isNaN(parseFloat(currentVal)) || parseFloat(currentVal) > parseFloat(max)) {
        flag_ajax = false;
    }

    if (flag_ajax) {
        priceKey.text(Frontend.formatNumber(parseFloat(currentVal) * Frontend.formatToNumber(price.text())));
        $.ajax({
            url: laroute.route('cart.change_number_cart'),
            type: "post",
            dataType: "JSON",
            data: {
                keyItem: keyItem,
                keyProduct: keyProduct,
                value: currentVal
            },
            success: function (data) {
                if (data.success === true) {
                    var dataList = data.data;
                    var totalPrice = dataList[keyItem]['totalPrice'];
                    var total = dataList[keyItem]['total'];
                    totalQuantityKey.text(Frontend.formatNumber(parseFloat(total)) + '品');
                    totalPriceKey.text(Frontend.formatNumber(parseFloat(totalPrice)));
                    //UPDATE TOTAL PRICE

                    var priceAll = 0;
                    $( ".item-price .total-price-item" ).each(function() {
                        priceAll += Frontend.formatToNumber($(this).text());
                    });
                    totalPriceAll.text(Frontend.formatNumber(priceAll)).append('<span style="font-size: 12px;margin-left: 5px;">円＋税 </span>');

                    //UPDATE TOTAL QUANTITY
                    var totalOldNumner = totalQuanOld.text();

                    var totalALL = Frontend.formatToNumber(totalOldNumner) - Frontend.formatToNumber(quanOld.val()) + Frontend.formatToNumber(currentVal);
                    if (totalALL === undefined) {
                        totalALL = 0;
                    }
                    $('#box-hide-quantity').text(Frontend.formatNumber(parseFloat(totalALL)));
                    totalQuanOld.text(Frontend.formatNumber(parseFloat(totalALL)));
                    quanOld.val(Frontend.formatNumber(parseFloat(currentVal)));
                }
            }
        });
    }
};

Frontend.formatToNumber = function (money) {
    if (!Number.isInteger(money) && money) {
        var number = Number(money.replace(/[^0-9\.]+/g, ""));
    } else {
        number = money;
    }
    return number;
};

Frontend.keyUpQuantity = function (element) {

    var min = $(element).attr('min');
    var max = $(element).attr('max');
    var current_val = $(element).val();
    if (parseFloat(current_val) < parseFloat(min)) {
        $(element).val(parseFloat(min));
    }
    if (parseFloat(current_val) > parseFloat(max)) {
        $(element).val(parseFloat(max));
    }
};
Frontend.formatNumber = function (number) {
    if (number !== '') {
        return number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
    }
};

Frontend.scrollTopPage = function (posittion = 0) {
    $("html, body").animate({scrollTop: posittion}, 1000);
};

Frontend.maxLengthInputNumber = function (event, element) {
    var maxlengthNumber = $(element).attr('maxlength');
    var inputValueLength = $(element).val().length + 1;

    if (maxlengthNumber < inputValueLength) {
        event.preventDefault();
    }
};

Frontend.addToCart = function (element, top = false) {
    var itemCode = $(element).parent().find('input.item-code').val();
    var shopCode = $(element).parent().find('input.shop-code').val();
    var quantity = $(element).parent().find('input[type=number]').val();
    var eventCode = $(element).parent().find('input.event-code').val();
    var dateReceiveRaw = $(element).parent().find('input.date-receive-raw').val();
    var timReceive = $(element).parent().find('input.time-receive').val();
    var requiredConfirmFlag = $(element).parent().find('input.required-confirm-flag').val();
    var requiredFlag = $(element).parent().find('label.confirm_checked input[type=radio]').val();
    $(element).addClass('active');
    $(element).find("img.lazy-load").css('display', 'inline-block');
    if ((requiredFlag !== undefined && requiredConfirmFlag !== '0' && top === false) || top === true || requiredConfirmFlag ==='0') {
        $(element).attr("onclick", null);
        $.ajax({
            url: laroute.route('cart.add_to_cart'),
            type: "post",
            dataType: "JSON",
            data: {
                itemCode: itemCode,
                quantity: quantity,
                requiredFlag: requiredFlag,
                shopCode: shopCode,
                eventCode :eventCode,
                dateReceiveRaw :dateReceiveRaw,
                timReceive : timReceive
            },
            success: function (data) {
                if (data.success === true) {
                    $('.basket-content .list-item-block').html(data.data);
                    toastr.success('カートへ追加しました。');
                    $(element).find("img.lazy-load").css('display', 'none');
                    $(element).removeClass('active');
                    $(element).attr("onclick", 'Frontend.addToCart(this,' + top + ')');
                    $('#total-all-price-cart').text(Frontend.formatNumber($('.sum-price-box-cart').val())+"円＋税");
                    $('#total-all-quantity-cart').text(Frontend.formatNumber($('.sum-quantity-box-cart').val()));
                    $('#box-hide-quantity').text(Frontend.formatNumber($('.sum-quantity-box-cart').val()));
                    $('#number-product-order').text(Frontend.formatNumber($('.sum-quantity-box-cart').val()));
                    $('#number-product-order').css('display','inline-block');
                    if( $(".shopping-cart-fix-bottom .basket-popup").css('display') == 'none') {
                        $('.shopping-cart-fix-bottom').css('display','block');
                        $('.shopping-cart-fix-bottom .box-cart').css('display', 'block');
                    }
                }else{
                    toastr.error('商品オプションが未選択です。');
                    $(element).find("img.lazy-load").css('display', 'none');
                    $(element).removeClass('active');
                    $(element).attr("onclick", 'Frontend.addToCart(this,' + top + ')');
                }
            }
        });
    } else {
        toastr.error('商品オプションが未選択です。');
        $(element).find("img.lazy-load").css('display', 'none');
        $(element).removeClass('active');
        $(element).attr("onclick", 'Frontend.addToCart(this,' + top + ')');
    }
};

Frontend.cancelOrder = function (slipNumber = '') {
    $('.loading-main').css('display', 'block');
    $.ajax({
        url: laroute.route('mypage.cancel_order_complete_action'),
        type: "post",
        dataType: "JSON",
        data: {
            slipNumber: slipNumber,
        },
        success: function (data) {
            if (data.success === true) {
                window.location.href = laroute.route('mypage.cancel_order_complete', {'slipNumber': slipNumber})
            }else{
                toastr.error('商品オプションが未選択です。');
            }
            $('.loading-main').css('display', 'none');
            $('#modal-cancel-complete').modal('hide');
        }
    });
};

function drawMap() {
    $('.complete-wrap .complete-map-chart').each(function () {
        var lat = $(this).attr('data-lat');
        var lng = $(this).attr('data-lng');
        var id = $(this).attr('id');

        var myLatlng = {lat: parseFloat(lat), lng: parseFloat(lng)};
        // Generate First Map

        var map = new google.maps.Map(document.getElementById(id), {
            zoom: 13,
            center: myLatlng
        });

        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: 'Click to zoom'
        });

        map.addListener('center_changed', function() {
            // 3 seconds after the center of the map has changed, pan back to the
            // marker.
            window.setTimeout(function() {
                map.panTo(marker.getPosition());
            }, 3000);
        });

        marker.addListener('click', function() {
            map.setZoom(8);
            map.setCenter(marker.getPosition());
        });
    });
}

Frontend.showPopupCancel = function(slipNumber){
    $('.button_footer_submit .lazy-load').css('display', 'inline-block');
    $('.button_footer_submit').addClass('active');
    $('.loading-main').css('display', 'block');
    $.ajax({
        url: laroute.route('mypage.show_popup_cancel'),
        type: "POST",
        dataType: "JSON",
        data: {
            slipNumber: slipNumber,
        },
        success: function (data) {
            if (data.success === true) {
                $('.loading-main').css('display', 'none');
                $('.button_footer_submit .lazy-load').css('display', 'none');
                $('.button_footer_submit').removeClass('active');
                $('.cancel-popup-confirm').html(data.data);
                $('#modal-cancel-complete').modal('show');
            }
        }
    });
};

Frontend.showPopupRemove = function(element, boxList){

    var dataKeyItem = $(element).attr('data-keyItem');
    var dataKeyProduct = $(element).attr('data-keyProduct');

    $('.loading-main').css('display', 'block');
    $.ajax({
        url: laroute.route('cart.show_popup_remove'),
        type: "POST",
        dataType: "JSON",
        data: {
            dataKeyItem: dataKeyItem,
            dataKeyProduct: dataKeyProduct,
            boxList: boxList
        },
        success: function (data) {
            if (data.success === true) {
                $('.loading-main').css('display', 'none');
                $('#main-wrap').append(data.data);
                $('#modal-cart-remove').modal('show');
            }
        }
    });
};

Frontend.windowPrint = function(){
    window.print();
};

Frontend.convertTimeShop = function (time) {
    return [time.slice(0, 2), ':', time.slice(2)].join('')
};





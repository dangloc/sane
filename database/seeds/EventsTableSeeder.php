<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('events')->truncate();
        DB::table('events')->insert([
            [
                'code' => 'A',
                'name' => '定番',
                'start_date' => '20190509',
                'end_date' => '20190511',
                'del_flag' => 0,
                'ins_datetime' => date('Y-m-d', strtotime(' -1 day')),
                'ins_id' => 1,
                'upd_datetime' => date('Y-m-d', strtotime(' -1 day')),
                'upd_id' => 1,
            ],
            [
                'code' => 'B',
                'name' => '定番',
                'start_date' => '20190509',
                'end_date' => '20190511',
                'del_flag' => 0,
                'ins_datetime' => date('Y-m-d', strtotime(' -1 day')),
                'ins_id' => 1,
                'upd_datetime' => date('Y-m-d', strtotime(' -1 day')),
                'upd_id' => 1,
            ],
            [
                'code' => 'C',
                'name' => '定番',
                'start_date' => '20190509',
                'end_date' => '20190511',
                'del_flag' => 0,
                'ins_datetime' => date('Y-m-d', strtotime(' -1 day')),
                'ins_id' => 1,
                'upd_datetime' => date('Y-m-d', strtotime(' -1 day')),
                'upd_id' => 1,
            ]
        ]);
    }
}

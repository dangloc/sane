@if ($paginator->hasPages())
    <div class="pull-right pagination">
        <ul class="pagination">
            @if ($paginator->onFirstPage())
                <li class="disabled">
                    <span><i class="fas fa-chevron-left"></i></span>
                </li>
            @else
                <li onclick="location.href='{{ $paginator->previousPageUrl() }}'">
                    <a href="{{ $paginator->previousPageUrl() }}">
                        <span><i class="fas fa-chevron-left"></i></span>
                    </a>
                </li>
            @endif
            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if($page == $paginator->currentPage() -1 ||$page == $paginator->currentPage() +1)
                            <li onclick="location.href='{{ $url }}'"
                                class="{{$page == $paginator->currentPage() ? 'active' : ''}}"><a
                                        href="{{ $url }}">{{ $page }}</a></li>
                        @elseif (($page == $paginator->total() -1) && ($paginator->total() -1 !=$paginator->currentPage()) ||($page == $paginator->total() -2) && ($paginator->total() -2 !=$paginator->currentPage())|| ($page == $paginator->total() -3)&& ($paginator->total() -3 !=$paginator->currentPage()))
                            <div><span>..</span></div>
                        @else
                            <li onclick="location.href='{{ $url }}'"
                                class="{{$page == $paginator->currentPage() ? 'active' : ''}}"><a
                                        href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach
            @if ($paginator->hasMorePages())
                <li onclick="location.href='{{ $paginator->nextPageUrl() }}'">
                    <a href="{{ $paginator->nextPageUrl() }}">
                        <span><i class="fas fa-chevron-right"></i></span>
                    </a>
                </li>
            @else
                <li class="disabled">
                    <span><i class="fas fa-chevron-right"></i></span>
                </li>
            @endif
        </ul>
    </div>
@endif
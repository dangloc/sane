<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\ActiveScope;

class Areas extends Model
{
    public $timestamps = false;
    protected $_alias = 'areas';
    protected $table = 'areas';
    protected $fillable = [
        'name',
        'del_flag',
        'ins_datetime',
        'ins_id',
        'upd_datetime',
        'upd_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * get all cities of areas
     */
    public function cities()
    {
        return $this->hasMany('App\Models\Cities','area_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * get all shops of area
     */
    public function shops()
    {
        return $this->hasMany('App\Models\Shop', 'area_id');
    }

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ActiveScope());
    }
}

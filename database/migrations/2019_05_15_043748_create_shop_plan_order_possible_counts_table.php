<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopPlanOrderPossibleCountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_plan_order_possible_counts', function (Blueprint $table) {
            $table->char('shop_code', 4)->comment('店舗コード');
            $table->char('receipt_date', 8)->comment('受取日');
            $table->char('item_code', 13)->comment('商品コード');
            $table->smallInteger('order_possible_count')->comment('計画注文可能数');
            $table->char('del_flag', 1)->comment('削除フラグ')->default(0);
            $table->dateTime('ins_datetime')->default(DB::raw('CURRENT_TIMESTAMP'))->comment('登録日時');
            $table->integer('ins_id')->length(11)->comment('登録者ID');
            $table->dateTime('upd_datetime')->comment('更新日時')->nullable();
            $table->integer('upd_id')->length(11)->comment('更新者ID')->nullable();

            $table->primary(array('shop_code', 'receipt_date', 'item_code'), 'shop_plan_order_possible_counts_keys');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_plan_order_possible_counts');
    }
}

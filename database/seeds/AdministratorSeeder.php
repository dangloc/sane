<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class AdministratorSeeder extends Seeder
{
    public function run()
    {
        // Reset admin table
        DB::table('administrators')->truncate();

        // Create virtual DB
        $faker = Faker::create();
        $admin = [];

        // Super admin
        $admin[] = [
            'name' => 'Administrator',
            'login_id' => 'admin',
            'password' => bcrypt('admin123'),
            'status' => '1',
            'del_flag' => '0',
            'ins_id' => 1,
            'upd_id' => null,
            'ins_datetime' => Carbon::now()->format('Y-m-d H:i:s'),
            'upd_datetime' => null
        ];

        // List fake admin for listing
        foreach (range(2, 20) as $index) {
            $admin[] = [
                'name' => $faker->name,
                'login_id' => $faker->userName,
                'password' => bcrypt('admin123'),
                'status' => rand(0,1)."",
                'del_flag' => '0',
                'ins_id' => 1,
                'upd_id' => null,
                'ins_datetime' => Carbon::now()->format('Y-m-d H:i:s'),
                'upd_datetime' => null
            ];
        }

        // Insert DB
        DB::table('administrators')->insert($admin);
    }

}

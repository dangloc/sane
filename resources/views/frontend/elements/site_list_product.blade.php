<img class="lazy-load" id ="item-load" src="{{asset('frontend/images/ajax-loader-item.gif')}}">
<div class="container">
    <div class="title-top">
        <h3>商品一覧</h3>
    </div>
    <div class="box-container">
        <div class="box-container-top">
            <div class="filter-product">
                <div class="box-detail">
                    <label>検索条件</label>
                    <div class="box-output">
                        <div class="box-input">
                            <input type="button" onclick="Frontend.scrollTopPage(500);" name="condition" value="条件を変更する">
                            <i class="fas fa-caret-right"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="store-product">
                <div class="box-detail">
                    <div class="store-link">
                        <ul>
                            <li><span>店舗</span><strong class="shop-name">{{$shopName}}</strong></li>
                            <li class="space">/</li>
                            <li><span>商品カテゴリ</span><strong class="event-name">{{$eventName}}</strong></li>
                            <li class="space">/</li>
                            <li><span>受取り日 </span><strong class="date-receive">{{$dateConvert['year']}}年 {{$dateConvert['month']}}月 {{$dateConvert['day']}}日({{dayJapan($dateReceive)}})</strong><strong>{{$time}}時</strong></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-container-bottom">
            <div class="filter-condition">
                <label>絞り込み条件<i class="fas fa-caret-up"></i></label>
                <div class="tag-top tags-condition">
                    <ul>
                        @if(!empty($categoryFilter))
                            @foreach($categoryFilter as $cate)
                                @if($categoryCode == $cate['code'])
                                    <li><a id="list-product-cate-{{$cate['code']}}"
                                           class="list-cate-product category-submit"
                                           href="javascript:Frontend.getProduct('{{$cate['code']}}');">{{$cate['name']}}</a>
                                    </li>
                                @else
                                    <li><a id="list-product-cate-{{$cate['code']}}" class="list-cate-product"
                                           href="javascript:Frontend.getProduct('{{$cate['code']}}');">{{$cate['name']}}</a>
                                    </li>
                                @endif
                            @endforeach
                        @endif
                    </ul>
                </div>
            </div>
            <div class="products-show">
                <div class="row">
                    @if (!empty($datas))
                        @foreach($datas as $data)
                            <div class="col-xs-12 col-sm-6 col-lg-3">
                                <div class="item-product">
                                    <h3 class="title"><a
                                                href="{{route('product.detail',['id'=>$data['code'],'shopcode'=>$data['shop_code'],'keyitem'=>0,'keyproduct'=>0])}}">{{$data['name']}}</a>
                                    </h3>
                                    <span class="red-text bonus-text">{{$data['bonus']}}</span>
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12">
                                            <div class="thumbnai">
                                                <a href="{{route('product.detail',['id'=>$data['code'],'shopcode'=>$data['shop_code'],'keyitem'=>0,'keyproduct'=>0])}}">
                                                    <img src="{{ ($data['item_image_folder_name'] ==null || $data['item_image_folder_name']=='_') ? asset('/frontend/images/item/noimage.jpg')
                                                    : config('const.url_item_image').$data['item_image_folder_name']}}" alt="">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <span>{{$data['content1']}}</span>
                                            <div class="price">
                                                <p>
                                                    <span class="unit">¥</span>
                                                    <span class="money">{{number_format($data['price'])}}</span>
                                                    <span class="tax">＋税</span>
                                                </p>
                                            </div>
                                            <div class="quantity">
                                                <button class="quantity-button quantity-down" onclick="Frontend.changeQuantity(this,'sub');"><i class="fas fa-minus"></i></button>
                                                <input type="number" class="number" value="1" min="1" max="999" step="1" onkeyup="Frontend.keyUpQuantity(this)" onblur="Frontend.blurQuantityTop(this)">
                                                <button class="quantity-button quantity-up" onclick="Frontend.changeQuantity(this,'up');"><i class="fas fa-plus"></i></button>
                                            </div>
                                        </div>
                                        <input type="hidden" class="item-code" value="{{$data['code']}}">
                                        <input type="hidden" class="shop-code" value="{{$data['shop_code']}}">
                                        <input type="hidden" class="event-code" value="{{$data['event_code']}}">
                                        <button class="add-to-cart" onclick="Frontend.addToCart(this,true)">
                                            <i class="fa fa-shopping-cart" aria-hidden="true"></i>カートへ追加
                                            <img class="lazy-load" src="{{asset('frontend/images/ajax-loader.gif')}}">
                                            <label class="bin-button"></label>
                                        </button>
                                </div>
                            </div>
                            </div>
                        @endforeach
                    @else
                        <span class="empty-search-product"> <strong>検索結果が0件です。</strong></span>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
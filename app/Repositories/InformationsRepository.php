<?php

namespace App\Repositories;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * class InformationsRepository.
 *
 * @package namespace App\Repositories;
 */
class InformationsRepository extends BaseRepository
{
    function __construct()
    {
        $this->_model = \App\Models\Informations::class;
    }

    /**
     * @return \Illuminate\Database\Query\Builder
     * getListInformation for site/index
     */

    public function getInfoPagination($paging){
        $data = $this->getModel()
            ->select('public_start_date', 'title', 'id')
            ->where(DB::raw('date_add(CAST(public_start_date AS DATETIME), INTERVAL public_start_time HOUR_SECOND)'), '<=', now())
            ->where(DB::raw('date_add(CAST(public_end_date AS DATETIME), INTERVAL public_end_time HOUR_SECOND)'), '>=', now())
            ->orderBy(DB::raw('date_add(CAST(public_start_date AS DATETIME), INTERVAL public_start_time HOUR_SECOND)'), 'DESC')
            ->paginate($paging);
        return $data;
    }

    public function getListInfo()
    {
        $data = $this->getModel()
            ->select('public_start_date', 'title', 'id')
            ->where(DB::raw('date_add(CAST(public_start_date AS DATETIME), INTERVAL public_start_time HOUR_SECOND)'), '<=', now())
            ->where(DB::raw('date_add(CAST(public_end_date AS DATETIME), INTERVAL public_end_time HOUR_SECOND)'), '>=', now())
            ->orderBy(DB::raw('date_add(CAST(public_start_date AS DATETIME), INTERVAL public_start_time HOUR_SECOND)'), 'DESC')
            ->get();
        return $data;
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Query\Builder
     * get Information detail
     */
    public function getInforDetail($id)
    {
        $data = $this->getModel()
            ->select('*')
            ->where('id', '=', $id)
            ->where(DB::raw('date_add(CAST(public_start_date AS DATETIME), INTERVAL public_start_time HOUR_SECOND)'), '<=', now())
            ->where(DB::raw('date_add(CAST(public_end_date AS DATETIME), INTERVAL public_end_time HOUR_SECOND)'), '>=', now())
            ->first();

        return $data;
    }

    public function searchCondition($request)
    {
        $sort = request()->get('sort', 'id');
        $direction = request()->get('direction', 'desc');
        $query = $this->getModel()->where('title', 'like', '%' . $request->get('title') . '%')
            ->when($this->validateDate(request()->get('public_start_date'), 'Y/m/d'), function ($query) {
                return $query->where('public_start_date', '>=', request()->get('public_start_date'));
            })->when($this->validateDate(request()->get('public_end_date'), 'Y/m/d'), function ($query) {
                return $query->where('public_end_date', '<=', request()->get('public_end_date'));
            });
        return $query->orderBy($sort, $direction);
    }
}

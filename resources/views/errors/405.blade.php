@if(strlen(strstr(url()->current(), 'management')) > 0)
   @include('errors.backend.404')
@else
    @include('errors.frontend.404')
@endif

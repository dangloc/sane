@extends('backend.layouts.main')
@section('content')

    <div id="Contentwrap">
        <div id="PageContent" class="no_mar_bottom">
            <h2><i class="fas fa-image"></i> バナー編集</h2>
            <div class="order">
                <table class="sp_block flowtable">
                    <tr>
                        <th scope="row" style="width: 150px;">バナー画像</th>
                        <td>
                            <img src="{{asset(Session::get('banner_url_temp_edit'))}}" width="260px">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" style="width: 150px;">リンク先URL</th>
                        <td>
                            {{Session::get('dataUpdate')['link_url']}}
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" style="width: 150px;">公開期間</th>
                        <td>
                            {{Session::get('dataUpdate')['public_start_date']}}〜{{Session::get('dataUpdate')['public_end_date']}}
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" style="width: 150px;">ステータス</th>
                        <td>
                            {{config('const.status_banner')[Session::get('dataUpdate')['status']]}}
                        </td>
                    </tr>
                </table>
            </div>
            <!--ContentFoot -->
        </div>
        <!--PageContent -->
        <form id="form_banner_confirm_edit" action="{{ route('banner.update',Session::get('data')['id']) }}" method="POST">
            {{ csrf_field() }}
            @method('PUT')
            <div class="contentfoot">
                <p class="btn Searchclear">
                    <button type="button" value=""
                            onclick="System.redirectNoLoading('{{ route('banner.edit',[Session::get('data')['id'] ,'flag' => config('const.flag')['from_confirm']]) }}')"><i
                            class="fas fa-undo"></i> 戻る
                    </button>
                </p>
                <p class="btn register">
                    <button type="button" value=""
                            onclick="System.showDialog('banner','confirm_edit')"
                            class="opener"><i class="fas fa-check"></i> 編集する
                    </button>
                </p>
            </div>
        </form>
        <!--PageContent -->

    </div>
    <!--Contentwrap -->
@endsection
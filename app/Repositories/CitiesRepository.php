<?php

namespace App\Repositories;

use App\Models\Customer;
use Illuminate\Support\Facades\DB;

/**
 * class CitiesRepository.
 *
 * @package namespace App\Repositories;
 */
class CitiesRepository extends BaseRepository
{
    function __construct()
    {
        $this->_model = \App\Models\Cities::class;
    }

    /**
     * @param $area_id
     * @return array
     * City by area
     */
    public function getCityByAreas($area_id)
    {
        $data = $this->getModel()->where('area_id', '=', $area_id)->get();
        if (!empty($data)) {
            return $data->toArray();
        }
        return array();
    }
}

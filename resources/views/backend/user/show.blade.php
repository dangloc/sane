@extends('backend.layouts.main')
@section('content')

    <div id="Contentwrap">

        <div class="contenttop detail-btn-group">
            <p class="btn Searchclear">
                <button type="button" onclick="System.redirectNoLoading('{{ route('user.index') }}')"><i class="fas fa-list"></i> 一覧ページに戻る</button>
            </p>
            <p class="btn Receipt">
                <a href="#buy_his"><button type="button" onclick="" value=""><i class="fas fa-shopping-cart"></i> 注文履歴</button></a>
            </p>
        </div>
        <!--ContentFoot -->

        <div id="PageContent">
            <h2><span><i class="fas fa-user"></i> {{$customer->name}}様</span></h2>
            <table class="listtable">
                <tr>
                    <th width="30%" scope="row">ID</th>
                    <td width="70%">{{$customer->id}}</td>
                </tr>
                <tr>
                    <th scope="row">会員/非会員</th>
                    <td>{{$has_password ? '会員' : '非会員'}}</td>
                </tr>
                <tr>
                    <th width="30%" scope="row">メールアドレス</th>
                    <td width="70%">{{$customer->email}}</td>
                </tr>
                <tr>
                    <th width="30%" scope="row">電話番号</th>
                    <td width="70%">{{$customer->tel}}</td>
                </tr>
                <tr>
                    <th width="30%" scope="row">サンエーカード番号</th>
                    <td width="70%">{{$customer->card_no}}</td>
                </tr>
            </table>
        </div>
        <!--PageContent -->

        <div id="PageContent" class="pt60">
            <h2 id="buy_his"><i class="fas fa-shopping-cart"></i> 注文履歴
            </h2>
            <div class="PageContentNav">
                @include('backend/elements/paging', ['results' => $orders])
                {{ $orders->appends(request()->input())->links() }}
            </div>
            <table class="OrderForm responsive tdcenter Bordertd TextL table-hover">
                <tr>
                    <th scope="col">@include("backend/elements.sortable", ["field" => "slip_number", "title" => "伝票番号"])</th>
                    <th scope="col">@include("backend/elements.sortable", ["field" => "status", "title" => "状態"])</th>
                    <th scope="col">@include("backend/elements.sortable", ["field" => "receptionist_date", "title" => "受付日<br/>受取日時"])</th>
                    <th scope="col">@include("backend/elements.sortable", ["field" => "shop_code", "title" => "店舗"])</th>
                    <th scope="col">@include("backend/elements.sortable", ["field" => "customer_name", "title" => "お名前（カナ）"])</th>
                    <th scope="col">@include("backend/elements.sortable", ["field" => "customer_tel", "title" => "電話番号"])</th>
                    <th scope="col">詳細</th>
                </tr>
                @if(!$orders->isEmpty())
                    @foreach($orders as $order)
                        <tr>
                            <td>{{$order->slip_number}}</td>
                            <td>{{$status_array[$order->status]}}</td>
                            <td>{{$order->receptionist_date != null ? date("Y/m/d", strtotime($order->receptionist_date)) : ''}}
                                <br/>{{$order->receipt_date != null ? date("Y/m/d", strtotime($order->receipt_date)) : ''}}&nbsp;{{$order->receipt_time != null ? $order->receipt_time.':00' : ''}}</td>
                            <td>{{$order->shop !== null ? $order->shop->name:''}}</td>
                            <td>{{$order->customer_name}}</td>
                            <td>{{$order->customer_tel}}</td>
                            <td><p class="btnmini Edit"><a
                                        href="{{route('order.show', $order->slip_number)}}" class="TextC">詳細</a>
                                </p></td>
                        </tr>
                    @endforeach
                @else
                    @include('backend/elements/no_data',['col' => 7])
                @endif
            </table>
            <div class="PageContentNav">
                {{ $orders->appends(request()->input())->links() }}
            </div>
        </div>

        <!--PageContent -->
        <div class="contenttop detail-btn-group">
            <p class="btn Searchclear">
                <button type="button" onclick="System.redirectNoLoading('{{ route('user.index') }}')"><i class="fas fa-list"></i> 一覧ページに戻る</button>
            </p>
            <p class="btn Receipt">
                <a href="#buy_his"><button type="button" onclick="" value=""><i class="fas fa-shopping-cart"></i> 注文履歴</button></a>
            </p>
        </div>
        <!--ContentFoot -->
    </div>
    <!--Contentwrap -->
@endsection
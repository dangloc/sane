@extends('backend.layouts.main')
@section('content')
    <div id="Contentwrap">
        @include('common.message_notification')
        <div id="PageContent">
            <h2><br/><i class="fas fa-image"></i> バナー一覧<span class="btn register btnMedium sp-mt-neg-77" style="margin-top:-40px;">
                    <button type="button" onclick="System.redirectNoLoading(' {{ route('banner.create') }} ')"
                            tabindex="14">
                        <i class="fas fa-plus"></i> バナー登録</button>
                </span>
            </h2>
            <div class="PageContentNav">
                @include('backend.elements.paging', ['results' => $listBanner])
                {{ $listBanner->appends(request()->input())->links() }}
            </div>
            <table class="OrderForm responsive tdcenter Bordertd table-hover">
                <tr>
                    <th scope="col" width="100px">@include("backend.elements.sortable", ["field" => "id", "title" => "ID"])</th>
                    <th scope="col">@include("backend.elements.sortable", ["field" => "banner_url", "title" => "バナー画像"])</th>
                    <th scope="col" style="width: 500px !important;">@include("backend.elements.sortable", ["field" => "public_start_date", "title" => "公開期間"])</th>
                    <th scope="col" width="130px">@include("backend.elements.sortable", ["field" => "status", "title" => "ステータス"])</th>
                    <th scope="col" style="max-width:130px">@include("backend.elements.sortable", ["field" => "link_url", "title" => "リンクURL"])</th>
                    <th scope="col" style="width: 110px;">編集</th>
                    <th scope="col" style="width: 110px;">削除</th>
                </tr>
                @if(!$listBanner->isEmpty())
                    @foreach($listBanner as $banner)
                        <tr>
                            <td>{{$banner->id}}</td>
                            <td style="text-align:left;"><img src="{{ asset($banner->banner_url)}}" width="200"></td>
                            <td style="text-align:left;">{{\Illuminate\Support\Carbon::parse($banner->public_start_date)->format('Y/m/d')}}
                                〜{{\Illuminate\Support\Carbon::parse($banner->public_end_date)->format('Y/m/d')}}</td>
                            <td style="text-align:left;" >{{config('const.status_banner')[$banner->status]}}</td>
                            <td style="text-align:left;" class="style-word link-content"><p class="width-240">{{$banner->link_url}}</p></td>
                            <td><p class="btnmini Edit">
                                    <a onclick="System.redirectNoLoading('{{ route('banner.edit',$banner->id) }}')"
                                       class="TextC">編集</a>
                                </p></td>
                            <td>
                                <form id="{{'form_banner_confirm_delete'.$banner->id}}" method="post"
                                      action="{{ route('banner.destroy', $banner->id) }}">
                                    {{ csrf_field() }}
                                    @method('DELETE')
                                    <p class="btnmini">
                                        <button href="#" type="button"
                                                onclick="System.showDialog('banner','confirm_delete', {{$banner->id}})"
                                                value="">削除
                                        </button>
                                    </p>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @else
                    @include('backend.elements.no_data',['col' => 7])
                @endif

            </table>
            <div class="PageContentNav">
                {{ $listBanner->appends(request()->input())->links() }}
            </div>
        </div>
        <!--PageContent -->

    </div>
    <!--Contentwrap -->
@endsection
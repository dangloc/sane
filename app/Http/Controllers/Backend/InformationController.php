<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\BackendController;
use App\Http\Requests\InformationCreateRequest;
use App\Repositories\InformationsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InformationController extends BackendController
{

    protected $informationRepository;

    public function __construct(InformationsRepository $informationsRepository)
    {
        $this->informationRepository = $informationsRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $informations = $this->informationRepository->searchCondition($request)->paginate(config('const.paging.backend'));
        return view('backend.information.index', compact('informations'))
            ->with([
                'title' => $request->get('title'),
                'public_start_date' => $request->get('public_start_date'),
                'public_end_date' => $request->get('public_end_date'),
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $start_date_wrong_format = (request()->session()->has('errors') and strpos(request()->session()->get('errors')->first('public_start_date'), 'YYYY/mm/dd形式で指定してください。') != false);
        $end_date_wrong_format = (request()->session()->has('errors') and strpos(request()->session()->get('errors')->first('public_end_date'), 'YYYY/mm/dd形式で指定してください。') != false);
        request()->session()->put('session_data', []);
        return view('backend.information.create')->with(
            [
                'start_date_wrong_format' => $start_date_wrong_format,
                'end_date_wrong_format' => $end_date_wrong_format,
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$request->session()->has('session_data')){
            return redirect()->route('information.index')->with('error', config('messages.SYSTEM_ERROR'));
        }
        $data = $request->session()->get('session_data');
        $data['public_start_time'] = substr_replace($data['public_start_time'], ":", 2, 0).':00';
        $data['public_end_time'] = substr_replace($data['public_end_time'], ":", 2, 0).':00';
        $request->session()->forget('session_data');
        try {
            $this->informationRepository->create($data);
            return redirect()->route('information.index')->with('success', config('messages.DB_INSERT'));
        } catch (\Exception $e) {
            return redirect()->route('information.index')->with('error', config('messages.SYSTEM_ERROR'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function confirm(InformationCreateRequest $request, $id = null)
    {
        $request->flash();
        $request->session()->put('session_data', $request->all());
        return view('backend.information.confirm')->with('information', $request->all());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $information = $this->informationRepository->find($id);
        if ($information == null) {
            return redirect()->route('information.index')->with('error', config('messages.NO_DATA'));
        } else {
            $information = $information->toArray();
        }
        $start_date_wrong_format = (request()->session()->has('errors') and strpos(request()->session()->get('errors')->first('public_start_date'), 'YYYY/mm/dd形式で指定してください。') != false);
        $end_date_wrong_format = (request()->session()->has('errors') and strpos(request()->session()->get('errors')->first('public_end_date'), 'YYYY/mm/dd形式で指定してください。') != false);

        request()->session()->put('session_data', $information);
        return view('backend.information.create')->with(
            [
                'information' =>  $information,
                'start_date_wrong_format' => $start_date_wrong_format,
                'end_date_wrong_format' => $end_date_wrong_format,
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!$request->session()->has('session_data'))
        {
            return redirect()->route('information.index')->with('error', config('messages.SYSTEM_ERROR'));
        }
        $data = $request->session()->get('session_data');
        $data['public_start_time'] = substr_replace($data['public_start_time'], ":", 2, 0).':00';
        $data['public_end_time'] = substr_replace($data['public_end_time'], ":", 2, 0).':00';
        $request->session()->forget('session_data');
        try {
            $this->informationRepository->update($id, $data);
            return redirect()->route('information.index')->with('success', config('messages.DB_UPDATE'));
        } catch (\Exception $e) {
            return redirect()->route('information.index')->with('error', config('messages.SYSTEM_ERROR'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->informationRepository->softDelete($id);
            return redirect()->route('information.index')->with('success', config('messages.DB_DELETE'));
        } catch (\Exception $e) {
            return redirect()->route('information.index')->with('error', config('messages.SYSTEM_ERROR'));
        }
    }
}

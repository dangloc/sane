@extends('backend.layouts.main')
@section('content')
    <div id="Contentwrap">
        @include('common.message_notification')
        <form action="{{ route('auth_login') }}" method="post">
            {{csrf_field()}}
            {{--for disable autocomplete in chrome --}}
            <input type="text" style="opacity: 0;position: absolute;">
            <input type="password" style="opacity: 0;position: absolute;">
            {{--for disable autocomplete in chrome --}}
            <div class="Login">
                <div class="LoginIn">
                    <p><img src="{{ asset('backend/images/common/logo-login.png') }}" width="320" alt="サンエーネット予約サービス管理画面"></p>
                    <dl>
                        <dt>ログインID<span class="req"></span></dt>
                        <dd>
                            <div class="required">
                                <input name="login_id" value="{{old('login_id')}}" type="text" autocomplete="off">
                            </div>
                            @include('common.validate_error', ['message' => $errors->first('login_id')])
                        </dd>
                        <dt>パスワード<span class="req"></span></dt>
                        <dd>
                            <div class="required">
                                <input type="password" value="{{old('password')}}" name="password">
                            </div>
                            @include('common.validate_error', ['message' => $errors->first('password')])
                        </dd>
                    </dl>
                </div>
                <!--LoginIn -->
                <div class="LoginBtnGroup">
                    <p class="btn register">
                        <button type="submit" onclick="System.showLoading()" value="">ログイン</button>
                    </p>
                </div>
            </div>
            <!--Login -->
        </form>
    </div>
    <!--Contentwrap -->
@endsection